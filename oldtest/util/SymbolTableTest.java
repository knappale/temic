package util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import core.SymbolTable;

import static org.junit.Assert.*;

import java.util.Random;

import org.eclipse.jdt.annotation.NonNull;


public class SymbolTableTest {
  public final int LOAD = 100000;
  // private ExtendedSymbolTable<@NonNull Namespace, Integer> table = new ExtendedSymbolTable<>();
  private SymbolTable<@NonNull Pair<@NonNull Namespace, String>, Integer> table = new SymbolTable<>();
  private Random rand = new Random();

  static enum Namespace {
    NS1, NS2, NS3, NS4, NS5
  }

  @Before
  public void setUp() throws Exception {
    table.push(new Pair<>(Namespace.NS1, "a"), 1);
    table.push(new Pair<>(Namespace.NS1, "b"), 2);
    table.push(new Pair<>(Namespace.NS2, "a"), 101);
    table.push(new Pair<>(Namespace.NS2, "b"), 102);
    table.push(new Pair<>(Namespace.NS1, "cc"), 3);
    table.push(new Pair<>(Namespace.NS1, ""), 13);
    table.push(new Pair<>(Namespace.NS1, "b"), 5);
    table.push(new Pair<>(Namespace.NS1, "cc"), 7);
    table.push(new Pair<>(Namespace.NS1, "b"), 7);
    table.push(new Pair<>(Namespace.NS2, "cc"), 103);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void get() throws Exception {
    System.out.println("testing get()");
    assertEquals(Integer.valueOf(1), table.top(new Pair<>(Namespace.NS1, "a")));
    assertEquals(Integer.valueOf(7), table.top(new Pair<>(Namespace.NS1, "b")));
    assertEquals(Integer.valueOf(7), table.top(new Pair<>(Namespace.NS1, "cc")));
    assertEquals(null, table.top(new Pair<>(Namespace.NS1, "c")));
    assertEquals(Integer.valueOf(13), table.top(new Pair<>(Namespace.NS1, "")));
    assertEquals(Integer.valueOf(101), table.top(new Pair<>(Namespace.NS2, "a")));
    assertEquals(Integer.valueOf(102), table.top(new Pair<>(Namespace.NS2, "b")));
    assertEquals(null, table.top(new Pair<>(Namespace.NS2, "c")));
    assertEquals(Integer.valueOf(103), table.top(new Pair<>(Namespace.NS2, "cc")));
  }

  @Test
  public void remove() throws Exception {
    System.out.println("testing remove()");
    table.pop(new Pair<>(Namespace.NS1, "cc"));
    assertEquals(Integer.valueOf(3), table.top(new Pair<>(Namespace.NS1, "cc")));
    assertEquals(Integer.valueOf(1), table.top(new Pair<>(Namespace.NS1, "a")));
    assertEquals(Integer.valueOf(7), table.top(new Pair<>(Namespace.NS1, "b")));
    table.pop(new Pair<>(Namespace.NS1, "b"));
    assertEquals(Integer.valueOf(3), table.top(new Pair<>(Namespace.NS1, "cc")));
    assertEquals(Integer.valueOf(1), table.top(new Pair<>(Namespace.NS1, "a")));
    assertEquals(Integer.valueOf(5), table.top(new Pair<>(Namespace.NS1, "b")));
    table.push(new Pair<>(Namespace.NS1, "cc"), 101);
    table.push(new Pair<>(Namespace.NS1, "cc"), 102);
    assertEquals(Integer.valueOf(102), table.top(new Pair<>(Namespace.NS1, "cc")));
    assertEquals(Integer.valueOf(1), table.top(new Pair<>(Namespace.NS1, "a")));
    assertEquals(Integer.valueOf(5), table.top(new Pair<>(Namespace.NS1, "b")));
    table.pop(new Pair<>(Namespace.NS1, "b"));
    assertEquals(Integer.valueOf(102), table.top(new Pair<>(Namespace.NS1, "cc")));
    assertEquals(Integer.valueOf(1), table.top(new Pair<>(Namespace.NS1, "a")));
    assertEquals(Integer.valueOf(2), table.top(new Pair<>(Namespace.NS1, "b")));
    assertEquals(null, table.top(new Pair<>(Namespace.NS1, "c")));
    table.pop(new Pair<>(Namespace.NS1, "b"));
    assertEquals(Integer.valueOf(102), table.top(new Pair<>(Namespace.NS1, "cc")));
    assertEquals(Integer.valueOf(1), table.top(new Pair<>(Namespace.NS1, "a")));
    assertEquals(null, table.top(new Pair<>(Namespace.NS1, "b")));
    assertEquals(null, table.top(new Pair<>(Namespace.NS1, "c")));
    assertEquals(Integer.valueOf(13), table.top(new Pair<>(Namespace.NS1, "")));
    table.pop(new Pair<>(Namespace.NS1, "b"));
  }

  private String randomString() {
    String s = "";
    for (int i = 0; i < 20; i++) {
      s = s + (char) ('a' + rand.nextInt(26));
    }
    return s;
  }

  @Test
  public void put() {
    System.out.println("testing put()");
    String[] buffer = new String[LOAD];
    for (int i = 0; i < LOAD; i++) {
      String s = randomString() + i;
      buffer[i] = s;
      table.push(new Pair<>(Namespace.NS1, s), i + 7);
    }
    for (int i = 0; i < LOAD; i++) {
      assertEquals(Integer.valueOf(i+7), table.top(new Pair<>(Namespace.NS1, buffer[i])));
    }
  }
}
