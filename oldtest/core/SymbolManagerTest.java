package core;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Vector;

import bdd.BDD;
import bdd.MultiBDD;
import bdd.VariableRange;
import bdd.VariableRenaming;
import syntax.Identifier;


public class SymbolManagerTest {
	private SymbolTable<syntax.@NonNull Identifier, Type> typeEnv;
	private SymbolManager sm;

	@Before
	public void setUp() throws Exception {
		typeEnv = new SymbolTable<>(); // new ExtendedSymbolTable<Namespace,Type>(Namespace.values());
		typeEnv.push(Identifier.variable("b1"), Type.booleanType());
		typeEnv.push(Identifier.variable("b2"), Type.booleanType());
		typeEnv.push(Identifier.constant("c1"), Type.booleanType());
		typeEnv.push(Identifier.constant("c2"), Type.integerType(0,255));
		typeEnv.push(Identifier.variable("i1"), Type.integerType(-16,15));
		typeEnv.push(Identifier.variable("i2"), Type.integerType(-16,16));
		Vector<@NonNull String> lit1 = new Vector<>();
		lit1.add("LIT01");
		lit1.add("LIT02");
		lit1.add("LIT03");
		lit1.add("LIT04");
		typeEnv.push(Identifier.variable("e1"), Type.enumType(lit1));
		Vector<String> vars = new Vector<String>();
		vars.add("i1"); vars.add("b1"); vars.add("i2"); vars.add("b2"); vars.add("e1");
		sm = new SymbolManager(vars, typeEnv);
		sm.declareConstant("c1", MultiBDD.castBDD(BDD.and(sm.getBDDWBForSymbol("b1").getBDD(), sm.getBDDWBForSymbol("b2").getBDD())));
		sm.declareConstant("c2", MultiBDD.intMultiBDD(42));
	}
	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void bivalentVariableSize() throws Exception {
		assertTrue(sm.bivalentVariableCount() == 32);
	}
	
	@Test
	public void bivalentVariables() throws Exception {
		Vector<String> bvars = sm.bivalentVariables();
		assertEquals("error", bvars.get(0));
		assertEquals("error'", bvars.get(1));
		assertEquals("i1.4", bvars.get(2));
		assertEquals("i1.4'", bvars.get(3));
		assertEquals("i1.3", bvars.get(4));
		assertEquals("i1.3'", bvars.get(5));
		assertEquals("i1.2", bvars.get(6));
		assertEquals("i1.2'", bvars.get(7));
		assertEquals("i1.1", bvars.get(8));
		assertEquals("i1.1'", bvars.get(9));
		assertEquals("i1.0", bvars.get(10));
		assertEquals("i1.0'", bvars.get(11));
		assertEquals("b1", bvars.get(12));
		assertEquals("b1'", bvars.get(13));
		assertEquals("i2.5", bvars.get(14));
		assertEquals("i2.5'", bvars.get(15));
		assertEquals("i2.4", bvars.get(16));
		assertEquals("i2.4'", bvars.get(17));
		assertEquals("i2.3", bvars.get(18));
		assertEquals("i2.3'", bvars.get(19));
		assertEquals("i2.2", bvars.get(20));
		assertEquals("i2.2'", bvars.get(21));
		assertEquals("i2.1", bvars.get(22));
		assertEquals("i2.1'", bvars.get(23));
		assertEquals("i2.0", bvars.get(24));
		assertEquals("i2.0'", bvars.get(25));
		assertEquals("b2", bvars.get(26));
		assertEquals("b2'", bvars.get(27));
		assertEquals("e1.1", bvars.get(28));
		assertEquals("e1.1'", bvars.get(29));
		assertEquals("e1.0", bvars.get(30));
		assertEquals("e1.0'", bvars.get(31));
	}
	
	@Test
	public void getBDDWBForSymbol() throws Exception {
		assertTrue(sm.getBDDWBForSymbol("error").getBDD()  == BDD.varBDD(1));
		assertTrue(sm.getBDDWBForSymbol("error'").getBDD() == BDD.varBDD(2));
		assertTrue(sm.getBDDWBForSymbol("i1.4").getBDD()   == BDD.varBDD(3));
		assertTrue(sm.getBDDWBForSymbol("i1.4'").getBDD()  == BDD.varBDD(4));
		assertTrue(sm.getBDDWBForSymbol("i1.1").getBDD()   == BDD.varBDD(9));
		assertTrue(sm.getBDDWBForSymbol("i1.1'").getBDD()  == BDD.varBDD(10));
		assertTrue(sm.getBDDWBForSymbol("i1.0").getBDD()   == BDD.varBDD(11));
		assertTrue(sm.getBDDWBForSymbol("i1.0'").getBDD()  == BDD.varBDD(12));
		assertTrue(sm.getBDDWBForSymbol("b2").getBDD()     == BDD.varBDD(27));
		assertTrue(sm.getBDDWBForSymbol("b2'").getBDD()    == BDD.varBDD(28));
		assertTrue(sm.getBDDWBForSymbol("e1.1").getBDD()   == BDD.varBDD(29));
		assertTrue(sm.getBDDWBForSymbol("e1.1'").getBDD()  == BDD.varBDD(30));
		assertTrue(sm.getBDDWBForSymbol("e1.0").getBDD()   == BDD.varBDD(31));
		assertTrue(sm.getBDDWBForSymbol("e1.0'").getBDD()  == BDD.varBDD(32));
		assertTrue(sm.getBDDWBForSymbol("xyqw")  == null);
		assertTrue(sm.getBDDWBForSymbol("xyqw'") == null);
		
		assertTrue(sm.getBDDWBForSymbol("c1").getBDD() == BDD.and(sm.getBDDWBForSymbol("b1").getBDD(), sm.getBDDWBForSymbol("b2").getBDD()));
	}
	
	@Test
	public void getMultiBDDForSymbol() throws Exception {
		assertTrue(sm.getMultiBDDForSymbol("i1").equals(MultiBDD.varMultiBDD(sm.getMultiIndex("i1"), -16)));
		assertTrue(sm.getMultiBDDForSymbol("c2").equals(MultiBDD.intMultiBDD(42)));
	}
		
	@Test
	public void getStateSpaceBDD() {
		Vector<Integer> mindex = new Vector<Integer>();
		mindex.add(25); mindex.add(23); mindex.add(21);
		mindex.add(19); mindex.add(17); mindex.add(15);	
		assertTrue(sm.stateSpaceBDD() == MultiBDD.leq(MultiBDD.varMultiBDD(mindex, -16), MultiBDD.intMultiBDD(16)));
	}
	
	@Test
	public void displayingOrder() throws Exception {
		Vector<String> displayingOrder = sm.displayingOrder();
		assertEquals(6, displayingOrder.size());
		assertEquals("error", displayingOrder.get(0));
		assertEquals("b1", displayingOrder.get(1));
		assertEquals("b2", displayingOrder.get(2));
		assertEquals("i1", displayingOrder.get(3));
		assertEquals("i2", displayingOrder.get(4));
		assertEquals("e1", displayingOrder.get(5));
	}
	
	@Test
	public void getMultiIndex() throws Exception {
		Vector<Integer> mindex = sm.getMultiIndex("error");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 1);
		
		mindex = sm.getMultiIndex("b1");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 13);
		
		mindex = sm.getMultiIndex("b2");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 27);
		
		mindex = sm.getMultiIndex("i1");
		assertTrue(mindex.size() == 5);
		assertTrue(mindex.get(0) == 11);
		assertTrue(mindex.get(1) == 9);
		assertTrue(mindex.get(2) == 7);
		assertTrue(mindex.get(3) == 5);
		assertTrue(mindex.get(4) == 3);
		
		mindex = sm.getMultiIndex("i1'");
		assertTrue(mindex.size() == 5);
		assertTrue(mindex.get(0) == 12);
		assertTrue(mindex.get(1) == 10);
		assertTrue(mindex.get(2) == 8);
		assertTrue(mindex.get(3) == 6);
		assertTrue(mindex.get(4) == 4);
		
		mindex = sm.getMultiIndex("i2");
		assertTrue(mindex.size() == 6);
		assertTrue(mindex.get(0) == 25);
		assertTrue(mindex.get(1) == 23);
		assertTrue(mindex.get(2) == 21);
		assertTrue(mindex.get(3) == 19);
		assertTrue(mindex.get(4) == 17);
		assertTrue(mindex.get(5) == 15);
		
		mindex = sm.getMultiIndex("i2'");
		assertTrue(mindex.size() == 6);
		assertTrue(mindex.get(0) == 26);
		assertTrue(mindex.get(1) == 24);
		assertTrue(mindex.get(2) == 22);
		assertTrue(mindex.get(3) == 20);
		assertTrue(mindex.get(4) == 18);
		assertTrue(mindex.get(5) == 16);
		
		mindex = sm.getMultiIndex("e1");
		assertTrue(mindex.size() == 2);
		assertTrue(mindex.get(0) == 31);
		assertTrue(mindex.get(1) == 29);
	}
	
	@Test
	public void declareLocalVariable() throws Exception {
		assertTrue(sm.localBivalentVariableCount() == 0);
		sm.declareLocalVariable("i1", Type.booleanType());
		assertTrue(sm.localBivalentVariableCount() == 1);
		sm.declareLocalVariable("i2", Type.integerType(-4, 3));
		assertTrue(sm.localBivalentVariableCount() == 4);
		sm.declareLocalVariable("b1", Type.booleanType());
		assertTrue(sm.localBivalentVariableCount() == 5);
		sm.declareLocalVariable("b1", Type.booleanType());
		assertTrue(sm.localBivalentVariableCount() == 6);
		
		Vector<Integer> mindex = sm.getMultiIndex("i1");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 33);
		
		mindex = sm.getMultiIndex("i2");
		assertTrue(mindex.size() == 3);
		assertTrue(mindex.get(0) == 36);
		assertTrue(mindex.get(1) == 35);
		assertTrue(mindex.get(2) == 34);
		
		mindex = sm.getMultiIndex("b1");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 38);
		
		mindex = sm.getMultiIndex("b2");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 27);
		
		sm.undeclareLocalVariable("b1");
		assertTrue(sm.localBivalentVariableCount() == 5);
		mindex = sm.getMultiIndex("b1");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 37);
	
		sm.undeclareLocalVariable("b1");
		assertTrue(sm.localBivalentVariableCount() == 4);
		mindex = sm.getMultiIndex("b1");
		assertTrue(mindex.size() == 1);
		assertTrue(mindex.get(0) == 13);
		
		sm.undeclareLocalVariable("i1");
		assertTrue(sm.localBivalentVariableCount() == 3);
		mindex = sm.getMultiIndex("i1");
		assertTrue(mindex.size() == 5);
		assertTrue(mindex.get(0) == 11);
		assertTrue(mindex.get(1) == 9);
		assertTrue(mindex.get(2) == 7);
		assertTrue(mindex.get(3) == 5);
		assertTrue(mindex.get(4) == 3);
		
		sm.undeclareLocalVariable("i2");
		assertTrue(sm.localBivalentVariableCount() == 0);
		mindex = sm.getMultiIndex("i2");
		assertTrue(mindex.size() == 6);
		assertTrue(mindex.get(0) == 25);
		assertTrue(mindex.get(1) == 23);
		assertTrue(mindex.get(2) == 21);
		assertTrue(mindex.get(3) == 19);
		assertTrue(mindex.get(4) == 17);
		assertTrue(mindex.get(5) == 15);
	}
	
	@Test
	public void getPrimedToUnprimed() throws Exception {
		VariableRenaming ren = sm.getPrimedToUnprimed();
		
		int indexPrimed = sm.getMultiIndex("b1'").get(0);
		int indexUnprimed = sm.getMultiIndex("b1").get(0);
		assertTrue(ren.apply(indexPrimed) == indexUnprimed);
		
		Vector<Integer> mindex = sm.getMultiIndex("i1");
		for (int digit=0; digit<mindex.size(); digit++) {
			indexPrimed = sm.getMultiIndex("i1'").get(digit);
			indexUnprimed = sm.getMultiIndex("i1").get(digit);
			assertTrue(ren.apply(indexPrimed) == indexUnprimed);
		}
	}
	
	@Test
	public void getUnprimedToPrimed() throws Exception {
		VariableRenaming ren = sm.getUnprimedToPrimed();
		
		int indexUnprimed = sm.getMultiIndex("b2").get(0);
		int indexPrimed = sm.getMultiIndex("b2'").get(0);
		assertTrue(ren.apply(indexUnprimed) == indexPrimed);
		
		Vector<Integer> mindex = sm.getMultiIndex("i2");
		for (int digit=0; digit<mindex.size(); digit++) {
			indexUnprimed = sm.getMultiIndex("i2").get(digit);
			indexPrimed = sm.getMultiIndex("i2'").get(digit);
			assertTrue(ren.apply(indexUnprimed) == indexPrimed);
		}
	}
	
	@Test
	public void getRangeUnprimed() {
		VariableRange range = sm.getRangeUnprimed();
		assertTrue(range.inRange(sm.getMultiIndex("b1").get(0)) == VariableRange.Relation.IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("b1'").get(0)) == VariableRange.Relation.NOT_IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1").get(0)) == VariableRange.Relation.IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1'").get(1)) == VariableRange.Relation.NOT_IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1'").get(0)) == VariableRange.Relation.RANGE_PASSED);
	}

	@Test
	public void getRangePrimed() {
		VariableRange range = sm.getRangePrimed();
		assertTrue(range.inRange(sm.getMultiIndex("b1").get(0)) == VariableRange.Relation.NOT_IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("b1'").get(0)) == VariableRange.Relation.IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1").get(0)) == VariableRange.Relation.NOT_IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1'").get(1)) == VariableRange.Relation.IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1'").get(0)) == VariableRange.Relation.IN_RANGE);
		assertTrue(range.inRange(sm.getMultiIndex("e1'").get(0)+1) == VariableRange.Relation.RANGE_PASSED);
	}
}
