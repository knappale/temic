package core;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Vector;


public class TypeTest {
  @SuppressWarnings("unused")
  private Type voidType1, voidType2;
  private Type booleanType1, booleanType2;
  private Type integerType1, integerType2, integerType3, integerType4, integerType5, integerType6, integerType7;
  private Type enumType1, enumType2, enumType3, enumType4;

  @Before
  public void setUp() throws Exception {
    voidType1 = Type.voidType();
    voidType2 = Type.voidType();
    booleanType1 = Type.booleanType();
    booleanType2 = Type.booleanType();
    integerType1 = Type.integerType(1, 17);
    integerType2 = Type.integerType(0, 17);
    integerType3 = Type.integerType(-10, 18);
    integerType4 = Type.integerType(0, 0);
    integerType5 = Type.integerType(0, -1);
    integerType6 = Type.integerType(99, 50);
    integerType7 = Type.integerType(1, 17);
    Vector<String> vals1 = new Vector<String>();
    vals1.add("IDLE");
    vals1.add("RUNNING");
    vals1.add("ERROR");
    enumType1 = Type.enumType(vals1);
    Vector<String> vals2 = new Vector<String>();
    vals2.add("ERROR");
    vals2.add("IDLE");
    vals2.add("RUNNING");
    vals2.add("MAINTENANCE");
    enumType2 = Type.enumType(vals2);
    Vector<String> vals3 = new Vector<String>();
    enumType3 = Type.enumType(vals3);
    enumType4 = Type.enumType(vals1);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void isVoid() throws Exception {
    assertTrue(voidType1.isVoid());
    assertFalse(booleanType1.isVoid());
    assertFalse(integerType1.isVoid());
    assertFalse(integerType5.isVoid());
    assertFalse(enumType1.isVoid());
  }

  @Test
  public void isBoolean() throws Exception {
    assertFalse(voidType1.isBoolean());
    assertTrue(booleanType1.isBoolean());
    assertFalse(integerType1.isBoolean());
    assertFalse(integerType5.isBoolean());
    assertFalse(enumType1.isBoolean());
  }

  @Test
  public void isInteger() throws Exception {
    assertFalse(booleanType1.isInteger());
    assertTrue(integerType1.isInteger());
    assertTrue(integerType5.isInteger());
    assertFalse(enumType1.isInteger());
  }

  @Test
  public void isEnum() throws Exception {
    assertFalse(voidType1.isEnum());
    assertFalse(booleanType1.isEnum());
    assertFalse(integerType1.isEnum());
    assertFalse(integerType5.isEnum());
    assertTrue(enumType1.isEnum());

  }

  @Test
  public void isEmpty() throws Exception {
    assertTrue(voidType1.isEmpty());
    assertFalse(booleanType1.isEmpty());
    assertFalse(integerType1.isEmpty());
    assertFalse(integerType2.isEmpty());
    assertTrue(integerType5.isEmpty());
    assertTrue(integerType6.isEmpty());
    assertFalse(enumType1.isEmpty());
    assertTrue(enumType3.isEmpty());
  }

  @Test
  public void size() throws Exception {
    assertTrue(voidType1.size() == 0);
    assertTrue(booleanType1.size() == 2);
    assertTrue(integerType1.size() == 17);
    assertTrue(integerType2.size() == 18);
    assertTrue(integerType5.size() == 0);
    assertTrue(integerType6.size() == 0);
    assertTrue(enumType1.size() == 3);
    assertTrue(enumType3.size() == 0);
  }

  @Test
  public void lowerBound() throws Exception {
    assertTrue(booleanType1.lowerBound() == 0);
    assertTrue(integerType1.lowerBound() == 1);
    assertTrue(integerType2.lowerBound() == 0);
    assertTrue(integerType3.lowerBound() == -10);
    assertTrue(integerType5.lowerBound() == 0);
    assertTrue(integerType6.lowerBound() == 0);
    try {
      voidType1.lowerBound();
      fail("no exception as result of calling lowerBound() on void type");
    } catch (IllegalArgumentException e) {
    }
    try {
      enumType1.lowerBound();
      fail("no exception as result of calling lowerBound() on enum type");
    } catch (IllegalArgumentException e) {
    }
  }

  @Test
  public void upperBound() throws Exception {
    assertTrue(booleanType1.upperBound() == 1);
    assertTrue(integerType1.upperBound() == 17);
    assertTrue(integerType2.upperBound() == 17);
    assertTrue(integerType3.upperBound() == 18);
    assertTrue(integerType5.upperBound() == -1);
    assertTrue(integerType6.upperBound() == -1);
    try {
      enumType1.upperBound();
      fail("no exception as result of calling upperBound() on enum type");
    } catch (IllegalArgumentException e) {
    }
  }

  @Test
  public void equals() throws Exception {
    assertTrue(booleanType1 == booleanType2);
    assertTrue(booleanType1.equals(booleanType2));
    assertFalse(booleanType1.equals(integerType1));
    assertFalse(booleanType1.equals(enumType1));
    assertTrue(integerType1.equals(integerType1));
    assertTrue(integerType1.equals(integerType7));
    assertFalse(integerType1.equals(integerType2));
    assertFalse(integerType1.equals(integerType3));
    assertFalse(integerType1.equals(integerType4));
    assertFalse(integerType2.equals(integerType3));
    assertFalse(integerType2.equals(integerType4));
    assertFalse(integerType3.equals(integerType4));
    assertFalse(integerType1.equals(integerType5));
    assertTrue(integerType5.equals(integerType5));
    assertTrue(integerType5.equals(integerType6));
    assertTrue(enumType1.equals(enumType1));
    assertTrue(enumType1.equals(enumType4));
    assertFalse(enumType1.equals(enumType2));
    assertFalse(enumType2.equals(enumType1));
    assertFalse(enumType1.equals(enumType3));
    assertFalse(enumType2.equals(enumType3));
    assertTrue(enumType3.equals(enumType3));
  }

  @Test
  public void subsets() throws Exception {
    assertTrue(booleanType1.subsets(booleanType1));
    assertTrue(booleanType1.subsets(booleanType2));
    assertFalse(booleanType1.subsets(integerType1));
    assertFalse(booleanType1.equals(enumType1));

    assertTrue(integerType1.subsets(integerType1));
    assertTrue(integerType1.subsets(integerType7));
    assertTrue(integerType1.subsets(integerType2));
    assertTrue(integerType1.subsets(integerType3));
    assertFalse(integerType1.subsets(integerType4));
    assertFalse(integerType1.subsets(integerType5));
    assertFalse(integerType1.subsets(integerType6));

    assertFalse(integerType2.subsets(integerType1));
    assertTrue(integerType2.subsets(integerType2));
    assertTrue(integerType2.subsets(integerType3));
    assertFalse(integerType2.subsets(integerType4));
    assertFalse(integerType2.subsets(integerType5));
    assertFalse(integerType2.subsets(integerType6));
    assertFalse(integerType2.subsets(integerType7));

    assertFalse(integerType3.subsets(integerType1));
    assertFalse(integerType3.subsets(integerType2));
    assertTrue(integerType3.subsets(integerType3));
    assertFalse(integerType3.subsets(integerType4));
    assertFalse(integerType3.subsets(integerType5));
    assertFalse(integerType3.subsets(integerType6));
    assertFalse(integerType3.subsets(integerType7));

    assertFalse(integerType4.subsets(integerType1));
    assertTrue(integerType4.subsets(integerType2));
    assertTrue(integerType4.subsets(integerType3));
    assertTrue(integerType4.subsets(integerType4));
    assertFalse(integerType4.subsets(integerType5));
    assertFalse(integerType4.subsets(integerType6));
    assertFalse(integerType4.subsets(integerType7));

    assertTrue(integerType5.subsets(integerType1));
    assertTrue(integerType5.subsets(integerType2));
    assertTrue(integerType5.subsets(integerType3));
    assertTrue(integerType5.subsets(integerType4));
    assertTrue(integerType5.subsets(integerType5));
    assertTrue(integerType5.subsets(integerType6));
    assertTrue(integerType5.subsets(integerType7));
    assertFalse(integerType5.subsets(booleanType1));
    assertFalse(integerType5.subsets(enumType1));

    assertTrue(enumType1.subsets(enumType1));
    assertTrue(enumType1.subsets(enumType2));
    assertFalse(enumType1.equals(enumType3));
    assertTrue(enumType1.equals(enumType4));

    assertFalse(enumType2.subsets(enumType1));
    assertTrue(enumType2.subsets(enumType2));
    assertFalse(enumType2.subsets(enumType3));
    assertFalse(enumType2.subsets(enumType4));

    assertTrue(enumType3.subsets(enumType1));
    assertTrue(enumType3.subsets(enumType2));
    assertTrue(enumType3.subsets(enumType3));
    assertFalse(enumType3.subsets(booleanType1));
    assertFalse(enumType3.subsets(integerType1));
  }

  @Test
  public void encodingUpperBound() throws Exception {
    assertSame(-1, voidType1.encodingUpperBound());
    assertSame(1, booleanType1.encodingUpperBound());
    assertSame(16, integerType1.encodingUpperBound());
    assertSame(17, integerType2.encodingUpperBound() );
    assertSame(28, integerType3.encodingUpperBound());
    assertSame(0, integerType4.encodingUpperBound());
    assertSame(-1, integerType5.encodingUpperBound());
    assertSame(-1, integerType6.encodingUpperBound());
    assertSame(7, enumType1.encodingUpperBound());
    assertSame(7, enumType2.encodingUpperBound());
    assertSame(7, enumType3.encodingUpperBound());
  }

  @Test
  public void numBits() throws Exception {
    try {
      voidType1.numBits();
      fail("no RuntimeException as result of calling numBits() on void type");
    }
    catch (RuntimeException e) {
    }
    assertTrue(booleanType1.numBits() == 1);
    assertTrue(integerType1.numBits() == 5);
    assertTrue(integerType2.numBits() == 5);
    assertTrue(integerType3.numBits() == 5);
    assertTrue(integerType4.numBits() == 0);
    try {
      integerType5.numBits();
      fail("no RuntimeException as result of calling numBits() on empty int type");
    }
    catch (RuntimeException e) {
    }
    assertEquals(3, enumType1.numBits());
    assertEquals(3, enumType2.numBits());
    assertEquals(3, enumType3.numBits());
  }
}
