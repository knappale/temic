package bdd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bdd.BDD;
import bdd.Valuation;
import core.SymbolManager;
import core.SymbolTable;
import core.Type;
import syntax.Identifier;

import java.util.Vector;
import java.io.Writer;
import java.io.PipedWriter;
import java.io.PipedReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.IOException;

import org.eclipse.jdt.annotation.NonNull;


public class ValuationTest {
  public static final int NODETABLE_SIZE = 10000;
  public static final int CACHE_SIZE = 10000;
  public static final int WRAPPERTABLE_SIZE = 1117;
  private Valuation val1, val2, val5;
  private Vector<String> symbols;
  private SymbolTable<syntax.Identifier, Type> typeEnv;
  private SymbolManager sm;

  @BeforeClass
  public static void bdd_initialize() throws Exception {
    bdd.BDD.reconfigure(NODETABLE_SIZE, CACHE_SIZE, WRAPPERTABLE_SIZE);
  }

  @Before
  public void setUp() throws Exception {
    // test untyped
    val1 = new Valuation();
    val2 = new Valuation();
    val2.add(1, true);
    val2.add(2, false);
    val2.add(5, false);
    val2.add(7, true);
    val2.add(8, true);
    val2.add(15, false);
    symbols = new Vector<String>();
    symbols.add("v1");
    symbols.add("v2");
    symbols.add("v1'");
    symbols.add("v2'");
    symbols.add("v5");
    symbols.add("v5'");
    symbols.add("v7");
    symbols.add("v7'");
    symbols.add("v9");
    symbols.add("v10");
    symbols.add("v11");
    symbols.add("v12");
    symbols.add("v13");
    symbols.add("v14");
    symbols.add("v15");
    symbols.add("v15'");

    // test typed
    typeEnv = new SymbolTable<>();
    typeEnv.push(Identifier.variable("b1"), Type.booleanType());
    typeEnv.push(Identifier.variable("b2"), Type.booleanType());
    typeEnv.push(Identifier.variable("b3"), Type.booleanType());
    typeEnv.push(Identifier.variable("i1"), Type.integerType(-16, 15));
    typeEnv.push(Identifier.variable("i2"), Type.integerType(100, 115));
    typeEnv.push(Identifier.variable("i3"), Type.integerType(0, 255));
    Vector<String> vars = new Vector<String>();
    vars.add("b1");
    vars.add("b2");
    vars.add("i1");
    vars.add("b3");
    vars.add("i2");
    vars.add("i3");
    sm = new SymbolManager(vars, typeEnv);
    val5 = new Valuation();
    val5.add(sm.getMultiIndex("error").get(0), false);
    val5.add(sm.getMultiIndex("b1").get(0), false);
    val5.add(sm.getMultiIndex("b2").get(0), true);
    val5.add(sm.getMultiIndex("b3").get(0), true);

    val5.add(sm.getMultiIndex("i1").get(0), false);
    val5.add(sm.getMultiIndex("i1").get(1), true);
    val5.add(sm.getMultiIndex("i1").get(2), false);
    val5.add(sm.getMultiIndex("i1").get(3), true);
    val5.add(sm.getMultiIndex("i1").get(4), false);

    val5.add(sm.getMultiIndex("i2").get(0), false);
    val5.add(sm.getMultiIndex("i2").get(1), false);
    val5.add(sm.getMultiIndex("i2").get(2), true);
    val5.add(sm.getMultiIndex("i2").get(3), false);

    val5.add(sm.getMultiIndex("i3").get(0), true);
    val5.add(sm.getMultiIndex("i3").get(1), true);
    val5.add(sm.getMultiIndex("i3").get(2), false);
    val5.add(sm.getMultiIndex("i3").get(3), false);
    val5.add(sm.getMultiIndex("i3").get(4), true);
    val5.add(sm.getMultiIndex("i3").get(5), false);
    val5.add(sm.getMultiIndex("i3").get(6), true);
    val5.add(sm.getMultiIndex("i3").get(7), true);
  }

  @Test
  public void isTrivial() {
    assertTrue(val1.isTrivial());
    assertFalse(val2.isTrivial());
  }

  @Test
  public void size() {
    assertTrue(val1.size() == 0);
    assertTrue(val2.size() == 6);
  }

  @Test
  public void add() {
    assertTrue(val2.size() == 6);
    assertTrue(val2.value(2) == false);
    assertTrue(val2.value(3) == null);
    val2.add(2, true);
    assertTrue(val2.size() == 6);
    assertTrue(val2.value(2) == true);
    val2.add(3, false);
    assertTrue(val2.size() == 7);
    assertTrue(val2.value(3) == false);
    try {
      val2.add(0, true);
    } catch (RuntimeException e) {
    }
  }

  @Test
  public void asBDD() {
    BDD bdd2 = val2.asBDD();
    Valuation val3 = bdd2.satisfyingValuation();
    assertEquals(val3, val2);
  }

  @Test
  public void specifyDontCares() {
    try {
      Writer systemOut = new OutputStreamWriter(System.out);
      PipedWriter out = new PipedWriter();
      BufferedReader in = new BufferedReader(new PipedReader(out, 2048));

      val2.print(systemOut, symbols);
      val2.print(out, symbols);
      assertTrue(in.readLine().equals(" v1, !v2, !v5,  v7,  v7', !v15"));

      Valuation val4 = val2.specifyDontCares(symbols);
      val4.print(systemOut, symbols);
      val4.print(out, symbols);
      assertTrue(in.readLine().equals(" v1, !v2, !v5,  v7, !v9, !v10, !v11, !v12, !v13, !v14, !v15"));
      in.close();
    } catch (IOException e) {
      fail();
    }
  }
}
