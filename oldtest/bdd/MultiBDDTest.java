package bdd;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
// import static org.junit.Assert.fail;
// import junit.framework.Assert;
// import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Vector;

import bdd.BDD;
import bdd.MultiBDD;
import bdd.MultiBDD.PairOfMultiBDDs;

public class MultiBDDTest {
	public static final int NODETABLE_SIZE = 10000;
	public static final int CACHE_SIZE = 10000;
	public static final int WRAPPERTABLE_SIZE = 1117;
	private MultiBDD int_0, int_1, int_2, int_3, int_4, int_5, int_6, int_7, int_8, int_9;
	private MultiBDD int_10, int_17, int_17copy, int_1152, int_max, int_maxMinus1;
	@SuppressWarnings("unused")
	private MultiBDD int_n1, int_n2, int_n3, int_n4, int_n5, int_n6, int_n7, int_n8, int_n9;
	private MultiBDD int_n10, int_n17, int_n54, int_min, int_minPlus1, int_mindiv2;
	@SuppressWarnings("unused")
	private MultiBDD var_A, var_B, var_C, var_D, var_E;
	private BDD v1, v2, v3;
	private BDD n1, n2, n3;
	
	@BeforeClass
	public static void bdd_initialize() throws Exception {
       		bdd.BDD.reconfigure(NODETABLE_SIZE, CACHE_SIZE, WRAPPERTABLE_SIZE);
	}
	
	@Before
	public void setUp() throws Exception {
		int_0 = MultiBDD.intMultiBDD(0);
		int_1 = MultiBDD.intMultiBDD(1);
		int_2 = MultiBDD.intMultiBDD(2);
		int_3 = MultiBDD.intMultiBDD(3);
		int_4 = MultiBDD.intMultiBDD(4);
		int_5 = MultiBDD.intMultiBDD(5);
		int_6 = MultiBDD.intMultiBDD(6);
		int_7 = MultiBDD.intMultiBDD(7);
		int_8 = MultiBDD.intMultiBDD(8);
		int_9 = MultiBDD.intMultiBDD(9);
		int_10 = MultiBDD.intMultiBDD(10);
		int_17 = MultiBDD.intMultiBDD(17);
		int_17copy = MultiBDD.intMultiBDD(17);
		int_1152 = MultiBDD.intMultiBDD(1152);
		int_max = MultiBDD.intMultiBDD(Integer.MAX_VALUE);
		int_maxMinus1 = MultiBDD.intMultiBDD(Integer.MAX_VALUE-1);
		int_n1 = MultiBDD.intMultiBDD(-1);
		int_n2 = MultiBDD.intMultiBDD(-2);
		int_n3 = MultiBDD.intMultiBDD(-3);
		int_n4 = MultiBDD.intMultiBDD(-4);
		int_n5 = MultiBDD.intMultiBDD(-5);
		int_n6 = MultiBDD.intMultiBDD(-6);
		int_n7 = MultiBDD.intMultiBDD(-7);
		int_n8 = MultiBDD.intMultiBDD(-8);
		int_n9 = MultiBDD.intMultiBDD(-9);
		int_n10 = MultiBDD.intMultiBDD(-10);
		int_n17 = MultiBDD.intMultiBDD(-17);
		int_n54 = MultiBDD.intMultiBDD(-54);
		int_min = MultiBDD.intMultiBDD(Integer.MIN_VALUE);
		int_minPlus1 = MultiBDD.intMultiBDD(Integer.MIN_VALUE+1);
		int_mindiv2 = MultiBDD.intMultiBDD(Integer.MIN_VALUE/2);
		
		Vector<Integer> mindex1 = new Vector<Integer>();
		mindex1.add(1); mindex1.add(2); mindex1.add(3);
		var_A = MultiBDD.varMultiBDD(mindex1, 0);
		v1 = BDD.varBDD(1); n1 = BDD.not(v1);
		v2 = BDD.varBDD(2); n2 = BDD.not(v2);
		v3 = BDD.varBDD(3); n3 = BDD.not(v3);
		var_B = MultiBDD.varMultiBDD(mindex1, -4);
	}
	
	@Test
	public void leq() throws Exception {
		assertTrue(MultiBDD.leq(int_0, int_0) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_min, int_min) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_0, int_1) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_1, int_0) == BDD.falseBDD());
		assertTrue(MultiBDD.leq(int_0, int_n1) == BDD.falseBDD());
		assertTrue(MultiBDD.leq(int_min, int_n54) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_min, int_0) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_min, int_17) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_min, int_max) == BDD.trueBDD());
		
		assertTrue(MultiBDD.leq(int_0, var_A) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_0, var_B) == v3);
		assertTrue(MultiBDD.leq(var_B, var_A) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(var_A, var_B) == BDD.falseBDD());
		assertTrue(MultiBDD.leq(var_A, int_0) == BDD.and(BDD.and(n1, n2), n3));
		assertTrue(MultiBDD.leq(int_1, var_A) == BDD.or(BDD.or(v1, v2), v3));
		assertTrue(MultiBDD.leq(var_A, var_A) == BDD.trueBDD());
		assertTrue(MultiBDD.leq(int_5, var_A) == BDD.or(BDD.and(v2,v3), BDD.and(v1,v3)));
		assertTrue(MultiBDD.leq(MultiBDD.add(var_A, int_9), int_10) ==
			       BDD.or(MultiBDD.equal(var_A,int_0), MultiBDD.equal(var_A, int_1)));
	}
	
	@Test
	public void less() throws Exception {
		assertTrue(MultiBDD.less(int_0, int_0) == BDD.falseBDD());
		assertTrue(MultiBDD.less(int_min, int_min) == BDD.falseBDD());
		assertTrue(MultiBDD.less(int_0, int_1) == BDD.trueBDD());
		assertTrue(MultiBDD.less(int_1, int_0) == BDD.falseBDD());
		assertTrue(MultiBDD.less(int_0, int_n1) == BDD.falseBDD());
		assertTrue(MultiBDD.less(int_n1, int_0) == BDD.trueBDD());
		assertTrue(MultiBDD.less(int_min, int_n54) == BDD.trueBDD());
		assertTrue(MultiBDD.less(int_min, int_0) == BDD.trueBDD());
		assertTrue(MultiBDD.less(int_min, int_17) == BDD.trueBDD());
		assertTrue(MultiBDD.less(int_min, int_max) == BDD.trueBDD());
		
		assertTrue(MultiBDD.less(int_n1, var_A) == BDD.trueBDD());
		assertTrue(MultiBDD.less(int_n1, var_B) == v3);
		assertTrue(MultiBDD.less(var_B, var_A) == BDD.trueBDD());
		assertTrue(MultiBDD.less(var_A, var_B) == BDD.falseBDD());
		assertTrue(MultiBDD.less(var_A, int_1) == BDD.and(BDD.and(n1, n2), n3));
		assertTrue(MultiBDD.less(var_A, int_0) == BDD.falseBDD());
		assertTrue(MultiBDD.less(int_0, var_A) == BDD.or(BDD.or(v1, v2), v3));
		assertTrue(MultiBDD.less(var_A, var_A) == BDD.falseBDD());
		assertTrue(MultiBDD.less(int_4, var_A) == BDD.or(BDD.and(v2,v3), BDD.and(v1,v3)));
		assertTrue(MultiBDD.less(MultiBDD.add(var_A, int_8), int_10) ==
		           BDD.or(MultiBDD.equal(var_A,int_0), MultiBDD.equal(var_A, int_1)));
		
	}

	@Test
	public void get() throws Exception {
		assertTrue(var_A.get(0) == BDD.varBDD(1));
		assertTrue(var_A.get(1) == BDD.varBDD(2));
		assertTrue(var_A.get(2) == BDD.varBDD(3));
		assertTrue(var_A.get(3) == BDD.falseBDD());
		assertTrue(var_B.get(3) == n3);
		assertTrue(var_A.get(31) == BDD.falseBDD());
		assertTrue(var_B.get(31) == n3);
	}
	
	@Test
	public void add() throws Exception {
		MultiBDD sum = MultiBDD.add(int_3, int_0);
		assertTrue(sum.equals(int_3));
		assertTrue(sum.carry() == BDD.falseBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_0, int_3);
		assertTrue(sum.equals(int_3));
		assertTrue(sum.carry() == BDD.falseBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_3, int_7);
		assertTrue(sum.equals(int_10));
		assertTrue(sum.carry() == BDD.falseBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_7, int_3);
		assertTrue(sum.equals(int_10));
		assertTrue(sum.carry() == BDD.falseBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());

		sum = MultiBDD.add(int_10, int_n7);
		assertTrue(sum.equals(int_3));
		assertTrue(sum.carry() == BDD.trueBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_n7, int_10);
		assertTrue(sum.equals(int_3));
		assertTrue(sum.carry() == BDD.trueBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_17, int_n17);
		assertTrue(sum.equals(int_0));
		assertTrue(sum.carry() == BDD.trueBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_max, int_1);
		assertTrue(sum.equals(int_min));
		assertTrue(sum.carry() == BDD.falseBDD());
		assertTrue(sum.overflow() == BDD.trueBDD());
		
		sum = MultiBDD.add(int_min, int_1);
		assertTrue(sum.equals(int_minPlus1));
		assertTrue(sum.carry() == BDD.falseBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(int_min, int_n1);
		assertTrue(sum.equals(int_max));
		assertTrue(sum.carry() == BDD.trueBDD());
		assertTrue(sum.overflow() == BDD.trueBDD());
		
		sum = MultiBDD.add(int_max, int_n1);
		assertTrue(sum.equals(int_maxMinus1));
		assertTrue(sum.carry() == BDD.trueBDD());
		assertTrue(sum.overflow() == BDD.falseBDD());
		
		sum = MultiBDD.add(var_B, int_4);
		assertTrue(sum.equals(var_A));
		assertTrue(sum.carry() == MultiBDD.leq(var_B, int_n1));
		assertTrue(sum.overflow() == BDD.falseBDD());
	}
	
	@Test
	public void inc() throws Exception {
		assertTrue(MultiBDD.inc(int_3).equals(int_4));
		assertTrue(MultiBDD.inc(int_max).equals(int_min));
	}
	
	@Test
	public void sub() throws Exception {
		assertTrue(MultiBDD.sub(int_7, int_7).equals(int_0));
		assertTrue(MultiBDD.sub(int_7, int_6).equals(int_1));
		assertTrue(MultiBDD.sub(int_7, int_1).equals(int_6));
		assertTrue(MultiBDD.sub(int_7, int_0).equals(int_7));
		assertTrue(MultiBDD.sub(int_7, int_n1).equals(int_8));
		assertTrue(MultiBDD.sub(int_max, int_max).equals(int_0));
		assertTrue(MultiBDD.sub(int_min, int_min).equals(int_0));
		assertTrue(MultiBDD.sub(int_max, int_0).equals(int_max));
		assertTrue(MultiBDD.sub(int_min, int_0).equals(int_min));
		assertTrue(MultiBDD.sub(int_max, int_1).equals(int_maxMinus1));
		assertTrue(MultiBDD.sub(int_minPlus1, int_1).equals(int_min));
	}
	
	@Test
	public void dec() throws Exception {
		assertTrue(MultiBDD.dec(int_4).equals(int_3));
		assertTrue(MultiBDD.dec(int_min).equals(int_max));
	}

	@Test
	public void negate() throws Exception {
		MultiBDD val = MultiBDD.negate(int_7);
		assertTrue(val.equals(int_n7));
		assertTrue(val.carry() == BDD.falseBDD());
		assertTrue(val.overflow() == BDD.falseBDD());

		val = MultiBDD.negate(int_n7);
		assertTrue(val.equals(int_7));
		assertTrue(val.carry() == BDD.falseBDD());
		assertTrue(val.overflow() == BDD.falseBDD());
		
		val = MultiBDD.negate(int_0);
		assertTrue(val.equals(int_0));
		assertTrue(val.carry() == BDD.trueBDD());
		assertTrue(val.overflow() == BDD.falseBDD());
		
		val = MultiBDD.negate(int_min);
		assertTrue(val.equals(int_min));
		assertTrue(val.carry() == BDD.falseBDD());
		assertTrue(val.overflow() == BDD.trueBDD());
		
		val = MultiBDD.negate(int_max);
		assertTrue(val.equals(int_minPlus1));
		assertTrue(val.carry() == BDD.falseBDD());
		assertTrue(val.overflow() == BDD.falseBDD());
	}
	
	@Test
	public void abs() throws Exception {
		assertTrue(MultiBDD.abs(int_0).equals(int_0));
		assertTrue(MultiBDD.abs(int_1).equals(int_1));
		assertTrue(MultiBDD.abs(int_n1).equals(int_1));
		assertTrue(MultiBDD.abs(int_n7).equals(int_7));
		assertTrue(MultiBDD.abs(int_max).equals(int_max));
		assertTrue(MultiBDD.abs(int_minPlus1).equals(int_max));
		assertTrue(MultiBDD.abs(int_min).equals(int_min));
		
		MultiBDD val = MultiBDD.abs(var_B);
		assertTrue(MultiBDD.equal(val, int_1) == BDD.or(MultiBDD.equal(var_B, int_1), MultiBDD.equal(var_B, int_n1)));
	}

	@Test
	public void shiftRightSigned() throws Exception {
		assertTrue(MultiBDD.shiftRightSigned(int_min, 0).equals(int_min));
		assertTrue(MultiBDD.shiftRightSigned(int_min, 1).equals(int_mindiv2));
		assertTrue(MultiBDD.shiftRightSigned(int_min, 29).equals(int_n4));
		assertTrue(MultiBDD.shiftRightSigned(int_min, 30).equals(int_n2));
		assertTrue(MultiBDD.shiftRightSigned(int_min, 31).equals(int_n1));
		assertTrue(MultiBDD.shiftRightSigned(int_min, 32).equals(int_n1));
		assertTrue(MultiBDD.shiftRightSigned(MultiBDD.add(int_min, int_10), 1).equals(MultiBDD.add(int_mindiv2, int_5)));
	}
	
	@Test
	public void shiftRight() throws Exception {
		assertTrue(MultiBDD.shiftRight(int_max, 31).equals(int_0));
		assertTrue(MultiBDD.shiftRight(int_max, 30).equals(int_1));
		assertTrue(MultiBDD.shiftRight(int_max, 29).equals(int_3));
		assertTrue(MultiBDD.shiftRight(int_max, 0).equals(int_max));
		assertTrue(MultiBDD.shiftRight(int_min, 31).equals(int_1));
		assertTrue(MultiBDD.shiftRight(int_min, 30).equals(int_2));
		assertTrue(MultiBDD.shiftRight(int_min, 29).equals(int_4));
		assertTrue(MultiBDD.shiftRight(int_10, 1).equals(int_5));
		assertTrue(MultiBDD.shiftRight(MultiBDD.add(int_min, int_10), 1).equals(MultiBDD.add(MultiBDD.negate(int_mindiv2), int_5)));
	}
	
	@Test
	public void shiftLeft() throws Exception {
		assertTrue(MultiBDD.shiftLeft(int_0, 1).equals(int_0));
		assertTrue(MultiBDD.shiftLeft(int_1, 1).equals(int_2));
		assertTrue(MultiBDD.shiftLeft(int_2, 1).equals(int_4));
		assertTrue(MultiBDD.shiftLeft(int_9, 0).equals(int_9));
		assertTrue(MultiBDD.shiftLeft(int_9, 7).equals(int_1152));
		assertTrue(MultiBDD.shiftLeft(int_9, 31).equals(int_min));
		assertTrue(MultiBDD.shiftLeft(int_9, 32).equals(int_0));
	}
	
	@Test
	public void rotateRightThroughCarry() throws Exception {
		MultiBDD val = MultiBDD.rotateRightThroughCarry(int_min, BDD.falseBDD());
		assertTrue(val.equals(MultiBDD.negate(int_mindiv2)));
		assertTrue(val.carry() == BDD.falseBDD());
		
		val = MultiBDD.rotateRightThroughCarry(int_min, BDD.trueBDD());
		assertTrue(val.equals(int_mindiv2));
		assertTrue(val.carry() == BDD.falseBDD());
		
		val = MultiBDD.rotateRightThroughCarry(int_n2, BDD.trueBDD());
		assertTrue(val.equals(int_n1));
		assertTrue(val.carry() == BDD.falseBDD());
		val = MultiBDD.rotateRightThroughCarry(val, BDD.trueBDD());
		assertTrue(val.equals(int_n1));
		assertTrue(val.carry() == BDD.trueBDD());
		val = MultiBDD.rotateRightThroughCarry(val, BDD.falseBDD());
		assertTrue(val.equals(int_max));
		assertTrue(val.carry() == BDD.trueBDD());
		
		val = MultiBDD.rotateRightThroughCarry(int_1, BDD.falseBDD());
		assertTrue(val.equals(int_0));
		assertTrue(val.carry() == BDD.trueBDD());
	}	
	
	@Test
	public void rotateLeftThroughCarry() throws Exception {
		MultiBDD val = MultiBDD.rotateLeftThroughCarry(int_0, BDD.falseBDD());
		assertTrue(val.equals(int_0));
		assertTrue(val.carry() == BDD.falseBDD());
		
		val = MultiBDD.rotateLeftThroughCarry(int_0, BDD.trueBDD());
		assertTrue(val.equals(int_1));
		assertTrue(val.carry() == BDD.falseBDD());
		
		val = MultiBDD.rotateLeftThroughCarry(int_1, BDD.trueBDD());
		assertTrue(val.equals(int_3));
		assertTrue(val.carry() == BDD.falseBDD());
		
		val = MultiBDD.rotateLeftThroughCarry(int_max, BDD.trueBDD());
		assertTrue(val.equals(int_n1));
		assertTrue(val.carry() == BDD.falseBDD());
		
		val = MultiBDD.rotateLeftThroughCarry(int_min, BDD.falseBDD());
		assertTrue(val.equals(int_0));
		assertTrue(val.carry() == BDD.trueBDD());
		
		val = MultiBDD.rotateLeftThroughCarry(int_min, BDD.trueBDD());
		assertTrue(val.equals(int_1));
		assertTrue(val.carry() == BDD.trueBDD());
	}

	@Test
	public void multiply() throws Exception {
		MultiBDD val = MultiBDD.multiply(int_0, int_10);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.multiply(int_n7, int_0);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.multiply(int_min, int_0);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.multiply(int_min, int_1);
		assertTrue(val.equals(int_min));
		
		val = MultiBDD.multiply(int_min, int_n1);
		assertTrue(val.equals(int_min));
		
		val = MultiBDD.multiply(int_max, int_1);
		assertTrue(val.equals(int_max));
		
		val = MultiBDD.multiply(int_max, int_n1);
		assertTrue(val.equals(int_minPlus1));
		
		val = MultiBDD.multiply(int_1, int_10);
		assertTrue(val.equals(int_10));
		
		val = MultiBDD.multiply(int_10, int_1);
		assertTrue(val.equals(int_10));
		
		val = MultiBDD.multiply(int_1, int_n10);
		assertTrue(val.equals(int_n10));
		
		val = MultiBDD.multiply(int_2, int_2);
		assertTrue(val.equals(int_4));
		
		val = MultiBDD.multiply(int_2, int_n3);
		assertTrue(val.equals(int_n6));
		
		val = MultiBDD.multiply(int_n3, int_n3);
		assertTrue(val.equals(int_9));
		
		val = MultiBDD.multiply(MultiBDD.intMultiBDD(45113), MultiBDD.intMultiBDD(22893));
		assertTrue(val.equals(MultiBDD.intMultiBDD(1032771909)));
		
		val = MultiBDD.multiply(MultiBDD.intMultiBDD(-45113), MultiBDD.intMultiBDD(22893));
		assertTrue(val.equals(MultiBDD.intMultiBDD(-1032771909)));

		val = MultiBDD.multiply(var_A, int_2);
		assertTrue(MultiBDD.leq(val, int_2) == BDD.or(MultiBDD.equal(var_A,int_0),MultiBDD.equal(var_A,int_1)));
		assertTrue(MultiBDD.less(int_10, val) == BDD.or(MultiBDD.equal(var_A,int_6),MultiBDD.equal(var_A,int_7)));
		assertTrue(MultiBDD.equal(val, int_10) == MultiBDD.equal(var_A, int_5));
	
		val = MultiBDD.multiply(var_A, var_A);
		assertTrue(MultiBDD.equal(val,MultiBDD.intMultiBDD(36)) == MultiBDD.equal(var_A, int_6));
		assertTrue(MultiBDD.leq(MultiBDD.intMultiBDD(36), val) == BDD.or(MultiBDD.equal(var_A,int_6),MultiBDD.equal(var_A,int_7)));

		val = MultiBDD.multiply(var_B, var_B);
		assertTrue(MultiBDD.equal(val, int_9) == BDD.or(MultiBDD.equal(var_B,int_3),MultiBDD.equal(var_B,int_n3)));
	}
	
	@Test
	public void div() throws Exception {
		MultiBDD val = MultiBDD.div(int_0, int_1);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.div(int_0, int_7);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.div(int_0, int_max);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.div(int_1, int_1);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.div(int_7, int_1);
		assertTrue(val.equals(int_7));
		
		val = MultiBDD.div(int_max, int_1);
		assertTrue(val.equals(int_max));
		
		val = MultiBDD.div(int_1, int_2);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.div(int_7, int_2);
		assertTrue(val.equals(int_3));
		
		val = MultiBDD.div(int_6, int_2);
		assertTrue(val.equals(int_3));
		
		val = MultiBDD.div(int_max, int_2);
		assertTrue(val.equals(MultiBDD.intMultiBDD(Integer.MAX_VALUE / 2)));
		
		val = MultiBDD.div(int_max, int_max);
		assertTrue(val.equals(int_1));
		
		// tests with negative arguments
		val = MultiBDD.div(int_n7, int_2);
		assertTrue(val.equals(int_n3));
		
		val = MultiBDD.div(int_7, int_n2);
		assertTrue(val.equals(int_n3));
		
		val = MultiBDD.div(int_n7, int_n2);
		assertTrue(val.equals(int_3));
		
		// division by zero (implementation dependent)
		val = MultiBDD.div(int_1, int_0);
		assertTrue(val.equals(int_n1));
		
		val = MultiBDD.div(int_0, int_0);
		assertTrue(val.equals(int_n1));
	}
	
	@Test
	public void mod() throws Exception {
		MultiBDD val = MultiBDD.mod(int_0, int_1);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_0, int_7);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_0, int_max);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_1, int_1);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_7, int_1);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_max, int_1);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_1, int_2);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.mod(int_7, int_2);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.mod(int_6, int_2);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(int_max, int_2);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.mod(int_max, int_max);
		assertTrue(val.equals(int_0));
		
		val = MultiBDD.mod(var_A, int_3);
		assertTrue(MultiBDD.equal(val, int_0) == BDD.or(BDD.or(MultiBDD.equal(var_A, int_0), MultiBDD.equal(var_A, int_3)), MultiBDD.equal(var_A, int_6)));
		
		// tests with negative arguments
		val = MultiBDD.mod(int_n7, int_2);
		assertTrue(val.equals(int_n1));
		
		val = MultiBDD.mod(int_7, int_n2);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.mod(int_n7, int_n2);
		assertTrue(val.equals(int_n1));
		
		// division by zero (implementation dependent)
		val = MultiBDD.mod(int_1, int_0);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.mod(int_0, int_0);
		assertTrue(val.equals(int_0));
	}
	
	@Test
	public void math_mod() throws Exception {
		MultiBDD val = MultiBDD.math_mod(int_7, int_3);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.math_mod(int_n7, int_3);
		assertTrue(val.equals(int_2));
		
		val = MultiBDD.math_mod(int_7, int_n3);
		assertTrue(val.equals(int_1));
		
		val = MultiBDD.math_mod(int_n7, int_n3);
		assertTrue(val.equals(int_2));
	}
	
	@Test
	public void math_div() throws Exception {
		MultiBDD val = MultiBDD.math_div(int_7, int_3);
		assertTrue(val.equals(int_2));
		
		val = MultiBDD.math_div(int_n7, int_3);
		assertTrue(val.equals(int_n3));
		
		val = MultiBDD.math_div(int_7, int_n3);
		assertTrue(val.equals(int_n2));
		
		val = MultiBDD.math_div(int_n7, int_n3);
		assertTrue(val.equals(int_3));
	}

	@Test
	public void negate64Bit() throws Exception {
		PairOfMultiBDDs val = MultiBDD.negate64Bit(int_n1, int_n1);
		assertTrue(val.getLow().equals(int_1));
		assertTrue(val.getHigh().equals(int_0));
		
		val = MultiBDD.negate64Bit(int_1, int_0);
		assertTrue(val.getLow().equals(int_n1));
		assertTrue(val.getHigh().equals(int_n1));
		
		val = MultiBDD.negate64Bit(int_0, int_0);
		assertTrue(val.getLow().equals(int_0));
		assertTrue(val.getHigh().equals(int_0));
	}
	
	@Test
	public void abs64Bit() throws Exception {
		PairOfMultiBDDs val = MultiBDD.abs64Bit(int_n1, int_n1);
		assertTrue(val.getLow().equals(int_1));
		assertTrue(val.getHigh().equals(int_0));
		
		val = MultiBDD.abs64Bit(int_1, int_0);
		assertTrue(val.getLow().equals(int_1));
		assertTrue(val.getHigh().equals(int_0));
		
	}
	
	
	@Test
	public void equal() throws Exception {
		assertTrue(MultiBDD.equal(int_10, int_10) == BDD.trueBDD());
		assertTrue(MultiBDD.equal(int_10, int_n10) == BDD.falseBDD());
		assertTrue(MultiBDD.equal(var_A, int_0) == BDD.and(BDD.and(n1, n2), n3));
		assertTrue(MultiBDD.equal(var_B, int_0) == BDD.and(BDD.and(n1, n2), v3));
		assertTrue(MultiBDD.equal(MultiBDD.add(var_B, int_4), var_A) == BDD.trueBDD());
	}
	
	@Test
	public void equals() throws Exception {
		assertTrue(int_17.equals(int_17copy));
		assertFalse(int_0.equals(int_1));
		assertFalse(int_0.equals(var_A));
		assertTrue(var_A.equals(var_A));
		assertFalse(var_A.equals(var_B));
	}
}
