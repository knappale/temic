package bdd;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import bdd.BDD;
import bdd.Valuation;
import bdd.VariableRange;
import bdd.VariableRangeInterval;
import bdd.VariableRenaming;

import java.util.Vector;


public class BDDTest {
	public static final int NODETABLE_SIZE = 10000;
	public static final int CACHE_SIZE = 10000;
	public static final int WRAPPERTABLE_SIZE = 1117;
 	private static BDD p1,p2,p3,p4,p5,p6,p7,p8;
	@SuppressWarnings("unused")
	private static BDD n1,n2,n3,n4,n5,n6,n7,n8;
	private static BDD bdd1, bdd2, bdd3, bdd4, bdd5, bdd6, bdd7, bdd8;
	// private static Random rd = new Random();
	private static BDD trueBDD;
	private static BDD falseBDD;
	
	private static BDD and(BDD arg1, BDD arg2) {
	    return BDD.and(arg1,arg2);
	}
	
    private static BDD and(BDD arg1, BDD arg2, BDD arg3) {
    	return BDD.and(BDD.and(arg1,arg2),arg3);
    }
    
	private static BDD and(BDD arg1, BDD arg2, BDD arg3, BDD arg4) {
    	return BDD.and(BDD.and(BDD.and(arg1,arg2),arg3),arg4);
    }
    
	private static BDD or(BDD arg1, BDD arg2) {
	    return BDD.or(arg1,arg2);
	}
    
    private static BDD or(BDD arg1, BDD arg2, BDD arg3) {
    	return BDD.or(BDD.or(arg1,arg2),arg3);
    }
    
	private static BDD or(BDD arg1, BDD arg2, BDD arg3, BDD arg4) {
    	return BDD.or(BDD.or(BDD.or(arg1,arg2),arg3),arg4);
    }
    
    private static BDD not(BDD arg1) {
    	return BDD.not(arg1);
    }
    
	private static BDD implies(BDD arg1, BDD arg2) {
	    return BDD.implies(arg1,arg2);
	}
	
	private static BDD biimplies(BDD arg1, BDD arg2) {
	    return BDD.biimplies(arg1,arg2);
	}
    
	private static BDD nand(BDD arg1, BDD arg2) {
	    return BDD.nand(arg1,arg2);
	}
    
	private static BDD nor(BDD arg1, BDD arg2) {
	    return BDD.nor(arg1,arg2);
	}
	
	private static BDD exor(BDD arg1, BDD arg2) {
	    return BDD.exor(arg1,arg2);
	}
	
	private static BDD ite(BDD arg1, BDD arg2, BDD arg3) {
		return BDD.ite(arg1,arg2,arg3);
	}
	
	private static BDD varBDD(int v) {
		return BDD.varBDD(v);
	}
	
	private static BDD exists(BDD f, VariableRange rng) {
		return BDD.exists(f,rng);
	}
	
	private static BDD forall(BDD f, VariableRange rng) {
		return BDD.forall(f,rng);
	}
    
	@BeforeClass
	public static void setUp() throws Exception {
        bdd.BDD.reconfigure(NODETABLE_SIZE, CACHE_SIZE, WRAPPERTABLE_SIZE);
        
        trueBDD = BDD.trueBDD();
        falseBDD = BDD.falseBDD();
		
        p1 = varBDD(1); n1 = not(p1);
        p2 = varBDD(2); n2 = not(p2);
        p3 = varBDD(3); n3 = not(p3);
        p4 = varBDD(4); n4 = not(p4);
        p5 = varBDD(5); n5 = not(p5);
        p6 = varBDD(6); n6 = not(p6);
        p7 = varBDD(7); n7 = not(p7);
        p8 = varBDD(8); n8 = not(p8);
        
		// bdd1 :<-> (3 /\ true ) \/ (~3 /\ false) <-> 3  
		bdd1 = ite(p3, trueBDD, falseBDD);
		
		// bdd2 :<-> (3 /\ false) \/ (~3 /\ true ) <-> ~3
        bdd2 = ite(p3, falseBDD, trueBDD);
        
        // bdd3 :<-> (2 /\ bdd2 ) \/ (~2 /\ bdd1 ) <-> (2 /\ ~3) \/ (~2 /\ 3)
        bdd3 = ite(p2, bdd2, bdd1);
        
        // bdd4 :<-> (2 /\ false) \/ (~2 /\ bdd1 ) <-> (~2 /\ 3)
        bdd4 = ite(p2, falseBDD, bdd1);
        
        // bdd5 :<-> [1 /\ ((2 /\ ~3) \/ (~2 /\ 3))] \/ [~1 /\ (~2 /\ 3)]
        //       <-> [1 /\   2 /\ ~3] \/ [~2 /\ 3]
        bdd5 = ite(p1, bdd3, bdd4);
        
        // bdd6 :<-> 1 /\ (2 \/ ~3)
        bdd6 = and(p1, or(p2, n3));
        
        // bdd7 :<-> (1 /\ 2) \/ (1 /\ 3) \/ (2 /\ 3)
        bdd7 = or(and(p1,p2),and(p1,p3),and(p2,p3));
        
        // bdd8 :<-> ~[~(~(2 /\ 3) => 1) \/ (~2 /\ ~3)] <-> ((2 /\ 3) \/ 1) /\ (2 \/ 3) <-> bdd7 
        bdd8 = nor(not(implies(nand(p2,p3),p1)),and(n2,n3));
        
        // bdd20
        // bdd20 = and(n1, n2);
	}	

	@AfterClass
	public static void tearDown() throws Exception {
	/*  System.out.println("wrapperTable.size() == " + BDD.wrapperTableSize());
		System.out.println("calling garbage collector and running finalization...");
		System.gc();
		System.runFinalization();
		System.out.println("wrapperTable.size() == " + BDD.wrapperTableSize()); */
		// assertTrue(BDD.wrapperTableSize() == 21);
	}

	@Test
	public void trueBDD() {
		System.out.println("testing trueBDD()");
    	BDD t = BDD.trueBDD();
        assertTrue(t != null);
        assertTrue(t == BDD.trueBDD());
        assertTrue(t.equals(BDD.trueBDD()));     
        assertTrue(BDD.trueBDD() == BDD.trueBDD());
        assertTrue(BDD.trueBDD().isTrue());
        assertFalse(BDD.trueBDD().isFalse());
        assertTrue(BDD.trueBDD().getVariable() == Integer.MAX_VALUE);
	}
	
	@Test
	public void falseBDD() {
        System.out.println("testing falseBDD()");
        
        assertTrue(BDD.falseBDD() == BDD.falseBDD());
        assertFalse(BDD.falseBDD().isTrue());
        assertTrue(BDD.falseBDD().isFalse());
        assertTrue(BDD.falseBDD().getVariable() == Integer.MAX_VALUE);
    }
	
	@Test
    public void varBDD() {
        System.out.println("testing varBDD()");
        assertTrue(varBDD(17) == varBDD(17));
        assertFalse(varBDD(17) == varBDD(16));
        assertFalse(varBDD(17).isTrue());
        assertFalse(varBDD(17).isFalse());
        assertTrue(varBDD(17).getVariable() == 17);
        assertTrue(varBDD(17).getThenSucc() == trueBDD);
        assertTrue(varBDD(17).getElseSucc() == falseBDD);
    }
	
	@Test
    public void ite() {
        System.out.println("testing ite()");
        
        assertFalse(bdd1 == bdd2 || bdd1 == bdd3 || bdd1 == bdd4 || bdd1 == bdd5 || bdd1 == bdd6 || bdd1 == bdd7);
        assertFalse(bdd2 == bdd3 || bdd2 == bdd4 || bdd2 == bdd5 || bdd2 == bdd6 || bdd2 == bdd7);
        assertFalse(bdd3 == bdd4 || bdd3 == bdd5 || bdd3 == bdd6 || bdd3 == bdd7);
        assertFalse(bdd4 == bdd5 || bdd4 == bdd6 || bdd4 == bdd7 || bdd5 == bdd6 || bdd5 == bdd7 || bdd6 == bdd7);
     
        assertTrue(bdd1 == p3);
        assertTrue(bdd2 == not(bdd1));
        assertTrue(bdd3 == or(and(p2,n3),bdd4));
        
        assertTrue(BDD.ite(trueBDD,bdd5,bdd1) == bdd5);
        assertTrue(BDD.ite(falseBDD,bdd5,bdd1) == bdd1);
        assertTrue(BDD.ite(bdd5,trueBDD,falseBDD) == bdd5);
        assertTrue(BDD.ite(bdd5, or(p2,p3), falseBDD) == bdd5);
        
        assertTrue(bdd7 == bdd8);
        assertTrue(and(bdd7,n3) == and(p1,p2,n3));       
    }

	@Test
	public void not() {
		System.out.println("testing not()");
	    assertTrue(not(falseBDD) == trueBDD);
	    assertTrue(not(trueBDD) == falseBDD);
	    assertFalse(not(bdd5) == bdd5);
	    assertTrue(not(not(bdd5)) == bdd5);
	    assertTrue(not(bdd3) == or(and(p2,p3),and(n2,n3)));
	}

	@Test
	public void and() {
		System.out.println("testing and()");   
	    assertTrue(and(bdd4, falseBDD) == falseBDD);
	    assertTrue(and(falseBDD, bdd4) == falseBDD);
	    assertTrue(and(bdd4, trueBDD) == bdd4);
	    assertTrue(and(trueBDD, bdd4) == bdd4);
	    assertFalse(and(bdd4, bdd8) == bdd4);
	    assertTrue(and(bdd4, bdd8) == and(p1,bdd4));
	    assertFalse(and(bdd4, bdd8) == bdd8);
	    assertFalse(and(bdd4, bdd8) == falseBDD);
	    assertTrue(and(bdd4, bdd8) == and(bdd8, bdd4));
	    assertTrue(and(and(p1,p2),p3) == and(p1,and(p2,p3)));
	}

	@Test
	public void or() {
		System.out.println("testing or()");
	    assertTrue(or(bdd4, falseBDD) == bdd4);
	    assertTrue(or(falseBDD, bdd4) == bdd4);
	    assertTrue(or(bdd4, trueBDD) == trueBDD);
	    assertTrue(or(trueBDD, bdd4) == trueBDD);
	    assertFalse(or(bdd4, bdd8) == bdd4);
	    assertFalse(or(bdd4, bdd8) == bdd8);
	    assertFalse(or(bdd4, bdd8) == trueBDD);
	    assertTrue(or(bdd4, bdd8) == or(bdd8, bdd4));
	    assertTrue(or(or(p1,p2),p3) == or(p1,or(p2,p3)));
	    assertTrue(not(or(bdd4, bdd8)) == and(not(bdd4),not(bdd8)));
	}

	@Test
	public void implies() {
		System.out.println("testing implies()");        
	    assertTrue(implies(bdd4, bdd8) == or(not(bdd4), bdd8));
	}

	@Test
	public void biimplies() {
		System.out.println("testing biimplies()");
	    assertTrue(biimplies(bdd4, bdd8) == biimplies(bdd8, bdd4));
	    assertTrue(biimplies(bdd4, bdd8) == and(implies(bdd4, bdd8), implies(bdd8, bdd4)));
	}

	@Test
	public void exor() {
		System.out.println("testing exor()");
	    assertTrue(exor(bdd1, trueBDD) == not(bdd1));
	    assertTrue(exor(bdd5, bdd8) == and(or(bdd5, bdd8), not(and(bdd5, bdd8))));
	}

	@Test
	public void nand() {
		System.out.println("testing nand()");
	    assertTrue(nand(bdd4, bdd8) == not(and(bdd4, bdd8)));
	}

	@Test
	public void nor() {
		System.out.println("testing nor()");
	    assertTrue(nor(bdd4, bdd8) == not(or(bdd4, bdd8)));
	}	
	
	@Test
    public void equals() {
        System.out.println("testing equals()");
        assertFalse(bdd4.equals(bdd5));
        assertTrue(bdd4.equals(bdd4));
        assertFalse(bdd4.equals(null));
    }
	
	@Test
    public void size() {
        System.out.println("testing size()");
        assertTrue(bdd1.size() == 3);
        assertTrue(bdd2.size() == 3);
        assertTrue(bdd3.size() == 5);
        assertTrue(bdd4.size() == 4);
        assertTrue(bdd5.size() == 7);
    }
	
	@Test
    public void size_iterator_based() {
        System.out.println("testing size()");
        assertTrue(bdd1.size() == 3);
        assertTrue(bdd2.size() == 3);
        assertTrue(bdd3.size() == 5);
        assertTrue(bdd4.size() == 4);
        assertTrue(bdd5.size() == 7);
    }
	
	@Test
    public void draw() {
        System.out.println("testing draw()");
        
        /* try {
            bdd5.draw(new FileWriter("output/bddtest-bdd05.dot"));
            bdd6.draw(new FileWriter("output/bddtest-bdd06.dot"));
            bdd7.draw(new FileWriter("output/bddtest-bdd07.dot"));
            bdd8.draw(new FileWriter("output/bddtest-bdd08.dot"));
        }
        catch(IOException e) {
            fail("IOException occured: " + e);
        } */
    }
	
	@Test
	public void cube() {
		System.out.print("testing cube()");
		System.out.println();
		boolean[] varset1 = new boolean[12];
		varset1[1] = true;
		varset1[2] = true;
		varset1[8] = true;
		BDD cube = BDD.cube(varset1);	
		assertTrue(cube == and(p1,p2,p8));
		boolean[] varset2 = new boolean[1];
		cube = BDD.cube(varset2);
		assertTrue(cube == trueBDD);
		varset2[0] = true;
		cube = BDD.cube(varset2);
		assertTrue(cube == varBDD(0));
	}
	
	@Test
    public void exists() {
        System.out.println("testing exists()");
        
        VariableRange from5to7 = new VariableRangeInterval(5,7);
        assertTrue(exists(trueBDD, from5to7) == trueBDD);
        assertTrue(exists(falseBDD, from5to7) == falseBDD);
        
        assertTrue(exists(p4, from5to7) == p4);
        assertTrue(exists(p8, from5to7) == p8);
        assertTrue(exists(p5, from5to7) == trueBDD);
        assertTrue(exists(and(p4,p5), from5to7) == p4);
        assertTrue(exists(and(p4,n5), from5to7) == p4);
        
        assertTrue(exists(bdd5, from5to7) == bdd5);
        assertTrue(exists(or(bdd5, p7), from5to7) == trueBDD);
        assertTrue(exists(and(bdd5, p7), from5to7) == bdd5);
        
        BDD n1p5p6p8 = and(n1,p5,p6,p8);
        BDD p1n5n6p8 = and(p1,n5,n6,p8);
        BDD p1n5p6n8 = and(p1,n5,p6,n8);
        BDD bdd11 = or(n1p5p6p8, p1n5n6p8, p1n5p6n8);
        BDD bdd12 = exists(bdd11, from5to7);
        BDD bdd13 = or(p1,p8);
        
        assertTrue(bdd12 == bdd13);
        
        VariableRange from1to1 = new VariableRangeInterval(1,1);
        VariableRange from2to2 = new VariableRangeInterval(2,2);
        VariableRange from3to3 = new VariableRangeInterval(3,3);
        
        BDD t1 = or(and(p1,n2,n3),and(n1,p2,n3),and(p1,n2,p3),and(p1,p2,p3));
        BDD t2 = exists(t1,from1to1);
        assertTrue(t2 == BDD.trueBDD());
        BDD t3 = exists(t1,from2to2);
        assertTrue(t3 == not(and(n1,p3)));
        BDD t4 = exists(t1,from3to3);
        assertTrue(t4 == not(and(n1,n2)));

        // testing quantification of big bdds
        /* int threads = 10;
        int length = 10;
        BDDTriple triple = BDD.monsterBDDs(threads,length);
        assertTrue(exists(triple.getBig(),triple.getEvenRange()) == triple.getOdd());
        assertTrue(exists(triple.getBig(),triple.getOddRange()) == triple.getEven());
        
        // performance test
        for (int i=0; i<1000; i++) {
        	VariableRangeSetImp range = VariableRangeSetImp.create();
        	for (int j=0; j<10; j++) {
        		range.add(1+rd.nextInt(length));
        	}
        	exists(triple.getBig(),range);
        } */
    }
    
    @Test
    public void forall() {
        System.out.println("testing forall()");
        
        VariableRange from5to7 = new VariableRangeInterval(5,7);
        
        BDD n1p5p6p8 = and(n1,p5,p6,p8);
        BDD p1n5n6p8 = and(p1,n5,n6,p8);
        BDD p1n5p6n8 = and(p1,n5,p6,n8);
        BDD bdd11 = or(n1p5p6p8, p1n5n6p8, p1n5p6n8);
        BDD bdd12 = exists(bdd11, from5to7);
        BDD bdd13 = not(forall(not(bdd11), from5to7));
        
        assertTrue(bdd12 == bdd13);
    }
    
    @Test
    public void renamedCopy() {
        System.out.println("testing renamedCopy");

        VariableRenaming subst = new VariableRenaming();
        subst.put(1, 4);
        subst.put(3, 6);
        assertTrue(subst.isStrictlyIncreasing());
        assertTrue(subst.apply(1) == 4);
        assertTrue(subst.apply(3) == 6);
        assertTrue(subst.apply(2) == 2);
        /* try {
        	subst.apply(2);
        	fail("IllegalArgumentException supposed to be thrown by apply()");
        }
        catch(IllegalArgumentException e) {}; */
        
        BDD bdd15 = BDD.renamedCopy(bdd5, subst);
        
        assertTrue(subst.apply(1) == 4);
        assertTrue(subst.apply(3) == 6);
        assertTrue(subst.apply(2) == 2); // subst is locked now
        assertTrue(subst.apply(4) == 4);

        BDD bdd16 = or(and(p4,or(and(p2,n6),and(n2,p6))), and(n4,n2,p6));
        assertTrue(bdd15 == bdd16);
        
        /* try {
            bdd15.draw(new FileWriter("output/bddtest-bdd15.dot"));
        }
        catch(IOException e) {
            fail("IOException occured: " + e);
        } */
        
        /* bdd20.printDot("output/bddtest-bdd20.dot");
        subst = VariableRenamingImp.create();
        subst.put(1, 3);
        subst.put(2, 4);
        BDD bdd21 = BDD.renamedCopy(bdd20, subst);
        bdd21.printDot("output/bddtest-bdd21.dot"); */
    }
    
    @Test
    public void satisfyingValuation() {
        System.out.println("testing satisfyingValuation()");

        assertNull(falseBDD.satisfyingValuation());
        assertTrue(trueBDD.satisfyingValuation().isTrivial());
        assertTrue(trueBDD.satisfyingValuation().asBDD() == trueBDD);
        assertFalse(p1.satisfyingValuation().isTrivial());
        assertTrue(p1.satisfyingValuation().asBDD() == p1);

        BDD n1p5p6p8 = and(n1,p5,p6,p8);
        BDD p1n5n6p8 = and(p1,n5,n6,p8);
        BDD p1n5p6n8 = and(p1,n5,p6,n8);
        BDD bdd11 = BDD.or(BDD.or(n1p5p6p8, p1n5n6p8), p1n5p6n8);
        Valuation val11 = bdd11.satisfyingValuation();
        assertFalse(val11.isTrivial());
        assertTrue(val11.asBDD() == n1p5p6p8 ||
                          val11.asBDD() == p1n5n6p8 ||
                          val11.asBDD() == p1n5p6n8);

        Vector<String> symbolicNames = new Vector<String>();
        symbolicNames.add("a"); //  p1
        symbolicNames.add("b"); //  p2
        symbolicNames.add("c"); //  p3
        symbolicNames.add("d"); //  p4
        symbolicNames.add("e"); //  p5
        symbolicNames.add("f"); //  p6
        symbolicNames.add("g"); //  p7
        symbolicNames.add("h"); //  p8
        symbolicNames.add("i"); // (p9)

        /* try {
            val11.print(new FileWriter("output/bddtest-val11.txt"));
            val11.print(new FileWriter("output/bddtest-val11-symbolic.txt"), symbolicNames);
            bdd11.draw(new FileWriter("output/bddtest-bdd11.dot"));
            bdd11.draw(new FileWriter("output/bddtest-bdd11-symbolic.dot"), symbolicNames);
        }
        catch(IOException e) {
            fail("IOException occured: " + e);
        } */

        BDD bdd12 = BDD.or(BDD.or(BDD.varBDD(3),BDD.varBDD(1)),BDD.varBDD(2));
        Valuation val12 = bdd12.satisfyingValuation();
        assertTrue(val12.asBDD() == BDD.varBDD(1));
    }

    @Test
    public void getVariable() {
    	System.out.println("testing getVariable()");
    	assertTrue(p1.getVariable() == 1);
    	assertTrue(n1.getVariable() == 1);
    	assertTrue(p2.getVariable() == 2);
    	assertTrue(n2.getVariable() == 2);
    	assertTrue(p8.getVariable() == 8);
    	assertTrue(n8.getVariable() == 8);
    }
 }
