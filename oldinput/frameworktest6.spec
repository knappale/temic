/* specification frameworktest6: long vertical positive chain */

specification frameworktest6;

var g : -1..1000 init g = 0;
var b : bool init ~b;

agent w = { };

guard cond_infty = forall x : 1..1000 . K[w] (~(b & g = x));
guard cond_down = forall x : 0..1000 . ((~b & g = x) -> ~(K[w] ~(~b & g = x)));
guard cond_left = forall x : 0..1000 . ((~b & g = x) ->  (K[w] ~(~b & g = x)));

action act_down
epre   cond_down
pre    ~b & 0 <= g & g < 1000
do     g := g + 1;

action act_left
epre   cond_left
pre    ~b & 0 <= g & g < 1000
do     g := g + 1, b := true;

action act_infty
epre   cond_infty
pre    ~b & g = 0
do     g := -1;

end;

interpret;

assertNR error;

assertR ~b & g = -1;

assertV forall x : 1..1000 . ~(EF (b & g = x));

assertV forall x : 0..1000 .   EF (~b & g = x);

assertR ~b & g = 1000;
