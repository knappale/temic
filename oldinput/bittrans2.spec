/* Bit-Transmission Protocol (Version 1, Example 7.1.1) */

specification bit_transmission_1;

var sbit : 0..1;
var ack : bool initial ack <-> false;
var rbit : 0..2 initial rbit = 2; // 2 for not set

agent sender   = { sbit, ack };
agent receiver = { rbit };

guard receiver_knows_bit = exists bit:0..1 . (K[receiver] (sbit = bit));
guard sender_knows_that_receiver_knows_bit = K[sender] (exists bit:0..1 . (K[receiver] (sbit = bit)));
guard receiver_knows_that_sender_knows_that_receiver_knows_bit = K[receiver] (K[sender] (exists bit:0..1 . (K[receiver] (sbit = bit))));

action sender_sends_bit_ok
epre ~sender_knows_that_receiver_knows_bit
do rbit := sbit;

action sender_sends_bit_failed
epre ~sender_knows_that_receiver_knows_bit
do;

action receiver_sends_ack_ok
epre receiver_knows_bit and ~receiver_knows_that_sender_knows_that_receiver_knows_bit
do ack := true;

action receiver_sends_ack_failed
epre receiver_knows_bit and ~receiver_knows_that_sender_knows_that_receiver_knows_bit
do;

end;

interpret;

check initial EF                        (exists bit:0..1 . (K[receiver] (sbit = bit))) ;
check initial AF                        (exists bit:0..1 . (K[receiver] (sbit = bit))) ;
check initial EG                        (exists bit:0..1 . (K[receiver] (sbit = bit))) ;
check initial EF              K[sender] (exists bit:0..1 . (K[receiver] (sbit = bit))) ;
check initial EF K[receiver] (K[sender] (exists bit:0..1 . (K[receiver] (sbit = bit))));
