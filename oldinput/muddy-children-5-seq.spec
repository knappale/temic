/* Specification of the muddy children problem (for 5 children) */

specification muddy_children_5;

// mudi means that child i has a muddy forehead

var mud1, mud2, mud3, mud4, mud5 : boolean;
let numMuddy : 0..5 = ([0..1] mud1) + ([0..1] mud2) + ([0..1] mud3) + ([0..1] mud4) + ([0..1] mud5);

// public counter indicating the current round

var round : 0..6;

// control state of the children:
// numTest = i means that children {1..i} have already testified in the current round

var numTest : 0..5;

// the public board that records the testimonies of the children and the father:
// f0 means that the father has announced in the initial round: "At least one of
// you has mud on your forehead."
// ~f0 means that the father has refrained from announcing "At least one of
// you has mud on your forehead." In the given epistemic framework, this is
// equivalent to the public announcement "None of you has mud on your forehead." 
// ci_j means that child i testified in the j-th round: "Yes, I know whether I have
// mud on my forehead or not."
// ~ci_j means that child i testified in the j-th round: "I don't know." 

var f0 : boolean;
var c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5 : boolean;

let child1saidYesInRound : 0..6 = [0..6]
		([0..1]  c1_1 &  c1_2 &  c1_3 &  c1_4 &  c1_5) +
	2 * ([0..1] ~c1_1 &  c1_2 &  c1_3 &  c1_4 &  c1_5) +
	3 * ([0..1] ~c1_1 & ~c1_2 &  c1_3 &  c1_4 &  c1_5) +
	4 * ([0..1] ~c1_1 & ~c1_2 & ~c1_3 &  c1_4 &  c1_5) +
	5 * ([0..1] ~c1_1 & ~c1_2 & ~c1_3 & ~c1_4 &  c1_5) +
	6 * ([0..1] ~c1_1 & ~c1_2 & ~c1_3 & ~c1_4 & ~c1_5);
                                  
let child2saidYesInRound : 0..6 = [0..6]
		([0..1]  c2_1 &  c2_2 &  c2_3 &  c2_4 &  c2_5) +
	2 * ([0..1] ~c2_1 &  c2_2 &  c2_3 &  c2_4 &  c2_5) +
	3 * ([0..1] ~c2_1 & ~c2_2 &  c2_3 &  c2_4 &  c2_5) +
	4 * ([0..1] ~c2_1 & ~c2_2 & ~c2_3 &  c2_4 &  c2_5) +
	5 * ([0..1] ~c2_1 & ~c2_2 & ~c2_3 & ~c2_4 &  c2_5) +
	6 * ([0..1] ~c2_1 & ~c2_2 & ~c2_3 & ~c2_4 & ~c2_5);

let child3saidYesInRound : 0..6 = [0..6]
		([0..1]  c3_1 &  c3_2 &  c3_3 &  c3_4 &  c3_5) +
	2 * ([0..1] ~c3_1 &  c3_2 &  c3_3 &  c3_4 &  c3_5) +
	3 * ([0..1] ~c3_1 & ~c3_2 &  c3_3 &  c3_4 &  c3_5) +
	4 * ([0..1] ~c3_1 & ~c3_2 & ~c3_3 &  c3_4 &  c3_5) +
	5 * ([0..1] ~c3_1 & ~c3_2 & ~c3_3 & ~c3_4 &  c3_5) +
	6 * ([0..1] ~c3_1 & ~c3_2 & ~c3_3 & ~c3_4 & ~c3_5);

let child4saidYesInRound : 0..6 = [0..6]
		([0..1]  c4_1 &  c4_2 &  c4_3 &  c4_4 &  c4_5) +
	2 * ([0..1] ~c4_1 &  c4_2 &  c4_3 &  c4_4 &  c4_5) +
	3 * ([0..1] ~c4_1 & ~c4_2 &  c4_3 &  c4_4 &  c4_5) +
	4 * ([0..1] ~c4_1 & ~c4_2 & ~c4_3 &  c4_4 &  c4_5) +
	5 * ([0..1] ~c4_1 & ~c4_2 & ~c4_3 & ~c4_4 &  c4_5) +
	6 * ([0..1] ~c4_1 & ~c4_2 & ~c4_3 & ~c4_4 & ~c4_5);
	
let child5saidYesInRound : 0..6 = [0..6]
		([0..1]  c5_1 &  c5_2 &  c5_3 &  c5_4 &  c5_5) +
	2 * ([0..1] ~c5_1 &  c5_2 &  c5_3 &  c5_4 &  c5_5) +
	3 * ([0..1] ~c5_1 & ~c5_2 &  c5_3 &  c5_4 &  c5_5) +
	4 * ([0..1] ~c5_1 & ~c5_2 & ~c5_3 &  c5_4 &  c5_5) +
	5 * ([0..1] ~c5_1 & ~c5_2 & ~c5_3 & ~c5_4 &  c5_5) +
	6 * ([0..1] ~c5_1 & ~c5_2 & ~c5_3 & ~c5_4 & ~c5_5);

// the secret board that caches the testimonies of the children in each round
// before they are published simultaneously on the public board.

var s1, s2, s3, s4, s5 : boolean;

let boardempty = ~f0 & ~s1 & ~s2 & ~s3 & ~s4 & ~s5 &
   ~c1_1 & ~c1_2 & ~c1_3 & ~c1_4 & ~c1_5 &
   ~c2_1 & ~c2_2 & ~c2_3 & ~c2_4 & ~c2_5 &
   ~c3_1 & ~c3_2 & ~c3_3 & ~c3_4 & ~c3_5 &
   ~c4_1 & ~c4_2 & ~c4_3 & ~c4_4 & ~c4_5 &
   ~c5_1 & ~c5_2 & ~c5_3 & ~c5_4 & ~c5_5;

// definition of the epistemic accessibility relations of father and children

agent father  = {mud1, mud2, mud3, mud4, mud5, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5                     };
agent child1  = {      mud2, mud3, mud4, mud5, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5, s1                 };
agent child2  = {mud1,       mud3, mud4, mud5, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5,     s2             };
agent child3  = {mud1, mud2,       mud4, mud5, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5,         s3         };
agent child4  = {mud1, mud2, mud3,       mud5, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5,             s4     };
agent child5  = {mud1, mud2, mud3, mud4,       round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c2_1, c2_2, c2_3, c2_4, c2_5, c3_1, c3_2, c3_3, c3_4, c3_5, c4_1, c4_2, c4_3, c4_4, c4_5, c5_1, c5_2, c5_3, c5_4, c5_5,                 s5 };

guard kchild1  = (K[child1 ] mud1 ) | (K[child1 ] (~mud1 ));
guard kchild2  = (K[child2 ] mud2 ) | (K[child2 ] (~mud2 ));
guard kchild3  = (K[child3 ] mud3 ) | (K[child3 ] (~mud3 ));
guard kchild4  = (K[child4 ] mud4 ) | (K[child4 ] (~mud4 ));
guard kchild5  = (K[child5 ] mud5 ) | (K[child5 ] (~mud5 ));

initial (round = 0) & (numTest = 0) & boardempty;

action father_yes
pre    (round = 0) & (1 <= numMuddy)
do 	   f0 := true,
       round := round + 1,
       numTest := 0; 

action father_no
pre    (round = 0) & (0 = numMuddy)
do     f0 := false,
       round := round + 1,
       numTest := 0; 

action child1_yes
epre   kchild1
pre    (1 <= round & round <= 5) & (numTest = 0)
do     s1 := true, numTest := 1;

action child1_no
epre   ~kchild1
pre    (1 <= round & round <= 5) & (numTest = 0)
do     s1 := false, numTest := 1;

action child2_yes
epre   kchild2
pre    (1 <= round & round <= 5) & (numTest = 1)
do     s2 := true, numTest := 2;

action child2_no
epre   ~kchild2
pre    (1 <= round & round <= 5) & (numTest = 1)
do     s2 := false, numTest := 2;

action child3_yes
epre   kchild3
pre    (1 <= round & round <= 5) & (numTest = 2)
do     s3 := true, numTest := 3;

action child3_no
epre   ~kchild3
pre    (1 <= round & round <= 5) & (numTest = 2)
do     s3 := false, numTest := 3;

action child4_yes
epre   kchild4
pre    (1 <= round & round <= 5) & (numTest = 3)
do     s4 := true, numTest := 4;

action child4_no
epre   ~kchild4
pre    (1 <= round & round <= 5) & (numTest = 3)
do     s4 := false, numTest := 4;

action child5_yes
epre   kchild5
pre    (1 <= round & round <= 5) & (numTest = 4)
do     s5 := true, numTest := 5;

action child5_no
epre   ~kchild5
pre    (1 <= round & round <= 5) & (numTest = 4)
do     s5 := false, numTest := 5;

action round1_publication
pre    (round = 1) & (numTest = 5)
do     c1_1 := s1, c2_1 := s2, c3_1 := s3, c4_1 := s4, c5_1 := s5,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false,
       round := round + 1, numTest := 0;
       
action round2_publication
pre    (round = 2) & (numTest = 5) 
do     c1_2 := s1, c2_2 := s2, c3_2 := s3, c4_2 := s4, c5_2 := s5,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false,
       round := round + 1, numTest := 0;

action round3_publication
pre    (round = 3) & (numTest = 5)
do     c1_3 := s1, c2_3 := s2, c3_3 := s3, c4_3 := s4, c5_3 := s5,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false,
       round := round + 1, numTest := 0;

action round4_publication
pre    (round = 4) & (numTest = 5)
do     c1_4 := s1, c2_4 := s2, c3_4 := s3, c4_4 := s4, c5_4 := s5,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false,
       round := round + 1, numTest := 0; 
       
action round5_publication
pre    (round = 5) & (numTest = 5) 
do     c1_5 := s1, c2_5 := s2, c3_5 := s3, c4_5 := s4, c5_5 := s5,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false,
       round := round + 1, numTest := 0; 
     
// action stutter
// pre    (round = 6)
// do;

end;

interpret;

assertR   (round = 6 &  mud1 & ~mud2 & mud3 & mud4 & ~mud5);

assertV AG (round = 6 &  mud1 -> child1saidYesInRound = numMuddy);
assertV AG (round = 6 & ~mud1 -> child1saidYesInRound = numMuddy+1);

assertV AG (round = 6 &  mud2 -> child2saidYesInRound = numMuddy);
assertV AG (round = 6 & ~mud2 -> child2saidYesInRound = numMuddy+1);

assertV AG (round = 6 &  mud3 -> child3saidYesInRound = numMuddy);
assertV AG (round = 6 & ~mud3 -> child3saidYesInRound = numMuddy+1);

assertV AG (round = 6 &  mud4 -> child4saidYesInRound = numMuddy);
assertV AG (round = 6 & ~mud4 -> child4saidYesInRound = numMuddy+1);

assertV AG (round = 6 &  mud5 -> child5saidYesInRound = numMuddy);
assertV AG (round = 6 & ~mud5 -> child5saidYesInRound = numMuddy+1);
