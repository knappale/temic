/* specification frameworktest4: horizontal negative chain */

specification frameworktest4;

var g : 0..10 init g = 0;

let g0 : bool = (g = 0);
let g1 : bool = (g = 1);
let g2 : bool = (g = 2);
let g3 : bool = (g = 3);
let g4 : bool = (g = 4);
let g5 : bool = (g = 5);
let g6 : bool = (g = 6);
let g7 : bool = (g = 7);
let g8 : bool = (g = 8);
let g9 : bool = (g = 9);
let g10 : bool = (g = 10);

agent w = { };

guard cond2 = K[w] (~g1);
guard cond3 = K[w] (~g2);
guard cond4 = K[w] (~g3);
guard cond5 = K[w] (~g4);
guard cond6 = K[w] (~g5);
guard cond7 = K[w] (~g6);
guard cond8 = K[w] (~g7);
guard cond9 = K[w] (~g8);
guard cond10 = K[w] (~g9);

action act1
pre    g0
do     g := 1;

action act2
epre   cond2
pre    g0
do     g := 2;

action act3
epre   cond3
pre    g0
do     g := 3;

action act4
epre   cond4
pre    g0
do     g := 4;

action act5
epre   cond5
pre    g0
do     g := 5;

action act6
epre   cond6
pre    g0
do     g := 6;

action act7
epre   cond7
pre    g0
do     g := 7;

action act8
epre   cond8
pre    g0
do     g := 8;

action act9
epre   cond9
pre    g0
do     g := 9;

action act10
epre   cond10
pre    g0
do     g := 10;

end;

interpret;

assertV (EF g1) & ~(EF g2) & (EF g3) & ~(EG g4) & (EF g5) & ~(EF g6) & (EF g7) & ~(EF g8) & (EF g9) & ~(EF g10);

