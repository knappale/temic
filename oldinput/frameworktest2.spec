/* specification frameworktest2: bakery mutex algorithm */

specification frameworktest2;

// state of the scheduler
var running : 0..2 init running = 0;

let running1 : bool = (running = 1);
let running2 : bool = (running = 2);

// ticket numbers for process 1 and process 2
var ticket1 : 0..3 init ticket1 = 0;
var ticket2 : 0..3 init ticket2 = 0;

// states of process 1 and process 2
var state1 : 0..2 init state1 = idle;
var state2 : 0..2 init state2 = idle;

let idle      : 0..2 = 0;
let trying    : 0..2 = 1;
let critical  : 0..2 = 2;

let idle1     : bool = (state1 = idle);
let trying1   : bool = (state1 = trying);
let critical1 : bool = (state1 = critical);

let idle2     : bool = (state2 = idle);
let trying2   : bool = (state2 = trying);
let critical2 : bool = (state2 = critical);

// agents

agent process1 = { state1, ticket1, running };
agent process2 = { state2, ticket2, running };
agent ignoramus = { };
agent God = { state1, state2, ticket1, ticket2, running };

// ----------------------------- scheduling ------------------------------

action select1
pre    running = 0
do     running := 1;

action select2
pre    running = 0
do     running := 2;

// ------------------------------ processes ------------------------------

action	trying1
pre	    running1 & idle1 & (ticket2 < 3)
do      running := 0,
        state1 := trying,
        ticket1 := 1 + ticket2;
        
action	trying2
pre	    running2 & idle2 & (ticket1 < 3)
do      running := 0,
        state2 := trying,
        ticket2 := 1 + ticket1;
        
action  critical1
pre     running1 & trying1 & (ticket2 = 0 | ticket1 < ticket2)
do      running := 0,
        state1 := critical;
        
action  critical2
pre     running2 & trying2 & (ticket1 = 0 | ticket2 < ticket1)
do      running := 0,
        state2 := critical;
        
action  idle1
pre     running1 & critical1
do      running := 0,
        state1 := idle,
        ticket1 := 0;
        
action  idle2
pre     running2 & critical2
do      running := 0,
        state2 := idle,
        ticket2 := 0;

action  unselect1
pre     running1 & ~((idle1 & (ticket2 < 3)) | (trying1 & (ticket2 = 0 | ticket1 < ticket2)) | critical1)
do      running := 0;

action  unselect2
pre     running2 & ~((idle2 & (ticket1 < 3)) | (trying2 & (ticket1 = 0 | ticket2 < ticket1)) | critical2)
do      running := 0;

end;

println("-- Due to the fact that the scheduler has been modeled explicitly");
println("-- the system diameter should be divided by 2.");
println();

// safety requirements

println("check no 1 is supposed to be: valid");
assertV AG ~(critical1 & critical2);

// liveness requirements

println();
println("check no 2 is supposed to be: NOT valid (without strong fairness for select1 and select2)");
assertNV AG (trying1 -> AF critical1);

println();
println("check no 3 is supposed to be: NOT valid (without strong fairness for select1 and select2)");
assertNV AG (trying2 -> AF critical2);

// four sanity checks

println();
println("check no 4 is supposed to be: valid");
assertV (EF critical1) and (EF critical2);

println();
println("check no 5 is supposed to be: NOT valid");
assertNV (AF critical1) or (AF critical2);

println();
println("check no 6 is supposed to be: valid");
assertV (~critical1 EU critical1) and (~critical2 EU critical2);

println();
println("check no 7 is supposed to be: NOT valid");
assertNV (~trying1 EU critical1) or (~trying2 EU critical2);

// reachability checks

println();
println("check no 8 is supposed to be: not reachable");
assertNR critical1 & critical2;

println();
println("check no 9 is supposed to be: reachable");
assertR critical1 & trying2;

println();
println("check no 10 is supposed to be: not reachable");
assertNR (idle1 & ticket1 > 0) | (idle2 & ticket2 > 0);

println();
println("check no 11 is supposed to be: reachable");
assertR ticket1 = 3;

println();
println("check no 12 is supposed to be: reachable");
assertR ticket2 = 3;

println();
println("check no 13 is supposed to be: not reachable");
assertNR ticket1 = 3 & ticket2 = 3;

// epistemic sanity checks

println();
println("check no 14 is supposed to be: valid");
assertV K[ignoramus] (~(critical1 & critical2));

println();
println("check no 15 is supposed to be: valid");
assertV AG (0 < ticket1 & ticket1 < ticket2 -> ~critical2);

println();
println("check no 16 is supposed to be: valid");
assertV AG (0 < ticket2 & ticket2 < ticket1 -> ~critical1);

println();
println("check no 17 is supposed to be: not valid");
assertNV AG (0 < ticket1 & ticket1 < ticket2 -> K[process1] (~critical2));

println();
println("check no 18 is supposed to be: not valid");
assertNV AG (0 < ticket2 & ticket2 < ticket1 -> K[process2] (~critical1));

println();
println("check no 19 is supposed to be: valid");
assertV AG (0 < ticket1 & ticket1 < ticket2 -> K[God] (~critical2));

println();
println("check no 20 is supposed to be: valid");
assertV AG (0 < ticket2 & ticket2 < ticket1 -> K[God] (~critical1));

// further sanity checks concerning examples and counter-examples

// checks for true and false

println();
println("check no 101 is supposed to be: valid");
assertV true;

println();
println("check no 102 is supposed to be: NOT valid");
assertNV false;

// checks for negation

println();
println("check no 103 is supposed to be: NOT valid");
assertNV ~true;

println();
println("check no 104 is supposed to be: valid");
assertV ~false;

// checks for the binary propositional AND-connective (important, not trivial):

println();
println("check no 105 is supposed to be: valid");
assertV (EF critical1) & (EF critical1);

println();
println("check no 106 is supposed to be: valid");
assertV (EF critical1) & idle1;

println();
println("check no 107 is supposed to be: NOT valid");
assertNV (EF critical1) & critical1;

println();
println("check no 108 is supposed to be: valid");
assertV idle1 & (EF critical1);

println();
println("check no 109 is supposed to be: NOT valid");
assertNV critical1 & (EF critical1);

println();
println("check no 110 is supposed to be: NOT valid");
assertNV idle1 & ~(EF critical1);

println();
println("check no 111 is supposed to be: NOT valid");
assertNV ~(EF critical1) & ~(EF critical2);

// checks for the binary propositional OR-connective

println();
println("check no 112 is supposed to be: NOT valid");
assertNV ~(EF critical1) | ~(EF critical1);

println();
println("check no 113 is supposed to be: NOT valid");
assertNV ~(EF critical1) | critical1;

println();
println("check no 114 is supposed to be: valid");
assertV ~(EF critical1) | idle1;

println();
println("check no 115 is supposed to be: NOT valid");
assertNV critical1 | ~(EF critical1);

println();
println("check no 116 is supposed to be: valid");
assertV idle1 | ~(EF critical1);

println();
println("check no 117 is supposed to be: valid");
assertV critical1 | (EF critical1);

println();
println("check no 118 is supposed to be: valid");
assertV (EF critical2) | (EF critical1);

// checks for the binary propositional connectives nand, nor, implies, exor und biimplies:

println();
println("check no 119 is supposed to be: NOT valid");
assertNV (EF critical2) nand (EF critical2);

println();
println("check no 120 is supposed to be: NOT valid");
assertNV (EF critical2) nand idle2;

println();
println("check no 121 is supposed to be: valid");
assertV (EF critical2) nand critical2;

println();
println("check no 122 is supposed to be: valid, but without example");
assertV ~(EF critical1) nor ~(EF critical1);

println();
println("check no 123 is supposed to be: valid");
assertV ~(EF critical1) nor critical1;

println();
println("check no 124 is supposed to be: NOT valid");
assertNV ~(EF critical1) nor idle1;

println();
println("check no 125 is supposed to be: NOT valid, but without counter-example");
assertNV (EF critical1) -> ~(EF critical1);

println();
println("check no 126 is supposed to be: NOT valid");
assertNV (EF critical1) -> critical1;

println();
println("check no 127 is supposed to be: NOT valid");
assertNV idle1 -> ~(EF critical1);

println();
println("check no 128 is supposed to be: valid");
assertV ~(EF critical1) -> ~(EF critical1);

println();
println("check no 129 is supposed to be: valid");
assertV ~(EF critical1) -> (EF critical1);

println();
println("check no 130 is supposed to be: valid");
assertV (EF critical1) -> (EF critical1);

println();
println("check no 131 is supposed to be: valid, but without example");
assertV ~(EF critical1) exor (EF critical1);

println();
println("check no 132 is supposed to be: valid, but without example");
assertV (EF critical1) exor ~(EF critical1);

println();
println("check no 133 is supposed to be: NOT valid, but without counter-example");
assertNV (EF critical1) exor (EF critical1);

println();
println("check no 134 is supposed to be: NOT valid, but without counter-example");
assertNV ~(EF critical1) exor ~(EF critical1);

println();
println("check no 135 is supposed to be: valid");
assertV critical1 exor (EF critical1);

println();
println("check no 136 is supposed to be: valid");
assertV ~(EF critical1) exor idle1;

println();
println("check no 137 is supposed to be: NOT valid");
assertNV idle1 exor (EF critical1);

println();
println("check no 138 is supposed to be: NOT valid");
assertNV ~(EF critical1) exor critical1;

println();
println("check no 139 is supposed to be: valid, but without example");
assertV (EF critical1) <-> (EF critical1);

println();
println("check no 140 is supposed to be: valid, but without example");
assertV ~(EF critical1) <-> ~(EF critical1);

println();
println("check no 141 is supposed to be: NOT valid, but without counter-example");
assertNV (EF critical1) <-> ~(EF critical1);

println();
println("check no 142 is supposed to be: NOT valid, but without counter-example");
assertNV ~(EF critical1) <-> (EF critical1);

println();
println("check no 143 is supposed to be: valid");
assertV idle1 <-> (EF critical1);

println();
println("check no 144 is supposed to be: valid");
assertV ~(EF critical1) <-> critical1;

println();
println("check no 145 is supposed to be: NOT valid");
assertNV critical1 <-> (EF critical1);

println();
println("check no 146 is supposed to be: NOT valid");
assertNV ~(EF critical1) <-> idle1;

// checks for the unary temporal EX-connective:

println();
println("check no 147 is supposed to be: NOT valid, but without counter-example");
assertNV EX false;

println();
println("check no 148 is supposed to be: valid");
assertV EX true;

println();
println("check no 149 is supposed to be: valid");
assertV EX EX true;

println();
println("check no 150 is supposed to be: valid");
assertV EX EX trying1;

println();
println("check no 151 is supposed to be: valid");
assertV EX EX EX EX trying1;

println();
println("check no 152 is supposed to be: NOT valid, but without counter-example");
assertNV EX EX critical1;

println();
println("check no 153 is supposed to be: valid");
assertV EX EX EX EX critical1;

println();
println("check no 154 is supposed to be: NOT valid, but without counter-example");
assertNV EX EX EX EX EX (critical1 & trying2);

println();
println("check no 155 is supposed to be: valid");
assertV EX EX EX EX EX EX (critical1 & trying2);

// checks for the binary temporal EU-connective:

println();
println("check no 156 is supposed to be: NOT valid, but without counter-example");
assertNV true EU false;

println();
println("check no 157 is supposed to be: valid");
assertV true EU true;

println();
println("check no 158 is supposed to be: valid");
assertV false EU true;

println();
println("check no 159 is supposed to be: NOT valid, but without counter-example");
assertNV false EU running1;

println();
println("check no 160 is supposed to be: valid");
assertV ~running1 EU critical2;

println();
println("check no 161 is supposed to be: valid");
assertV ~running1 EU (EX EX (idle1 & idle2));

println();
println("check no 162 is supposed to be: NOT valid, but without counter-example");
assertNV ~running1 EU critical1;

// checks for the unary temporal EG-connective:

println();
println("check no 163 is supposed to be: NOT valid, but without counter-example");
assertNV EG (running = 0);

println();
println("check no 164 is supposed to be: valid (without strong fairness for select1)");
assertV EG ~running1;

println();
println("check no 165 is supposed to be: valid (without strong fairness for select2)");
assertV EG ~running2;

