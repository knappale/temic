/* Bit-Transmission Protocol (Version 1, Example 7.1.1) */

specification bit_transmission_1;

var s_bit : 0..1;
var s_ack : bool initial s_ack <-> false;
// var s_ack_valid : bool initial s_ack_valid <-> false;
var r_bit : 0..1 initial r_bit = 0;
var r_bit_valid : bool initial r_bit_valid <-> false;

// agent sender   = { s_bit, s_ack, s_ack_valid };
// agent receiver = { r_bit, r_bit_valid };
agent sender   = { s_bit, s_ack };
agent receiver = { r_bit, r_bit_valid };

guard receiver_knows_bit = exists bit:0..1 . (K[receiver] (s_bit = bit));
guard sender_knows_that_receiver_knows_bit = K[sender] (exists bit:0..1 . (K[receiver] (s_bit = bit)));
guard receiver_knows_that_sender_knows_that_receiver_knows_bit = K[receiver] (K[sender] (exists bit:0..1 . (K[receiver] (s_bit = bit))));

action sender_sends_bit_ok
epre ~sender_knows_that_receiver_knows_bit
do r_bit := s_bit, r_bit_valid := true;

action sender_sends_bit_failed
epre ~sender_knows_that_receiver_knows_bit
do;

action receiver_sends_ack_ok
epre receiver_knows_bit and ~receiver_knows_that_sender_knows_that_receiver_knows_bit
// do s_ack := true, s_ack_valid := true;
do s_ack := true;

action receiver_sends_ack_failed
epre receiver_knows_bit and ~receiver_knows_that_sender_knows_that_receiver_knows_bit
do;

end;

interpret;

check initial EF                        (exists bit:0..1 . (K[receiver] (s_bit = bit))) ;
check initial AF                        (exists bit:0..1 . (K[receiver] (s_bit = bit))) ;
check initial EG                        (exists bit:0..1 . (K[receiver] (s_bit = bit))) ;
check initial EF              K[sender] (exists bit:0..1 . (K[receiver] (s_bit = bit))) ;
check initial EF K[receiver] (K[sender] (exists bit:0..1 . (K[receiver] (s_bit = bit))));
