/* The Needham-Schroeder Protocol, version 1 */

// identities of principals
#define UNDEF       (0)
#define ALICE		(1)
#define BOB			(2)
#define INTRUDER	(3)

// messages types
#define NOMSG		(0)
#define MSG1		(1)
#define MSG2		(2)
#define MSG3		(3)

// nonces
#define UNKNOWN		(-1)

// atomic regions
#define NONE        (0)
#define I_SENDING   (1)
#define ALICE_MSG1  (4)
#define BOB_MSG2    (5)
#define ALICE_MSG3  (6)
#define BOB_FINAL   (7)

#define MAX_TIME    (30)

specification needham_schroeder_2;

var atomic : 0..7 init atomic = NONE;
var time : 0..12 init time = 0;

var chan_mtype :  0..3 init chan_mtype = NOMSG;
var chan_recip :  0..3 init chan_recip = UNDEF;
var chan_pos1  : -1..7 init chan_pos1  = UNKNOWN;
var chan_pos2  : -1..7 init chan_pos2  = UNKNOWN;
var chan_pos3  :  0..3 init chan_pos3  = UNDEF;
var sent_by_intruder : bool init sent_by_intruder = false; 

var alice_nonce         :  0..1;
var alice_step          :  1..3 init alice_step = 1;
var alice_partner       :  0..3 init alice_partner = UNDEF;
var alice_msg2_pos1     : -1..7 init alice_msg2_pos1 = UNKNOWN;
var alice_msg2_pos2     : -1..7 init alice_msg2_pos2 = UNKNOWN;  let alice_partner_nonce : -1..7 = alice_msg2_pos2;
var alice_msg2_pos3     :  0..3 init alice_msg2_pos3 = UNDEF;

var bob_nonce           :  2..3;
var bob_step            :  1..3 init bob_step = 1;
var bob_msg1_pos1       :  0..3 init bob_msg1_pos1 = UNDEF;      let bob_partner : 0..3 = bob_msg1_pos1;
var bob_msg1_pos2       : -1..7 init bob_msg1_pos2 = UNKNOWN;    let bob_partner_nonce : -1..7 = bob_msg1_pos2;
var bob_msg2_extended   :  bool init bob_msg2_extended = false;
var bob_msg3_pos1       : -1..7 init bob_msg3_pos1 = UNKNOWN;

var intruder_nonce_for_alice :  4..5;
var intruder_nonce_for_bob   :  6..7;
var intruder_nonce1          : -1..7 init intruder_nonce1 = UNKNOWN; // decrypted and learnt nonce no 1
var intruder_nonce2          : -1..7 init intruder_nonce2 = UNKNOWN; // decrypted and learnt nonce no 2
var intruder_msg_mtype       :  0..3 init intruder_msg_mtype = NOMSG;
var intruder_msg_pos1        : -1..7 init intruder_msg_pos1 = UNKNOWN;
var intruder_msg_pos2        : -1..7 init intruder_msg_pos2 = UNKNOWN;
var intruder_msg_pos3        :  0..3 init intruder_msg_pos3 = UNDEF;
var intruder_intercepted_msg_mtype :  0..3 init intruder_intercepted_msg_mtype = NOMSG;
var intruder_intercepted_msg_recip :  0..3 init intruder_intercepted_msg_recip = UNDEF;
var intruder_intercepted_msg_pos1  : -1..7; init intruder_intercepted_msg_pos1  = UNKNOWN;
var intruder_intercepted_msg_pos2  : -1..7; init intruder_intercepted_msg_pos2  = UNKNOWN;
var intruder_intercepted_msg_pos3  :  0..3; init intruder_intercepted_msg_pos3  = UNDEF;

agent alice = { alice_nonce, alice_step, alice_partner, alice_msg2_pos1, alice_msg2_pos2, alice_msg2_pos3, chan_mtype, chan_recip };
agent bob = { bob_nonce, bob_step, bob_msg1_pos1, bob_msg1_pos2, bob_msg3_pos1, chan_mtype, chan_recip };
agent intruder = { intruder_nonce_for_alice, intruder_nonce_for_bob, intruder_nonce1, intruder_nonce2, intruder_msg_mtype, intruder_msg_pos1, intruder_msg_pos2, intruder_msg_pos3, intruder_intercepted_msg_mtype, intruder_intercepted_msg_recip, chan_mtype, chan_recip, sent_by_intruder };

/******************************************************************************************/

/* alice knows that her partner knows that her partner's nonce is shared with alice */
guard alice_K_alice_partner_K_alice_partner_nonce_shared_with_alice =
  (alice_partner = BOB -> (K[alice] (K[bob] (~(exists n:2..3 . (K[intruder] (bob_nonce = n))))))) &
  (alice_partner = INTRUDER -> (K[alice] (K[intruder] (~(exists n:4..5 . (K[bob] (intruder_nonce_for_alice = n)))))));

/* bob knows that his partner knows that his partner's nonce is shared with bob */
guard bob_K_bob_partner_K_bob_partner_nonce_shared_with_bob =
  (bob_partner = ALICE -> (K[bob] (K[alice] (~(exists n:0..1 . (K[intruder] (alice_nonce = n))))))) &
  (bob_partner = INTRUDER -> (K[bob] (K[intruder] (~(exists n:6..7 . (K[alice] (intruder_nonce_for_bob = n)))))));
  
/******************************************************************************************/

action alice_chooses_bob
pre atomic = NONE & alice_step = 1 & chan_mtype = NOMSG & time < MAX_TIME
do atomic := ALICE_MSG1, alice_partner := BOB;

action alice_chooses_intruder
pre atomic = NONE & alice_step = 1 & chan_mtype = NOMSG & time < MAX_TIME
do atomic := ALICE_MSG1, alice_partner := INTRUDER;

action alice_encrypts_and_sends_msg1
pre atomic = ALICE_MSG1
do chan_mtype := MSG1,
   chan_recip := alice_partner,
   chan_pos1 := ALICE,
   chan_pos2 := alice_nonce,
   chan_pos3 := UNDEF,
   sent_by_intruder := false,
   alice_step := 2,
   atomic := NONE,
   time := time+1;

action bob_receives_and_decrypts_msg1
pre atomic = NONE & bob_step = 1 & chan_mtype = MSG1 & chan_recip = BOB & time < MAX_TIME
do atomic := BOB_MSG2,
   bob_msg1_pos1 := chan_pos1,
   bob_msg1_pos2 := chan_pos2,
   chan_mtype := NOMSG;
   
action bob_encrypts_and_sends_msg2
pre atomic = BOB_MSG2
do chan_mtype := MSG2, bob_msg2_extended := false,
   chan_recip := bob_partner,
   chan_pos1 := bob_partner_nonce,
   chan_pos2 := bob_nonce,
   chan_pos3 := UNDEF,
   sent_by_intruder := false,
   bob_step := 2,
   atomic := NONE,
   time := time+1;

action bob_encrypts_and_sends_msg2_extended
pre atomic = BOB_MSG2
do chan_mtype := MSG2, bob_msg2_extended := true,
   chan_recip := bob_partner,
   chan_pos1 := bob_partner_nonce,
   chan_pos2 := bob_nonce,
   chan_pos3 := BOB,
   sent_by_intruder := false,
   bob_step := 2,
   atomic := NONE,
   time := time+1;
   
action alice_receives_and_decrypts_msg2
pre atomic = NONE & alice_step = 2 & chan_mtype = MSG2 & chan_recip = ALICE & time < MAX_TIME
do atomic := ALICE_MSG3,
   alice_msg2_pos1 := chan_pos1,
   alice_msg2_pos2 := chan_pos2,
   alice_msg2_pos3 := chan_pos3,
   chan_mtype := NOMSG;
   
action alice_encrypts_and_sends_msg3
epre alice_K_alice_partner_K_alice_partner_nonce_shared_with_alice
pre atomic = ALICE_MSG3
do chan_mtype := MSG3,
   chan_recip := alice_partner,
   chan_pos1 := alice_partner_nonce,
   chan_pos2 := UNKNOWN,
   chan_pos3 := UNDEF,
   sent_by_intruder := false,
   alice_step := 3,
   atomic := NONE,
   time := time+1;
   
action bob_receives_and_decrypts_msg3
pre atomic = NONE & bob_step = 2 & chan_mtype = MSG3 & chan_recip = BOB & time < MAX_TIME
do atomic := BOB_FINAL,
   bob_msg3_pos1 := chan_pos1,
   chan_mtype := NOMSG;

action bob_accepts_msg3
epre bob_K_bob_partner_K_bob_partner_nonce_shared_with_bob
pre atomic = BOB_FINAL
do bob_step := 3,
   atomic := NONE,
   time := time+1;

/******************************************************************************************/

/* Intruder may receive and decrypt messages that are addressed to him
   (and therefore encrypted with his public key). If the message is of
   type MSG1 or MSG2, Intruder may learn from the decrypted payload the
   nonce of the sender of the message. */

action intruder_receives_and_decrypts_msg
pre atomic = NONE & chan_mtype != NOMSG & chan_recip = INTRUDER & time < MAX_TIME
do intruder_msg_mtype := chan_mtype,
   intruder_msg_pos1 := chan_pos1,
   intruder_msg_pos2 := chan_pos2,
   intruder_msg_pos3 := chan_pos3,
   intruder_nonce1 := ([0..1]  (chan_mtype = MSG1)) * chan_pos2 +
                      ([0..1] ~(chan_mtype = MSG1)) * intruder_nonce1,
   intruder_nonce2 := ([0..1]  (chan_mtype = MSG2)) * chan_pos2 +
                      ([0..1] ~(chan_mtype = MSG2)) * intruder_nonce2,
   chan_mtype := NOMSG, time := time+1;

/* Intruder may intercept messages that are not addressed to him,
   if there is no other message that is already stored in the buffer
   and if the message wasn't sent by Intruder. */

action intruder_intercepts_msg
pre atomic = NONE & chan_mtype != NOMSG & chan_recip != INTRUDER & intruder_intercepted_msg_mtype = NOMSG & sent_by_intruder = false & time < MAX_TIME
do intruder_intercepted_msg_mtype := chan_mtype,
   intruder_intercepted_msg_recip := chan_recip,
   intruder_intercepted_msg_pos1 := chan_pos1,
   intruder_intercepted_msg_pos2 := chan_pos2,
   intruder_intercepted_msg_pos3 := chan_pos3,
   chan_mtype := NOMSG, time := time+1;

/* Intruder may send arbitrary messages, and that means... */
   
/* Intruder may choose any valid message type and either Alice or Bob
   as recipient of the message. In the case of a message of type MSG1 or MSG2,
   Intruder may furthermore choose whether he wants to lie about his own identity
   or to reveal his identity. In any case, Intruder truthfully sets the flag
   sent_by_intruder, but this flag can be seen only by Intruder and is read
   by nobody else than Intruder. It has the only purpose to prevent Intruder
   from intercepting his own messages (in particular if he lied about his
   sender identity). */

action intruder_chooses_msg1_and_alice_and_truth
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG1, chan_recip := ALICE, chan_pos1 := INTRUDER, sent_by_intruder := true, atomic := I_SENDING;

action intruder_chooses_msg2_and_alice_and_truth
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG2, chan_recip := ALICE, chan_pos3 := INTRUDER, sent_by_intruder := true, atomic := I_SENDING;
   
action intruder_chooses_msg3_and_alice
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG3, chan_recip := ALICE, sent_by_intruder := true, atomic := I_SENDING;

action intruder_chooses_msg1_and_bob_and_truth
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG1, chan_recip := BOB, chan_pos1 := INTRUDER, sent_by_intruder := true, atomic := I_SENDING;

action intruder_chooses_msg2_and_bob_and_truth
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG2, chan_recip := BOB, chan_pos3 := INTRUDER, sent_by_intruder := true, atomic := I_SENDING;
   
action intruder_chooses_msg3_and_bob
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG3, chan_recip := BOB, sent_by_intruder := true, atomic := I_SENDING;

action intruder_chooses_msg1_and_alice_and_deceit
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG1, chan_recip := ALICE, chan_pos1 := BOB, sent_by_intruder := true, atomic := I_SENDING;

action intruder_chooses_msg2_and_alice_and_deceit
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG2, chan_recip := ALICE, chan_pos3 := BOB, sent_by_intruder := true, atomic := I_SENDING;
   
action intruder_chooses_msg1_and_bob_and_deceit
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG1, chan_recip := BOB, chan_pos1 := ALICE, sent_by_intruder := true, atomic := I_SENDING;

action intruder_chooses_msg2_and_bob_and_deceit
pre atomic = NONE & chan_mtype = NOMSG & time < MAX_TIME
do chan_mtype := MSG2, chan_recip := BOB, chan_pos3 := ALICE, sent_by_intruder := true, atomic := I_SENDING;

/* Intruder may choose the payload of the intercepted message
   and send the prepared message. If Intruder chooses this alternative,
   he overwrites the information concerning his identity that is stored
   in chan_pos1 or chan_pos3 (see above). */

action intruder_chooses_intercepted_payload_and_sends_the_message
pre atomic = I_SENDING & intruder_intercepted_msg_mtype != NOMSG
do chan_pos1 := intruder_intercepted_msg_pos1,
   chan_pos2 := intruder_intercepted_msg_pos2,
   chan_pos3 := intruder_intercepted_msg_pos3,
   intruder_intercepted_msg_mtype := NOMSG,
   atomic := NONE, time := time+1;
   
/* Intruder may compose the remaining parts of the payload of the message.
   In the case of MSG1 or MSG2, Intruder must not guess the nonce of the person
   that he pretends to be (if he lies about his identity). In the case of MSG2 or MSG3,
   Intruder must not guess the nonce of the recipient of the message. */
         
/* alternatives for MSG1 */         

action intruder_chooses_nonce1_and_encrypts_and_sends_msg1
pre atomic = I_SENDING & chan_mtype = MSG1
do chan_pos2 := intruder_nonce1,
   intruder_nonce_for_alice := ([0..1] (chan_recip = ALICE & chan_pos1 = INTRUDER)) * intruder_nonce1 + ([0..1] (chan_recip != ALICE | chan_pos1 != INTRUDER)) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] (chan_recip = BOB   & chan_pos1 = INTRUDER)) * intruder_nonce1 + ([0..1] (chan_recip != BOB   | chan_pos1 != INTRUDER)) * intruder_nonce_for_bob,
   chan_pos3 := UNDEF,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce2_and_encrypts_and_sends_msg1
pre atomic = I_SENDING & chan_mtype = MSG1
do chan_pos2 := intruder_nonce2,
   intruder_nonce_for_alice := ([0..1] (chan_recip = ALICE & chan_pos1 = INTRUDER)) * intruder_nonce2 + ([0..1] (chan_recip != ALICE | chan_pos1 != INTRUDER)) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] (chan_recip = BOB   & chan_pos1 = INTRUDER)) * intruder_nonce2 + ([0..1] (chan_recip != BOB   | chan_pos1 != INTRUDER)) * intruder_nonce_for_bob,
   chan_pos3 := UNDEF,
   atomic := NONE, time := time+1;
   
action intruder_chooses_4or6_and_encrypts_and_sends_msg1
pre atomic = I_SENDING & chan_mtype = MSG1 & chan_pos1 = INTRUDER
do chan_pos2 := ([0..1] chan_recip = ALICE) * 4 + ([0..1] chan_recip = BOB) * 6,
   intruder_nonce_for_alice := ([0..1] chan_recip = ALICE) * 4 + ([0..1] chan_recip != ALICE) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] chan_recip = BOB)   * 6 + ([0..1] chan_recip != BOB)   * intruder_nonce_for_bob,
   chan_pos3 := UNDEF,
   atomic := NONE, time := time+1;

action intruder_chooses_5or7_and_encrypts_and_sends_msg1
pre atomic = I_SENDING & chan_mtype = MSG1 & chan_pos1 = INTRUDER
do chan_pos2 := ([0..1] chan_recip = ALICE) * 5 + ([0..1] chan_recip = BOB) * 7,
   intruder_nonce_for_alice := ([0..1] chan_recip = ALICE) * 5 + ([0..1] chan_recip != ALICE) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] chan_recip = BOB)   * 7 + ([0..1] chan_recip != BOB)   * intruder_nonce_for_bob,
   chan_pos3 := UNDEF,
   atomic := NONE, time := time+1;

/* alternatives for MSG2 */

action intruder_chooses_nonce1_4or6_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2 & chan_pos3 = INTRUDER
do chan_pos1 := intruder_nonce1,
   chan_pos2 := ([0..1] chan_recip = ALICE) * 4 + ([0..1] chan_recip = BOB) * 6,
   intruder_nonce_for_alice := ([0..1] chan_recip = ALICE) * 4 + ([0..1] chan_recip != ALICE) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] chan_recip = BOB)   * 6 + ([0..1] chan_recip != BOB)   * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce1_5or7_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2 & chan_pos3 = INTRUDER
do chan_pos1 := intruder_nonce1,
   chan_pos2 := ([0..1] chan_recip = ALICE) * 5 + ([0..1] chan_recip = BOB) * 7,
   intruder_nonce_for_alice := ([0..1] chan_recip = ALICE) * 5 + ([0..1] chan_recip != ALICE) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] chan_recip = BOB)   * 7 + ([0..1] chan_recip != BOB)   * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce1_nonce1_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2
do chan_pos1 := intruder_nonce1,
   chan_pos2 := intruder_nonce1,
   intruder_nonce_for_alice := ([0..1] (chan_recip = ALICE & chan_pos3 = INTRUDER)) * intruder_nonce1 + ([0..1] (chan_recip != ALICE | chan_pos3 != INTRUDER)) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] (chan_recip = BOB   & chan_pos3 = INTRUDER)) * intruder_nonce1 + ([0..1] (chan_recip != BOB   | chan_pos3 != INTRUDER)) * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce1_nonce2_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2
do chan_pos1 := intruder_nonce1,
   chan_pos2 := intruder_nonce2,
   intruder_nonce_for_alice := ([0..1] (chan_recip = ALICE & chan_pos3 = INTRUDER)) * intruder_nonce2 + ([0..1] (chan_recip != ALICE | chan_pos3 != INTRUDER)) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] (chan_recip = BOB   & chan_pos3 = INTRUDER)) * intruder_nonce2 + ([0..1] (chan_recip != BOB   | chan_pos3 != INTRUDER)) * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce2_4or6_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2 & chan_pos3 = INTRUDER
do chan_pos1 := intruder_nonce2,
   chan_pos2 := ([0..1] chan_recip = ALICE) * 4 + ([0..1] chan_recip = BOB) * 6,
   intruder_nonce_for_alice := ([0..1] chan_recip = ALICE) * 4 + ([0..1] chan_recip != ALICE) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] chan_recip = BOB)   * 6 + ([0..1] chan_recip != BOB)   * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce2_5or7_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2 & chan_pos3 = INTRUDER
do chan_pos1 := intruder_nonce2,
   chan_pos2 := ([0..1] chan_recip = ALICE) * 5 + ([0..1] chan_recip = BOB) * 7,
   intruder_nonce_for_alice := ([0..1] chan_recip = ALICE) * 5 + ([0..1] chan_recip != ALICE) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] chan_recip = BOB)   * 7 + ([0..1] chan_recip != BOB)   * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce2_nonce1_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2
do chan_pos1 := intruder_nonce2,
   chan_pos2 := intruder_nonce1,
   intruder_nonce_for_alice := ([0..1] (chan_recip = ALICE & chan_pos3 = INTRUDER)) * intruder_nonce1 + ([0..1] (chan_recip != ALICE | chan_pos3 != INTRUDER)) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] (chan_recip = BOB   & chan_pos3 = INTRUDER)) * intruder_nonce1 + ([0..1] (chan_recip != BOB   | chan_pos3 != INTRUDER)) * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;

action intruder_chooses_nonce2_nonce2_and_encrypts_and_sends_msg2
pre atomic = I_SENDING & chan_mtype = MSG2
do chan_pos1 := intruder_nonce2,
   chan_pos2 := intruder_nonce2,
   intruder_nonce_for_alice := ([0..1] (chan_recip = ALICE & chan_pos3 = INTRUDER)) * intruder_nonce2 + ([0..1] (chan_recip != ALICE | chan_pos3 != INTRUDER)) * intruder_nonce_for_alice,
   intruder_nonce_for_bob   := ([0..1] (chan_recip = BOB   & chan_pos3 = INTRUDER)) * intruder_nonce2 + ([0..1] (chan_recip != BOB   | chan_pos3 != INTRUDER)) * intruder_nonce_for_bob,
   atomic := NONE, time := time+1;
   
/* alternatives for MSG3 */

action intruder_chooses_nonce1_and_encrypts_and_sends_msg3
pre atomic = I_SENDING & chan_mtype = MSG3
do chan_pos1 := intruder_nonce1,
   chan_pos2 := UNKNOWN,
   chan_pos3 := UNDEF,
   atomic := NONE, time := time+1;
   
action intruder_chooses_nonce2_and_encrypts_and_sends_msg3
pre atomic = I_SENDING & chan_mtype = MSG3
do chan_pos1 := intruder_nonce2,
   chan_pos2 := UNKNOWN,
   chan_pos3 := UNDEF,
   atomic := NONE, time := time+1;

action time_tick
pre time < MAX_TIME
do time := time+1;


end;

interpret;

checkR atomic = NONE & alice_step = 3 & bob_step = 3;

checkR atomic = NONE & alice_step = 3 & bob_step = 3 & ~bob_msg2_extended;

checkR atomic = NONE & alice_step = 3 & bob_step = 3 & bob_msg2_extended;

checkR atomic = NONE & alice_step = 3 & bob_step = 1;