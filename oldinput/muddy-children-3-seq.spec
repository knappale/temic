/* Specification of the muddy children problem (for 3 children) */

specification muddy_children_3;

// mudi means that child i has a muddy forehead

var mud1, mud2, mud3;

// public counter indicating the current round
// and macros to advance the counter

var r0, r1, r2;

let round0 = ~r2 & ~r1 & ~r0;  // in round round0 the father makes his initial announcement
let round1 = ~r2 & ~r1 &  r0;  // in round round1 the children testify for the 1st time
let round2 = ~r2 &  r1 & ~r0;  // in round round2 the children testify for the 2nd time
let round3 = ~r2 &  r1 &  r0;  // in round round3 the children testify for the 3rd time
let round4 =  r2 & ~r1 & ~r0;  // round4 means "finished"

let inc0 = ~r0;
let inc1 = r1 exor r0;
let inc2 = r2 exor (r1 & r0);

// control state of the children:
// ti means that child i has already testified in the current round

var t1, t2, t3;

let notest  = ~t1 & ~t2 & ~t3;
let alltest =  t1 &  t2 &  t3;

// the public board that records the testimonies of the children and the father:
// f0 means that the father has announced in the initial round: "At least one of
// you has mud on your forehead."
// ~f0 means that the father has refrained from announcing "At least one of
// you has mud on your forehead." In the given epistemic framework, this is
// equivalent to the public announcement "None of you has mud on your forehead." 
// cij means that child i testified in the j-th round: "Yes, I know whether I have
// mud on my forehead or not."
// ~cij means that child i testified in the j-th round: "I don't know." 

var f0;
var c11, c12, c13;
var c21, c22, c23;
var c31, c32, c33;

// the secret board that caches the testimonies of the children in each round
// before they are published simultaneously on the public board.

var s1, s2, s3;

let boardempty = ~f0 & ~c11 & ~c12 & ~c13 & ~c21 & ~c22 & ~c23 & ~c31 & ~c32 & ~c33 & ~s1 & ~s2 & ~s3;

// definition of the epistemic accessibility relations of father and children

agent father = {mud1, mud2, mud3, r0, r1, r2, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33            };
agent child1 = {      mud2, mud3, r0, r1, r2, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33, s1        };
agent child2 = {mud1,       mud3, r0, r1, r2, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33,     s2    };
agent child3 = {mud1, mud2,       r0, r1, r2, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33,         s3};

guard kchild1 = (K[child1] mud1) | (K[child1] (~mud1));
guard kchild2 = (K[child2] mud2) | (K[child2] (~mud2));
guard kchild3 = (K[child3] mud3) | (K[child3] (~mud3));

initial round0 & notest & boardempty;

action father_yes
pre    round0 & (mud1 | mud2 | mud3)
do 	   f0 := true,
       r0 := inc0, r1 := inc1, r2 := inc2,
       t1 := false, t2 := false, t3 := false; 

action father_no
pre    round0 & ~(mud1 | mud2 | mud3)
do     f0 := false,
       r0 := inc0, r1 := inc1, r2 := inc2,
       t1 := false, t2 := false, t3 := false;

action child1_yes
epre   kchild1
pre    (round1 | round2 | round3) & ~t1
do     s1 := true, t1 := true;

action child1_no
epre   ~kchild1
pre    (round1 | round2 | round3) & ~t1
do     s1 := false, t1 := true;

action child2_yes
epre   kchild2
pre    (round1 | round2 | round3) & t1 & ~t2
do     s2 := true, t2 := true;

action child2_no
epre   ~kchild2
pre    (round1 | round2 | round3) & t1 & ~t2
do     s2 := false, t2 := true;

action child3_yes
epre   kchild3
pre    (round1 | round2 | round3) & t1 & t2 & ~t3 
do     s3 := true, t3 := true;

action child3_no
epre   ~kchild3
pre    (round1 | round2 | round3) & t1 & t2 & ~t3
do     s3 := false, t3 := true;

action round1_publication
pre    round1 & alltest
do     c11 := s1, c21 := s2, c31 := s3,
       s1 := false, s2 := false, s3 := false,
       r0 := inc0, r1 := inc1, r2 := inc2,
       t1 := false, t2 := false, t3 := false;
       
action round2_publication
pre    round2 & alltest 
do     c12 := s1, c22 := s2, c32 := s3,
       s1 := false, s2 := false, s3 := false,
       r0 := inc0, r1 := inc1, r2 := inc2,
       t1 := false, t2 := false, t3 := false;

action round3_publication
pre    round3 & alltest
do     c13 := s1, c23 := s2, c33 := s3,
       s1 := false, s2 := false, s3 := false,
       r0 := inc0, r1 := inc1, r2 := inc2,
       t1 := false, t2 := false, t3 := false;
       
action stutter
pre    round4
do;

end;

interpret;


