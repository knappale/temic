
specification copy_card;

var term_state : 0..2  initial term_state = 0; // idle
// 0 : idle
// 1 : exp_res_authenticate
// 2 : exp_res_pay
var term_value : 0..2;
var term_money : -10..10 initial term_money = 0;

var card_state : 0..1 initial card_state = 0; // idle
// 0 : idle
// 1 : exp_load
var card_balance : 0..10 initial card_balance = 0;
var card_nonce : 0..1;

var buf_message_type : 0..5 initial buf_message_type = 0;
// 0 : no valid massage in buffer
// 1 : authenticate
// 2 : res_authenticate
// 3 : load
// 4 : pay
// 5 : res_pay
var buf_is_hashed : boolean initial buf_is_hashed = false;
var buf_nonce : 0..1 initial buf_nonce = 0;
var buf_value : 0..2 initial buf_value = 0;

action term_authenticate
pre buf_message_type = 0
do  term_money := term_money + term_value,
    term_state := 1,
    buf_message_type := 1;
    
action term_failed
pre buf_message_type = 2 & term_state != 1
do  buf_message_type := 0;

action term_load
pre buf_message_type = 2 & term_state = 1
do  term_state := 0,
    buf_message_type := 3,
    buf_is_hashed := true,
    buf_value := term_value;
    
action card_failed
pre buf_message_type = 3 & (card_state != 1 | card_nonce != buf_nonce | card_balance + buf_value > 10)
do  card_state := 0,
    buf_message_type := 0;
    
action card_load
pre buf_message_type = 3 & ~(card_state != 1 | card_nonce != buf_nonce | card_balance + buf_value > 10)
do  card_state := 0,
    buf_message_type := 0,
    card_balance := card_balance + buf_value;

action card_authenticate_nonce_0
pre buf_message_type = 1
do  card_state := 1,
	card_nonce := 0,
    buf_message_type := 2,
    buf_is_hashed := false,
    buf_nonce := 0;

action card_authenticate_nonce_1
pre buf_message_type = 1
do  card_state := 1,
	card_nonce := 1,
    buf_message_type := 2,
    buf_is_hashed := false,
    buf_nonce := 1;

end;

checkT AG (card_balance <= term_money);