specification quantification_test_v1;

var a, b : 2..6;
var p : 4..81 initial p = a * b;
var step : 1..2;
var k : boolean;

initial step = 1 & ~k;

agent goofy = { step, k, p };

guard goofy_knows_a = (exists x:1..10 . (K[goofy] a = x));
guard goofy_knows_a_and_b = exists x:1..10 . exists y:1..10 . (K[goofy] ((a = x & b = y) | (a = y & b = x)));

guard goofy_1 = K[goofy] a = 1;
guard goofy_2 = K[goofy] a = 2;
guard goofy_3 = K[goofy] a = 3;
guard goofy_4 = K[goofy] a = 4;
guard goofy_5 = K[goofy] a = 5;
guard goofy_6 = K[goofy] a = 6;
guard goofy_7 = K[goofy] a = 7;
guard goofy_8 = K[goofy] a = 8;
guard goofy_9 = K[goofy] a = 9;
guard goofy_10 = K[goofy] a = 10;

action step1_yes
// epre   (goofy_1 | goofy_2 | goofy_3 | goofy_4 | goofy_5 | goofy_6 | goofy_7 | goofy_8 | goofy_9 | goofy_10)
epre   goofy_knows_a_and_b
pre    step = 1
do     k := true, step := 2;

action step1_no
// epre   ~(goofy_2 | goofy_3 | goofy_4 | goofy_5 | goofy_6 | goofy_7 | goofy_8 | goofy_9)
epre   ~goofy_knows_a_and_b
pre    step = 1
do     k := false, step := 2;

end;

interpret;

checkR (step = 2 &  k);
checkR (step = 2 & ~k &  ((a=2 & b=6) | (a=6 & b=2) | (a=3 & b=4) | (a=4 & b=3)));
checkR (step = 2 & ~k & ~((a=2 & b=6) | (a=6 & b=2) | (a=3 & b=4) | (a=4 & b=3)));
checkR (step = 2);

