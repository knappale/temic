/* specification frameworktest5:  vertical positive chain */

specification frameworktest5;

var g : -1..10 init g = 0;
var b : bool init ~b;

let g_infty : bool = ~b & (g = -1);
let g0 : bool = ~b & (g = 0);
let g1 : bool = ~b & (g = 1);
let g2 : bool = ~b & (g = 2);
let g3 : bool = ~b & (g = 3);
let g4 : bool = ~b & (g = 4);
let g5 : bool = ~b & (g = 5);
let g6 : bool = ~b & (g = 6);
let g7 : bool = ~b & (g = 7);
let g8 : bool = ~b & (g = 8);
let g9 : bool = ~b & (g = 9);
let g10 : bool = ~b & (g = 10);
let g1prime : bool = b & (g = 1);
let g2prime : bool = b & (g = 2);
let g3prime : bool = b & (g = 3);
let g4prime : bool = b & (g = 4);
let g5prime : bool = b & (g = 5);
let g6prime : bool = b & (g = 6);
let g7prime : bool = b & (g = 7);
let g8prime : bool = b & (g = 8);
let g9prime : bool = b & (g = 9);
let g10prime : bool = b & (g = 10);

agent w = { };

guard cond_infty = K[w] (~(g1prime | g2prime | g3prime | g4prime | g5prime | g6prime | g7prime | g8prime | g9prime | g10prime));
guard cond1 = K[w] (~g0);
guard cond2 = K[w] (~g1);
guard cond3 = K[w] (~g2);
guard cond4 = K[w] (~g3);
guard cond5 = K[w] (~g4);
guard cond6 = K[w] (~g5);
guard cond7 = K[w] (~g6);
guard cond8 = K[w] (~g7);
guard cond9 = K[w] (~g8);
guard cond10 = K[w] (~g9);

action act1
epre   ~cond1
pre    g0
do     g := 1;

action act2
epre   ~cond2
pre    g1
do     g := 2;

action act3
epre   ~cond3
pre    g2
do     g := 3;

action act4
epre   ~cond4
pre    g3
do     g := 4;

action act5
epre   ~cond5
pre    g4
do     g := 5;

action act6
epre   ~cond6
pre    g5
do     g := 6;

action act7
epre   ~cond7
pre    g6
do     g := 7;

action act8
epre   ~cond8
pre    g7
do     g := 8;

action act9
epre   ~cond9
pre    g8
do     g := 9;

action act10
epre   ~cond10
pre    g9
do     g := 10;

action act1prime
epre   cond1
pre    g0
do     g := 1, b := true;

action act2prime
epre   cond2
pre    g1
do     g := 2, b := true;

action act3prime
epre   cond3
pre    g2
do     g := 3, b := true;

action act4prime
epre   cond4
pre    g3
do     g := 4, b := true;

action act5prime
epre   cond5
pre    g4
do     g := 5, b := true;

action act6prime
epre   cond6
pre    g5
do     g := 6, b := true;

action act7prime
epre   cond7
pre    g6
do     g := 7, b := true;

action act8prime
epre   cond8
pre    g7
do     g := 8, b := true;

action act9prime
epre   cond9
pre    g8
do     g := 9, b := true;

action act10prime
epre   cond10
pre    g9
do     g := 10, b := true;

action act_infty
epre   cond_infty
pre    g0
do     g := -1;

end;

interpret;

assertV (EF g_infty) & (EF g0) & (EF g1) & (EF g2) & (EF g3) & (EF g4) & (EF g5) & (EF g6) & (EF g7) & (EF g8) & (EF g9) & (EF g10) & 
        ~(EF g1prime) & ~(EF g2prime) & ~(EF g3prime) & ~(EF g4prime) & ~(EF g5prime) & ~(EF g6prime) & ~(EF g7prime) & ~(EF g8prime) & ~(EF g9prime) & ~(EF g10prime);
