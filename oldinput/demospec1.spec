/* This is demospec1. */

specification demospec1;

var a : 0..99;
var b1 : boolean;

initial a = 0;
initial b1 = false;

action act1
pre true
do a := a+1;

action act2
pre a > 50
do b1 := true;

end

checkRT a = 50 and b1;
checkRT a = 51 and b1;
// checkRT error;


