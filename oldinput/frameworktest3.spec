/* specification frameworktest2: association and commutativity of addition,
   interleaved addition of numbers */
   
specification frameworktest3;

var sum : -17..1023;
var done01, done02, done03, done04, done05, done06, done07, done08, done09, done10: bool;
/* var done11, done12, done13, done14, done15, done16, done17, done18, done19, done20: bool;
var done21, done22, done23, done24, done25 : bool; */

initial sum = 0 &
        ~done01 & ~done02 & ~done03 & ~done04 & ~done05 &
        ~done06 & ~done07 & ~done08 & ~done09 & ~done10;
        /* & ~done11 & ~done12 & ~done13 & ~done14 & ~done15 &
        ~done16 & ~done17 & ~done18 & ~done19 & ~done20 &
        ~done21 & ~done22 & ~done23 & ~done24 & ~done25; */        

action add01
pre    ~done01
do     done01 := true, sum := sum + 1;

action add02
pre    ~done02
do     done02 := true, sum := sum + 2;

action add03
pre    ~done03
do     done03 := true, sum := sum + 3;

action add04
pre    ~done04
do     done04 := true, sum := sum + 4;

action add05
pre    ~done05
do     done05 := true, sum := sum + 5;

action add06
pre    ~done06
do     done06 := true, sum := sum + 6;

action add07
pre    ~done07
do     done07 := true, sum := sum + 7;

action add08
pre    ~done08
do     done08 := true, sum := sum + 8;

action add09
pre    ~done09
do     done09 := true, sum := sum + 9;

action add10
pre    ~done10
do     done10 := true, sum := sum + 10;

/* action add11
pre    ~done11
do     done11 := true, sum := sum + 11;

action add12
pre    ~done12
do     done12 := true, sum := sum + 12;

action add13
pre    ~done13
do     done13 := true, sum := sum + 13;

action add14
pre    ~done14
do     done14 := true, sum := sum + 14;

action add15
pre    ~done15
do     done15 := true, sum := sum + 15;

action add16
pre    ~done16
do     done16 := true, sum := sum + 16;

action add17
pre    ~done17
do     done17 := true, sum := sum + 17;

action add18
pre    ~done18
do     done18 := true, sum := sum + 18;

action add19
pre    ~done19
do     done19 := true, sum := sum + 19;

action add20
pre    ~done20
do     done20 := true, sum := sum + 20;

action add21
pre    ~done21
do     done21 := true, sum := sum + 21;

action add22
pre    ~done22
do     done22 := true, sum := sum + 22;

action add23
pre    ~done23
do     done23 := true, sum := sum + 23;

action add24
pre    ~done24
do     done24 := true, sum := sum + 24;

action add25
pre    ~done25
do     done25 := true, sum := sum + 25; */

end;

println();
println("check no 1 is supposed to be: not reachable");
assertNR error;

println();
println("check no 2 is supposed to be: not reachable");
assertNR sum > 55;

println();
println("check no 3 is supposed to be: reachable");
assertR sum = 55;

println();
println("check no 4 is supposed to be: NOT valid");
assertNV EF (sum > 55);

println();
println("check no 5 is supposed to be: NOT valid");
assertNV EF (sum < 0);

println();
println("check no 6 is supposed to be: valid");
assertV forall s : 1..10 . EX (sum = s);

println();
println("check no 7 is supposed to be: NOT valid");
assertNV EX (sum < 1);

println();
println("check no 8 is supposed to be: NOT valid");
assertNV EX (sum > 10);

println();
println("check no 9 is supposed to be: NOT valid");
assertNV exists s : -17..1023 . AX (sum = s);

println();
println("check no 10 is supposed to be: valid");
assertV AF (sum = 55);

println();
println("check no 11 is supposed to be: valid");
assertV AG (0 <= sum & sum <= 55);

println();
println("check no 12 is supposed to be: NOT valid");
assertNV AG (sum = 55);

println();
println("check no 13 is supposed to be: valid");
assertV AG (sum = 55 -> AG (sum = 55));

println();
println("check no 14 is supposed to be: valid");
assertV AG (forall s : 0..54 . sum = s -> AX (exists d : 1..10 . sum = s + d));  // corrected, 17.7.2011

println();
println("check no 15 is supposed to be: NOT valid");
assertNV AG (forall s : 0..54 . sum = s -> AX (exists d : 1..9 . sum = s + d));  // corrected, 17.7.2011

println();
println("check no 16 is supposed to be: NOT valid");
assertNV AG (forall s : 0..54 . sum = s -> AX (forall d : 1..10 . sum = s + d)); // corrected, 17.7.2011

println();
println("check no 17 is supposed to be: valid");
assertV AG (forall q : 10..-7 . false);

println();
println("check no 18 is supposed to be: valid");
assertV AG (~(exists q : 0..-1 . true));

println();
println("check no 19 is supposed to be: valid");
assertV (sum < 55) AU (sum = 55);

println();
println("check no 20 is supposed to be: NOT valid");
assertNV (sum < 54) AU (sum = 55);

println();
println("check no 21 is supposed to be: valid");
assertV (sum <= 45) EU (sum = 55);

println();
println("check no 22 is supposed to be: valid");
assertV forall s, t : 0..55, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10 : bool . ((EF (sum = s & done01 = d1 & done02 = d2 & done03 = d3 & done04 = d4 & done05 = d5 & done06 = d6 & done07 = d7 & done08 = d8 & done09 = d9 & done10 = d10)) & (EF (sum = t & done01 = d1 & done02 = d2 & done03 = d3 & done04 = d4 & done05 = d5 & done06 = d6 & done07 = d7 & done08 = d8 & done09 = d9 & done10 = d10)) -> (s = t));

println();
println("check no 23 is supposed to be: NOT valid");
assertNV forall s : 0..55, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10 : bool . ((EF (sum = s & done01 = d1 & done02 = d2 & done03 = d3 & done04 = d4 & done05 = d5 & done06 = d6 & done07 = d7 & done08 = d8 & done09 = d9 & done10 = d10)) & (EF (sum = s & done01 = e1 & done02 = e2 & done03 = e3 & done04 = e4 & done05 = e5 & done06 = e6 & done07 = e7 & done08 = e8 & done09 = e9 & done10 = e10)) -> (d1=e1 & d2=e2 & d3=e3 & d4=e4 & d5=e5 & d6=e6 & d7=e7 & d8=e8 & d9=e9 & d10=e10));

println();
println("check no 24 is supposed to be: valid");
assertV forall s : 0..55 . EF (sum = s);


