/* Specification frameworktest1 */

specification frameworktest1;
var v1, v2, v3, v4, v5, v6, v7, v8, v9 : boolean;
var x, y, z: -128..127;
var a : -128..127; // see test no 154
var p : 1..99;
let c1 : boolean = 2 * (100 div x) = 10 & 100 mod x = 0;
let x_squared : -1047552..1048576 = x*x;
let max_value :  2147483647 .. 2147483647 = 2147483647;
let min_value : -2147483648..-2147483648 = -2147483648;
end;

println();
print("Propositional logic:");
println();

println("The following formulas are supposed to be");
println("a) totally defined and");
println("b) universally valid.");
println();

print("[001] "); assertSF true;
println();

print("[002] "); assertSF v1 | true;
println();

print("[003] "); assertSF ~~true;
println();

print("[004] "); assertSF v1 -> v1;
println();

print("[005] "); assertSF v1 <-> v1;
println();

print("[006] "); assertSF ~~v1 <-> v1;
println();

print("[007] "); assertSF v1 & ~v1 <-> false;
println();

print("[008] "); assertSF true <-> v1 | ~v1;
println();

print("[009] "); assertSF v1 & true <-> v1;
println();

print("[010] "); assertSF v1 | true <-> true;
println();

print("[011] "); assertSF v1 & false <-> false;
println();

print("[012] "); assertSF v1 | false <-> v1;
println();

print("[013] "); assertSF v1 & v1 <-> v1;
println();

print("[014] "); assertSF v1 | v1 <-> v1;
println();

print("[015] "); assertSF ((v1 & v2) | v3) <-> ((v1 | v3) & (v2 | v3));
println();

print("[016] "); assertSF ((v1 | v2) & v3) <-> ((v1 & v3) | (v2 & v3));
println();

print("[017] "); assertSF ~(v1 | v2) <-> (~v1 & ~v2);
println();

print("[018] "); assertSF ~(v1 & v2) <-> (~v1 | ~v2);
println();

print("[019] "); assertSF ((v1 & v2) | v1) <-> v1;
println();

print("[020] "); assertSF ((v1 | v2) & v1) <-> v1;
println();

print("[021] "); assertSF (v1 | v2) <-> (~v1 -> v2);
println();

print("[022] "); assertSF (v1 & v2) <-> ~(v1 -> ~v2);
println();

print("[023] "); assertSF (v1 -> v2) <-> (~v2 -> ~v1);
println();

print("[024] "); assertSF false -> v1;
println();

print("[025] "); assertSF v1 -> true;
println();

print("[026] "); assertSF v1 & v2 -> v1;
println();

print("[027] "); assertSF v1 -> v1 | v2;
println();

print("[028] "); assertSF v2 -> v1 -> v2;
println();

print("[029] "); assertSF v1 & (v1 -> v2) -> v2;
println();

print("[030] "); assertSF ~v2 & (v1 -> v2) -> ~v1;
println();

print("[031] "); assertSF (v1 -> v2) & (v2 -> v3) -> (v1 -> v2);
println();

print("[032] "); assertSF ((v1 -> v3) & (v2 -> v3)) <-> ((v1 | v2) -> v3);
println();

print("[033] "); assertSF (v1 <-> v2) <-> (v1 -> v2) & (v2 -> v1);
println();

print("[034] "); assertSF v1 exor v2 <-> (v1 & ~v2) | (~v1 & v2);
println();

print("[035] "); assertSF v1 nor v2 <-> ~(v1 | v2);
println();

print("[036] "); assertSF v1 nand v2 <-> ~(v1 & v2);
println();

print("[037] "); assertSF (v1 = v2) <-> (v1 <-> v2);
println();

print("[038] "); assertSF ((v1 -> false) -> false) <-> v1;
println();

print("[039] "); assertSF (v1 -> (v2 -> v3)) -> ((v1 -> v2) -> (v1 -> v3));
println();

print("[040] "); assertSF v1 | v9 -> v9 | v1;
println();

println("Quantification over variables of type boolean:");
println();

print("[041] "); assertSF exists v1 . v1;
println();

print("[042] "); assertSF exists v1 . ~v1;
println();

print("[043] "); assertSF ~(forall v2 . v2);
println();

print("[044] "); assertSF ~(forall v2 . ~v2);
println();

print("[045] "); assertSF exists v2, v1 . true;
println();

print("[046] "); assertSF forall v1, v2, v3 . true;
println();

print("[047] "); assertSF (exists v1, v3 . ~v1&v2&v3&v4&v5 | v1&~v2&v3&v4&~v5 | v7&~v8) <-> v2&v4&v5 | ~v2&v4&~v5 | v7&~v8;
println();

print("[048] "); assertSF (exists v6 . ~v1&v2&v3&v4&v5 | v1&~v2&v3&v4&~v5 | v7&~v8) <-> (~v1&v2&v3&v4&v5 | v1&~v2&v3&v4&~v5 | v7&~v8);
println();

print("[049] "); assertSF forall v1 . v1 -> v1;
println();

print("[050] "); assertSF ~(forall v1 . v1 & v2 | v3) <-> (exists v1 . ~(v1 & v2 | v3));
println();

print("[051] "); assertSF (exists v1 . ~(v1 & v2 | v3)) <-> ~v3;
println();

print("[052] "); assertSF (forall v3 . v1&v2&v3&v4 | ~v3&v5&v6&v7&v8) <-> v1&v2&v4&v5&v6&v7&v8;
println();

print("[053] "); assertSF (forall v2 . v1&v2&v3&v4&v5&v6&v7&v8) <-> false;
println();

print("[054] "); assertSF exists v1 . v1|v2|v3|~v4|v5 <-> true;
println();

println("Arithmetic, without division by zero:");
println();

print("[101] "); assertSF 0=0 & ~(1<>1) & ~(1<1) & (1<=1) & ~(1=0) & (0<1) & (0<=1) & (-1<1);
println();

print("[102] "); assertSF 2147483647 = max_value & -2147483648 = min_value;
println();

print("[103] "); assertSF (min_value < min_value+1) & (min_value < max_value) & (max_value -(1) < max_value);
println();

print("[104] "); assertSF (x - 1 = x -1); // LHS: minus operator; RHS: negative sign
println();

print("[105] "); assertSF (45113 *  22893 =  1032771909) & (-45113 * 22893 = -1032771909);
println();

print("[106] "); assertSF (45113 * -22893 = -1032771909) & (-45113 * -22893 = 1032771909);
println();

print("[107] "); assertSF (45113 * 0 = 0) & (0 * 22893 = 0) & (45113 * 1 = 45113) & (1 * 22893 = 22893) & (71321 * -1 = -71321) & (-1 * 71321 = -71321);
println();

print("[108] "); assertSF (-0 = 0) & (0 * y = 0) & (x * 0 = 0) & (1 * y = y) & (y * 1 = y);
println();

// print("[109] "); assertSF (x*x + 2*x*y + y*y) = (x+y) * (x+y);
// println();

// print("[110] "); assertSF (x*x + (-2*x*y) + y*y) = (x-y) * (x-y);
// println();

// print("[111] "); assertSF (x+y) * (x-y) = x*x - y*y;
// println();

// print("[112] "); assertSF x*x*x + 3 * x*x*y + 3 * x*y*y + y*y*y = (x+y) * (x+y) * (x+y);
// println();

// print("[113] "); assertSF ~(x*x*x + y*y*y = z*z*z & x!=0 & y!=0 & z!=0); 
// println();

// print("[114] "); assertSF x_squared + 2*x*y + y*y = (x+y) * (x+y);
// println();

print("[115] "); assertSF (1 * min_value = min_value) & (-1 * min_value = min_value)   & (0 * min_value = 0);
println();

print("[116] "); assertSF (1 * max_value = max_value) & (-1 * max_value = min_value+1) & (0 * max_value = 0);
println();
              
print("[117] "); assertSF (0 div 1 = 0) & (1 div 1 = 1) & (7 div 1 = 7) & (max_value div 1 = max_value);
println();

print("[118] "); assertSF (0 div 2 = 0) & (1 div 2 = 0) & (2 div 2 = 1) & (3 div 2 = 1) & (4 div 2 = 2);
println();

print("[119] "); assertSF (0 div max_value = 0) & ((max_value-1) div max_value = 0) & (max_value div max_value = 1); 
println();

print("[120] "); assertSF (max_value div 17 = 126322567) & ((max_value - max_value mod 17) div 17 = 126322567) & (((max_value - max_value mod 17)-1) div 17 = 126322566); 
println();

print("[121] "); assertSF (-7 div 2 = -3) & (7 div -2 = -3) & (-7 div -2 = 3) & (-7 mod 2 = -1) & (7 mod -2 = 1) & (-7 mod -2 = -1); 
println();

print("[122] "); assertSF (0 mod 1 = 0) & (1 mod 1 = 0) & (7 mod 1 = 0) & (max_value mod 1 = 0) & (-7 mod 1 = 0) & (min_value mod 1 = 0);
println();

print("[123] "); assertSF (min_value div min_value = 1) & (min_value mod min_value = 0);
println();

print("[124] "); assertSF (min_value div max_value = -1) & (min_value mod max_value = -1);
println();

print("[125] "); assertSF ((min_value+1) div max_value = -1) & ((min_value+1) mod max_value = 0);
println();

print("[126] "); assertSF (max_value div min_value = 0) & (max_value mod min_value = max_value);
println();

println("Casts:");
println();

print("[151] "); assertSF (([0..1] false) = 0) & (([0..1] true) = 1) & (([-7..2] false) = 0) & (([-7..2] true) = 1);
println();

print("[152] "); assertSF (([bool] 0) = false) & (([bool] 1) = true) & (([bool] -1) = true) & (([bool] 17) = true);
println();

print("[153] "); assertSF (([0..2] -3) = 0) & (([0..2] -2) = 1) & (([0..2] -1) = 2) & (([0..2] 0) = 0) & (([0..2] 1) = 1) & (([0..2] 2) = 2) & (([0..2] 3) = 0);
println();

print("[153] "); assertSF (([-1..1] -3) = 0) & (([-1..1] -2) = 1) & (([-1..1] -1) = -1) & (([-1..1] 0) = 0) & (([-1..1] 1) = 1) & (([-1..1] 2) = -1) & (([-1..1] 3) = 0);
println();

print("[154] "); assertSF ([-131..-122] a) = (-131) + ((a - (-131)) mod ((-122+1)-(-131))) ;
println();

print("[155] "); assertSF (([0..2 or extremum] min_value) = 0) & (([0..2 or extremum] -1) = 0) & (([0..2 or extremum] 0) = 0) & (([0..2 or extremum] 1) = 1) & (([0..2 or extremum] 2) = 2) & (([0..2 or extremum] 3) = 2) & (([0..2 or extremum] max_value) = 2);
println();

println("Quantification over variables of type integer:");
println();

print("[201] "); assertSF (exists a : 1..255 . x + a = y) <-> x < y;
println();

print("[202] "); assertSF forall x : -128..127, y : -128..127 . (exists a : 1..255 . x + a = y) <-> x < y;
println();

print("[203] "); assertSF forall x : -127..127 . exists y : -127..127 . x = -y;
println();

print("[204] "); assertSF ~(forall x : -128..127 . exists y : -128..127 . x = -y);
println();

print("[205] "); assertSF ~(forall p : 2..99 . forall x : 2..99, y : 2..99 . p != x * y);
println();

print("[206] "); assertSF forall p : 2..99 . (~(exists t : 2..99 . t < p & p mod t = 0) -> (forall x : 2..99 . forall y : 2..99 . p != x * y));
println();

print("[207] "); assertSF forall z : -10..10 .    exists x1:-2..2, x2:-2..2, x3:-2..2, x4:-2..2, x5:-2..2 . x1 + x2 + x3 + x4 + x5 = z;
println();

print("[208] "); assertSF (z < -10 | 10 < z) -> ~(exists x1:-2..2, x2:-2..2, x3:-2..2, x4:-2..2, x5:-2..2 . x1 + x2 + x3 + x4 + x5 = z);
println();

print("[209] "); assertSF ~(exists x : 0..-1 . true);
println();

print("[210] "); assertSF forall x : 0..10, y : -3..-17 . false;
println();

print("[211] "); assertSF forall q : 10..10 . q = 10;
println();

print("[212] "); assertSF forall q : 10..9 . false;
println();

print("[213] "); assertSF ~(exists q : 10..9 . true);
println();

println("Division by zero:");
println();

println("The following formulas are only partially defined on the state space.");
println("They need not to be universally valid on their domains.");
println();

print("[301] "); checkSF 100 div x = 5 & 100 mod x = 0;
println();

print("[302] "); assertSF c1 <-> (100 div x = 5 & 100 mod x = 0);
println();

print("[303] "); assertSF c1 <-> (2 * (100 div x) = 10 & 100 mod x = 0);
println();

end;

