/* Specification of the muddy children problem (for 8 children) */

specification muddy_children_8;

// mudi means that child i has a muddy forehead

var mud1, mud2, mud3, mud4, mud5, mud6, mud7, mud8 : boolean;
// let numMuddy : 0..10 = ([0..1] mud1) + ([0..1] mud2) + ([0..1] mud3) + ([0..1] mud4) + ([0..1] mud5) + ([0..1] mud6) + ([0..1] mud7) + ([0..1] mud8);
let numMuddy = ([0..1] mud1) + ([0..1] mud2) + ([0..1] mud3) + ([0..1] mud4) + ([0..1] mud5) + ([0..1] mud6) + ([0..1] mud7) + ([0..1] mud8);

// public counter indicating the current round

var round : 0..9;

// control state of the children

var testified : boolean;

// the public board that records the testimonies of the children and the father:
// f0 means that the father has announced in the initial round: "At least one of
// you has mud on your forehead."
// ~f0 means that the father has refrained from announcing "At least one of
// you has mud on your forehead." In the given epistemic framework, this is
// equivalent to the public announcement "None of you has mud on your forehead." 
// ci_j means that child i testified in the j-th round: "Yes, I know whether I have
// mud on my forehead or not."
// ~ci_j means that child i testified in the j-th round: "I don't know." 

var f0 : boolean;
var c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8 : boolean;

// the secret board that caches the testimonies of the children in each round
// before they are published simultaneously on the public board.

var s1, s2, s3, s4, s5, s6, s7, s8 : boolean;

let boardempty = ~f0 & ~s1 & ~s2 & ~s3 & ~s4 & ~s5 & ~s6 & ~s7 & ~s8 &
   ~c1_1 & ~c1_2 & ~c1_3 & ~c1_4 & ~c1_5 & ~c1_6 & ~c1_7 & ~c1_8 &
   ~c2_1 & ~c2_2 & ~c2_3 & ~c2_4 & ~c2_5 & ~c2_6 & ~c2_7 & ~c2_8 &
   ~c3_1 & ~c3_2 & ~c3_3 & ~c3_4 & ~c3_5 & ~c3_6 & ~c3_7 & ~c3_8 &
   ~c4_1 & ~c4_2 & ~c4_3 & ~c4_4 & ~c4_5 & ~c4_6 & ~c4_7 & ~c4_8 &
   ~c5_1 & ~c5_2 & ~c5_3 & ~c5_4 & ~c5_5 & ~c5_6 & ~c5_7 & ~c5_8 &
   ~c6_1 & ~c6_2 & ~c6_3 & ~c6_4 & ~c6_5 & ~c6_6 & ~c6_7 & ~c6_8 &
   ~c7_1 & ~c7_2 & ~c7_3 & ~c7_4 & ~c7_5 & ~c7_6 & ~c7_7 & ~c7_8 &
   ~c8_1 & ~c8_2 & ~c8_3 & ~c8_4 & ~c8_5 & ~c8_6 & ~c8_7 & ~c8_8;
   
// definition of the epistemic accessibility relations of father and children

agent father  = {mud1, mud2, mud3, mud4, mud5, mud6, mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8                                 };
agent child1  = {      mud2, mud3, mud4, mud5, mud6, mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, s1                             };
agent child2  = {mud1,       mud3, mud4, mud5, mud6, mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,     s2                         };
agent child3  = {mud1, mud2,       mud4, mud5, mud6, mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,         s3                     };
agent child4  = {mud1, mud2, mud3,       mud5, mud6, mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,             s4                 };
agent child5  = {mud1, mud2, mud3, mud4,       mud6, mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,                 s5             };
agent child6  = {mud1, mud2, mud3, mud4, mud5,       mud7, mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,                     s6         };
agent child7  = {mud1, mud2, mud3, mud4, mud5, mud6,       mud8, round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,                         s7     };
agent child8  = {mud1, mud2, mud3, mud4, mud5, mud6, mud7,       round, testified, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8,                             s8 };

guard kchild1  = (K[child1 ] mud1 ) | (K[child1 ] (~mud1 ));
guard kchild2  = (K[child2 ] mud2 ) | (K[child2 ] (~mud2 ));
guard kchild3  = (K[child3 ] mud3 ) | (K[child3 ] (~mud3 ));
guard kchild4  = (K[child4 ] mud4 ) | (K[child4 ] (~mud4 ));
guard kchild5  = (K[child5 ] mud5 ) | (K[child5 ] (~mud5 ));
guard kchild6  = (K[child6 ] mud6 ) | (K[child6 ] (~mud6 ));
guard kchild7  = (K[child7 ] mud7 ) | (K[child7 ] (~mud7 ));
guard kchild8  = (K[child8 ] mud8 ) | (K[child8 ] (~mud8 ));

initial (round = 0) & (~testified) & boardempty;

action father_yes
pre    (round = 0) & (1 <= numMuddy)
do 	   f0 := true,
       round := round + 1,
       testified := false; 

action father_no
pre    (round = 0) & (0 = numMuddy)
do     f0 := false,
       round := round + 1,
       testified := false; 

action children00000000
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00000001
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00000010
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00000011
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00000100
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00000101
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00000110
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00000111
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00001000
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00001001
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00001010
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00001011
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00001100
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00001101
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00001110
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00001111
epre   ~kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00010000
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00010001
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00010010
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00010011
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00010100
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00010101
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00010110
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00010111
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00011000
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00011001
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00011010
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00011011
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00011100
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00011101
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00011110
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00011111
epre   ~kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00100000
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00100001
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00100010
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00100011
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00100100
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00100101
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00100110
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00100111
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00101000
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00101001
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00101010
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00101011
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00101100
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00101101
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00101110
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00101111
epre   ~kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00110000
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00110001
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00110010
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00110011
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00110100
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00110101
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00110110
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00110111
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children00111000
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children00111001
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children00111010
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children00111011
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children00111100
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children00111101
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children00111110
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children00111111
epre   ~kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01000000
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01000001
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01000010
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01000011
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01000100
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01000101
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01000110
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01000111
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01001000
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01001001
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01001010
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01001011
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01001100
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01001101
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01001110
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01001111
epre   ~kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01010000
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01010001
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01010010
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01010011
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01010100
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01010101
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01010110
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01010111
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01011000
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01011001
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01011010
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01011011
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01011100
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01011101
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01011110
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01011111
epre   ~kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01100000
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01100001
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01100010
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01100011
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01100100
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01100101
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01100110
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01100111
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01101000
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01101001
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01101010
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01101011
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01101100
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01101101
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01101110
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01101111
epre   ~kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01110000
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01110001
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01110010
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01110011
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01110100
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01110101
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01110110
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01110111
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children01111000
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children01111001
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children01111010
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children01111011
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children01111100
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children01111101
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children01111110
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children01111111
epre   ~kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 := false, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10000000
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10000001
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10000010
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10000011
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10000100
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10000101
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10000110
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10000111
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10001000
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10001001
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10001010
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10001011
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10001100
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10001101
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10001110
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10001111
epre    kchild8 & ~kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10010000
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10010001
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10010010
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10010011
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10010100
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10010101
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10010110
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10010111
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10011000
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10011001
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10011010
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10011011
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10011100
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10011101
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10011110
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10011111
epre    kchild8 & ~kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10100000
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10100001
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10100010
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10100011
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10100100
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10100101
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10100110
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10100111
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10101000
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10101001
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10101010
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10101011
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10101100
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10101101
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10101110
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10101111
epre    kchild8 & ~kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10110000
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10110001
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10110010
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10110011
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10110100
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10110101
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10110110
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10110111
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children10111000
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children10111001
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children10111010
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children10111011
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children10111100
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children10111101
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children10111110
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children10111111
epre    kchild8 & ~kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 := false, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11000000
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11000001
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11000010
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11000011
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11000100
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11000101
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11000110
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11000111
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11001000
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11001001
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11001010
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11001011
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11001100
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11001101
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11001110
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11001111
epre    kchild8 &  kchild7 & ~kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11010000
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11010001
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11010010
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11010011
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11010100
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11010101
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11010110
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11010111
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11011000
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11011001
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11011010
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11011011
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11011100
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11011101
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11011110
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11011111
epre    kchild8 &  kchild7 & ~kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 := false, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11100000
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11100001
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11100010
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11100011
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11100100
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11100101
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11100110
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11100111
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11101000
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11101001
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11101010
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11101011
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11101100
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11101101
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11101110
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11101111
epre    kchild8 &  kchild7 &  kchild6 & ~kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 := false, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11110000
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11110001
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11110010
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11110011
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11110100
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11110101
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11110110
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11110111
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 & ~kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 := false, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action children11111000
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 := false,
       testified := true; 

action children11111001
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 := false, s1 :=  true,
       testified := true; 

action children11111010
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 := false,
       testified := true; 

action children11111011
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 & ~kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 := false, s2 :=  true, s1 :=  true,
       testified := true; 

action children11111100
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 := false,
       testified := true; 

action children11111101
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 & ~kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 := false, s1 :=  true,
       testified := true; 

action children11111110
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 & ~kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 := false,
       testified := true; 

action children11111111
epre    kchild8 &  kchild7 &  kchild6 &  kchild5 &  kchild4 &  kchild3 &  kchild2 &  kchild1
pre    1 <= round & round <= 8 & ~testified
do     s8 :=  true, s7 :=  true, s6 :=  true, s5 :=  true, s4 :=  true, s3 :=  true, s2 :=  true, s1 :=  true,
       testified := true;

action round1_publication
pre    (round = 1) & (testified)
do     c1_1 := s1, c2_1 := s2, c3_1 := s3, c4_1 := s4, c5_1 := s5, c6_1 := s6, c7_1 := s7, c8_1 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;
       
action round2_publication
pre    (round = 2) & (testified) 
do     c1_2 := s1, c2_2 := s2, c3_2 := s3, c4_2 := s4, c5_2 := s5, c6_2 := s6, c7_2 := s7, c8_2 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;

action round3_publication
pre    (round = 3) & (testified)
do     c1_3 := s1, c2_3 := s2, c3_3 := s3, c4_3 := s4, c5_3 := s5, c6_3 := s6, c7_3 := s7, c8_3 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;

action round4_publication
pre    (round = 4) & (testified)
do     c1_4 := s1, c2_4 := s2, c3_4 := s3, c4_4 := s4, c5_4 := s5, c6_4 := s6, c7_4 := s7, c8_4 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;
       
action round5_publication
pre    (round = 5) & (testified) 
do     c1_5 := s1, c2_5 := s2, c3_5 := s3, c4_5 := s4, c5_5 := s5, c6_5 := s6, c7_5 := s7, c8_5 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;

action round6_publication
pre    (round = 6) & (testified)
do     c1_6 := s1, c2_6 := s2, c3_6 := s3, c4_6 := s4, c5_6 := s5, c6_6 := s6, c7_6 := s7, c8_6 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;

action round7_publication
pre    (round = 7) & (testified)
do     c1_7 := s1, c2_7 := s2, c3_7 := s3, c4_7 := s4, c5_7 := s5, c6_7 := s6, c7_7 := s7, c8_7 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;
       
action round8_publication
pre    (round = 8) & (testified) 
do     c1_8 := s1, c2_8 := s2, c3_8 := s3, c4_8 := s4, c5_8 := s5, c6_8 := s6, c7_8 := s7, c8_8 := s8,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false,
       round := round + 1, testified := false;
     
action stutter
pre    (round = 9)
do;

end;

interpret;


