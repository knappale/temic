/* specification frameworktest7: testing epistemic guard evaluation */

specification frameworktest7;

var c : bool; init c = false;

agent bob = { };
agent intruder = { c };

guard myguard = K[bob] ~(K[intruder] (false));

action mystep
epre myguard
pre ~c
do c := true;


end;

interpret;

assertR c = true;


