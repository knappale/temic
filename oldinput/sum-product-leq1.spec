/* Specification of the Sum-and-Product-Riddle (Version 2) */

specification sum_and_product;

var a, b : 2..99 initial a <= b;
var s : 4..198 	initial s = a + b;
var p : 4..9801	initial p = a * b;
var step : 1..6 initial step = 1;
var p1, p2, p3, p4, s2 : boolean initial p1 <-> false & p2 <-> false & p3 <-> false & p4 <-> false & s2 <-> false; 

agent gauss   = {    p, step, p1, p2, p3, p4     };
agent euler   = { s,    step, p1, p2, p3, p4, s2 };

let gauss_knows_a = (exists x:2..99 . (K[gauss] a = x));
let gauss_knows_b = (exists x:2..99 . (K[gauss] b = x));
let euler_knows_a = (exists x:2..99 . (K[euler] a = x));
let euler_knows_b = (exists x:2..99 . (K[euler] b = x));
let euler_knows_gauss_does_not_know_a = K[euler] ~(exists x:2..99 . (K[gauss] a = x));

action step1_euler_yes
epre   euler_knows_gauss_does_not_know_a
pre    step = 1
do     s2 := true, step := step + 1;

action step1_euler_no
epre   ~euler_knows_gauss_does_not_know_a
pre    step = 1
do     s2 := false, step := step + 1;

action step2_gauss_yes
epre   gauss_knows_a
pre    step = 2
do     p1 := true, step := step + 1;

action step2_gauss_no
epre   ~gauss_knows_a
pre    step = 2
do     p1 := false, step := step + 1;

action step3_euler_publish
pre    step = 3
do     p2 := s2, step := step + 1;

action step4_gauss_yes
epre   gauss_knows_a
pre    step = 4
do     p3 := true, step := step + 1;

action step4_gauss_no
epre   ~gauss_knows_a
pre    step = 4
do     p3 := false, step := step + 1;

action step5_euler_yes
epre   euler_knows_a
pre    step = 5
do     p4 := true, step := step + 1;

action step5_euler_no
epre   ~euler_knows_a
pre    step = 5
do     p4 := false, step := step + 1;

action stutter
pre    step = 6
do;

end;

interpret;

check reachable (step = 6 & !p1 & p2 & p3 & p4);
// check reachable (step = 6 & !p1 & p2 & p3 & p4 &  (a = 4 & b = 13));
// check reachable (step = 6 & !p1 & p2 & p3 & p4 & !(a = 4 & b = 13)); 

// check initial EF (step = 6 & !p1 & p2 & p3 & p4);
// check initial AG (step = 6 & !p1 & p2 & p3 & p4 -> a = 4 & b = 13);

// check initial EF (step = 6 & !p1 & p2 & p3 & p4);
