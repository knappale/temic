/* Specification of the muddy children problem (for 10 children) */

specification muddy_children_10;

// mudi means that child i has a muddy forehead

var mud1, mud2, mud3, mud4, mud5, mud6, mud7, mud8, mud9, mud10 : boolean;
let numMuddy : 0..10 = ([0..1] mud1) + ([0..1] mud2) + ([0..1] mud3) + ([0..1] mud4) + ([0..1] mud5) + ([0..1] mud6) + ([0..1] mud7) + ([0..1] mud8) + ([0..1] mud9) + ([0..1] mud10);

// public counter indicating the current round

var round : 0..11;

// control state of the children:
// numTest = i means that children {1..i} have already testified in the current round

var numTest : 0..10;

// the public board that records the testimonies of the children and the father:
// f0 means that the father has announced in the initial round: "At least one of
// you has mud on your forehead."
// ~f0 means that the father has refrained from announcing "At least one of
// you has mud on your forehead." In the given epistemic framework, this is
// equivalent to the public announcement "None of you has mud on your forehead." 
// ci_j means that child i testified in the j-th round: "Yes, I know whether I have
// mud on my forehead or not."
// ~ci_j means that child i testified in the j-th round: "I don't know." 

var f0 : boolean;
var c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10 : boolean;

// the secret board that caches the testimonies of the children in each round
// before they are published simultaneously on the public board.

var s1, s2, s3, s4, s5, s6, s7, s8, s9, s10 : boolean;

let boardempty = ~f0 & ~s1 & ~s2 & ~s3 & ~s4 & ~s5 & ~s6 & ~s7 & ~s8 & ~s9 & ~s10 &
   ~c1_1 & ~c1_2 & ~c1_3 & ~c1_4 & ~c1_5 & ~c1_6 & ~c1_7 & ~c1_8 & ~c1_9 & ~c1_10 &
   ~c2_1 & ~c2_2 & ~c2_3 & ~c2_4 & ~c2_5 & ~c2_6 & ~c2_7 & ~c2_8 & ~c2_9 & ~c2_10 &
   ~c3_1 & ~c3_2 & ~c3_3 & ~c3_4 & ~c3_5 & ~c3_6 & ~c3_7 & ~c3_8 & ~c3_9 & ~c3_10 &
   ~c4_1 & ~c4_2 & ~c4_3 & ~c4_4 & ~c4_5 & ~c4_6 & ~c4_7 & ~c4_8 & ~c4_9 & ~c4_10 &
   ~c5_1 & ~c5_2 & ~c5_3 & ~c5_4 & ~c5_5 & ~c5_6 & ~c5_7 & ~c5_8 & ~c5_9 & ~c5_10 &
   ~c6_1 & ~c6_2 & ~c6_3 & ~c6_4 & ~c6_5 & ~c6_6 & ~c6_7 & ~c6_8 & ~c6_9 & ~c6_10 &
   ~c7_1 & ~c7_2 & ~c7_3 & ~c7_4 & ~c7_5 & ~c7_6 & ~c7_7 & ~c7_8 & ~c7_9 & ~c7_10 &
   ~c8_1 & ~c8_2 & ~c8_3 & ~c8_4 & ~c8_5 & ~c8_6 & ~c8_7 & ~c8_8 & ~c8_9 & ~c8_10 &
   ~c9_1 & ~c9_2 & ~c9_3 & ~c9_4 & ~c9_5 & ~c9_6 & ~c9_7 & ~c9_8 & ~c9_9 & ~c9_10 &
   ~c10_1 & ~c10_2 & ~c10_3 & ~c10_4 & ~c10_5 & ~c10_6 & ~c10_7 & ~c10_8 & ~c10_9 & ~c10_10;

// definition of the epistemic accessibility relations of father and children

agent father  = {mud1, mud2, mud3, mud4, mud5, mud6, mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10                                          };
agent child1  = {      mud2, mud3, mud4, mud5, mud6, mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10, s1                                      };
agent child2  = {mud1,       mud3, mud4, mud5, mud6, mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,     s2                                  };
agent child3  = {mud1, mud2,       mud4, mud5, mud6, mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,         s3                              };
agent child4  = {mud1, mud2, mud3,       mud5, mud6, mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,             s4                          };
agent child5  = {mud1, mud2, mud3, mud4,       mud6, mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,                 s5                      };
agent child6  = {mud1, mud2, mud3, mud4, mud5,       mud7, mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,                     s6                  };
agent child7  = {mud1, mud2, mud3, mud4, mud5, mud6,       mud8, mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,                         s7              };
agent child8  = {mud1, mud2, mud3, mud4, mud5, mud6, mud7,       mud9, mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,                             s8          };
agent child9  = {mud1, mud2, mud3, mud4, mud5, mud6, mud7, mud8,       mud10, round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,                                 s9      };
agent child10 = {mud1, mud2, mud3, mud4, mud5, mud6, mud7, mud8, mud9,        round, numTest, f0, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, c1_8, c1_9, c1_10, c2_1, c2_2, c2_3, c2_4, c2_5, c2_6, c2_7, c2_8, c2_9, c2_10, c3_1, c3_2, c3_3, c3_4, c3_5, c3_6, c3_7, c3_8, c3_9, c3_10, c4_1, c4_2, c4_3, c4_4, c4_5, c4_6, c4_7, c4_8, c4_9, c4_10, c5_1, c5_2, c5_3, c5_4, c5_5, c5_6, c5_7, c5_8, c5_9, c5_10, c6_1, c6_2, c6_3, c6_4, c6_5, c6_6, c6_7, c6_8, c6_9, c6_10, c7_1, c7_2, c7_3, c7_4, c7_5, c7_6, c7_7, c7_8, c7_9, c7_10, c8_1, c8_2, c8_3, c8_4, c8_5, c8_6, c8_7, c8_8, c8_9, c8_10, c9_1, c9_2, c9_3, c9_4, c9_5, c9_6, c9_7, c9_8, c9_9, c9_10, c10_1, c10_2, c10_3, c10_4, c10_5, c10_6, c10_7, c10_8, c10_9, c10_10,                                     s10 };

guard kchild1  = (K[child1 ] mud1 ) | (K[child1 ] (~mud1 ));
guard kchild2  = (K[child2 ] mud2 ) | (K[child2 ] (~mud2 ));
guard kchild3  = (K[child3 ] mud3 ) | (K[child3 ] (~mud3 ));
guard kchild4  = (K[child4 ] mud4 ) | (K[child4 ] (~mud4 ));
guard kchild5  = (K[child5 ] mud5 ) | (K[child5 ] (~mud5 ));
guard kchild6  = (K[child6 ] mud6 ) | (K[child6 ] (~mud6 ));
guard kchild7  = (K[child7 ] mud7 ) | (K[child7 ] (~mud7 ));
guard kchild8  = (K[child8 ] mud8 ) | (K[child8 ] (~mud8 ));
guard kchild9  = (K[child9 ] mud9 ) | (K[child9 ] (~mud9 ));
guard kchild10 = (K[child10] mud10) | (K[child10] (~mud10));

initial (round = 0) & (numTest = 0) & boardempty;

action father_yes
pre    (round = 0) & (1 <= numMuddy)
do 	   f0 := true,
       round := round + 1,
       numTest := 0; 

action father_no
pre    (round = 0) & (0 = numMuddy)
do     f0 := false,
       round := round + 1,
       numTest := 0; 

action child1_yes
epre   kchild1
pre    (1 <= round & round <= 10) & (numTest = 0)
do     s1 := true, numTest := 1;

action child1_no
epre   ~kchild1
pre    (1 <= round & round <= 10) & (numTest = 0)
do     s1 := false, numTest := 1;

action child2_yes
epre   kchild2
pre    (1 <= round & round <= 10) & (numTest = 1)
do     s2 := true, numTest := 2;

action child2_no
epre   ~kchild2
pre    (1 <= round & round <= 10) & (numTest = 1)
do     s2 := false, numTest := 2;

action child3_yes
epre   kchild3
pre    (1 <= round & round <= 10) & (numTest = 2)
do     s3 := true, numTest := 3;

action child3_no
epre   ~kchild3
pre    (1 <= round & round <= 10) & (numTest = 2)
do     s3 := false, numTest := 3;

action child4_yes
epre   kchild4
pre    (1 <= round & round <= 10) & (numTest = 3)
do     s4 := true, numTest := 4;

action child4_no
epre   ~kchild4
pre    (1 <= round & round <= 10) & (numTest = 3)
do     s4 := false, numTest := 4;

action child5_yes
epre   kchild5
pre    (1 <= round & round <= 10) & (numTest = 4)
do     s5 := true, numTest := 5;

action child5_no
epre   ~kchild5
pre    (1 <= round & round <= 10) & (numTest = 4)
do     s5 := false, numTest := 5;

action child6_yes
epre   kchild6
pre    (1 <= round & round <= 10) & (numTest = 5) 
do     s6 := true, numTest := 6;

action child6_no
epre   ~kchild6
pre    (1 <= round & round <= 10) & (numTest = 5)
do     s6 := false, numTest := 6;

action child7_yes
epre   kchild7
pre    (1 <= round & round <= 10) & (numTest = 6)
do     s7 := true, numTest := 7;

action child7_no
epre   ~kchild7
pre    (1 <= round & round <= 10) & (numTest = 6)
do     s7 := false, numTest := 7;

action child8_yes
epre   kchild8
pre    (1 <= round & round <= 10) & (numTest = 7)
do     s8 := true, numTest := 8;

action child8_no
epre   ~kchild8
pre    (1 <= round & round <= 10) & (numTest = 7)
do     s8 := false, numTest := 8;

action child9_yes
epre   kchild9
pre    (1 <= round & round <= 10) & (numTest = 8)
do     s9 := true, numTest := 9;

action child9_no
epre   ~kchild9
pre    (1 <= round & round <= 10) & (numTest = 8)
do     s9 := false, numTest := 9;

action child10_yes
epre   kchild10
pre    (1 <= round & round <= 10) & (numTest = 9)
do     s10 := true, numTest := 10;

action child10_no
epre   ~kchild10
pre    (1 <= round & round <= 10) & (numTest = 9)
do     s10 := false, numTest := 10;

action round1_publication
pre    (round = 1) & (numTest = 10)
do     c1_1 := s1, c2_1 := s2, c3_1 := s3, c4_1 := s4, c5_1 := s5, c6_1 := s6, c7_1 := s7, c8_1 := s8, c9_1 := s9, c10_1 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0;
       
action round2_publication
pre    (round = 2) & (numTest = 10) 
do     c1_2 := s1, c2_2 := s2, c3_2 := s3, c4_2 := s4, c5_2 := s5, c6_2 := s6, c7_2 := s7, c8_2 := s8, c9_2 := s9, c10_2 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0;

action round3_publication
pre    (round = 3) & (numTest = 10)
do     c1_3 := s1, c2_3 := s2, c3_3 := s3, c4_3 := s4, c5_3 := s5, c6_3 := s6, c7_3 := s7, c8_3 := s8, c9_3 := s9, c10_3 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0;

action round4_publication
pre    (round = 4) & (numTest = 10)
do     c1_4 := s1, c2_4 := s2, c3_4 := s3, c4_4 := s4, c5_4 := s5, c6_4 := s6, c7_4 := s7, c8_4 := s8, c9_4 := s9, c10_4 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 
       
action round5_publication
pre    (round = 5) & (numTest = 10) 
do     c1_5 := s1, c2_5 := s2, c3_5 := s3, c4_5 := s4, c5_5 := s5, c6_5 := s6, c7_5 := s7, c8_5 := s8, c9_5 := s9, c10_5 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 

action round6_publication
pre    (round = 6) & (numTest = 10)
do     c1_6 := s1, c2_6 := s2, c3_6 := s3, c4_6 := s4, c5_6 := s5, c6_6 := s6, c7_6 := s7, c8_6 := s8, c9_6 := s9, c10_6 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 

action round7_publication
pre    (round = 7) & (numTest = 10)
do     c1_7 := s1, c2_7 := s2, c3_7 := s3, c4_7 := s4, c5_7 := s5, c6_7 := s6, c7_7 := s7, c8_7 := s8, c9_7 := s9, c10_7 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 
       
action round8_publication
pre    (round = 8) & (numTest = 10) 
do     c1_8 := s1, c2_8 := s2, c3_8 := s3, c4_8 := s4, c5_8 := s5, c6_8 := s6, c7_8 := s7, c8_8 := s8, c9_8 := s9, c10_8 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 

action round9_publication
pre    (round = 9) & (numTest = 10)
do     c1_9 := s1, c2_9 := s2, c3_9 := s3, c4_9 := s4, c5_9 := s5, c6_9 := s6, c7_9 := s7, c8_9 := s8, c9_9 := s9, c10_9 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 

action round10_publication
pre    (round = 10) & (numTest = 10)
do     c1_10 := s1, c2_10 := s2, c3_10 := s3, c4_10 := s4, c5_10 := s5, c6_10 := s6, c7_10 := s7, c8_10 := s8, c9_10 := s9, c10_10 := s10,
       s1 := false, s2 := false, s3 := false, s4 := false, s5 := false, s6 := false, s7 := false, s8 := false, s9 := false, s10 := false,
       round := round + 1, numTest := 0; 
     
action stutter
pre    (round = 11)
do;

end;

interpret;


