/*
 * x = 0
 * r = 0
 * -------
 * Thread
 * -------
 * r := x;
 * x := r;
 * -------
 * r = 1?
 */
specification single;

var x : 0..2 initial x = 0;
var r : 0..2 initial r = 0;
var step : 1..3 initial step = 1;

agent t = { step, r };

action K_1
// guard K[t] (EF x = 1) // this produces out-of-thin air results
guard step = 1 and K[t] ((EF (r = 0 & x = 1)) and (EF (r = 1 & x = 1))) // too much?
// guard step = 1 and K[t] EF (r = 0 & x = 1)
do    r := 1, step := step + 1;

action K_2
// guard K[t] (EF x = 2) // this produces out-of-thin air results
guard step = 1 and K[t] ((EF (r = 0 & x = 2)) and (EF (r = 2 & x = 2))) // too much?
// guard step = 1 and K[t] EF (r = 0 & x = 2)
do    r := 2, step := step + 1;

action nK
guard step = 1
do    r := x, step := step + 1;

action set
guard step = 2
do    x := 1, step := step + 1;

action stutter
guard step = 3
do    ;

end;

check initial EF (r = 0); // holds
check initial EF (r = 1 | r = 2); // does not hold

check reachable (step = 3 & r = 0); // holds
check reachable (step = 3 & (r = 1 | r = 2)); // does not hold
