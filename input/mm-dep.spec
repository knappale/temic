/**
 * Sect. 6.3, first example (right)
 *
 * x = 0, y = 0
 * r1 = r2 = 0
 * ----------------------------------
 * Thread 1        | Thread 2
 * ----------------------------------
 * r1 := x;        | r2 := y;
 * if (r1 == 1)    | if (r2 == 1)
 *   y := 1;       |   x := 1;
 * ----------------------------------
 * r1 = r2 = 1?
 */
specification dep;

var x : 0..2 initial x = 0;
var y : 0..2 initial y = 0;
var r1 : 0..2 initial r1 = 0;
var r2 : 0..2 initial r2 = 0;
var step1 : 1..3 initial step1 = 1;
var step2 : 1..3 initial step2 = 1;

agent t1 = { step1, r1 };
agent t2 = { step2, r2 };

action K1_1
guard K[t1] ((EF (r1 = 0 & x = 1)) and (EF (r1 = 1 & x = 1)))
// guard K[t1] EF (r1 = 0 & x = 1)
pre   step1 = 1
do    r1 := 1, step1 := step1 + 1;

action K1_2
guard K[t1] ((EF (r1 = 0 & x = 2)) and (EF (r1 = 2 & x = 2)))
// guard K[t1] EF (r1 = 0 & x = 2)
pre   step1 = 1
do    r1 := 2, step1 := step1 + 1;

action nK1
pre   step1 = 1
do    r1 := x, step1 := step1 + 1;

action a1
pre   step1 = 2 & r1 = 1
do    y := 1, step1 := step1 + 1;

action a1n
pre   step1 = 2 & r1 != 1
do    step1 := step1 + 1;

action stutter1
pre   step1 = 3
do    ;

// -----------

action K2_1
guard K[t2] ((EF (r2 = 0 & y = 1)) and (EF (r2 = 1 & y = 1)))
// guard K[t2] EF (r2 = 0 & y = 1)
pre   step2 = 1
do    r2 := 1, step2 := step2 + 1;

action K2_2
guard K[t2] ((EF (r2 = 0 & y = 2)) and (EF (r2 = 2 & y = 2)))
// guard K[t2] EF (r2 = 0 & y = 2)
pre   step2 = 1
do    r2 := 2, step2 := step2 + 1;

action nK2
pre   step2 = 1
do    r2 := y, step2 := step2 + 1;

action a2
pre   step2 = 2 & r2 = 1
do    x := 1, step2 := step2 + 1;

action a2n
pre   step2 = 2 & r2 != 1
do    step2 := step2 + 1;

action stutter2
pre   step2 = 3
do    ;

end;

check initial EF (r1 = 0 & r2 = 0); // holds
check initial EF (r1 = 1 & r2 = 1); // does not hold
check initial EF (r1 = 2 | r2 = 2); // does not hold

check reachable (step1 = 3 & step2 = 3 & r1 = 0 & r2 = 0); // holds
check reachable (step1 = 3 & step2 = 3 & r1 = 1 & r2 = 1); // does not hold

check initial not K[t1] (EF x = 0); // does not hold
check initial K[t1] EF x = 1; // does not hold
check initial K[t1] not (EF x = 2); // holds
