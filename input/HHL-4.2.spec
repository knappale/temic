/**
 * Hendrik Wietze de Haan, Wim H. Hesselink, Gerard R. Renardel de Lavalette.
 * Knowledge-Based Asynchronous Programming.
 * Fundamenta Informaticae 63(2-3), 2004, pp. 259-281.
 * Ex. Sect. 4.2
 */

specification HHL_4_2;

var p : 0..1 initial p = 0;
var q : 0..1 initial q = 0;

agent a = { p };
agent b = { q };

action act1
guard M[a] q = 1
pre p = 0
do p := 1;

action act2
guard M[b] p = 1
pre q = 0
do q := 1;

action act3
pre true
do;

end;
