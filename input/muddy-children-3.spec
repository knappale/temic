/**
 * Specification of the muddy children problem (for 3 children)
 *
 * Ronald Fagin, Joseph Y. Halpern, Yoram Moses, Moshe Y. Vardi.
 * Reasoning About Knowledge.
 * MIT Press, 2003.
 * Sect. 7.1, Ex. 7.2.5
 *
 * Hans van Ditmarsch, Wiebe van der Hoek, Barteld Kooi.
 * Dynamic Epistemic Logic.
 * Springer, 2008.
 * Sect. 4.10
 */

specification muddy_children_3;

// mudi means that child i has a muddy forehead
var mud1, mud2, mud3 : boolean;

// public counter indicating the current round
var round : 0..4;

// control state of the children:
// ti means that child i has already testified in the current round
var t1, t2, t3 : boolean;

let notest  = !t1 & !t2 & !t3;
let alltest =  t1 &  t2 &  t3;

var numMuddy : 0..3 initial   (numMuddy = 0 <-> (!mud1 & !mud2 & !mud3))
                            & (numMuddy = 1 <-> ((mud1 & !mud2 & !mud3) | (!mud1 & mud2 & !mud3) | (!mud1 & !mud2 & mud3)))
                            & (numMuddy = 2 <-> ((mud1 & mud2 & !mud3) | (mud1 & !mud2 & mud3) | (!mud1 & mud2 & mud3)))
                            & (numMuddy = 3 <-> (mud1 & mud2 & mud3));
var child1SaidYes : -1..4 initial child1SaidYes = -1;

// the public board that records the testimonies of the children and the father:
// f0 means that the father has announced in the initial round: "At least one of
// you has mud on your forehead."
// ~f0 means that the father has refrained from announcing "At least one of
// you has mud on your forehead." In the given epistemic framework, this is
// equivalent to the public announcement "None of you has mud on your forehead." 
// cij means that child i testified in the j-th round: "Yes, I know whether I have
// mud on my forehead or not."
// ~cij means that child i testified in the j-th round: "I don't know." 
var f0 : boolean;
var c11, c12, c13 : boolean;
var c21, c22, c23 : boolean;
var c31, c32, c33 : boolean;

// the secret board that caches the testimonies of the children in each round
// before they are published simultaneously on the public board.
var s1, s2, s3 : boolean;

let boardempty = !f0 & !c11 & !c12 & !c13
                     & !c21 & !c22 & !c23
                     & !c31 & !c32 & !c33 & !s1 & !s2 & !s3;

// definition of the epistemic accessibility relations of father and children

agent father = { mud1, mud2, mud3, round, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33             };
agent child1 = {       mud2, mud3, round, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33, s1         };
agent child2 = { mud1,       mud3, round, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33,     s2     };
agent child3 = { mud1, mud2,       round, t1, t2, t3, f0, c11, c12, c13, c21, c22, c23, c31, c32, c33,         s3 };

let kchild1 = (K[child1] mud1) or (K[child1] (~mud1));
let kchild2 = (K[child2] mud2) or (K[child2] (~mud2));
let kchild3 = (K[child3] mud3) or (K[child3] (~mud3));

initial   round = 0 & !t1 & !t2 & !t3 & !s1 & !s2 & !s3
        & !f0 & !c11 & !c12 & !c13
              & !c21 & !c22 & !c23
              & !c31 & !c32 & !c33;

action father_yes
guard round = 0 & (mud1 | mud2 | mud3)
do f0 := true,
   round := round+1,
   t1 := false, t2 := false, t3 := false; 

action father_no
pre round = 0 & !(mud1 | mud2 | mud3)
do f0 := false,
   round := round+1,
   t1 := false, t2 := false, t3 := false;

action child1_yes0
guard kchild1 and (0 < round & round < 4 & !t1 & child1SaidYes != -1)
do s1 := true, t1 := true;

action child1_yes1
guard kchild1 and (0 < round & round < 4 & !t1 & child1SaidYes = -1)
do s1 := true, t1 := true, child1SaidYes := round;

action child1_no
guard ~kchild1 and (0 < round & round < 4 & !t1)
do s1 := false, t1 := true;

action child2_yes
guard kchild2 and (0 < round & round < 4 & !t2)
do s2 := true, t2 := true;

action child2_no
guard ~kchild2 and (0 < round & round < 4 & !t2)
do s2 := false, t2 := true;

action child3_yes
guard kchild3 and (0 < round & round < 4 & !t3)
do s3 := true, t3 := true;

action child3_no
guard ~kchild3 and (0 < round & round < 4 & !t3)
do s3 := false, t3 := true;

action round1_publication
guard round = 1 and alltest
do c11 := s1, c21 := s2, c31 := s3,
   s1 := false, s2 := false, s3 := false,
   round := round+1,
   t1 := false, t2 := false, t3 := false;
       
action round2_publication
guard round = 2 and alltest 
do c12 := s1, c22 := s2, c32 := s3,
   s1 := false, s2 := false, s3 := false,
   round := round+1,
   t1 := false, t2 := false, t3 := false;

action round3_publication
guard round = 3 and alltest
do c13 := s1, c23 := s2, c33 := s3,
   s1 := false, s2 := false, s3 := false,
   round := round+1,
   t1 := false, t2 := false, t3 := false;
       
action stutter
guard round = 4
do;

end;

check initial AG (round = 4 &  mud1 -> child1SaidYes = numMuddy);
check initial AG (round = 4 & !mud1 -> child1SaidYes = numMuddy+1);


