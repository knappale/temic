/**
 * Specification of the Sum-and-Product-Riddle (asymmetric version)
 *
 * Hans van Ditmarsch, Wiebe van der Hoek, Barteld Kooi.
 * Dynamic Epistemic Logic.
 * Springer, 2008.
 * Sect. 4.11
 */

specification sum_and_product;

var a, b : 2..99 initial a <= b;
var s : 4..198 	initial s = a + b;
var p : 4..9801	initial p = a * b;
var step : 1..6 initial step = 1;
var p1, p2, p3, p4, s2: boolean initial !p1 & !p2 & !p3 & !p4 & !s2; 

agent P   = {    p, step, p1, p2, p3, p4     };
agent S   = { s,    step, p1, p2, p3, p4, s2 };

let P_knows_a = (exists x:2..99 . (K[P] a = x));
let P_knows_b = (exists x:2..99 . (K[P] b = x));
let S_knows_a = (exists x:2..99 . (K[S] a = x));
let S_knows_b = (exists x:2..99 . (K[S] b = x));
let S_knows_P_does_not_know_a = K[S] ~(exists x:2..99 . (K[P] a = x));
// let S_knows_P_does_not_know_a = K[S] (forall x:2..99 . ~(K[P] a = x));

action step1_S_yes
epre   S_knows_P_does_not_know_a
pre    step = 1
do     s2 := true, step := step + 1;

action step1_S_no
epre   ~S_knows_P_does_not_know_a
pre    step = 1
do     s2 := false, step := step + 1;

action step2_P_yes
epre   P_knows_a
pre    step = 2
do     p1 := true, step := step + 1;

action step2_P_no
epre   ~P_knows_a
pre    step = 2
do     p1 := false, step := step + 1;

action step3_S_publish
pre    step = 3
do     p2 := s2, step := step + 1;

action step4_P_yes
epre   P_knows_a
pre    step = 4
do     p3 := true, step := step + 1;

action step4_P_no
epre   ~P_knows_a
pre    step = 4
do     p3 := false, step := step + 1;

action step5_S_yes
epre   S_knows_a
pre    step = 5
do     p4 := true, step := step + 1;

action step5_S_no
epre   ~S_knows_a
pre    step = 5
do     p4 := false, step := step + 1;

action stutter
pre    step = 6
do;

end;

check initial EF (step = 6 & !p1 & p2 & p3 & p4);
check initial AG ((step = 6 & !p1 & p2 & p3 & p4) -> a = 4 & b = 13);

check reachable (step = 6 & !p1 & p2 & p3 & p4);
check reachable (step = 6 & !p1 & p2 & p3 & p4 &  (a = 4 & b = 13));
check reachable (step = 6 & !p1 & p2 & p3 & p4 & !(a = 4 & b = 13)); 
