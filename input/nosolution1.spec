/**
 * Ronald Fagin, Joseph Y. Halpern, Yoram Moses, Moshe Y. Vardi.
 * Reasoning About Knowledge.
 * MIT Press, 2003.
 * Exc. 7.5
 */

specification nosolution1;

var b : boolean initial !b;

agent blind = { };
let knowsWontHappen = K[blind] ~b;

action act1
guard knowsWontHappen
do b := true;

end;
