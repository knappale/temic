// Sect 5.3, Ex. 9, rule system R1

specification KMR_R1;

var x : 0..3 initial x = 0;

agent a = { };

action act1
guard K[a] x != 1
pre x = 0
do x := 3;

action act2
guard K[a] x != 3
pre x = 0
do x := 1;

end;
