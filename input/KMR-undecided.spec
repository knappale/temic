// Sect 4.3, Ex. 8

specification KMR_undecided;

var p : boolean initial p <-> true;
var q : boolean initial q <-> false;
// var c : boolean initial q <-> false;

agent a = { q };
agent b = { };

action act
guard (!q) and (K[b] M[a] p)
do q := true; // , c := true;

end;
