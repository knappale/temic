/**
 * Ronald Fagin, Joseph Y. Halpern, Yoram Moses, Moshe Y. Vardi.
 * Reasoning About Knowledge.
 * MIT Press, 2003.
 * Ex. 4.1.1, Ex. 7.1.1
 */
 
specification bit_transmission;

var sbit, ack, rbit, snt : boolean initial (ack | rbit | snt) <-> false;

agent S = { sbit, ack }; agent R = { rbit, snt };

let R_knows_bit = exists bit:boolean . K[R] sbit <-> bit;

action S_sends_bit_ok
guard not K[S] R_knows_bit do rbit := sbit, snt := true; 
action S_sends_bit_failed
guard not K[S] R_knows_bit do ;
action R_sends_ack_ok    
guard R_knows_bit and not K[R] K[S] R_knows_bit do ack := true;
action R_sends_ack_failed
guard R_knows_bit and not K[R] K[S] R_knows_bit do ;

end;

check initial EF           R_knows_bit;
check initial EF      K[S] R_knows_bit;
check initial EF K[R] K[S] R_knows_bit;
