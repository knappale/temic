// Sect 5.3, Ex. 9, rule system R5

specification KMR_R5;

var x : 0..3 initial x = 0;

agent a = { };

action act1
guard K[a] x != 1
pre x = 0
do x := 3;

action act2
guard K[a] x != 3
pre x = 0
do x := 1;

action act3
guard K[a] (x != 1 & x != 2)
pre x = 0
do x := 2;

end;
