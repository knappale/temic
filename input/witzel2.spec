/**
 * Andreas Witzel, Jonathan A. Zvesper.
 * Epistemic Logic and Explicit Knowledge in Distributed Programming.
 * Proc. 7th Intl. Conf. Autonomous Agents and Multiagent Systems (AAMAS 2008).
 * 2008, pp. 1463-1466.
 */

specification witzel2;

var x_a_1 : 0..1;
var x_a_2 : 0..1;
var x_a_b_1 : 0..2 initial x_a_b_1 = 2;
var x_a_b_2 : 0..2 initial x_a_b_2 = 2;
var x_a_c_1 : 0..2 initial x_a_c_1 = 2;
var x_a_c_2 : 0..2 initial x_a_c_2 = 2;
var comm_a_b_1 : bool initial comm_a_b_1 <-> false;
var comm_a_b_2 : bool initial comm_a_b_2 <-> false;
var comm_a_c_1 : bool initial comm_a_c_1 <-> false;
var comm_a_c_2 : bool initial comm_a_c_2 <-> false;

var x_b_1 : 0..1;
var x_b_2 : 0..1;
var x_b_a_1 : 0..2 initial x_b_a_1 = 2;
var x_b_a_2 : 0..2 initial x_b_a_2 = 2;
var x_b_c_1 : 0..2 initial x_b_c_1 = 2;
var x_b_c_2 : 0..2 initial x_b_c_2 = 2;
var comm_b_a_1 : bool initial comm_b_a_1 <-> false;
var comm_b_a_2 : bool initial comm_b_a_2 <-> false;
var comm_b_c_1 : bool initial comm_b_c_1 <-> false;
var comm_b_c_2 : bool initial comm_b_c_2 <-> false;

var x_c_1 : 0..1;
var x_c_2 : 0..1;
var x_c_a_1 : 0..2 initial x_c_a_1 = 2;
var x_c_a_2 : 0..2 initial x_c_a_2 = 2;
var x_c_b_1 : 0..2 initial x_c_b_1 = 2;
var x_c_b_2 : 0..2 initial x_c_b_2 = 2;
var comm_c_a_1 : bool initial comm_c_a_1 <-> false;
var comm_c_a_2 : bool initial comm_c_a_2 <-> false;
var comm_c_b_1 : bool initial comm_c_b_1 <-> false;
var comm_c_b_2 : bool initial comm_c_b_2 <-> false;

agent alice   = { x_a_1, x_a_2, x_a_b_1, x_a_b_2, x_a_c_1, x_a_c_2, comm_a_b_1, comm_a_b_2, comm_a_c_1, comm_a_c_2 };
agent bob     = { x_b_1, x_b_2, x_b_a_1, x_b_a_2, x_b_c_1, x_b_c_2, comm_b_a_1, comm_b_a_2, comm_b_c_1, comm_b_c_2 };
agent charlie = { x_c_1, x_c_2, x_c_a_1, x_c_a_2, x_c_b_1, x_c_b_2, comm_c_a_1, comm_c_a_2, comm_c_b_1, comm_c_b_2 };

action m_a2b_1
do x_b_a_1 := x_a_1, comm_a_b_1 := true;

action m_a2b_2
do x_b_a_2 := x_a_2, comm_a_b_2 := true;

action m_a2c_1
do x_c_a_1 := x_a_1, comm_a_c_1 := true;

action m_a2c_2
do x_c_a_2 := x_a_2, comm_a_c_2 := true;

action m_b2a_1
do x_a_b_1 := x_b_1, comm_b_a_1 := true;

action m_b2a_2
do x_a_b_2 := x_b_2, comm_b_a_2 := true;

action m_b2c_1
do x_c_b_1 := x_b_1, comm_b_c_1 := true;

action m_b2c_2
do x_c_b_2 := x_b_2, comm_b_c_2 := true;

action m_c2a_1
do x_a_c_1 := x_c_1, comm_c_a_1 := true;

action m_c2a_2
do x_a_c_2 := x_c_2, comm_c_a_2 := true;

action m_c2b_1
do x_b_c_1 := x_c_1, comm_c_b_1 := true;

action m_c2b_2
do x_b_c_2 := x_c_2, comm_c_b_2 := true;

end;

check initial AG exists bit:0..1 . K[alice] x_a_1 = bit ;
