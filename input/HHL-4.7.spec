/**
 * Hendrik Wietze de Haan, Wim H. Hesselink, Gerard R. Renardel de Lavalette.
 * Knowledge-Based Asynchronous Programming.
 * Fundamenta Informaticae 63(2-3), 2004, pp. 259-281.
 * Ex. Sect. 4.7
 */

specification HHL_4_7;

var day : 0..5 initial day = 0;
var exam : 0..4;
var written : bool initial written <-> false;

agent P = { day, written };

action act1
guard day < 5 and (day = exam) and (not K[P] day = exam) and not written
do written := true, day := day+1;

action act2
guard day < 5 and (day != exam)
do day := day+1;

action stutter
do;

end;

check reachable exam = 2 & written;
check initial EF written;