/**
 * Sect 6.3, last example (right)
 *
 * x = y = 0
 * r1 = r2 = r3 = 0
 * -----------------------------------------------------
 * Thread 1        | Thread 2        | Thread 3
 * -----------------------------------------------------
 * r1 := x;        | r2 := x;        | r3 := y;        
 * if (r1 == 0)    | y := r2;        | x := r3;
 *   x := 1;       |                 |  
 * -----------------------------------------------------
 * r1 = r2 = r3 = 1?
 */
specification switch;

var x : 0..2 initial x = 0;
var y : 0..2 initial y = 0;
var r1 : 0..2 initial r1 = 0;
var r2 : 0..2 initial r2 = 0;
var r3 : 0..2 initial r3 = 0;
var step1 : 1..3 initial step1 = 1;
var step2 : 1..3 initial step2 = 1;
var step3 : 1..3 initial step3 = 1;

agent t1 = { step1, r1 };
agent t2 = { step2, r2 };
agent t3 = { step3, r3 };

action a11_1
// guard K[t1] (EF x = 1)
guard K[t1] ((EF (r1 = 0 & x = 1)) and (EF (r1 = 1 & x = 1)))
pre   step1 = 1
do    r1 := 1, step1 := 2;

action a11_2
// guard K[t1] (EF x = 2)
guard K[t1] ((EF (r1 = 0 & x = 2)) and (EF (r1 = 2 & x = 2)))
pre   step1 = 1
do    r1 := 2, step1 := 2;

action a11
pre   step1 = 1
do    r1 := x, step1 := 2;

action a12
pre   step1 = 2 and r1 = 0
do    x := 1, step1 := 3;

action a12n
pre   step1 = 2 and r1 != 0
do    step1 := 3;

action stutter1
pre   step1 = 3
do    ;

// -----------

action a21_1
// guard K[t2] (EF x = 1)
guard K[t2] ((EF (r2 = 0 & x = 1)) and (EF (r2 = 1 & x = 1)))
pre   step2 = 1
do    r2 := 1, step2 := 2;

action a21_2
// guard K[t2] (EF x = 2)
guard K[t2] ((EF (r2 = 0 & x = 2)) and (EF (r2 = 2 & x = 2)))
pre   step2 = 1
do    r2 := 2, step2 := 2;

action a21
pre   step2 = 1
do    r2 := x, step2 := 2;

action a22
pre   step2 = 2
do    y := r2, step2 := 3;

action stutter2
pre   step2 = 3
do    ;

// -----------

action a31_1
// guard K[t3] (EF y = 1)
guard K[t3] ((EF (r3 = 0 & y = 1)) and (EF (r3 = 1 & y = 1)))
pre   step3 = 1
do    r3 := 1, step3 := 2;

action a31_2
// guard K[t3] (EF y = 2)
guard K[t3] ((EF (r3 = 0 & y = 2)) and (EF (r3 = 2 & y = 2)))
pre   step3 = 1
do    r3 := 2, step3 := 2;

action a31
pre   step3 = 1
do    r3 := y, step3 := 2;

action a32
pre   step3 = 2
do    x := r3, step3 := 3;

action stutter3
pre   step3 = 3
do    ;

end;

check initial EF (r1 = 0 & r2 = 0 & r3 = 0);
check initial EF (r1 = 1 & r2 = 1 & r3 = 1);

check reachable (step1 = 3 & step2 = 3 & step3 = 3 & r1 = 0 & r2 = 0 & r3 = 0);
check reachable (step1 = 3 & step2 = 3 & step3 = 3 & r1 = 1 & r2 = 1 & r3 = 1);
