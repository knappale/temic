/**
 * Sect. 6.3, first example (left)
 *
 * x = y = 0
 * r1 = r2 = 0
 * ----------------------------------
 * Thread 1        | Thread 2
 * ----------------------------------
 * r1 := x;        | r2 := y;
 * y := 1;         | x := 1;
 * ----------------------------------
 * r1 = r2 = 1?
 */
specification simple;

var x, y, r1, r2 : 0..2 initial x = 0 & y = 0 & r1 = 0 & r2 = 0;
var step1, step2 : 1..3 initial step1 = 1 & step2 = 1;
// var step2 : 1..3 initial step2 = 1;

agent t1 = { step1, r1 };
agent t2 = { step2, r2 };

action K1_0
guard step1 = 1 and K[t1] ((EF (r1 = 0 & x = 0)) and (EF (r1 = 0 & x = 0)))
// guard step1 = 1 and K[t1] ((EF (r1 = 0 & x = 0)) and (AX r1 = 0 implies (AF (r1 = 0 & x = 0))))
do    r1 := 0, step1 := 2;

action K1_1
guard step1 = 1 and K[t1] ((EF (r1 = 0 & x = 1)) and (EF (r1 = 1 & x = 1)))
// guard step1 = 1 and K[t1] ((EF (r1 = 0 & x = 1)) and (AX r1 = 1 implies (AF (r1 = 1 & x = 1))))
do    r1 := 1, step1 := 2;

action K1_2
guard step1 = 1 and K[t1] ((EF (r1 = 0 & x = 2)) and (EF (r1 = 2 & x = 2)))
// guard step1 = 1 and K[t1] ((EF (r1 = 0 & x = 2)) and (AX r1 = 2 implies (AF (r1 = 2 & x = 2))))
do    r1 := 2, step1 := 2;

action nK1
guard step1 = 1
do    r1 := x, step1 := 2;

action a1
guard step1 = 2
do    y := 1, step1 := 3;

action stutter1
guard step1 = 3
do    ;

// -----------

action K2_0
guard step2 = 1 and K[t2] ((EF (r2 = 0 & y = 0)) and (EF (r2 = 0 & y = 0)))
// guard step2 = 1 and K[t2] ((EF (r2 = 0 & y = 0)) and (AX r2 = 0 implies (AF (r2 = 0 & y = 0))))
do    r2 := 0, step2 := 2;

action K2_1
guard step2 = 1 and K[t2] ((EF (r2 = 0 & y = 1)) and (EF (r2 = 1 & y = 1)))
// guard step2 = 1 and K[t2] ((EF (r2 = 0 & y = 1)) and (AX r2 = 1 implies (AF (r2 = 1 & y = 1))))
do    r2 := 1, step2 := 2;

action K2_2
guard step2 = 1 and K[t2] ((EF (r2 = 0 & y = 2)) and (EF (r2 = 2 & y = 2)))
// guard step2 = 1 and K[t2] ((EF (r2 = 0 & y = 2)) and (AX r2 = 2 implies (AF (r2 = 2 & y = 2))))
do    r2 := 2, step2 := 2;

action nK2
guard step2 = 1
do    r2 := y, step2 := 2;

action a2
guard step2 = 2
do    x := 1, step2 := 3;

action stutter2
guard step2 = 3
do    ;

end;

check initial EF (r1 = 0 & r2 = 0); // holds
check initial EF (r1 = 1 & r2 = 1); // holds
check initial EF (r1 = 2 | r2 = 2); // does not hold

check reachable (step1 = 3 & step2 = 3 & r1 = 0 & r2 = 0); // holds
check reachable (step1 = 3 & step2 = 3 & r1 = 1 & r2 = 1); // holds
