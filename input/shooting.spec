specification shooting;

var dead, loaded : boolean initial !dead & loaded;

agent H = { };

action H_pull
guard (not K[H] dead) do loaded := false, dead := dead | loaded;
action H_load_unloaded
guard (not K[H] dead) and !loaded do loaded := true;

end;

// check initial EF dead;
check reachable dead;
