// Sect 5.3, Ex. 9, rule system R3

specification KMR_R3;

var y : 0..2 initial y = 0;

agent a = { };

action act1
guard K[a] y != 1 & y != 2
pre y = 0
do y := 2;

end;
