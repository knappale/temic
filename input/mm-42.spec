/**
 * Sect. 6.3, last example (left)
 *
 * x = y = 0
 * r1 = r2 = r3 = 0
 * ----------------------------------
 * Thread 1        | Thread 2
 * ----------------------------------
 * r1 := x;        | r3 := y;
 * if (r1 == 0)    | x := r3;
 *   x := 1        |
 * r2 := x;        |
 * y := r2;        |
 * ----------------------------------
 * r1 = r2 = r3 = 1?
 */
specification te;

var x : 0..2 initial x = 0;
var y : 0..2 initial y = 0;
var r1 : 0..1 initial r1 = 0;
var r2 : 0..1 initial r2 = 0;
var r3 : 0..2 initial r3 = 0;
var step1 : 1..5 initial step1 = 1;
var step2 : 1..3 initial step2 = 1;

agent t1 = { step1, r1, r2 };
agent t2 = { step2, r3 };

action a11_1
guard K[t1] ((EF (r1 = 0 & x = 1)) and (EF (r1 = 1 & x = 1)))
pre  step1 = 1
do   r1 := 1, step1 := 2;

action a11_2
guard K[t1] ((EF (r1 = 0 & x = 2)) and (EF (r1 = 2 & x = 2)))
pre  step1 = 1
do   r1 := 2, step1 := 2;

action a11_n
pre  step1 = 1
do   r1 := x, step1 := 2;

action a12
pre  step1 = 2 & r1 = 0
do   x := 1, step1 := 3;

action a12n
pre  step1 = 2 & r1 != 0
do   step1 := 3;

action a13_1
guard K[t1] ((EF (r2 = 0 & x = 1)) and (EF (r2 = 1 & x = 1)))
pre  step1 = 3
do   r2 := 1, step1 := 4;

action a13_2
guard K[t1] ((EF (r2 = 0 & x = 2)) and (EF (r2 = 2 & x = 2)))
pre  step1 = 3
do   r2 := 2, step1 := 4;

action a13_n
pre  step1 = 3
do   r2 := x, step1 := 4;

action a14
pre  step1 = 4
do   y := r2, step1 := 5;

action stutter1
pre  step1 = 5
do   ;

// -----------

action a21_1
guard K[t2] ((EF (r3 = 0 & y = 1)) and (EF (r3 = 1 & y = 1)))
pre  step2 = 1
do   r3 := 1, step2 := 2;

action a21_2
guard K[t2] ((EF (r3 = 0 & y = 2)) and (EF (r3 = 2 & y = 2)))
pre  step2 = 1
do   r3 := 2, step2 := 2;

action a21n
pre  step2 = 1
do   r3 := y, step2 := 2;

action a22
pre  step2 = 2
do   x := r3, step2 := 3;

action stutter2
pre  step2 = 3
do   ;

end;

check reachable (step1 = 5 & step2 = 3 & r1 = 0 & r2 = 0 & r3 = 0); // holds
check reachable (step1 = 5 & step2 = 3 & r1 = 1 & r2 = 1 & r3 = 1); // holds
check reachable (step1 = 5 & r3 = 1); // holds
