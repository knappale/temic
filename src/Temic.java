import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import parser.generated.ParseException;
import parser.generated.TemicParser;
import parser.generated.TokenMgrError;
import util.Formatter;


public class Temic {
  public static void main(String[] args) {
    jdd.util.Options.verbose = false;
    java.util.Locale.setDefault(java.util.Locale.US);
    logic.KripkeStructure.logger.setLevel(java.util.logging.Level.INFO);
    system.InterpretedSystem.logger.setLevel(java.util.logging.Level.INFO);
    system.Specification.logger.setLevel(java.util.logging.Level.INFO);

    if (args == null || args.length == 0) {
      usage();
      return;
    }

    var inputFile = args[0];
    if (inputFile.trim().equals("-d")) {
      logic.KripkeStructure.logger.setLevel(java.util.logging.Level.FINE);
      system.InterpretedSystem.logger.setLevel(java.util.logging.Level.FINE);
      system.Specification.logger.setLevel(java.util.logging.Level.FINE);
      if (args.length == 1) {
        usage();
        return;
      }
      inputFile = args[1];
    }

    try {
      var rdr = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
      var parser = new TemicParser(rdr);

      var specification = parser.specification();
      var interpretedSystem = specification.interpret();
      for (var assertion : specification.getAssertions())
        System.out.println(assertion.check(interpretedSystem));
    }
    catch (FileNotFoundException fnfe) {
      System.err.println("Error opening file " + Formatter.quoted(inputFile) + ": " + fnfe.getMessage());
      System.exit(-1);
    }
    catch (TokenMgrError tme) {
      System.err.println("Error parsing file " + Formatter.quoted(inputFile) + ": " + tme.getMessage());
      System.exit(-1);
    }
    catch (ParseException pe) {
      System.err.println("Error parsing file " + Formatter.quoted(inputFile) + ": " + pe.getMessage());
      System.exit(-1);
    }
  }

  public static void usage() {
    System.err.println("Usage: java Temic [-d] <spec>");
    System.exit(-1);
  }
}
