package logic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import state.StateVariable;
import state.TransitionRelation;
import system.Declaration;
import util.Formatter;


@NonNullByDefault
public class Agent implements Declaration {
  private static final Map<Agent, TransitionRelation> accessibilityRelations = new HashMap<>();

  private final String name;
  private final Set<StateVariable> observables = new HashSet<>();

  public Agent(String name, Set<StateVariable> observables) {
    this.name = name;
    this.observables.addAll(observables);
  }

  @Override
  public String getName() {
    return this.name;
  }

  public Set<StateVariable> getObservables() {
    return this.observables;
  }

  /**
   * @return the accessibility relation of this agent
   */
  public TransitionRelation getAccessibilityRelation() {
    var accessibilityRelation = accessibilityRelations.get(this);
    if (accessibilityRelation != null)
      return accessibilityRelation;

    accessibilityRelation = TransitionRelation.id(this.getObservables());
    accessibilityRelations.put(this, accessibilityRelation);
    return accessibilityRelation;
  }

  @Override
  public String declaration() {
    var builder = new StringBuilder();
    builder.append("agent ");
    builder.append(this.getName());
    builder.append(" = ");
    builder.append(Formatter.set(this.observables, v -> v.getName()));
    builder.append(";");
    return builder.toString();
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}