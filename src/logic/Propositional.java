package logic;

import java.util.ArrayList;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.StateSet;
import state.Variable;
import system.Run;
import util.Formatter;


@NonNullByDefault
public interface Propositional<T extends Formula> extends Formula {
  public interface Visitor<T extends Formula, U> {
    public U onFalse();
    public U onTrue();
    public U onId(T inner);
    public U onNot(T inner);
    public U onAnd(T left, T right);
    public U onOr(T left, T right);
    public U onImplies(T left, T right);
    public U onEquiv(T left, T right);
    public U onQuantified(Quantified.Kind kind, List<Variable> boundVariables, T matrix);
  }

  public abstract <U> U accept(Visitor<T, U> visitor);

  public abstract Propositional<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF);

  @SuppressWarnings("unchecked")
  public default Propositional<T> neg() {
    return this.neg(f -> (T)f.neg(), f -> (T)f.nnf());
  }

  public abstract Propositional<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF);

  @SuppressWarnings("unchecked")
  public default Propositional<T> nnf() {
    return this.nnf(f -> (T)f.neg(), f -> (T)f.nnf());
  }

  public abstract StateSet satisfyingStates(Function<T, @NonNull StateSet> inner);

  public abstract @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner);
  public default @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
    return this.neg().getExample(start, inner);
  }

  public abstract <U extends Formula> Propositional<U> convert(Function<T, U> inner);

  public abstract String toString();

  @SuppressWarnings("unchecked")
  public static <T extends Formula> False<T> falseFrm() {
    return False.FALSEFRM;
  }

  @SuppressWarnings("unchecked")
  public static <T extends Formula> True<T> trueFrm() {
    return True.TRUEFRM;
  }

  public static class False<T extends Formula> implements Propositional<T> {
    @SuppressWarnings("rawtypes")
    private static final Propositional.False FALSEFRM = new Propositional.False();

    public False() {
    }

    @Override
    public True<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return trueFrm();
    }

    @Override
    public False<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return this;
    }

    @Override
    public boolean isPropositional() {
      return true;
    }

    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return StateSet.empty();
    }

    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return null;
    }

    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      if (start.isEmpty())
        return null;
      return new Run(start.any());
    }

    @Override
    public <U extends Formula> False<U> convert(Function<T, U> inner) {
      return falseFrm();
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onFalse();
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return (this.getClass() == object.getClass());
    }

    @Override
    public String toString() {
      return "false";
    }
  }

  public static class True<T extends Formula> implements Propositional<T> {
    @SuppressWarnings("rawtypes")
    private static final Propositional.True TRUEFRM = new Propositional.True();

    public True() {
    }

    @Override
    public False<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return falseFrm();
    }

    @Override
    public True<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return this;
    }

    @Override
    public boolean isPropositional() {
      return true;
    }

    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return StateSet.all();
    }

    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      if (start.isEmpty())
        return null;
      return new Run(start.any());
    }

    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return null;
    }

    @Override
    public <U extends Formula> True<U> convert(Function<T, U> inner) {
      return trueFrm();
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onTrue();
    }

    @Override
    public int hashCode() {
      return 1;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return (this.getClass() == object.getClass());
    }

    @Override
    public String toString() {
      return "true";
    }
  }

  public static class Id<T extends Formula> implements Propositional<T> {
    private final T formula;

    public Id(T formula) {
      this.formula = formula;
    }

    @Override
    public Id<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new Id<>(innerNeg.apply(this.formula));
    }

    @SuppressWarnings("null")
    @Override
    public Id<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var formulaNNF = innerNNF.apply(this.formula);
      if (this.formula.equals(formulaNNF))
        return this;
      return new Id<>(formulaNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.formula.isPropositional();
    }

    @SuppressWarnings("null")
    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return inner.apply(this.formula);
    }

    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return inner.apply(this.formula, start);
    }

    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return inner.apply(this.formula, start);
    }

    @Override
    public <U extends Formula> Id<U> convert(Function<T, U> inner) {
      return new Id<>(inner.apply(this.formula));
    }

    @Override
    public int getPrecedence() {
      return 1;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onId(this.formula);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.formula);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() == object.getClass()) {
        Id<?> other = (Id<?>)object;
        return Objects.equals(this.formula, other.formula);
      }
      return this.formula.equals(object);
    }

    @Override
    public String toString() {
      return new StringBuilder(this.formula.toString()).toString();
    }
  }

  public static class Not<T extends Formula> implements Propositional<T> {
    private final T formula;

    public Not(T formula) {
      this.formula = formula;
    }

    @Override
    public Id<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new Id<>(innerNNF.apply(this.formula));
    }

    @Override
    public Id<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new Id<>(innerNeg.apply(this.formula));
    }

    @Override
    public boolean isPropositional() {
      return this.formula.isPropositional();
    }

    @SuppressWarnings({ "null", "unchecked" })
    @Override
    public StateSet satisfyingStates(Function<T, @NonNull StateSet> inner) {
      return inner.apply((T)this.formula.neg());
      // return inner.apply(this.formula).complement();
    }

    @SuppressWarnings("unchecked")
    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return inner.apply((T)this.formula.neg(), start);
    }

    @SuppressWarnings("unchecked")
    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return inner.apply((T)this.formula.neg(), start);
    }

    @Override
    public <U extends Formula> Not<U> convert(Function<T, U> inner) {
      return new Not<>(inner.apply(this.formula));
    }

    @Override
    public int getPrecedence() {
      return 2;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onNot(this.formula);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.formula);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      Not<?> other = (Not<?>)object;
      return Objects.equals(this.formula, other.formula);
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append("not ");
      builder.append(Formatter.parenthesised(this.formula, f -> f.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class And<T extends Formula> implements Propositional<T> {
    private final T left;
    private final T right;

    public And(T left, T right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Or<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new Or<>(innerNeg.apply(this.left), innerNeg.apply(this.right));
    }

    @SuppressWarnings("null")
    @Override
    public And<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var leftNNF = innerNNF.apply(this.left);
      var rightNNF = innerNNF.apply(this.right);
      if (this.left.equals(leftNNF) && this.right.equals(rightNNF))
        return this;
      return new And<>(leftNNF, rightNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.left.isPropositional() && this.right.isPropositional();
    }

    @SuppressWarnings("null")
    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return inner.apply(this.left).intersection(inner.apply(this.right));
    }

    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      var leftRun = inner.apply(this.left, start);
      // Only a run consisting of a single (initial) state can be continued
      if (leftRun != null && leftRun.length() == 0)
        return inner.apply(this.right, start.intersection(leftRun.getInitialState().singleton()));
      var rightRun = inner.apply(this.left, start);
      // Only a run consisting of a single (initial) state can be continued
      if (rightRun != null && rightRun.length() == 0)
        return inner.apply(this.left, start.intersection(rightRun.getInitialState().singleton()));
      return null;
    }

    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      var leftRun = inner.apply(this.left, start);
      if (leftRun != null)
        return leftRun;
      return inner.apply(this.right, start);
    }

    @Override
    public <U extends Formula> And<U> convert(Function<T, U> inner) {
      return new And<>(inner.apply(this.left), inner.apply(this.right));
    }

    @Override
    public int getPrecedence() {
      return 11;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onAnd(this.left, this.right);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      And<?> other = (And<?>)object;
      return  Objects.equals(this.left, other.left) &&
              Objects.equals(this.right, other.right);
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      builder.append(" and ");
      builder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Or<T extends Formula> implements Propositional<T> {
    private final T left;
    private final T right;

    public Or(T left, T right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public And<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new And<>(innerNeg.apply(this.left), innerNeg.apply(this.right));
    }

    @SuppressWarnings("null")
    @Override
    public Or<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var leftNNF = innerNNF.apply(this.left);
      var rightNNF = innerNNF.apply(this.right);
      if (this.left.equals(leftNNF) && this.right.equals(rightNNF))
        return this;
      return new Or<>(leftNNF, rightNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.left.isPropositional() && this.right.isPropositional();
    }

    @SuppressWarnings("null")
    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return inner.apply(this.left).union(inner.apply(this.right));
    }

    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      var leftRun = inner.apply(this.left, start);
      if (leftRun != null)
        return leftRun;
      return inner.apply(this.right, start);
    }

    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      var leftRun = inner.apply(this.left, start);
      // TODO check duality with and
      if (leftRun != null && leftRun.length() == 0)
        return inner.apply(this.right, start.intersection(leftRun.getInitialState().singleton()));
      var rightRun = inner.apply(this.left, start);
      if (rightRun != null && rightRun.length() == 0)
        return inner.apply(this.left, start.intersection(rightRun.getInitialState().singleton()));
      return null;
    }

    @Override
    public <U extends Formula> Or<U> convert(Function<T, U> inner) {
      return new Or<>(inner.apply(this.left), inner.apply(this.right));
    }

    @Override
    public int getPrecedence() {
      return 12;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onOr(this.left, this.right);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      Or<?> other = (Or<?>)object;
      return  Objects.equals(this.left, other.left) &&
              Objects.equals(this.right, other.right);
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      builder.append(" or ");
      builder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Implies<T extends Formula> implements Propositional<T> {
    private final T left;
    private final T right;

    public Implies(T left, T right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public And<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new And<T>(innerNNF.apply(this.left), innerNeg.apply(this.right));
    }

    @SuppressWarnings("null")
    @Override
    public Or<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var leftNegNNF = innerNNF.apply(innerNeg.apply(this.left));
      var rightNNF = innerNNF.apply(this.right);
      return new Or<>(leftNegNNF, rightNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.left.isPropositional() && this.right.isPropositional();
    }

    @SuppressWarnings({ "null", "unchecked" })
    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return inner.apply((T)this.left.neg()).union(inner.apply(this.right));
      // return inner.apply(this.left).complement().union(inner.apply(this.right));
    }

    @SuppressWarnings("unchecked")
    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return new Or<>((T)this.left.neg(), this.right).getExample(start, inner);
    }

    @SuppressWarnings("unchecked")
    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return new Or<>((T)this.left.neg(), this.right).getCounterExample(start, inner);
    }

    @Override
    public <U extends Formula> Implies<U> convert(Function<T, U> inner) {
      return new Implies<>(inner.apply(this.left), inner.apply(this.right));
    }

    @Override
    public int getPrecedence() {
      return 13;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onImplies(this.left, this.right);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      Implies<?> other = (Implies<?>)object;
      return  Objects.equals(this.left, other.left) &&
              Objects.equals(this.right, other.right);
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      builder.append(" implies ");
      builder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Equiv<T extends Formula> implements Propositional<T> {
    private final T left;
    private final T right;

    public Equiv(T left, T right) {
      this.left = left;
      this.right = right;
    }

    @SuppressWarnings({ "unchecked", "null" })
    @Override
    public Or<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var leftNNF = innerNNF.apply(this.left);
      var leftNegNNF = innerNNF.apply(innerNeg.apply(this.left));
      var rightNNF = innerNNF.apply(this.right);
      var rightNegNNF = innerNNF.apply(innerNeg.apply(this.right));
      return new Or<>((T)new And<T>(leftNNF, rightNegNNF),
                      (T)new And<T>(rightNNF, leftNegNNF));
    }

    @SuppressWarnings({ "null", "unchecked" })
    @Override
    public And<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var leftNNF = innerNNF.apply(this.left);
      var leftNegNNF = innerNNF.apply(innerNeg.apply(this.left));
      var rightNNF = innerNNF.apply(this.right);
      var rightNegNNF = innerNNF.apply(innerNeg.apply(this.right));
      return new And<>((T)new Or<T>(leftNegNNF, rightNNF), (T)new Or<T>(rightNegNNF, leftNNF));
    }

    @Override
    public boolean isPropositional() {
      return this.left.isPropositional() && this.right.isPropositional();
    }

    @SuppressWarnings({ "null", "unchecked" })
    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      return inner.apply((T)this.left.neg()).union(inner.apply(this.right)).intersection(inner.apply((T)this.right.neg()).union(inner.apply(this.left)));
    }

    @SuppressWarnings("unchecked")
    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return new And<>((T)new Or<>((T)this.left.neg(), this.right),
                       (T)new Or<>((T)this.right.neg(), this.left)).getExample(start, inner);
    }

    @SuppressWarnings("unchecked")
    @Override
    public @Nullable Run getCounterExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      return new And<>((T)new Or<>((T)this.left.neg(), this.right),
                       (T)new Or<>((T)this.right.neg(), this.left)).getCounterExample(start, inner);
    }

    @Override
    public <U extends Formula> Equiv<U> convert(Function<T, U> inner) {
      return new Equiv<>(inner.apply(this.left), inner.apply(this.right));
    }

    @Override
    public int getPrecedence() {
      return 14;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onEquiv(this.left, this.right);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      Equiv<?> other = (Equiv<?>)object;
      return  Objects.equals(this.left, other.left) &&
              Objects.equals(this.right, other.right);
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      builder.append(" equiv ");
      builder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Quantified<T extends Formula> implements Propositional<T> {
    public enum Kind {
      FORALL, EXISTS;

      public Kind neg() {
        switch (this) {
          case FORALL:
            return EXISTS;
          case EXISTS:
            return FORALL;
          default:
            throw new RuntimeException("Unknown quantifier type.");
        }
      }

      public String toString() {
        switch (this) {
          case FORALL:
            return "forall";
          case EXISTS:
            return "exists";
          default:
            throw new RuntimeException("Unknown quantifier type.");
        }
      }
    }

    private final Kind kind;
    private final List<Variable> boundVariables = new ArrayList<>();
    private final T matrix;

    public Quantified(Kind kind, Collection<Variable> boundVariables, T matrix) {
      this.kind = kind;
      this.boundVariables.addAll(boundVariables);
      this.matrix = matrix;
    }

    @Override
    public Quantified<T> neg(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      return new Quantified<T>(this.kind.neg(), this.boundVariables, innerNeg.apply(this.matrix));
    }

    @SuppressWarnings("null")
    @Override
    public Quantified<T> nnf(Function<T, T> innerNeg, Function<T, T> innerNNF) {
      var matrixNNF = innerNNF.apply(this.matrix);
      if (this.matrix.equals(matrixNNF))
        return this;
      return new Quantified<T>(this.kind, this.boundVariables, matrixNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.matrix.isPropositional();
    }

    @SuppressWarnings("null")
    @Override
    public StateSet satisfyingStates(Function<T, StateSet> inner) {
      var extended = StateSet.declareVariables(this.boundVariables);
      var satisfyingStates = inner.apply(this.matrix);
      switch (this.kind) {
        case FORALL:
          satisfyingStates = extended.complement().union(satisfyingStates).forall(this.boundVariables);
          break;
        case EXISTS:
          satisfyingStates = extended.intersection(satisfyingStates).exists(this.boundVariables);
          break;
      }
      StateSet.undeclareVariables(this.boundVariables);
      return satisfyingStates;
    }

    @Override
    public @Nullable Run getExample(StateSet start, BiFunction<T, StateSet, @Nullable Run> inner) {
      var extended = StateSet.declareVariables(boundVariables);
      var extendedRun = inner.apply(this.matrix, start);
      if (extendedRun != null && extendedRun.length() == 0) {
        var extendedStart = extendedRun.getInitialState().singleton();
        switch (this.kind) {
          case FORALL:
            extendedRun = new Run(extended.complement().union(extendedStart).forall(this.boundVariables).any());
            break;
          case EXISTS:
            extendedRun = new Run(extended.intersection(extendedStart).exists(this.boundVariables).any());
            break;
        }
      }
      else
        extendedRun = null;
      StateSet.undeclareVariables(boundVariables);
      return extendedRun;
    }

    @Override
    public <U extends Formula> Quantified<U> convert(Function<T, U> inner) {
      return new Quantified<>(this.kind, this.boundVariables, inner.apply(this.matrix));
    }

    @Override
    public int getPrecedence() {
      return 8;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onQuantified(this.kind, this.boundVariables, this.matrix);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.kind, this.boundVariables, this.matrix);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      Quantified<?> other = (Quantified<?>)object;
      return  this.kind == other.kind &&
              Objects.equals(this.boundVariables, other.boundVariables) &&
              Objects.equals(this.matrix, other.matrix);
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.kind);
      builder.append(" ");
      builder.append(Formatter.separated(this.boundVariables, Variable::boundDeclaration));
      builder.append(" . ");
      builder.append(this.matrix);
      return builder.toString();
    }
  }
}
