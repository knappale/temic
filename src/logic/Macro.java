package logic;

import org.eclipse.jdt.annotation.NonNullByDefault;

import state.StateSet;
import system.Declaration;


@NonNullByDefault
public interface Macro extends Formula, Declaration {
  public String getName();
  public Formula getFormula();

  @Override
  public default int getPrecedence() {
    return 1;
  }

  public static class State extends StateFormula implements Macro {
    private final String name;
    private final StateFormula formula;

    public State(String name, StateFormula formula) {
      this.name = name;
      this.formula = formula;
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public StateFormula getFormula() {
      return this.formula;
    }

    @Override
    public StateFormula neg() {
      return this.formula.neg();
    }

    @Override
    public StateFormula nnf() {
      return this.formula.nnf();
    }

    @Override
    public boolean isPropositional() {
      return this.formula.isPropositional();
    }

    @Override
    public StateSet satisfyingStates() {
      return this.formula.satisfyingStates();
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
      return this.formula.accept(visitor);
    }

    @Override
    public int getPrecedence() {
      return 1;
    }
  }

  public static class Epistemic extends EpistemicFormula implements Macro {
    private final String name;
    private final EpistemicFormula formula;

    public Epistemic(String name, EpistemicFormula formula) {
      this.name = name;
      this.formula = formula;
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public EpistemicFormula getFormula() {
      return this.formula;
    }

    @Override
    public EpistemicFormula neg() {
      return this.formula.neg();
    }

    @Override
    public EpistemicFormula nnf() {
      return this.formula.nnf();
    }

    @Override
    public boolean isPropositional() {
      return this.formula.isPropositional();
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return this.formula.toTemporalEpistemic();
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
      return this.formula.accept(visitor);
    }

    @Override
    public String toString() {
      return this.name;
    }
  }

  @Override
  public default String declaration() {
    StringBuilder builder = new StringBuilder();
    builder.append("let ");
    builder.append(this.getName());
    builder.append(" = ");
    builder.append(this.getFormula());
    builder.append(";");
    return builder.toString();
  }
}
