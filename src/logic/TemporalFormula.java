package logic;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.Expression;


@NonNullByDefault
public abstract class TemporalFormula implements Formula {
  public interface Visitor<T> {
    public T onAtom(Expression.Boolean formula);
    public T onPropositional(logic.Propositional<TemporalFormula> formula);
    public T onTemporal(logic.Temporal<TemporalFormula> formula);
  }

  @NonNullByDefault({})
  public static class Cases<T> implements Visitor<T> {
    private Function<Expression.@NonNull Boolean, T> atomFun = null;
    private Function<logic.@NonNull Propositional<@NonNull TemporalFormula>, T> propositionalFun = null;
    private Function<logic.@NonNull Temporal<@NonNull TemporalFormula>, T> temporalFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public @NonNull Cases<T> atom(Function<Expression.@NonNull Boolean, T> atomFun) {
      this.atomFun = atomFun;
      return this;
    }
    
    public @NonNull Cases<T> propositional(Function<logic.@NonNull Propositional<@NonNull TemporalFormula>, T> propositionalFun) {
      this.propositionalFun = propositionalFun;
      return this;
    }
    
    public @NonNull Cases<T> temporal(Function<logic.@NonNull Temporal<@NonNull TemporalFormula>, T> temporalFun) {
      this.temporalFun = temporalFun;
      return this;
    }
    
    public @NonNull Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply(TemporalFormula formula) {
      return formula.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on variable";
      return this.otherwiseFun.get();
    }

    public T onAtom(Expression.@NonNull Boolean formula) {
      return this.atomFun != null ? this.atomFun.apply(formula) : otherwise();
    }

    public T onPropositional(logic.@NonNull Propositional<@NonNull TemporalFormula> formula) {
      return this.propositionalFun != null ? this.propositionalFun.apply(formula) : otherwise();
    }

    public T onTemporal(logic.@NonNull Temporal<@NonNull TemporalFormula> formula) {
      return this.temporalFun != null ? this.temporalFun.apply(formula) : otherwise();
    }
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public abstract TemporalEpistemicFormula toTemporalEpistemic();

  public abstract TemporalFormula neg();
  public abstract TemporalFormula nnf();

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      TemporalFormula other = (TemporalFormula)object;
      return Objects.equals(this.toString(), other.toString());
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  public abstract String toString();

  public static class Atom extends TemporalFormula {
    private final Expression.Boolean atom;

    public Atom(Expression.Boolean atom) {
      this.atom = atom;
    }

    @Override
    public Atom neg() {
      return new Atom(this.atom.neg());
    }

    @Override
    public Atom nnf() {
      var atomNNF = this.atom.nnf();
      if (this.atom.equals(atomNNF))
        return this;
      return new Atom(atomNNF);
    }

    @Override
    public boolean isPropositional() {
      return true;
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return new TemporalEpistemicFormula.Atom(this.atom);
    }

    @Override
    public int getPrecedence() {
      return this.atom.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalFormula.Visitor<T> visitor) {
      return visitor.onAtom(this.atom);
    }

    @Override
    public String toString() {
      return this.atom.toString();
    }
  }

  public static class Propositional extends TemporalFormula {
    private final logic.Propositional<TemporalFormula> propositional;

    public Propositional(logic.Propositional<TemporalFormula> propositional) {
      this.propositional = propositional;
    }

    @Override
    public Propositional neg() {
      return new Propositional(this.propositional.neg(f -> f.neg(), f -> f.nnf()));
    }

    @Override
    public Propositional nnf() {
      var propositionalNNF = this.propositional.nnf(f -> f.neg(), f -> f.nnf());
      if (this.propositional.equals(propositionalNNF))
        return this;
      return new Propositional(propositionalNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.propositional.isPropositional();
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return new TemporalEpistemicFormula.Propositional(this.propositional.convert(f -> f.toTemporalEpistemic()));
    }

    @Override
    public int getPrecedence() {
      return this.propositional.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalFormula.Visitor<T> visitor) {
      return visitor.onPropositional(this.propositional);
    }

    @Override
    public String toString() {
      return this.propositional.toString();
    }
  }

  public static class Temporal extends TemporalFormula {
    private final logic.Temporal<TemporalFormula> temporal;

    public Temporal(logic.Temporal<TemporalFormula> temporal) {
      this.temporal = temporal;
    }

    @Override
    public Temporal neg() {
      return new Temporal(this.temporal.neg(f -> f.neg()));
    }

    @Override
    public Temporal nnf() {
      var temporalNNF = this.temporal.nnf(f -> f.nnf());
      if (this.temporal.equals(temporalNNF))
        return this;
      return new Temporal(temporalNNF);
    }

    @Override
    public boolean isPropositional() {
      return false;
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return new TemporalEpistemicFormula.Temporal(this.temporal.convert(f -> f.toTemporalEpistemic()));
    }

    @Override
    public int getPrecedence() {
      return this.temporal.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalFormula.Visitor<T> visitor) {
      return visitor.onTemporal(this.temporal);
    }

    @Override
    public String toString() {
      return this.temporal.toString();
    }
  }
}
