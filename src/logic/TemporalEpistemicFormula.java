package logic;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.Expression;


@NonNullByDefault
public abstract class TemporalEpistemicFormula implements Formula {
  public interface Visitor<T> {
    public T onAtom(Expression.Boolean formula);
    public T onPropositional(logic.Propositional<TemporalEpistemicFormula> formula);
    public T onTemporal(logic.Temporal<TemporalEpistemicFormula> formula);
    public T onEpistemic(logic.Epistemic<TemporalEpistemicFormula> formula);
  }

  @NonNullByDefault({})
  public static class Cases<T> implements Visitor<T> {
    private Function<Expression.@NonNull Boolean, T> atomFun = null;
    private Function<logic.@NonNull Propositional<@NonNull TemporalEpistemicFormula>, T> propositionalFun = null;
    private Function<logic.@NonNull Temporal<@NonNull TemporalEpistemicFormula>, T> temporalFun = null;
    private Function<logic.@NonNull Epistemic<@NonNull TemporalEpistemicFormula>, T> epistemicFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public @NonNull Cases<T> atom(Function<Expression.@NonNull Boolean, T> atomFun) {
      this.atomFun = atomFun;
      return this;
    }
    
    public @NonNull Cases<T> propositional(Function<logic.@NonNull Propositional<@NonNull TemporalEpistemicFormula>, T> propositionalFun) {
      this.propositionalFun = propositionalFun;
      return this;
    }
    
    public @NonNull Cases<T> temporal(Function<logic.@NonNull Temporal<@NonNull TemporalEpistemicFormula>, T> temporalFun) {
      this.temporalFun = temporalFun;
      return this;
    }
    
    public @NonNull Cases<T> epistemic(Function<logic.@NonNull Epistemic<@NonNull TemporalEpistemicFormula>, T> epistemicFun) {
      this.epistemicFun = epistemicFun;
      return this;
    }
    
    public @NonNull Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply(TemporalEpistemicFormula formula) {
      return formula.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on variable";
      return this.otherwiseFun.get();
    }

    public T onAtom(Expression.@NonNull Boolean formula) {
      return this.atomFun != null ? this.atomFun.apply(formula) : otherwise();
    }

    public T onPropositional(logic.@NonNull Propositional<@NonNull TemporalEpistemicFormula> formula) {
      return this.propositionalFun != null ? this.propositionalFun.apply(formula) : otherwise();
    }

    public T onTemporal(logic.@NonNull Temporal<@NonNull TemporalEpistemicFormula> formula) {
      return this.temporalFun != null ? this.temporalFun.apply(formula) : otherwise();
    }

    public T onEpistemic(logic.@NonNull Epistemic<@NonNull TemporalEpistemicFormula> formula) {
      return this.epistemicFun != null ? this.epistemicFun.apply(formula) : otherwise();
    }
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public abstract TemporalEpistemicFormula neg();
  public abstract TemporalEpistemicFormula nnf();

  private static final TemporalEpistemicFormula FALSEFRM = new TemporalEpistemicFormula.Propositional(logic.Propositional.falseFrm());
  private static final TemporalEpistemicFormula TRUEFRM = new TemporalEpistemicFormula.Propositional(logic.Propositional.trueFrm());

  public static TemporalEpistemicFormula falseFrm() {
    return FALSEFRM;
  }

  public static TemporalEpistemicFormula trueFrm() {
    return TRUEFRM;
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      TemporalFormula other = (TemporalFormula)object;
      return Objects.equals(this.toString(), other.toString());
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  public abstract String toString();

  public static class Atom extends TemporalEpistemicFormula {
    private final Expression.Boolean atom;

    public Atom(Expression.Boolean atom) {
      this.atom = atom;
    }

    @Override
    public Atom neg() {
      return new Atom(this.atom.neg());
    }

    @Override
    public Atom nnf() {
      var atomNNF = this.atom.nnf();
      if (this.atom.equals(atomNNF))
        return this;
      return new Atom(atomNNF);
    }

    @Override
    public boolean isPropositional() {
      return true;
    }

    @Override
    public int getPrecedence() {
      return this.atom.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalEpistemicFormula.Visitor<T> visitor) {
      return visitor.onAtom(this.atom);
    }

    @Override
    public int hashCode() {
      return this.atom.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() == object.getClass()) {
        Atom other = (Atom)object;
        return Objects.equals(this.atom, other.atom);
      }
      return this.atom.equals(object);
    }

    @Override
    public String toString() {
      return this.atom.toString();
    }
  }

  public static class Propositional extends TemporalEpistemicFormula {
    private final logic.Propositional<TemporalEpistemicFormula> propositional;

    public Propositional(logic.Propositional<TemporalEpistemicFormula> propositional) {
      this.propositional = propositional;
    }

    @Override
    public Propositional neg() {
      return new Propositional(this.propositional.neg(f -> f.neg(), f -> f.nnf()));
    }

    @Override
    public Propositional nnf() {
      var propositionalNNF = this.propositional.nnf(f -> f.neg(), f -> f.nnf());
      if (this.propositional.equals(propositionalNNF))
        return this;
      return new Propositional(propositionalNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.propositional.isPropositional();
    }

    @Override
    public int getPrecedence() {
      return this.propositional.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalEpistemicFormula.Visitor<T> visitor) {
      return visitor.onPropositional(this.propositional);
    }

    @Override
    public int hashCode() {
      return this.propositional.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() == object.getClass()) {
        Propositional other = (Propositional)object;
        return Objects.equals(this.propositional, other.propositional);
      }
      return this.propositional.equals(object);
    }

    @Override
    public String toString() {
      return this.propositional.toString();
    }
  }

  public static class Temporal extends TemporalEpistemicFormula {
    private final logic.Temporal<TemporalEpistemicFormula> temporal;

    public Temporal(logic.Temporal<TemporalEpistemicFormula> temporal) {
      this.temporal = temporal;
    }

    @Override
    public Temporal neg() {
      return new Temporal(this.temporal.neg(f -> f.neg()));
    }

    @Override
    public Temporal nnf() {
      var temporalNNF = this.temporal.nnf(f -> f.nnf());
      if (this.temporal.equals(temporalNNF))
        return this;
      return new Temporal(temporalNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.temporal.isPropositional();
    }

    @Override
    public int getPrecedence() {
      return this.temporal.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalEpistemicFormula.Visitor<T> visitor) {
      return visitor.onTemporal(this.temporal);
    }

    @Override
    public int hashCode() {
      return this.temporal.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() == object.getClass()) {
        Temporal other = (Temporal)object;
        return Objects.equals(this.temporal, other.temporal);
      }
      return this.temporal.equals(object);
    }

    @Override
    public String toString() {
      return this.temporal.toString();
    }
  }

  public static class Epistemic extends TemporalEpistemicFormula {
    private final logic.Epistemic<TemporalEpistemicFormula> epistemic;

    public Epistemic(logic.Epistemic<TemporalEpistemicFormula> epistemic) {
      this.epistemic = epistemic;
    }

    @Override
    public Epistemic neg() {
      return new Epistemic(this.epistemic.neg(f -> f.neg()));
    }

    @Override
    public Epistemic nnf() {
      var epistemicNNF = this.epistemic.nnf(f -> f.nnf());
      if (this.epistemic.equals(epistemicNNF))
        return this;
      return new Epistemic(epistemicNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.epistemic.isPropositional();
    }

    @Override
    public int getPrecedence() {
      return this.epistemic.getPrecedence();
    }

    @Override
    public <T> T accept(TemporalEpistemicFormula.Visitor<T> visitor) {
      return visitor.onEpistemic(this.epistemic);
    }

    @Override
    public int hashCode() {
      return this.epistemic.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() == object.getClass()) {
        Epistemic other = (Epistemic)object;
        return Objects.equals(this.epistemic, other.epistemic);
      }
      return this.epistemic.equals(object);
    }

    @Override
    public String toString() {
      return this.epistemic.toString();
    }
  }
}
