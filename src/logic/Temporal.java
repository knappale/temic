package logic;

import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNullByDefault;

import state.StateSet;
import state.TransitionRelation;
import util.Formatter;


@NonNullByDefault
public interface Temporal<T extends Formula> extends Formula {
  public interface Visitor<T extends Formula, U> {
    public U onUnary(Unary.Kind kind, T inner);
    public U onBinary(T left, Binary.Kind kind, T right);
  }

  public abstract <U> U accept(Visitor<T, U> visitor);

  public abstract <U extends Formula> Temporal<U> convert(Function<T, U> inner);

  public abstract Temporal<T> neg(Function<T, T> innerNeg);

  @SuppressWarnings("unchecked")
  public default Temporal<T> neg() {
    return this.nnf(f -> (T)f.neg());
  }

  public abstract Temporal<T> nnf(Function<T, T> innerNNF);

  @SuppressWarnings("unchecked")
  public default Temporal<T> nnf() {
    return this.nnf(f -> (T)f.nnf());
  }

  @Override
  public default boolean isPropositional() {
    return false;
  }

  public abstract StateSet satisfyingStates(TransitionRelation positiveRelation, TransitionRelation notNegativeRelation, Function<T, StateSet> inner);

  public default StateSet satisfyingStates(TransitionRelation relation, Function<T, StateSet> inner) {
    return this.satisfyingStates(relation, relation, inner);
  }

  public abstract String toString();

  public static class Unary<T extends Formula> implements Temporal<T> {
    public enum Kind {
      EX, AX, EF, AF, EG, AG;

      public Kind neg() {
        switch (this) {
          case EX: return AX;
          case AX: return EX;
          case AF: return EG;
          case EF: return AG;
          case EG: return AF;
          case AG: return EF;
          default: throw new RuntimeException("Unknown unary temporal modality");
        }
      }
        
      public String pathQuantifier() {
        switch (this) {
          case EX:
          case EF:
          case EG: return "E";
          case AX:
          case AF:
          case AG: return "A";
          default: throw new RuntimeException("Unknown unary temporal modality");
        }
      }

      public String temporalModality() {
        switch (this) {
          case EX:
          case AX: return "X";
          case EF:
          case AF: return "F";
          case EG:
          case AG: return "G";
          default: throw new RuntimeException("Unknown unary temporal modality");
        }
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.pathQuantifier());
        builder.append(this.temporalModality());
        return builder.toString();
      }
    }

    private final Kind kind;
    private final T formula;

    public Unary(Kind kind, T formula) {
      this.kind = kind;
      this.formula = formula;
    }

    @Override
    public Unary<T> neg(Function<T, T> innerNeg) {
      return new Unary<T>(this.kind.neg(), innerNeg.apply(this.formula));
    }

    @SuppressWarnings("null")
    @Override
    public Unary<T> nnf(Function<T, T> innerNNF) {
      var formulaNNF = innerNNF.apply(this.formula);
      if (this.formula.equals(formulaNNF))
        return this;
      return new Unary<T>(this.kind, formulaNNF);
    }

    @Override
    public StateSet satisfyingStates(TransitionRelation positiveRelation, TransitionRelation notNegativeRelation, Function<T, StateSet> inner) {
      switch (kind) {
        case EX: return positiveRelation.EX(inner.apply(this.formula));
        case AX: return notNegativeRelation.AX(inner.apply(this.formula));
        case EF: return positiveRelation.EF(inner.apply(this.formula));
        case AF: return notNegativeRelation.AF(inner.apply(this.formula));
        case EG: return positiveRelation.EG(inner.apply(this.formula));
        case AG: return notNegativeRelation.AG(inner.apply(this.formula));
        default: throw new RuntimeException("Unknown unary temporal modality");
      }
    }

    @Override
    public <U extends Formula> Unary<U> convert(Function<T, U> inner) {
      return new Unary<>(this.kind, inner.apply(this.formula));
    }

    @Override
    public int getPrecedence() {
      return 7;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onUnary(this.kind, this.formula);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.kind);
      builder.append(" ");
      builder.append(this.formula);
      return builder.toString();
    }
  }

  public static class Binary<T extends Formula> implements Temporal<T> {
    public enum Kind {
      EU, AU, ER, AR;

      public Kind neg() {
        switch (this) {
          case EU: return AR;
          case AU: return ER;
          case ER: return AU;
          case AR: return EU;
          default: throw new RuntimeException("Unknown unary temporal modality");
        }
      }
        
      public String pathQuantifier() {
        switch (this) {
          case EU:
          case ER: return "E";
          case AU:
          case AR: return "A";
          default: throw new RuntimeException("Unknown unary temporal modality");
        }
      }

      public String temporalModality() {
        switch (this) {
          case EU:
          case AU: return "U";
          case ER:
          case AR: return "R";
          default: throw new RuntimeException("Unknown unary temporal modality");
        }
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.pathQuantifier());
        builder.append(this.temporalModality());
        return builder.toString();
      }
    }

    private final Kind kind;
    private final T left;
    private final T right;

    public Binary(T left, Kind kind, T right) {
      this.left = left;
      this.kind = kind;
      this.right = right;
    }

    @Override
    public Binary<T> neg(Function<T, T> innerNeg) {
      return new Binary<T>(innerNeg.apply(this.left), this.kind.neg(), innerNeg.apply(this.right));
    }

    @SuppressWarnings("null")
    @Override
    public Binary<T> nnf(Function<T, T> innerNNF) {
      var leftNNF = innerNNF.apply(this.left);
      var rightNNF = innerNNF.apply(this.right);
      if (this.left.equals(leftNNF) && this.right.equals(rightNNF))
        return this;
      return new Binary<T>(leftNNF, this.kind, rightNNF);
    }

    @Override
    public StateSet satisfyingStates(TransitionRelation positiveRelation, TransitionRelation notNegativeRelation, Function<T, StateSet> inner) {
      switch (kind) {
        case EU: return positiveRelation.EU(inner.apply(left), inner.apply(right));
        case AU: return notNegativeRelation.AU(inner.apply(left), inner.apply(right));
        case ER: return positiveRelation.ER(inner.apply(left), inner.apply(right));
        case AR: return notNegativeRelation.AR(inner.apply(left), inner.apply(right));
        default: throw new RuntimeException("Unknown binary temporal modality");
      }
    }

    @Override
    public <U extends Formula> Binary<U> convert(Function<T, U> inner) {
      return new Binary<>(inner.apply(this.left), this.kind, inner.apply(this.right));
    }

    @Override
    public int getPrecedence() {
      return 7;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onBinary(this.left, this.kind, this.right);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.kind.pathQuantifier());
      builder.append("[");
      builder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      builder.append(" ");
      builder.append(this.kind.temporalModality());
      builder.append(" ");
      builder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      builder.append("]");
      return builder.toString();
    }
  }
}
