package logic;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.Expression;
import state.StateSet;


@NonNullByDefault
public abstract class StateFormula implements Formula {
  public interface Visitor<T> {
    public T onAtom(Expression.Boolean formula);
    public T onPropositional(logic.Propositional<StateFormula> formula);
  }

  @NonNullByDefault({})
  public class Cases<T> implements Visitor<T> {
    private Function<Expression.@NonNull Boolean, T> atomFun = null;
    private Function<logic.@NonNull Propositional<@NonNull StateFormula>, T> propositionalFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public @NonNull Cases<T> atom(Function<Expression.@NonNull Boolean, T> atomFun) {
      this.atomFun = atomFun;
      return this;
    }
    
    public @NonNull Cases<T> propositional(Function<logic.@NonNull Propositional<@NonNull StateFormula>, T> propositionalFun) {
      this.propositionalFun = propositionalFun;
      return this;
    }
    
    public @NonNull Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      return StateFormula.this.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on variable";
      return this.otherwiseFun.get();
    }

    public T onAtom(Expression.@NonNull Boolean formula) {
      return this.atomFun != null ? this.atomFun.apply(formula) : otherwise();
    }

    public T onPropositional(logic.@NonNull Propositional<@NonNull StateFormula> formula) {
      return this.propositionalFun != null ? this.propositionalFun.apply(formula) : otherwise();
    }
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public abstract StateFormula neg();
  public abstract StateFormula nnf();

  public abstract StateSet satisfyingStates();

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      StateFormula other = (StateFormula)object;
      return Objects.equals(this.toString(), other.toString());
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  public static class Atom extends StateFormula {
    private final Expression.Boolean atom;

    public Atom(Expression.Boolean atom) {
      this.atom = atom;
    }

    @Override
    public Atom neg() {
      return new Atom(this.atom.neg());
    }

    @Override
    public Atom nnf() {
      var atomNNF = this.atom.nnf();
      if (this.atom.equals(atomNNF))
        return this;
      return new Atom(atomNNF);
    }

    @Override
    public boolean isPropositional() {
      return true;
    }

    @Override
    public StateSet satisfyingStates() {
      return StateSet.satisfyingStates(this.atom);
    }

    @Override
    public int getPrecedence() {
      return this.atom.getPrecedence();
    }

    @Override
    public <T> T accept(StateFormula.Visitor<T> visitor) {
      return visitor.onAtom(this.atom);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.atom);
    }

    @Override
    public String toString() {
      return this.atom.toString();
    }
  }

  public static class Propositional extends StateFormula {
    private final logic.Propositional<StateFormula> propositional;

    public Propositional(logic.Propositional<StateFormula> propositional) {
      this.propositional = propositional;
    }

    @Override
    public Propositional neg() {
      return new Propositional(this.propositional.neg(f -> f.neg(), f -> f.nnf()));
    }

    @Override
    public Propositional nnf() {
      var propositionalNNF = this.propositional.nnf(f -> f.neg(), f -> f.nnf());
      if (this.propositional.equals(propositionalNNF))
        return this;
      return new Propositional(propositionalNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.propositional.isPropositional();
    }

    @Override
    public StateSet satisfyingStates() {
      return this.propositional.satisfyingStates(f -> f.satisfyingStates());
    }

    @Override
    public int getPrecedence() {
      return this.propositional.getPrecedence();
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
      return visitor.onPropositional(this.propositional);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.propositional);
    }

    @Override
    public String toString() {
      return this.propositional.toString();
    }
  }
}
