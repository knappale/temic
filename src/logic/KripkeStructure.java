package logic;

import static java.util.stream.Collectors.toSet;

import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.SpaceManager;
import state.StateSet;
import state.TransitionRelation;
import system.Action;
import system.InterpretedAction;
import system.InterpretedSystem;


@NonNullByDefault
public class KripkeStructure {
  @SuppressWarnings("null")
  public static final Logger logger = Logger.getLogger(KripkeStructure.class.getName());

  private final TransitionRelation transitionRelation;
  private final StateSet initialStates;

  private final StateSet reachableStates;
  private final StateSet reachableDeadlockStates;

  private KripkeStructure(StateSet initialStates, TransitionRelation transitionRelation, StateSet reachableStates) {
    this.transitionRelation = transitionRelation;
    this.initialStates = initialStates;
    this.reachableStates = reachableStates;
    this.reachableDeadlockStates = this.reachableStates.intersection(transitionRelation.preStates().complement());
    if (!this.reachableDeadlockStates.isEmpty())
      logger.warning("Kripke structure with reachable deadlock states like " + this.reachableDeadlockStates.any());
    
  }

  public KripkeStructure(StateSet initialStates, TransitionRelation transitionRelation) {
    this(initialStates, transitionRelation, transitionRelation.reachableStates(initialStates));
  }

  public static KripkeStructure create(StateSet initialStates, Set<TransitionRelation> transitionRelations) {
    var transitionRelation = transitionRelations.stream().reduce(TransitionRelation.empty(), TransitionRelation::union);
    var reachableStates = transitionRelation.reachableStates(initialStates);
    transitionRelation = transitionRelation.restrictPre(reachableStates);
    return new KripkeStructure(initialStates, transitionRelation, reachableStates);
  }

  /**
   * @return the initial state set of this Kripke structure
   */
  public StateSet initialStates() {
    return this.initialStates;
  }

  /**
   * @return the transition relation of this Kripke structure
   */
  public TransitionRelation transitionRelation() {
    return this.transitionRelation;
  }

  /**
   * @return the reachable state set of this Kripke structure
   */
  public StateSet reachableStates() {
    return this.reachableStates;
  }

  /**
   * @return the reachable deadlock state set of this Kripke structure
   */
  public StateSet reachableDeadlockStates() {
    return this.reachableDeadlockStates;
  }

  /**
   * Determine whether this Kripke structure subsets another Kripke structure.
   *
   * @param other some Kripke structure
   * @return whether this Kripke structure subsets {@code other}
   */
  public boolean subsets(KripkeStructure other) {
    return this.initialStates.subsets(other.initialStates) &&
           this.transitionRelation.subsets(other.transitionRelation);
  }

  /**
   * Interpret an action over this Kripke structure.
   *
   * @param action an action
   * @return the interpreted action resulting from interpreting {@code action} over this Kripke structure
   */
  public InterpretedAction interpret(Action action) {
    return new InterpretedAction(action, this.satisfyingStates(action.getTemporalEpistemicPre()));
  }

  /**
   * Interpret a set of actions over this Kripke structure yielding an interpreted system.
   *
   * @param actions a set of actions
   * @return the interpreted system resulting from interpreting all {@code actions} over this Kripke structure
   */
  public InterpretedSystem interpret(Set<Action> actions) {
    return new InterpretedSystem(this.initialStates, actions.stream().map(action -> this.interpret(action)).collect(toSet()));
  }

  /**
   * Determine whether a temporal formula holds in this Kripke structure.
   *
   * @param formula a temporal formula
   * @return whether {@code formula} holds in this Kripke structure
   */
  public boolean satisfies(TemporalFormula formula) {
    return this.initialStates.subsets(this.satisfyingStates(formula));
  }

  private final TemporalFormula.Cases<StateSet> temporal =
      new TemporalFormula.Cases<StateSet>().atom(a -> StateSet.satisfyingStates(a)).
                                            propositional(p -> p.satisfyingStates(f -> this.temporal.apply(f))).
                                            temporal(t -> t.satisfyingStates(this.transitionRelation(), f -> this.temporal.apply(f)));

  public StateSet satisfyingStates(TemporalFormula formula) {
    return this.temporal.apply(formula);
  }

  /**
   * Determine whether an epistemic formula holds in this Kripke structure.
   *
   * @param formula an epistemic formula
   * @return whether {@code formula} holds in this Kripke structure
   */
  public boolean satisfies(EpistemicFormula formula) {
    return this.initialStates.subsets(this.satisfyingStates(formula));
  }

  private final EpistemicFormula.Cases<StateSet> epistemic =
      new EpistemicFormula.Cases<StateSet>().atom(a -> StateSet.satisfyingStates(a)).
                                             propositional(p -> p.satisfyingStates(f -> this.epistemic.apply(f))).
                                             epistemic(e -> e.satisfyingStates(this.reachableStates(), f -> this.epistemic.apply(f)));

  public StateSet satisfyingStates(EpistemicFormula formula) {
    return this.epistemic.apply(formula);
  }

  /**
   * Determine whether a temporal-epistemic formula holds in this Kripke structure.
   *
   * @param formula a temporal-epistemic formula
   * @return whether {@code formula} holds in this Kripke structure
   */
  public boolean satisfies(TemporalEpistemicFormula formula) {
    return this.initialStates.subsets(this.satisfyingStates(formula));
  }

  private final TemporalEpistemicFormula.Cases<StateSet> temporalEpistemic =
      new TemporalEpistemicFormula.Cases<StateSet>().atom(a -> StateSet.satisfyingStates(a)).
                                                     propositional(p -> p.satisfyingStates(f -> this.temporalEpistemic.apply(f))).
                                                     temporal(t -> t.satisfyingStates(this.transitionRelation(), f -> this.temporalEpistemic.apply(f))).
                                                     epistemic(e -> e.satisfyingStates(this.reachableStates(), f -> this.temporalEpistemic.apply(f)));

  public StateSet satisfyingStates(TemporalEpistemicFormula formula) {
    return this.temporalEpistemic.apply(formula);
  }

  public String info() {
    var builder = new StringBuilder();
    builder.append(SpaceManager.info());
    builder.append("\nnumber of legal states.................... ");
    builder.append(StateSet.all().count());
    builder.append("\nnumber of initial states.................. ");
    builder.append(this.initialStates().count());
    builder.append("\nnumber of reachable states................ ");
    builder.append(this.reachableStates().count());
    builder.append("\nnumber of reachable deadlock states....... ");
    builder.append(this.reachableDeadlockStates().count());
    builder.append("\nnumber of transitions .................... ");
    builder.append(this.transitionRelation().count());
    return builder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.transitionRelation, this.initialStates);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      KripkeStructure other = (KripkeStructure)object;
      return Objects.equals(this.transitionRelation, other.transitionRelation) &&
             Objects.equals(this.initialStates, other.initialStates);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
