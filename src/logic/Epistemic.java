package logic;

import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNullByDefault;

import state.StateSet;


@NonNullByDefault
public interface Epistemic<T extends Formula> extends Formula {
  public interface Visitor<T extends Formula, U> {
    public U onModal(Modal.Kind kind, Agent agent, T formula);
  }

  public abstract <U> U accept(Visitor<T, U> visitor);

  public abstract <U extends Formula> Epistemic<U> convert(Function<T, U> inner);

  /**
   * Negate this epistemic formula Q[a] . \varphi such that the
   * result is in negation normal form.
   *
   * @param innerNeg a function determining \neg\varphi
   * @return \neg Q[a] . \varphi in negation normal form
   */
  public abstract Epistemic<T> neg(Function<T, T> innerNeg);

  @SuppressWarnings("unchecked")
  public default Epistemic<T> neg() {
    return this.neg(f -> (T)f.neg());
  }

  /**
   * Put this epistemic formula Q[a] . \varphi into negation normal form.
   *
   * @param innerNNF a function determining NNF(\varphi)
   * @return NNF(Q[a] . \varphi)
   */
  public abstract Epistemic<T> nnf(Function<T, T> innerNNF);

  @SuppressWarnings("unchecked")
  public default Epistemic<T> nnf() {
    return this.nnf(f -> (T)f.nnf());
  }

  @Override
  public default boolean isPropositional() {
    return false;
  }

  /**
   * Determine \{ s \mid S, s \models Q[a] . \varphi \} for this
   * epistemic formula Q[a] . \varphi, where S is the set of accessible states.
   *
   * @param acc a set of states potentially accessible
   * @param inner a function determining \{ s \mid S, s \models \varphi \}
   * @return \{ s \mid S, s \models Q[a] . \varphi \}
   */
  public default StateSet satisfyingStates(StateSet acc, Function<T, StateSet> inner) {
    return this.satisfyingStates(acc, acc, inner);
  }

  /**
   * Determine \{ s \mid (P, N), s \models Q[a] . \varphi \} for this
   * epistemic formula Q[a] . \varphi, where P is a set of states that is
   * accessible and N a set of states that cannot be excluded to be accessible.
   *
   * @param incl a set of states that are accessible
   * @param notExcl a set of states that cannot be excluded to be accessible
   * @param inner a function determining \{ s \mid (P, N), s \models \varphi \}
   * @return \{ s \mid (P, N), s \models Q[a] . \varphi \}
   */
  public abstract StateSet satisfyingStates(StateSet incl, StateSet notExcl, Function<T, StateSet> inner);

  public abstract String toString();

  public static class Modal<T extends Formula> implements Epistemic<T> {
    public enum Kind {
      K, M;

      public Kind neg() {
        switch (this) {
          case K: return M;
          case M: return K;
          default: throw new RuntimeException("Unknown epistemic modality.");
        }
      }

      @Override
      public String toString() {
        switch (this) {
          case K: return "K";
          case M: return "M";
          default: throw new RuntimeException("Unknown epistemic modality.");
        }
      }
    }

    private final Kind kind;
    private final Agent agent;
    private final T formula;

    public Modal(Kind kind, Agent agent, T formula) {
      this.kind = kind;
      this.agent = agent;
      this.formula = formula;
    }

    /**
     * Determine \{ s \mid S, s \models K[agent] \varphi \} where S
     * is a set of potentially accessible states.
     *
     * @param agent an agent
     * @param acc a set of states potentially accessible
     * @param f the set of states \{ s \mid S, s \models \varphi \}
     * @return \{ s \mid \forall s' \in [s]_a \cap S . S, s' \models_p \varphi \} =
     *         \{ s \mid \not \exists s' \in [s]_a \cap S . S, s' \not\models \varphi \}
     */
    public StateSet K(Agent agent, StateSet acc, StateSet f) {
      return agent.getAccessibilityRelation().preImage(acc.intersection(f.complement())).complement();
    }

    /**
     * Determine \{ s \mid S, s \models M[agent] \varphi \} where S
     * is a set of potentially accessible states.
     *
     * @param agent an agent
     * @param acc a set of states potentially accessible
     * @param f the set of states \{ s \mid S, s \models \varphi \}
     * @return \{ s \mid \exists s' \in [s]_a \cap S . S, s' \models \varphi \}
     */
    public StateSet M(Agent agent, StateSet acc, StateSet f) {
      return agent.getAccessibilityRelation().preImage(acc.intersection(f));
    }

    @Override
    public Modal<T> neg(Function<T, T> innerNeg) {
      return new Modal<T>(this.kind.neg(), this.agent, innerNeg.apply(this.formula));
    }

    @Override
    public Modal<T> nnf(Function<T, T> innerNNF) {
      return new Modal<T>(this.kind, this.agent, innerNNF.apply(this.formula));
    }

    @Override
    public StateSet satisfyingStates(StateSet incl, StateSet notExcl, Function<T, StateSet> inner) {
      switch (this.kind) {
        case K: return this.K(this.agent, notExcl, inner.apply(this.formula));
        case M: return this.M(this.agent, incl, inner.apply(this.formula));
        default: throw new RuntimeException("Unknown epistemic modality.");
      }
    }

    @Override
    public int getPrecedence() {
      return 7;
    }

    @Override
    public <U> U accept(Visitor<T, U> visitor) {
      return visitor.onModal(this.kind, this.agent, this.formula);
    }

    @Override
    public <U extends Formula> Modal<U> convert(Function<T, U> inner) {
      return new Modal<U>(this.kind, this.agent, inner.apply(this.formula));
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.kind);
      builder.append("[");
      builder.append(this.agent.getName());
      builder.append("] ");
      builder.append(this.formula);
      return builder.toString();
    }
  }
}
