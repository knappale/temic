package logic;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public interface Formula {
  /**
   * Determine the negation of the given formula.  If neg() is called
   * on a subtype of Formula, then a formula of exactly this subtype
   * is to be returned.
   * 
   * @return the negated formula
   */
  public abstract Formula neg();

  /**
   * Determine the negation normal form of the given formula.
   * If nnf() is called on a subtype of Formula, then a formula of
   * exactly this subtype is to be returned.
   * 
   * @return the formula in negation normal form
   */
  public abstract Formula nnf();

  /**
   * @return whether this formula is purely propositional
   */
  public abstract boolean isPropositional();

  public abstract int getPrecedence();

  public default boolean hasPrecedence(Formula other) {
    return this.getPrecedence() <= other.getPrecedence();
  }
}
