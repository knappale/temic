package logic;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.Expression;


@NonNullByDefault
public abstract class EpistemicFormula implements Formula {
  public interface Visitor<T> {
    public T onAtom(Expression.Boolean expression);
    public T onPropositional(logic.Propositional<EpistemicFormula> formula);
    public T onEpistemic(logic.Epistemic<EpistemicFormula> formula);
  }

  @NonNullByDefault({})
  public static class Cases<T> implements Visitor<T> {
    private Function<Expression.@NonNull Boolean, T> atomFun = null;
    private Function<logic.@NonNull Propositional<@NonNull EpistemicFormula>, T> propositionalFun = null;
    private Function<logic.@NonNull Epistemic<@NonNull EpistemicFormula>, T> epistemicFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public @NonNull Cases<T> atom(Function<Expression.@NonNull Boolean, T> atomFun) {
      this.atomFun = atomFun;
      return this;
    }
    
    public @NonNull Cases<T> propositional(Function<logic.@NonNull Propositional<@NonNull EpistemicFormula>, T> propositionalFun) {
      this.propositionalFun = propositionalFun;
      return this;
    }
    
    public @NonNull Cases<T> epistemic(Function<logic.@NonNull Epistemic<@NonNull EpistemicFormula>, T> epistemicFun) {
      this.epistemicFun = epistemicFun;
      return this;
    }
    
    public @NonNull Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply(EpistemicFormula formula) {
      return formula.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on variable";
      return this.otherwiseFun.get();
    }

    public T onAtom(Expression.@NonNull Boolean formula) {
      return this.atomFun != null ? this.atomFun.apply(formula) : otherwise();
    }

    public T onPropositional(logic.@NonNull Propositional<@NonNull EpistemicFormula> formula) {
      return this.propositionalFun != null ? this.propositionalFun.apply(formula) : otherwise();
    }

    public T onEpistemic(logic.@NonNull Epistemic<@NonNull EpistemicFormula> formula) {
      return this.epistemicFun != null ? this.epistemicFun.apply(formula) : otherwise();
    }
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public abstract TemporalEpistemicFormula toTemporalEpistemic();

  public abstract EpistemicFormula neg();
  public abstract EpistemicFormula nnf();

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      EpistemicFormula other = (EpistemicFormula)object;
      return Objects.equals(this.toString(), other.toString());
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  public abstract String toString();

  public static class Atom extends EpistemicFormula {
    private final Expression.Boolean atom;

    public Atom(Expression.Boolean atom) {
      this.atom = atom;
    }

    @Override
    public Atom neg() {
      return new Atom(this.atom.neg());
    }

    @Override
    public Atom nnf() {
      var atomNNF = this.atom.nnf();
      if (this.atom.equals(atomNNF))
        return this;
      return new Atom(atomNNF);
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return new TemporalEpistemicFormula.Atom(this.atom);
    }

    @Override
    public boolean isPropositional() {
      return true;
    }

    @Override
    public int getPrecedence() {
      return this.atom.getPrecedence();
    }

    @Override
    public <T> T accept(EpistemicFormula.Visitor<T> visitor) {
      return visitor.onAtom(this.atom);
    }

    @Override
    public String toString() {
      return this.atom.toString();
    }
  }

  public static class Propositional extends EpistemicFormula {
    private final logic.Propositional<EpistemicFormula> propositional;

    public Propositional(logic.Propositional<EpistemicFormula> propositional) {
      this.propositional = propositional;
    }

    @Override
    public Propositional neg() {
      return new Propositional(this.propositional.neg(f -> f.neg(), f -> f.nnf()));
    }

    @Override
    public Propositional nnf() {
      var propositionalNNF = this.propositional.nnf(f -> f.neg(), f -> f.nnf());
      if (this.propositional.equals(propositionalNNF))
        return this;
      return new Propositional(propositionalNNF);
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return new TemporalEpistemicFormula.Propositional(this.propositional.convert(f -> f.toTemporalEpistemic()));
    }

    @Override
    public boolean isPropositional() {
      return this.propositional.isPropositional();
    }

    @Override
    public int getPrecedence() {
      return this.propositional.getPrecedence();
    }

    @Override
    public <T> T accept(EpistemicFormula.Visitor<T> visitor) {
      return visitor.onPropositional(this.propositional);
    }

    @Override
    public String toString() {
      return this.propositional.toString();
    }
  }

  public static class Epistemic extends EpistemicFormula {
    private final logic.Epistemic<EpistemicFormula> epistemic;

    public Epistemic(logic.Epistemic<EpistemicFormula> epistemic) {
      this.epistemic = epistemic;
    }

    @Override
    public Epistemic neg() {
      return new Epistemic(this.epistemic.neg(f -> f.neg()));
    }

    @Override
    public Epistemic nnf() {
      var epistemicNNF = this.epistemic.nnf(f -> f.nnf());
      if (this.epistemic.equals(epistemicNNF))
        return this;
      return new Epistemic(epistemicNNF);
    }

    @Override
    public boolean isPropositional() {
      return this.epistemic.isPropositional();
    }

    @Override
    public TemporalEpistemicFormula toTemporalEpistemic() {
      return new TemporalEpistemicFormula.Epistemic(this.epistemic.convert(f -> f.toTemporalEpistemic()));
    }

    @Override
    public int getPrecedence() {
      return this.epistemic.getPrecedence();
    }

    @Override
    public <T> T accept(EpistemicFormula.Visitor<T> visitor) {
      return visitor.onEpistemic(this.epistemic);
    }

    @Override
    public String toString() {
      return this.epistemic.toString();
    }
  }
}
