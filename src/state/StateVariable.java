package state;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public interface StateVariable extends Variable {
  public interface Visitor<T> {
    public T onBoolean(Boolean variable);
    public T onInteger(Integer variable);
  }

  @NonNullByDefault({})
  public class Cases<T> implements Visitor<T> {
    private Function<@NonNull Boolean, T> booleanVarFun = null;
    private Function<@NonNull Integer, T> integerVarFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public Cases<T> booleanVar(Function<@NonNull Boolean, T> booleanVarFun) {
      this.booleanVarFun = booleanVarFun;
      return this;
    }
    
    public Cases<T> integerVar(Function<@NonNull Integer, T> integerVarFun) {
      this.integerVarFun = integerVarFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply(StateVariable variable) {
      return variable.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on state variable";
      return this.otherwiseFun.get();
    }

    public T onBoolean(Boolean variable) {
      return this.booleanVarFun != null ? this.booleanVarFun.apply(variable) : otherwise();
    }

    public T onInteger(Integer variable) {
      return this.integerVarFun != null ? this.integerVarFun.apply(variable) : otherwise();
    }
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public default <T> T apply(Cases<T> cases) {
    return cases.apply(this);
  }

  public abstract StateVariable prime();
  public abstract StateVariable unprime();
  public abstract Variable aux(int n);

  public abstract BDD unchangedBDD();
  
  public class Boolean extends Variable.Boolean implements StateVariable {
    private List<Variable.Boolean> auxs = new ArrayList<>();

    public Boolean(String name) {
      super(name);
    }

    @Override
    public <T> T accept(StateVariable.Visitor<T> visitor) {
      return visitor.onBoolean(this);
    }

    @Override
    public StateVariable.Boolean prime() {
      return new StateVariable.Boolean(this.getName() + "'") {
        public StateVariable.Boolean prime() {
          return this;
        }

        public StateVariable.Boolean unprime() {
          return StateVariable.Boolean.this;
        }
      };
    }

    @Override
    public StateVariable.Boolean unprime() {
      return this;
    }

    @Override
    public Variable.Boolean aux(int n) {
      if (n < this.auxs.size())
        return this.auxs.get(n);
      var name = this.auxs.isEmpty() ? this.getName() : this.auxs.get(this.auxs.size()-1).getName();
      for (int i = this.auxs.size()-1; i < n; i++) {
        name += "*";
        this.auxs.add(new Variable.Boolean(name));
      }
      return this.auxs.get(n);
    }

    @Override
    public BDD unchangedBDD() {
      return BDD.biimplies(this.unprime().getBDD(), this.prime().getBDD());
    }
  }

  public class Integer extends Variable.Integer implements StateVariable {
    private List<Variable.Integer> auxs = new ArrayList<>();

    public Integer(String name, Type.Integer type) {
      super(name, type);
    }

    public Integer(String name, int lower, int upper) {
      this(name, Type.integerType(lower, upper));
    }

    @Override
    public <T> T accept(StateVariable.Visitor<T> visitor) {
      return visitor.onInteger(this);
    }

    @Override
    public StateVariable.Integer prime() {
      return new StateVariable.Integer(this.getName() + "'", this.getType()) {
        public StateVariable.Integer prime() {
          return this;
        }

        public StateVariable.Integer unprime() {
          return StateVariable.Integer.this;
        }
      };
    }

    @Override
    public StateVariable.Integer unprime() {
      return this;
    }

    @Override
    public Variable.Integer aux(int n) {
      if (n < this.auxs.size())
        return this.auxs.get(n);
      var name = this.auxs.isEmpty() ? this.getName() : this.auxs.get(this.auxs.size()-1).getName();
      var type = this.getType();
      for (int i = this.auxs.size()-1; i < n; i++) {
        name += "*";
        this.auxs.add(new Variable.Integer(name, type));
      }
      return this.auxs.get(n);
    }

    @Override
    public BDD unchangedBDD() {
      return IntBDD.equal(this.unprime().getIntBDD(), this.prime().getIntBDD());
    }
  }
}
