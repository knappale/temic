package state;

import static java.util.stream.Collectors.toMap;

import static util.Objects.assertNonNull;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * A BDD-based state manager.
 */
@NonNullByDefault
public class SpaceManager {
  private static final Map<StateVariable, Vector<Integer>> stateVariableIndices = new HashMap<>();
  private static final Map<StateVariable, Vector<Integer>> primedStateVariableIndices = new HashMap<>();
  private static BDD stateSpaceBDD = BDD.trueBDD();
  private static BDD transitionRelationBDD = BDD.trueBDD();
  private static final Map<Variable, Vector<Integer>> auxVariableIndices = new HashMap<>();
  private static final Map<Variable, Vector<Integer>> localVariableIndices = new HashMap<>();
  private static final Map<Variable, @NonNull BDD[]> variableBDDs = new HashMap<>();

  public static Set<Integer> unprimedRange = new HashSet<>();
  public static Set<Integer> primedRange = new HashSet<>();
  public static Set<Integer> auxRange = new HashSet<>();
  public static Map<Integer, Integer> unprimedToPrimedMap = new HashMap<>();
  public static Map<Integer, Integer> primedToUnprimedMap = new HashMap<>();
  public static Map<Integer, Integer> unprimedToAuxMap = new HashMap<>();
  public static Map<Integer, Integer> primedToAuxMap = new HashMap<>();

  private SpaceManager() {
  }

  @SuppressWarnings("null")
  public static void initialise(List<? extends StateVariable> stateVariables) {
    clear();

    for (var stateVariable : stateVariables)
      declareStateVariable(stateVariable);

    /*
    // Sort state variables increasingly by their first BDD variable index
    Function<StateVariable, @NonNull Integer> firstIdx = v -> assertNonNull(stateVariableIndices.get(v)).get(0);
    var sortedStateVariables = new TreeSet<StateVariable>((v1, v2) -> assertNonNull(firstIdx.apply(v1)).compareTo(firstIdx.apply(v2)));
    sortedStateVariables.addAll(stateVariableIndices.keySet());
    for (var stateVariable : sortedStateVariables)
      declareLocalVariable(stateVariable.aux(0));
    */

    for (var stateVariable : stateVariables) {
      unprimedRange.addAll(stateVariableIndices.get(stateVariable.unprime()));
      primedRange.addAll(primedStateVariableIndices.get(stateVariable.prime()));
      auxRange.addAll(auxVariableIndices.get(stateVariable.aux(0)));
    }

    unprimedToPrimedMap = getIndicesMap(stateVariables.stream().collect(toMap(StateVariable::unprime, StateVariable::prime)));
    primedToUnprimedMap = getIndicesMap(stateVariables.stream().collect(toMap(StateVariable::prime, StateVariable::unprime)));
    unprimedToAuxMap = getIndicesMap(stateVariables.stream().collect(toMap(StateVariable::unprime, stateVariable -> stateVariable.aux(0))));
    primedToAuxMap = getIndicesMap(stateVariables.stream().collect(toMap(StateVariable::prime, stateVariable -> stateVariable.aux(0))));
  }

  public static void clear() {
    BDD.clear();
    StateSet.clear();
    TransitionRelation.clear();
    stateVariableIndices.clear();
    primedStateVariableIndices.clear();
    stateSpaceBDD = BDD.trueBDD();
    transitionRelationBDD = BDD.trueBDD();
    auxVariableIndices.clear();
    localVariableIndices.clear();
    variableBDDs.clear();

    unprimedRange = new HashSet<>();
    primedRange = new HashSet<>();
    auxRange = new HashSet<>();
    unprimedToPrimedMap = new HashMap<>();
    primedToUnprimedMap = new HashMap<>();
    unprimedToAuxMap = new HashMap<>();
    primedToAuxMap = new HashMap<>();
  }

  private static @NonNull BDD[] declareBitVariables(int numBits, int idx, int inc) {
    var miBDDs = new @NonNull BDD[numBits];
    for (var i = 0; i < numBits; i++) {
      miBDDs[i] = BDD.varBDD(idx);
      idx += inc;
    }
    return miBDDs;
  }

  private static Vector<Integer> declareVariable(Variable.Boolean variable, int idx) {
    var miBDDs = declareBitVariables(1, idx, 1);
    var mi = new Vector<Integer>(1);
    mi.add(BDD.getVar(miBDDs[0]));
    variableBDDs.put(variable, miBDDs);
    return mi;
  }

  private static Vector<Integer> declareVariable(Variable.Integer variable, int idx, int inc) {
    var type = variable.getType();
    var numBits = type.numBits();
    var miBDDs = declareBitVariables(numBits, idx, inc);
    var mi = new Vector<Integer>(numBits);
    for (int i = 0; i < numBits; i++)
      mi.add(BDD.getVar(miBDDs[i]));
    variableBDDs.put(variable, IntBDD.add(IntBDD.extended(miBDDs), IntBDD.intMultiBDD(type.decode(0))));
    return mi;
  }

  private static int nextIndex() {
    return Stream.of(stateVariableIndices.values().stream(),
                     primedStateVariableIndices.values().stream(),
                     auxVariableIndices.values().stream(),
                     localVariableIndices.values().stream()).flatMap(s -> s).map(mi -> mi.stream().reduce(0, Math::max)).reduce(0, Math::max).intValue()+1;
  }

  private static void declareStateVariable(StateVariable variable) {
    var idx = nextIndex();
    new StateVariable.Cases<@Nullable Void>().booleanVar(v -> { declareStateVariable(v, idx); return null; }).
                                              integerVar(v -> { declareStateVariable(v, idx); return null; }).
                                              apply(variable);
  }

  private static void declareStateVariable(StateVariable.Boolean variable, int idx) {
    stateVariableIndices.put(variable.unprime(), declareVariable(variable.unprime(), idx));
    primedStateVariableIndices.put(variable.prime(), declareVariable(variable.prime(), idx+1));
    auxVariableIndices.put(variable.aux(0), declareVariable(variable.aux(0), idx+2));
  }

  private static void declareStateVariable(StateVariable.Integer variable, int idx) {
    stateVariableIndices.put(variable.unprime(), declareVariable(variable.unprime(), idx, 3));
    primedStateVariableIndices.put(variable.prime(), declareVariable(variable.prime(), idx+1, 3));
    auxVariableIndices.put(variable.aux(0), declareVariable(variable.aux(0), idx+2, 3));

    // Upper bound for unprimed and primed variable
    var encodingUpperBound = IntBDD.intMultiBDD(variable.getType().size()-1);
    stateSpaceBDD = BDD.and(stateSpaceBDD, IntBDD.leq(IntBDD.varMultiBDD(getIndices(variable.unprime())), encodingUpperBound));
    transitionRelationBDD = BDD.and(transitionRelationBDD, BDD.and(stateSpaceBDD, IntBDD.leq(IntBDD.varMultiBDD(getIndices(variable.prime())), encodingUpperBound)));
  }

  static BDD declareLocalVariable(Variable variable) {
    var idx = nextIndex();
    return new Variable.Cases<BDD>().booleanVar(v -> declareLocalVariable(v, idx)).
                                     integerVar(v -> declareLocalVariable(v, idx)).
                                     apply(variable);
  }

  private static BDD declareLocalVariable(Variable.Boolean variable, int idx) {
    localVariableIndices.put(variable, declareVariable(variable, idx));
    return BDD.trueBDD();
  }

  private static BDD declareLocalVariable(Variable.Integer variable, int idx) {
    localVariableIndices.put(variable, declareVariable(variable, idx, 1));

    // Upper bound for local variable
    var encodingUpperBound = IntBDD.intMultiBDD(variable.getType().size()-1);
    return IntBDD.leq(IntBDD.varMultiBDD(getIndices(variable)), encodingUpperBound);
  }

  static void undeclareLocalVariable(Variable variable) {
    localVariableIndices.remove(variable);
    variableBDDs.remove(variable);
  }

  /**
   * @return sorted set of all (unprimed) state variables
   */
  static SortedSet<StateVariable> getStateVariables() {
    var sortedVariables = new TreeSet<StateVariable>((v1, v2) -> v1.getName().compareTo(v2.getName()));
    sortedVariables.addAll(stateVariableIndices.keySet());
    return sortedVariables;
  }

  /**
   * @return sorted set of all unprimed and primed state variables
   */
  static SortedSet<StateVariable> getTransitionVariables() {
    var sortedVariables = new TreeSet<StateVariable>((v1, v2) -> v1.getName().compareTo(v2.getName()));
    sortedVariables.addAll(stateVariableIndices.keySet());
    sortedVariables.addAll(primedStateVariableIndices.keySet());
    return sortedVariables;
  }

  static BigInteger countStates(BDD bdd) {
    var idxs = new TreeSet<Integer>();
    for (var variable : getStateVariables())
      idxs.addAll(stateVariableIndices.get(variable));
    return BDD.countSatVals(bdd, idxs);
  }

  static BigInteger countTransitions(BDD bdd) {
    var idxs = new TreeSet<Integer>();
    for (var variable : getStateVariables()) {
      idxs.addAll(stateVariableIndices.get(variable));
      idxs.addAll(primedStateVariableIndices.get(variable.prime()));
    }
    return BDD.countSatVals(bdd, idxs);
  }
 
  /**
   * Returns the BDD which a boolean variable stands for.
   *
   * @param variable a boolean variable
   * @return the BDD location which {@code variable} stands for
   */
  @SuppressWarnings("null")
  static BDD getBDD(Variable.Boolean variable) {
    // return BDD.varBDD(getIndices(variable).get(0));
    return variableBDDs.get(variable)[0];
  }
	
  /**
   * Returns the locations of the BDDs which an integer variable stands for.
   *
   * @param variable an integer variable
   * @return the BDD locations which {@code variable} stands for
   */
  @SuppressWarnings("null")
  static @NonNull BDD[] getIntBDD(Variable.Integer variable) {
    return variableBDDs.get(variable);
    // return IntBDD.add(IntBDD.varMultiBDD(getIndices(variable)), IntBDD.intMultiBDD(variable.getType().decode(0)));
    // return IntBDD.varMultiBDD(this.getIndices(variable));
  }

  static Map<Variable, Value> getValuation(BDD.Valuation bddValuation) {
    var valuation = new HashMap<Variable, Value>();
    for (var variable : getStateVariables())
      valuation.put(variable, decode(variable, bddValuation));
    return valuation;
  }

  static Vector<Integer> getIndices(Variable variable) {
    var idxs = localVariableIndices.get(variable);
    if (idxs != null)
      return idxs;
    idxs = auxVariableIndices.get(variable);
    if (idxs != null)
      return idxs;
    idxs = stateVariableIndices.get(variable);
    if (idxs != null)
      return idxs;
    idxs = primedStateVariableIndices.get(variable);
    if (idxs != null)
      return idxs;
    throw new IllegalArgumentException("No indices for variable " + Formatter.quoted(variable));
  }

  static Value decode(Variable variable, BDD.Valuation bddValuation) {
    return new Variable.Cases<Value>().booleanVar(v -> new Value.Boolean(decode(v, bddValuation))).
                                       integerVar(v -> new Value.Integer(decode(v, bddValuation))).
                                       apply(variable);
  }

  private static boolean decode(Variable.Boolean variable, BDD.Valuation bddValuation) {
    return bddValuation.get(getIndices(variable).get(0));
  }

  private static int decode(Variable.Integer variable, BDD.Valuation bddValuation) {
    var idxs = getIndices(variable);
    var value = 0;
    var twopowersbit = 1;
    for (var bit = 0; bit < idxs.size(); bit++) {
      if (bddValuation.get(idxs.get(bit)))
        value += twopowersbit;
      twopowersbit *= 2;
    }
    return variable.getType().decode(value);
  }

  static BDD.Valuation encode(Variable variable, Value value) {
    return new Variable.Cases<BDD.Valuation>().booleanVar(v -> encode(v, value.getBoolean())).
                                               integerVar(v -> encode(v, value.getInteger())).
                                               apply(variable);
  }

  private static BDD.Valuation encode(Variable.Boolean variable, boolean value) {
    var bddValuation = new BDD.Valuation();
    bddValuation.put(getIndices(variable).get(0), value);
    return bddValuation;
  }

  private static BDD.Valuation encode(Variable.Integer variable, int value) {
    var bddValuation = new BDD.Valuation();
    var idxs = getIndices(variable);
    value = variable.getType().encode(value);
    for (var bit = 0; bit < idxs.size(); bit++) {
      bddValuation.put(idxs.get(bit), (value & 1) != 0);
      value = value >>> 1;
    }
    return bddValuation;
  }

  /**
   * @return a BDD location representing the set of all valuations of unprimed state variables
   */
  static BDD stateSpaceBDD() {
    return stateSpaceBDD;
  }
    
  /**
   * @return the BDD representing the set of all valuations of unprimed and primed state variables
   */
  static BDD transitionRelationBDD() {
    return transitionRelationBDD;
  }

  /**
   * Compute an indices mapping from a variable renaming.
   *
   * The variable renaming must be such that indices are mapped injectively and monotonically.
   *
   * @param renaming a variable renaming
   * @return an indices mapping corresponding to {@code renaming}
   */
  private static Map<Integer, Integer> getIndicesMap(Map<Variable, Variable> renaming) {
    var indicesMap = new HashMap<Integer, Integer>();
    for (var from : renaming.keySet()) {
      var to = assertNonNull(renaming.get(from));
      var fromIndices = getIndices(from);
      var toIndices = getIndices(to);
      for (var i = 0; i < fromIndices.size(); i++)
        indicesMap.put(fromIndices.get(i), toIndices.get(i));
    }
    return indicesMap;
  }

  public static String info() {
    var builder = new StringBuilder();
    builder.append("number of declared variables ............. ");
    builder.append(getStateVariables().size());
    builder.append("\nnumber of unprimed BDD variables ......... ");
    builder.append(getStateVariables().stream().map(unprimed -> assertNonNull(stateVariableIndices.get(unprimed)).size()).reduce(0, (n1, n2) -> n1 + n2));
    return builder.toString();
  }
}
