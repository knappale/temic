package state;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * An expression.
 */
@NonNullByDefault
public interface Expression {
  public interface Visitor<T> extends Boolean.Visitor<T>, Integer.Visitor<T> {
  }

  public abstract Type getType();

  /**
   * Determine the BDD characterising where this expression can
   * be evaluated successfully.
   *
   * @return the BDD characterising the evaluation domain of this expression
   */
  public abstract BDD domainBDD();

  public abstract int getPrecedence();

  public default boolean hasPrecedence(Expression other) {
    return this.getPrecedence() <= other.getPrecedence();
  }

  public abstract <T> T accept(Visitor<T> visitor);

  @Override
  public abstract String toString();

  public static Value.Boolean TRUEEXPR = new Value.Boolean(true);
  public static Value.Boolean FALSEEXPR = new Value.Boolean(false);

  public static Value.Boolean trueExpr() {
    return TRUEEXPR;
  }

  public static Value.Boolean falseExpr() {
    return FALSEEXPR;
  }

  public static interface Boolean extends Expression {
    public interface Visitor<T> {
      public T onBooleanVariable(Variable.Boolean variable);
      public T onBooleanMacro(Macro.Boolean macro);
      public T onBoolean(boolean value);
      public T onNot(Expression.Boolean expression);
      public T onConnective(Expression.Boolean left, Boolean.Connective.Kind kind, Expression.Boolean right);
      public T onComparison(Expression.Integer left, Boolean.Comparison.Kind kind, Expression.Integer right);
    }

    @Override
    public default Type.Boolean getType() {
      return Type.booleanType();
    }

    public abstract Expression.Boolean neg();

    public default Expression.Boolean nnf() {
      return this.neg().neg();
    }

    /**
     * Determine the BDD representing this boolean expression
     * symbolically as a boolean function.
     *
     * @return the BDD of the symbolic representation of this boolean expression
     */
    public abstract BDD getBDD();

    public static class Not implements Boolean {
      private final Expression.Boolean inner;

      public Not(Expression.Boolean expression) {
        this.inner = expression;
      }

      @Override
      public Expression.Boolean neg() {
        return this.inner; // TODO .neg().neg();
      }

      @Override
      public BDD getBDD() {
        return BDD.not(this.inner.getBDD());
      }

      @Override
      public BDD domainBDD() {
        return this.inner.domainBDD();
      }

      @Override
      public int getPrecedence() {
        return 2;
      }

      @Override
      public <T> T accept(Expression.Visitor<T> visitor) {
        return visitor.onNot(this.inner);
      }

      @Override
      public int hashCode() {
        return Objects.hash(this.inner);
      }

      @Override
      public boolean equals(@Nullable Object object) {
        if (this == object)
          return true;
        if (object == null)
          return false;
        if (this.getClass() != object.getClass())
          return false;
        Not other = (Not)object;
        return Objects.equals(this.inner, other.inner);
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("!");
        builder.append(Formatter.parenthesised(this.inner, e -> e.hasPrecedence(this)));
        return builder.toString();
      }
    }

    public static class Connective implements Boolean {
      public enum Kind {
        AND, OR, XOR, IMPL, BIIMPL;

        public String toString() {
          switch (this) {
            case AND: return "&";
            case OR: return "|";
            case XOR: return "^";
            case IMPL: return "->";
            case BIIMPL: return "<->";
            default: throw new RuntimeException("Unknown boolean connective.");
          }
        }
      }

      private final Kind kind;
      private final Expression.Boolean left;
        private final Expression.Boolean right;

      public Connective(Expression.Boolean left, Kind kind, Expression.Boolean right) {
        this.left = left;
        this.kind = kind;
        this.right = right;
      }

      @Override
      public Connective neg() {
        switch (this.kind) {
          case AND: return new Connective(this.left.neg(), Kind.OR, this.right.neg());
          case OR: return new Connective(this.left.neg(), Kind.AND, this.right.neg());
          case XOR: return new Connective(this.left, Kind.BIIMPL, this.right);
          case IMPL: return new Connective(this.left, Kind.AND, this.right.neg());
          case BIIMPL: return new Connective(this.left, Kind.XOR, this.right);
          default: throw new RuntimeException("Unknown boolean connective.");
        }
      }

      @Override
      public BDD getBDD() {
        var leftBDD = this.left.getBDD();
        var rightBDD = this.right.getBDD();
        switch (this.kind) {
          case AND: return BDD.and(leftBDD, rightBDD);
          case OR: return BDD.or(leftBDD, rightBDD);
          case XOR: return BDD.exor(leftBDD, rightBDD);
          case IMPL: return BDD.implies(leftBDD, rightBDD);
          case BIIMPL: return BDD.biimplies(leftBDD, rightBDD);
          default: throw new RuntimeException("Unknown boolean connective.");
        }
      }

      @Override
      public BDD domainBDD() {
        return BDD.and(this.left.domainBDD(), this.right.domainBDD());
      }

      @Override
      public int getPrecedence() {
        switch (this.kind) {
          case AND: return 11;
          case OR: return 12;
          case XOR: return 13;
          case IMPL: return 14;
          case BIIMPL: return 15;
          default: throw new RuntimeException("Unknown boolean connective.");
        }
      }

      @Override
      public <T> T accept(Expression.Visitor<T> visitor) {
        return visitor.onConnective(this.left, this.kind, this.right);
      }

      @Override
      public int hashCode() {
        return Objects.hash(this.kind, this.left, this.right);
      }

      @Override
      public boolean equals(@Nullable Object object) {
        if (this == object)
          return true;
        if (object == null)
          return false;
        if (this.getClass() != object.getClass())
          return false;
        Connective other = (Connective)object;
        return this.kind == other.kind &&
               Objects.equals(this.left, other.left) &&
               Objects.equals(this.right, other.right);
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Formatter.parenthesised(this.left, e -> e.hasPrecedence(this)));
        builder.append(" ");
        builder.append(this.kind);
        builder.append(" ");
        builder.append(Formatter.parenthesised(this.right, e -> e.hasPrecedence(this)));
        return builder.toString();
      }
    }

    public static class Comparison implements Boolean {
      public enum Kind {
        LT, LEQ, EQ, NEQ, GEQ, GT;

        public Kind neg() {
          switch (this) {
            case LT: return GEQ;
            case LEQ: return GT;
            case EQ: return NEQ;
            case NEQ: return EQ;
            case GEQ: return LT;
            case GT: return LEQ;
            default: throw new RuntimeException("Unknown comparison operator.");
          } 
        }
          
        @Override
        public String toString() {
          switch (this) {
            case LT: return "<";
            case LEQ: return "<=";
            case EQ: return "=";
            case NEQ: return "!=";
            case GEQ: return ">=";
            case GT: return ">";
            default: throw new RuntimeException("Unknown comparison operator.");
          } 
        }
      }

      private final Kind kind;
      private final Expression.Integer left;
      private final Expression.Integer right;

      public Comparison(Expression.Integer left, Kind kind, Expression.Integer right) {
        this.left = left;
        this.kind = kind;
        this.right = right;
      }

      @Override
      public Comparison neg() {
        return new Comparison(this.left, kind.neg(), this.right);
      }

      @Override
      public BDD getBDD() {
        var leftIntBDD = this.left.getIntBDD();
        var rightIntBDD = this.right.getIntBDD();
        switch (this.kind) {
          case LT: return IntBDD.less(leftIntBDD, rightIntBDD);
          case LEQ: return IntBDD.leq(leftIntBDD, rightIntBDD);
          case EQ: return IntBDD.equal(leftIntBDD, rightIntBDD);
          case NEQ: return BDD.not(IntBDD.equal(leftIntBDD, rightIntBDD));
          case GEQ: return IntBDD.leq(rightIntBDD, leftIntBDD);
          case GT: return IntBDD.less(rightIntBDD, leftIntBDD);
          default: throw new RuntimeException("Unknown arithmetic operator.");
        } 
      }

      @Override
      public BDD domainBDD() {
        return BDD.and(this.left.domainBDD(), this.right.domainBDD());
      }

      @Override
      public int getPrecedence() {
        return 10;
      }

      @Override
      public <T> T accept(Expression.Visitor<T> visitor) {
        return visitor.onComparison(this.left, this.kind, this.right);
      }

      @Override
      public int hashCode() {
        return Objects.hash(this.kind, this.left, this.right);
      }

      @Override
      public boolean equals(@Nullable Object object) {
        if (this == object)
          return true;
        if (object == null)
          return false;
        if (this.getClass() != object.getClass())
          return false;
        Comparison other = (Comparison)object;
        return this.kind == other.kind &&
               Objects.equals(this.left, other.left) &&
               Objects.equals(this.right, other.right);
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Formatter.parenthesised(this.left, e -> e.hasPrecedence(this)));
        builder.append(" ");
        builder.append(this.kind);
        builder.append(" ");
        builder.append(Formatter.parenthesised(this.right, e -> e.hasPrecedence(this)));
        return builder.toString();
      }
    }
  }

  public static abstract class Integer implements Expression {
    public interface Visitor<T> {
      public T onIntegerVariable(Variable.Integer variable);
      public T onIntegerMacro(Macro.Integer macro);
      public T onInteger(int value);
      public T onMinus(Expression.Integer inner);
      public T onArithmetic(Expression.Integer left, Integer.Arithmetic.Kind kind, Expression.Integer right);
    }

    @Override
    public abstract Type.Integer getType();

    /**
     * Determine the multi-BDD representing this integer expression
     * symbolically as an integer function.
     *
     * @return the multi-BDD of the symbolic representation of
     *         this integer expression
     */
    public abstract @NonNull BDD[] getIntBDD();

    public static class Minus extends Integer {
      private final Expression.Integer inner;

      public Minus(Expression.Integer expression) {
        this.inner = expression;
      }

      @Override
      public Type.Integer getType() {
        return this.inner.getType().minus();
      }

      @Override
      public @NonNull BDD[] getIntBDD() {
        return IntBDD.negate(this.inner.getIntBDD());
      }

      @Override
      public BDD domainBDD() {
        return this.inner.domainBDD();
      }

      @Override
      public int getPrecedence() {
        return 2;
      }

      @Override
      public <T> T accept(Expression.Visitor<T> visitor) {
        return visitor.onMinus(this.inner);
      }

      @Override
      public int hashCode() {
        return Objects.hash(this.inner);
      }

      @Override
      public boolean equals(@Nullable Object object) {
        if (this == object)
          return true;
        if (object == null)
          return false;
        if (this.getClass() != object.getClass())
          return false;
        Minus other = (Minus)object;
        return Objects.equals(this.inner, other.inner);
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("-");
        builder.append(Formatter.parenthesised(this.inner, e -> e.hasPrecedence(this)));
        return builder.toString();
      }
    }

    public static class Arithmetic extends Integer {
      public enum Kind {
        ADD, SUB, MULT, DIV, MOD;

        @Override
        public String toString() {
          switch (this) {
            case ADD: return "+";
            case SUB: return "-";
            case MULT: return "*";
            case DIV: return "/";
            case MOD: return "%";
            default: throw new RuntimeException("Unknown arithmetic operator.");
          } 
        }
      }

      private final Kind kind;
      private final Expression.Integer left;
      private final Expression.Integer right;

      public Arithmetic(Expression.Integer left, Kind kind, Expression.Integer right) {
        this.left = left;
        this.kind = kind;
        this.right = right;
      }

      @Override
      public Type.Integer getType() {
        Type.Integer leftType = this.left.getType();
        Type.Integer rightType = this.right.getType();
        switch (this.kind) {
          case ADD: return leftType.add(rightType);
          case SUB: return leftType.sub(rightType);
          case MULT: return leftType.mult(rightType);
          case DIV: return leftType.div(rightType);
          case MOD: return leftType.mod(rightType);
          default: throw new RuntimeException("Unknown arithmetic operator.");
        }
      }

      @Override
      public BDD[] getIntBDD() {
        var leftIntBDD = this.left.getIntBDD();
        var rightIntBDD = this.right.getIntBDD();
        switch (this.kind) {
          case ADD: return IntBDD.add(leftIntBDD, rightIntBDD);
          case SUB: return IntBDD.sub(leftIntBDD, rightIntBDD);
          case MULT: return IntBDD.multiply(leftIntBDD, rightIntBDD);
          case DIV: return IntBDD.div(leftIntBDD, rightIntBDD);
          case MOD: return IntBDD.mod(leftIntBDD, rightIntBDD);
          default: throw new RuntimeException("Unknown arithmetic operator.");
        } 
      }

      @Override
      public BDD domainBDD() {
        BDD leftDomain = this.left.domainBDD();
        BDD rightDomain = this.right.domainBDD();
        switch (this.kind) {
          case ADD:
          case SUB:
          case MULT: {
            return BDD.and(leftDomain, rightDomain);
          }
          case DIV:
          case MOD: {
            Boolean divisorNonZero = new Boolean.Comparison(this.right, Boolean.Comparison.Kind.NEQ, new Value.Integer(0));
            return BDD.and(BDD.and(leftDomain, rightDomain), divisorNonZero.getBDD());
          }
          default: throw new RuntimeException("Unknown arithmetic operator.");
        } 
      }

      @Override
      public int getPrecedence() {
        switch (this.kind) {
          case ADD:
          case SUB: return 9;
          case MULT:
          case DIV:
          case MOD: return 8;
          default: throw new RuntimeException("Unknown arithmetic operator.");
        }
      }

      @Override
      public <T> T accept(Expression.Visitor<T> visitor) {
        return visitor.onArithmetic(this.left, this.kind, this.right);
      }

      @Override
      public int hashCode() {
        return Objects.hash(this.kind, this.left, this.right);
      }

      @Override
      public boolean equals(@Nullable Object object) {
        if (this == object)
          return true;
        if (object == null)
          return false;
        if (this.getClass() != object.getClass())
          return false;
        Arithmetic other = (Arithmetic)object;
        return this.kind == other.kind &&
               Objects.equals(this.left, other.left) &&
               Objects.equals(this.right, other.right);
      }

      @Override
      public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Formatter.parenthesised(this.left, e -> e.hasPrecedence(this)));
        builder.append(" ");
        builder.append(this.kind);
        builder.append(" ");
        builder.append(Formatter.parenthesised(this.right, e -> e.hasPrecedence(this)));
        return builder.toString();
      }
    }
  }
}
