package state;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;

import static util.Objects.assertNonNull;


/**
 * Class Type represents the types of syntactic objects of the input language of
 * TEMIC.
 * 
 * @version 1.1
 * @since 2011-04-05
 */
@NonNullByDefault
public abstract class Type {
  public static class Void extends Type {
    private Void() {
    }

    @Override
    public boolean isVoid() {
      return true;
    }

    @Override
    public int size() {
      return 0;
    }

    @Override
    public boolean subsets(Type other) {
      return (other instanceof Void);
    }

    @Override
    public String toString() {
      return "void";
    }
  }

  public static class Boolean extends Type {
    private Boolean() {
    }

    @Override
    public boolean isBoolean() {
      return true;
    }

    @Override
    public int size() {
      return 2;
    }

    public Value.Boolean getDefault() {
      return new Value.Boolean(false);
    }

    @Override
    public boolean subsets(Type other) {
      return (other instanceof Boolean);
    }

    @Override
    public String toString() {
      return "boolean";
    }

    @Override
    public int hashCode() {
      return 1;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      return object instanceof Boolean;
    }
  }

  public static class Integer extends Type {
    private final int lower;
    private final int upper;

    private Integer(int lower, int upper) {
      this.lower = lower;
      this.upper = upper;
    }

    @Override
    public boolean isInteger() {
      return true;
    }

    @Override
    public int size() {
      return this.upper-this.lower+1;
    }

    public int getLower() {
      return this.lower;
    }

    public int getUpper() {
      return this.upper;
    }

    public Value.Integer getDefault() {
      return new Value.Integer(this.lower);
    }

    public int encode(int value) {
      return value - this.lower;
    }

    public int decode(int code) {
      return code + this.lower;
    }

    public Integer minus() {
      if (this.isEmpty())
        return emptyIntegerType;
      return new Integer(-this.upper, -this.lower);
    }

    public Integer add(Integer other) {
      if (this.isEmpty() || other.isEmpty())
        return emptyIntegerType;
      return Type.integerType(this.lower+other.lower, this.upper+other.upper);
    }

    public Integer sub(Integer other) {
      if (this.isEmpty() || other.isEmpty())
        return emptyIntegerType;
      return Type.integerType(this.lower-other.upper, this.upper-other.lower);
    }

    public Integer mult(Integer other) {
      if (this.isEmpty() || other.isEmpty())
        return emptyIntegerType;
      Set<java.lang.Integer> set = Sets.elements(this.lower*other.lower, this.lower*other.upper, this.upper*other.lower, this.upper*other.upper);
      return Type.integerType(util.Math.min(set), util.Math.max(set));
    }

    public Integer div(Integer other) {
      if (this.isEmpty() || other.isEmpty())
        return emptyIntegerType;
      if (other.lower == 0 && other.upper == 0)
        return emptyIntegerType;
      
      Set<java.lang.Integer> set = new HashSet<>();
      if (other.lower != 0) {
        set.add(util.Math.div(this.lower, other.lower));
        set.add(util.Math.div(this.upper, other.lower));
      }
      if (other.upper != 0) {
        set.add(util.Math.div(this.lower, other.upper));
        set.add(util.Math.div(this.upper, other.upper));
      }
      if (other.lower <= -1 && -1 <= other.upper) {
        set.add((-1)*this.lower);
        set.add((-1)*this.upper);
      }
      if (other.lower <= 1 && 1 <= other.upper) {
        set.add(this.lower);
        set.add(this.upper);
      }
      return Type.integerType(util.Math.min(set), util.Math.max(set));
    }

    public Integer mod(Integer other) {
      if (this.isEmpty() || other.isEmpty())
        return emptyIntegerType;
      if (other.lower == 0 && other.upper != 0)
        return emptyIntegerType;

      Set<java.lang.Integer> set = Sets.elements(java.lang.Math.abs(other.lower), java.lang.Math.abs(other.upper));
      return Type.integerType(0, util.Math.max(set)-1);
    }

    @Override
    public boolean subsets(Type type) {
      try {
        Integer other = (Integer)type;
        return this.isEmpty() || ((other.lower <= this.lower) && (this.upper <= other.upper));
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    public String format(int value) {
      return assertNonNull(String.format("%" + java.lang.Math.max(("" + this.lower).length(), ("" + this.upper).length()) + "d", value));
    }

    @Override
    public int hashCode() {
      return this.upper - this.lower;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Integer other = (Integer)object;
        return this.lower == other.lower &&
               this.upper == other.upper;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.lower);
      builder.append("..");
      builder.append(this.upper);
      return builder.toString();
    }
  }

  private static final Type.Void voidType = new Type.Void();
  private static final Type.Boolean booleanType = new Type.Boolean();
  private static final Type.Integer emptyIntegerType = new Type.Integer(0, -1);

  public static Type.Void voidType() {
    return voidType;
  }

  public static Type.Boolean booleanType() {
    return booleanType;
  }

  public static Type.Integer integerType(int lowerBound, int upperBound) {
    if (lowerBound <= upperBound)
      return new Type.Integer(lowerBound, upperBound);
    return emptyIntegerType;
  }

  /**
   * @return true iff this type is the error type
   */
  public boolean isError() {
    return false;
  }

  /**
   * @return true iff this type is the void type
   */
  public boolean isVoid() {
    return false;
  }

  /**
   * @return true iff this type is the boolean type
   */
  public boolean isBoolean() {
    return false;
  }

  /**
   * @return true iff this type is a bounded integer type
   */
  public boolean isInteger() {
    return false;
  }

  /**
   * @return true iff this type is empty
   */
  public boolean isEmpty() {
    return (this.size() == 0);
  }

  /**
   * @return the size of this type
   */
  public abstract int size();

  /**
   * Returns the number of binary digits that are necessary for encoding values of
   * this type.
   * 
   * @pre this.size() > 0
   * @return the number of binary digits that are necessary for encoding values of
   *         this type.
   */
  public int numBits() {
    int encodingUpperBound = this.size()-1;
    if (encodingUpperBound >= 0)
      return util.Math.ceil(util.Math.log2(encodingUpperBound + 1));
    throw new IllegalArgumentException("numBits() called for empty type");
  }

  /**
   * Determine whether this type is an extensional sub-type of another type.
   *
   * @param other another type
   * @return whether this type is an extensional sub-type of {@code other}
   */
  public abstract boolean subsets(Type other);
}
