package state;

import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class TransitionRelation {
  private static final Map<BDD, WeakReference<@Nullable TransitionRelation>> canonical = new HashMap<>();
  final BDD bdd;

  static void clear() {
    canonical.clear();
  }

  private TransitionRelation(BDD bdd) {
    this.bdd = bdd;
  }

  static TransitionRelation relation(BDD bdd) {
    var transitionRelationRef = canonical.get(bdd);
    if (transitionRelationRef != null) {
      TransitionRelation transitionRelation = transitionRelationRef.get();
      if (transitionRelation != null)
        return transitionRelation;
    }

    // var restrictedBDD = BDD.and(bdd, SpaceManager.transitionRelationBDD());
    // TransitionRelation transitionRelation = new TransitionRelation(restrictedBDD);
    TransitionRelation transitionRelation = new TransitionRelation(bdd);
    canonical.put(bdd, new WeakReference<>(transitionRelation));
    // canonical.put(restrictedBDD, new WeakReference<>(transitionRelation));
    return transitionRelation;
  }

  public static TransitionRelation empty() {
    return relation(BDD.falseBDD());
  }

  public static TransitionRelation all() {
    return relation(BDD.trueBDD());
  }

  public static TransitionRelation id(Set<? extends StateVariable> variables) {
    var relationBDD = BDD.trueBDD();
    for (var variable : variables)
      relationBDD = BDD.and(relationBDD, variable.unchangedBDD());
    return relation(relationBDD);
  }

  public static TransitionRelation assignBoolean(StateVariable.Boolean variable, Expression.Boolean expression) {
    return relation(BDD.biimplies(variable.prime().getBDD(), expression.getBDD()));
  }

  public static TransitionRelation assignInteger(StateVariable.Integer variable, Expression.Integer expression) {
    return relation(IntBDD.equal(variable.prime().getIntBDD(), expression.getIntBDD()));
  }

  public TransitionRelation intersection(TransitionRelation other) {
    return relation(BDD.and(this.bdd, other.bdd));
  }

  public TransitionRelation union(TransitionRelation other) {
    return relation(BDD.or(this.bdd, other.bdd));
  }

  public TransitionRelation complement() {
    return relation(BDD.and(BDD.not(this.bdd), SpaceManager.transitionRelationBDD()));
  }

  public TransitionRelation restrictPre(StateSet pre) {
    return relation(BDD.and(this.bdd, pre.bdd));
  }

  /**
   * Compute the set of states that are reachable from a set of states
   * by this transition relation.
   *
   * @param from a set of states
   * @return the set of states that are reachable {@code from} by this transition relation
   */
  public StateSet reachableStates(StateSet from) {
    var previouslyReachableStatesBDD = BDD.falseBDD();
    var reachableStatesBDD = from.bdd;
    var actionBDD = this.bdd;
    while (reachableStatesBDD != previouslyReachableStatesBDD) {
      previouslyReachableStatesBDD = reachableStatesBDD;
      reachableStatesBDD = BDD.or(reachableStatesBDD, this.image(reachableStatesBDD, actionBDD));
    }
    return StateSet.states(reachableStatesBDD);
  }

  /**
   * @return some transition in this transition relation
   * @pre !this.isEmpty()
   */
  public Transition any() {
    var val = BDD.satisfyingValuation(this.bdd);
    if (val != null)
      return new Transition(val);
    throw new IllegalArgumentException("No transition in empty transition relation.");
  }

  /**
   * @return whether this transition relation is empty
   */
  public boolean isEmpty() {
    return this.bdd == empty().bdd;
  }

  /**
   * @param transition a transition
   * @return whether this transition relation contains {@code transition}
   */
  public boolean contains(Transition transition) {
    return transition.singleton().subsets(this);
  }

  /**
   * @param other a transition relation
   * @return whether this transition relation is contained in the {@code other} transition relation
   */
  public boolean subsets(TransitionRelation other) {
    return BDD.implies(this.bdd, other.bdd) == BDD.trueBDD();
  }

  public BigInteger count() {
    return SpaceManager.countTransitions(this.bdd);
  }

  /**
   * Returns the image of a set of states under this transition relation.
   *
   * @param pre a set of states
   * @return the image of {@code pre} under this transition relation
   */
  public StateSet image(StateSet pre) {
    return StateSet.states(this.image(pre.bdd, this.bdd));
  }   
    
  /**
   * Returns the BDD representing the image of a BDD representing a set of states
   * under a BDD representing a transition relation.
   *
   * @param preBDD a BDD representing a set of states
   * @param actionBDD a BDD representing a transition relation
   * @return the BDD representing the image of {@code preBDD} under {@code actionBDD}
   */
  private BDD image(BDD preBDD, BDD actionBDD) {
    var primedSuccessorStateBDD = BDD.exists(BDD.and(preBDD, actionBDD), SpaceManager.unprimedRange);	
    return BDD.rename(primedSuccessorStateBDD, SpaceManager.primedToUnprimedMap);
  }
    
  /**
   * Returns the pre-image of a set of states under this transition relation.
   *
   * @param post a set of states
   * @return the pre-image of {@code post} under this transition relation
   */
  public StateSet preImage(StateSet post) {
    return StateSet.states(this.preImage(post.bdd, this.bdd));
  }   
    
  /**
   * Returns the pre-states of this transition relation.
   *
   * @return the pre-states of this transition relation
   */
  public StateSet preStates() {
    return StateSet.states(this.preImage(BDD.trueBDD(), this.bdd));
  }   
    
  /**
   * Returns the BDD representing the pre-image of a BDD representing
   * a set of states under BDD representing a transition relation.
   *
   * @param postBDD a BDD representing a set of states
   * @param actionBDD a BDD representing a transition relation
   * @return the BDD representing the pre-image of {@code postBDD} under {@code actionBDD}
   */
  private BDD preImage(BDD postBDD, BDD actionBDD) {
    var primedPostStateBDD = BDD.rename(postBDD, SpaceManager.unprimedToPrimedMap);
    return BDD.exists(BDD.and(primedPostStateBDD, actionBDD), SpaceManager.primedRange);
  }   

  /**
   * @return the transitive hull of this transition relation
   */
  public TransitionRelation transitiveHull() {
    var oldHullBDD = this.bdd;
    var hullBDD = this.bdd;
    var fromAuxBDD = BDD.rename(this.bdd, SpaceManager.unprimedToAuxMap);
    do {
      oldHullBDD = hullBDD;
      hullBDD = BDD.or(hullBDD,
                       BDD.exists(BDD.and(BDD.rename(hullBDD, SpaceManager.primedToAuxMap),
                                          fromAuxBDD),
                                  SpaceManager.auxRange));
    } while (oldHullBDD != hullBDD);
    return relation(hullBDD);
  }

  public StateSet EX(StateSet f) {
    return this.preImage(f);
  }

  public StateSet AX(StateSet f) {
    return this.EX(f.complement()).complement();
  }

  public StateSet EF(StateSet f) {
    return this.EU(StateSet.all(), f);
  }

  public StateSet AF(StateSet f) {
    return this.EG(f.complement()).complement();
  }

  /**
   * Calculates nu(A) with A := Z. f /\ EX Z, i.e. [[EG f]].
   *
   * @param f a set of states
   * @return the set of states nu Z. f /\ EX Z, i.e. [[EG f]]
   */
  public StateSet EG(StateSet f) {
    var fBDD = f.bdd;

    var Zold = BDD.falseBDD();
    var Znew = BDD.trueBDD();
    while (Znew != Zold) {
      Zold = Znew;
      Znew = BDD.and(fBDD, this.preImage(Zold, this.bdd));
    }

    return StateSet.states(Znew);
  }

  public StateSet AG(StateSet f) {
    return this.EF(f.complement()).complement();
  }

  /**
   * Calculates mu(B) with B := Z. g \/ (f /\ EX Z), i.e. [[f EU g]].
   *
   * @param f a set of states
   * @param g a set of states
   * @return the set of states mu Z. g \/ (f /\ EX Z), i.e. [[f EU g]]
   */
  public StateSet EU(StateSet f, StateSet g) {
    var fBDD = f.bdd;
    var gBDD = g.bdd;
    
    var Zold = BDD.trueBDD();
    var Znew = BDD.falseBDD();
    while (Znew != Zold) {
      Zold = Znew;
      Znew = BDD.or(gBDD, BDD.and(fBDD, this.preImage(Zold, this.bdd)));
    }

    return StateSet.states(Znew);
  }

  public StateSet AU(StateSet f, StateSet g) {
    return this.ER(f.complement(), g.complement()).complement();
  }

  public StateSet ER(StateSet f, StateSet g) {
    return this.EU(g, f.intersection(g)).union(this.EG(g));
  }

  public StateSet AR(StateSet f, StateSet g) {
    return this.EU(f.complement(), g.complement()).complement();
  }

  public TransitionRelation largestBisimulationRelation(TransitionRelation other) {
    var Bold = BDD.falseBDD();
    var Bnew = BDD.trueBDD();
    var thisBDD = this.bdd; // in x, x'
    var otherBDD = BDD.rename(other.bdd, SpaceManager.unprimedToAuxMap); // in z, z'
    while (!Bold.equals(Bnew)) {
      Bold = Bnew;
      var Bp = BDD.rename(Bnew, SpaceManager.primedToAuxMap); // x -> x', z -> z'
      Bnew = BDD.and(BDD.forall(BDD.implies(thisBDD, BDD.and(BDD.exists(otherBDD, SpaceManager.auxRange), Bp)), SpaceManager.unprimedRange),  // \forall x' ... \exists z'
                     BDD.forall(BDD.implies(otherBDD, BDD.and(BDD.exists(thisBDD, SpaceManager.auxRange), Bp)), SpaceManager.unprimedRange)); // \forall z' ... \exists x'
    }
    return relation(BDD.rename(Bold, SpaceManager.primedToAuxMap)); // z -> x'
  }
  
  @Override
  public int hashCode() {
    return this.bdd.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TransitionRelation other = (TransitionRelation)object;
      return this.bdd == other.bdd;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    if (this.isEmpty())
      return "{ }";

    var builder = new StringBuilder();
    var reduced = this;
    var n = 0;
    var sep = "";
    while (!reduced.isEmpty() && n < 100) {
      builder.append(sep);
      var transition = reduced.any();
      builder.append(transition);
      reduced = reduced.intersection(transition.singleton().complement());
      n++;
      sep = "\n";
    }
    if (!reduced.isEmpty())
      builder.append("\n...");
    return builder.toString();
  }
}
