package state;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import system.Declaration;


@NonNullByDefault
public interface Macro extends Expression, Declaration {
  public abstract String getName();
  public abstract Expression getExpression();

  @Override
  public default int getPrecedence() {
    return 1;
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public static class Boolean implements Expression.Boolean, Macro {
    private final String name;
    private final Expression.Boolean expression;

    public Boolean(String name, Expression.Boolean expression) {
      this.name = name;
      this.expression = expression;
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public Expression.Boolean getExpression() {
      return this.expression;
    }

    @Override
    public Expression.Boolean neg() {
      return new Expression.Boolean.Not(this.expression);
    }

    @Override
    public Expression.Boolean nnf() {
      return this.expression.nnf();
    }

    @Override
    public BDD getBDD() {
      return this.expression.getBDD();
    }

    @Override
    public BDD domainBDD() {
      return BDD.trueBDD();
    }

    @Override
    public <T> T accept(Expression.Visitor<T> visitor) {
      return this.expression.accept(visitor);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.name, this.expression);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Macro.Boolean other = (Macro.Boolean)object;
        return Objects.equals(this.name, other.name);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      return this.getName();
    }
  }

  public static class Integer extends Expression.Integer implements Macro {
    private final String name;
    private final Expression.Integer expression;

    public Integer(String name, Expression.Integer expression) {
      this.name = name;
      this.expression = expression;
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public Expression.Integer getExpression() {
      return this.expression;
    }

    @Override
    public Type.Integer getType() {
      return this.expression.getType();
    }

    @Override
    public @NonNull BDD[] getIntBDD() {
      return this.expression.getIntBDD();
    }

    @Override
    public BDD domainBDD() {
      return BDD.trueBDD();
    }

    @Override
    public <T> T accept(Expression.Visitor<T> visitor) {
      return this.expression.accept(visitor);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.name, this.expression);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Macro.Integer other = (Macro.Integer)object;
        return Objects.equals(this.name, other.name);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      return this.getName();
    }
  }

  @Override
  public default String declaration() {
    StringBuilder builder = new StringBuilder();
    builder.append("let ");
    builder.append(this.getName());
    builder.append(" = ");
    builder.append(this.getExpression());
    builder.append(";");
    return builder.toString();
  }
}
