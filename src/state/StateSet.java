package state;

import static java.util.stream.Collectors.toSet;

import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * BDD-based set of states.
 *
 * A state set is always a subset of the state space, i.e., the BDD
 * underlying a state set implies the state space BDD.
 */
@NonNullByDefault
public class StateSet {
  private static final Map<BDD, WeakReference<@Nullable StateSet>> canonical = new HashMap<>();
  final BDD bdd;

  static void clear() {
    canonical.clear();
  }

  private StateSet(BDD bdd) {
    this.bdd = bdd;
  }

  static StateSet states(BDD bdd) {
    var stateSetRef = canonical.get(bdd);
    if (stateSetRef != null) {
      var stateSet = stateSetRef.get();
      if (stateSet != null)
        return stateSet;
    }

    // var restrictedBDD = BDD.and(bdd, SpaceManager.stateSpaceBDD());
    var stateSet = new StateSet(bdd);
    canonical.put(bdd, new WeakReference<>(stateSet));
    return stateSet;
  }

  /**
   * @return set of all declared variables
   */
  public static Set<StateVariable> getVariables() {
    return SpaceManager.getStateVariables();
  }

  public static StateSet declareVariables(Collection<? extends Variable> variables) {
    var ext = StateSet.all();
    for (var variable : variables)
      ext = ext.intersection(states(SpaceManager.declareLocalVariable(variable)));
    return ext;
  }

  public static void undeclareVariables(Collection<? extends Variable> variables) {
    for (var variable : variables)
      SpaceManager.undeclareLocalVariable(variable);
  }

  public static StateSet satisfyingStates(Expression.Boolean formula) {
    return states(BDD.and(formula.getBDD(), SpaceManager.stateSpaceBDD()));
  }

  public static StateSet empty() {
    return states(BDD.falseBDD());
  }

  public static StateSet all() {
    return states(SpaceManager.stateSpaceBDD());
  }

  public StateSet intersection(StateSet other) {
    return states(BDD.and(this.bdd, other.bdd));
  }

  public StateSet union(StateSet other) {
    return states(BDD.or(this.bdd, other.bdd));
  }

  public StateSet complement() {
    return states(BDD.and(BDD.not(this.bdd), SpaceManager.stateSpaceBDD()));
  }

  public StateSet forall(Collection<Variable> variables) {
    var rng = variables.stream().flatMap(v -> SpaceManager.getIndices(v).stream()).collect(toSet());
    return states(BDD.forall(this.bdd, rng));
  }

  public StateSet exists(Collection<Variable> variables) {
    var rng = variables.stream().flatMap(v -> SpaceManager.getIndices(v).stream()).collect(toSet());
    return states(BDD.exists(this.bdd, rng));
  }

  /**
   * @return some state in this set of states
   * @pre !this.isEmpty()
   */
  public State any() {
    var val = BDD.satisfyingValuation(this.bdd);
    if (val == null)
      throw new IllegalArgumentException("No state in empty set of states.");
    return new State(val);
  }

  /**
   * @return whether this set of states is empty
   */
  public boolean isEmpty() {
    return this.bdd == empty().bdd;
  }

  public boolean contains(State state) {
    return state.singleton().subsets(this);
  }

  public boolean subsets(StateSet other) {
    return BDD.implies(this.bdd, other.bdd) == BDD.trueBDD();
  }

  public BigInteger count() {
    return SpaceManager.countStates(this.bdd);
  }

  @Override
  public int hashCode() {
    return this.bdd.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      StateSet other = (StateSet)object;
      return this.bdd == other.bdd;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    if (this.isEmpty())
      return "{ }";

    var builder = new StringBuilder();
    var reduced = this;
    var n = 0;
    var sep = "";
    while (!reduced.isEmpty() && n < 100) {
      builder.append(sep);
      var state = reduced.any();
      builder.append(state);
      reduced = reduced.intersection(state.singleton().complement());
      n++;
      sep = "\n";
    }
    if (!reduced.isEmpty())
      builder.append("\n...");
    return builder.toString();
  }
}
