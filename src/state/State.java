package state;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class State {
  private Map<Variable, Value> valuation = new HashMap<>();

  public State(BDD.Valuation bddValuation) {
    for (var variable : SpaceManager.getStateVariables())
      valuation.put(variable, SpaceManager.decode(variable, bddValuation));
  }

  public State(Map<Variable, Value> valuation) {
    for (var variable : SpaceManager.getStateVariables())
      if (valuation.get(variable) == null)
        valuation.put(variable, variable.getDefault());
    this.valuation = valuation;
  }

  public StateSet singleton() {
    BDD.Valuation bddValuation = new BDD.Valuation();
    for (var variable : SpaceManager.getStateVariables()) {
      var value = this.valuation.get(variable);
      if (value == null)
        continue;

      bddValuation.putAll(SpaceManager.encode(variable, value));
    }
    return StateSet.states(bddValuation.asBDD());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.valuation);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      State other = (State)object;
      return Objects.equals(this.valuation, other.valuation);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    var sep = "";
    for (var variable : SpaceManager.getStateVariables()) {
      var value = this.valuation.get(variable);
      if (value == null)
        continue;

      builder.append(sep);
      builder.append(new Variable.Cases<String>().booleanVar(v -> value.getBoolean() ? v.getName() : "!" + v.getName()).
                                                  integerVar(v -> v.getName() + " = " + value.getInteger()).
                                                  apply(variable));
      sep = ", ";
    }
    return builder.toString();
  }
}
