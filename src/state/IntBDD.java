package state;

import java.util.Vector;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * BDD-based arithmetic.
 *
 * @since 2011-05-05
 */
@NonNullByDefault
class IntBDD {
  public static final int INT_SIZE = 32;
  private static final int INT_MSB = INT_SIZE - 1;

  public static @NonNull BDD[] intMultiBDD(int n) {
    var bdds = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++) {
      bdds[digit] = ((n & 1) != 0) ? BDD.trueBDD() : BDD.falseBDD();
      n = n >>> 1;
    }
    return bdds;
  }

  public static @NonNull BDD[] varMultiBDD(Vector<Integer> multiIndex) {
    if (multiIndex.size() > INT_SIZE - 1)
      throw new IllegalArgumentException("multi index too long");

    var bdds = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      bdds[digit] = (digit < multiIndex.size()) ? BDD.varBDD(multiIndex.get(digit)) : BDD.falseBDD();

    return bdds;
  }

  static @NonNull BDD[] extended(@NonNull BDD[] a) {
    var extended = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      extended[digit] = digit < a.length ? a[digit] : BDD.falseBDD();
    return extended;
  }

  static @NonNull BDD[] filledWith(BDD f) {
    var filled = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      filled[digit] = f;
    return filled;
  }

  static @NonNull BDD[] addCarry(@NonNull BDD[] a, @NonNull BDD[] b, BDD @Nullable [] carryOut) {
    var sum = new @NonNull BDD[INT_SIZE];
    var carry = BDD.falseBDD();

    for (int digit = 0; digit < INT_SIZE; digit++) {
      sum[digit] = BDD.exor(BDD.exor(a[digit], b[digit]), carry);
      carry = BDD.or(BDD.or(BDD.and(a[digit], b[digit]),
                                                BDD.and(a[digit], carry)),
                               BDD.and(b[digit], carry));
    }
    if (carryOut != null && carryOut.length > 0)
      carryOut[0] = carry;
    return sum;
  }

  static @NonNull BDD[] rotateRightThroughCarry(@NonNull BDD[] a, BDD carryIn, @NonNull BDD[] carryOut) {
    var rotated = new @NonNull BDD[INT_SIZE];
    carryOut[0] = a[0];
    System.arraycopy(a, 1, rotated, 0, INT_SIZE - 1);
    rotated[INT_MSB] = carryIn;
    return rotated;
  }

  static @NonNull BDD[] rotateLeftThroughCarry(@NonNull BDD[] a, BDD carryIn, @NonNull BDD[] carryOut) {
    var rotated = new @NonNull BDD[INT_SIZE];
    rotated[0] = carryIn;
    carryOut[0] = a[INT_SIZE-1];
    System.arraycopy(a, 0, rotated, 1, INT_SIZE - 1);
    return rotated;
  }

  static @NonNull BDD[] @NonNull [] negateExt(@NonNull BDD[] low, @NonNull BDD[] high) {
    var twosComplementLowCarry = new @NonNull BDD[1];
    var twosComplementLow = addCarry(not(low), intMultiBDD(1), twosComplementLowCarry);
    var twosComplementHigh = add(not(high), ite(twosComplementLowCarry[0], intMultiBDD(1), intMultiBDD(0)));
    return new @NonNull BDD[] @NonNull []{ twosComplementLow, twosComplementHigh };
  }

  static @NonNull BDD[] @NonNull [] absExt(@NonNull BDD[] low, @NonNull BDD[] high) {
    var negated = negateExt(low, high);
    return new @NonNull BDD[] @NonNull []{ ite(high[INT_MSB], negated[0], low), ite(high[INT_MSB], negated[1], high) };
  }

  static @NonNull BDD[] @NonNull [] multiplyExt(@NonNull BDD[] a, @NonNull BDD[] b) {
    var productHigh = IntBDD.intMultiBDD(0);
    var productLow = b;
    for (int i = 0; i < INT_SIZE; i++) {
      // multiply a by LSB of productLow
      var aTimesProduct0 = ite(productLow[0], a, intMultiBDD(0));
      // add aTimesProduct0 to productHigh
      productHigh = add(productHigh, aTimesProduct0);
      // shift 64bit product register right 1 bit
      var carry = productHigh[0];
      productHigh = IntBDD.shiftRightSigned(productHigh, 1);
      var carryOut = new BDD[1];
      productLow = rotateRightThroughCarry(productLow, carry, carryOut);
    }
    return new @NonNull BDD[] @NonNull []{ productLow, productHigh };
  }

  static @NonNull BDD[] @NonNull [] divideExt(@NonNull BDD[] dividendLow, @NonNull BDD[] dividendHigh, @NonNull BDD[] divisor) {
    var absDivisor = abs(divisor);
    // akku := |dividend|
    var absDividend = IntBDD.absExt(dividendLow, dividendHigh);
    var akkuLow = absDividend[0];
    var akkuHigh = absDividend[1];
    // shift 64bit akku left 1 bit
    var carryOut = new @NonNull BDD[1];
    akkuLow = rotateLeftThroughCarry(akkuLow, BDD.falseBDD(), carryOut);
    akkuHigh = rotateLeftThroughCarry(akkuHigh, carryOut[0], carryOut);
    // repeat 32 times
    for (int i = 0; i < INT_SIZE; i++) {
      // subtract the absolute value of divisor from akkuHigh
      var remainder = sub(akkuHigh, absDivisor);
      // case differentiation: if remainder >= 0, then akkuHigh := remainder;
      // if remainder < 0, then leave akkuHigh unchanged.
      var MSBRemainder = remainder[INT_MSB];
      akkuHigh = ite(MSBRemainder, akkuHigh, remainder);
      // shift 64bit akku to the left 1 bit, setting the new LSB to MSBRemainderNeg
      akkuLow = rotateLeftThroughCarry(akkuLow, BDD.not(MSBRemainder), carryOut);
      akkuHigh = rotateLeftThroughCarry(akkuHigh, carryOut[0], carryOut);
    }
    // shift akkuHigh right 1 bit
    akkuHigh = IntBDD.shiftRight(akkuHigh, 1);
    // restore correct sign of quotient
    akkuLow = ite(BDD.exor(dividendHigh[INT_MSB], divisor[INT_MSB]), negate(akkuLow), akkuLow);
    // restore correct sign of remainder
    akkuHigh = ite(dividendHigh[INT_MSB], negate(akkuHigh), akkuHigh);
    return new @NonNull BDD[] @NonNull [] { akkuLow,  akkuHigh };
  }

  public static @NonNull BDD[] add(@NonNull BDD[] a, @NonNull BDD[] b) {
    return addCarry(a, b, null);
  }

  public static @NonNull BDD[] inc(@NonNull BDD[] a) {
    return add(a, intMultiBDD(1));
  }

  public static @NonNull BDD[] sub(@NonNull BDD[] a, @NonNull BDD[] b) {
    return add(a, negate(b));
  }

  public static @NonNull BDD[] dec(@NonNull BDD[] a) {
    return add(a, intMultiBDD(-1));
  }

  public static @NonNull BDD[] negate(@NonNull BDD[] a) {
    return inc(not(a));
  }

  public static @NonNull BDD[] abs(@NonNull BDD[] a) {
    return ite(a[INT_MSB], negate(a), a);
  }

  public static @NonNull BDD[] shiftRightSigned(@NonNull BDD[] a, int numBits) {
    var shifted = new @NonNull BDD[INT_SIZE];
    if (INT_SIZE - numBits >= 0) System.arraycopy(a, 0 + numBits, shifted, 0, INT_SIZE - numBits);
    for (int digit = INT_SIZE - numBits; digit < INT_SIZE; digit++)
      shifted[digit] = a[INT_MSB];
    return shifted;
  }

  public static @NonNull BDD[] shiftRight(@NonNull BDD[] a, int numBits) {
    var shifted = new @NonNull BDD[INT_SIZE];
    if (INT_SIZE - numBits >= 0) System.arraycopy(a, 0 + numBits, shifted, 0, INT_SIZE - numBits);
    for (int digit = INT_SIZE - numBits; digit < INT_SIZE; digit++)
      shifted[digit] = BDD.falseBDD();
    return shifted;
  }

  public static @NonNull BDD[] shiftLeft(@NonNull BDD[] a, int numBits) {
    var shifted = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < numBits; digit++)
      shifted[digit] = BDD.falseBDD();
    if (INT_SIZE - numBits >= 0) System.arraycopy(a, numBits - numBits, shifted, numBits, INT_SIZE - numBits);
    return shifted;
  }

  public static @NonNull BDD[] and(@NonNull BDD[] a, @NonNull BDD[] b) {
    var and = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      and[digit] = BDD.and(a[digit], b[digit]);
    return and;
  }

  public static @NonNull BDD[] or(@NonNull BDD[] a, @NonNull BDD[] b) {
    var or = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      or[digit] = BDD.or(a[digit], b[digit]);
    return or;
  }

  public static @NonNull BDD[] exor(@NonNull BDD[] a, @NonNull BDD[] b) {
    var exor = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      exor[digit] = BDD.exor(a[digit], b[digit]);
    return exor;
  }

  /**
   * Ones complement.
   *
   * @param a an integer array representing a set of integers in twos complement
   * @return {@code a}'s ones complement
   */
  public static @NonNull BDD[] not(@NonNull BDD[] a) {
    var not = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      not[digit] = BDD.not(a[digit]);
    return not;
  }

  /**
   * If-then-else.
   *
   * @param f a BDD representing a boolean function
   * @param g an integer array representing an integer function in twos complement
   * @param h an integer array representing an integer function in twos complement
   * @return if {@code f} then {@code g} else {@code h}
   */
  public static @NonNull BDD[] ite(BDD f, @NonNull BDD[] g, @NonNull BDD[] h) {
    var ite = new @NonNull BDD[INT_SIZE];
    for (int digit = 0; digit < INT_SIZE; digit++)
      ite[digit] = BDD.ite(f, g[digit], h[digit]);
    return ite;
  }

  public static @NonNull BDD[] multiply(@NonNull BDD[] a, @NonNull BDD[] b) {
    return multiplyExt(a, b)[0];
  }

  public static @NonNull BDD[] div(@NonNull BDD[] a, @NonNull BDD[] b) {
    return divideExt(a, filledWith(a[INT_MSB]), b)[0];
  }

  public static @NonNull BDD[] mod(@NonNull BDD[] a, @NonNull BDD[] b) {
    return divideExt(a, filledWith(a[INT_MSB]), b)[1];
  }

  /**
   * Mathematical modulo.
   *
   * @param a an integer array representing an integer function in twos complement
   * @param b an integer array representing an integer function in twos complement
   * @return (0 <= mod(a, b) ? mod(a, b) : abs(b) + mod(a, b))
   */
  public static BDD[] math_mod(@NonNull BDD[] a, @NonNull BDD[] b) {
    var a_mod_b = mod(a, b);
    return ite(leq(intMultiBDD(0), a_mod_b), a_mod_b, add(abs(b), a_mod_b));
  }

  /**
   * Mathematical division.
   *
   * @param a an integer array representing an integer function in twos complement
   * @param b an integer array representing an integer function in twos complement
   * @return (0 <= mod(a, b) ? a / b : (0 <= b ? -1 : 1) + div(a, b))
   */
  public static @NonNull BDD[] math_div(@NonNull BDD[] a, @NonNull BDD[] b) {
    var pair = divideExt(a, filledWith(a[INT_MSB]), b);
    var a_div_b = pair[0];
    var a_mod_b = pair[1];
    return ite(leq(intMultiBDD(0), a_mod_b),
               a_div_b,
               add(ite(leq(intMultiBDD(0), b),
                       intMultiBDD(-1),
                       intMultiBDD(1)), a_div_b));
  }

  public static BDD leq(@NonNull BDD[] a, @NonNull BDD[] b) {
    return BDD.or(less(a, b), equal(a, b));
  }

  public static BDD less(@NonNull BDD[] a, @NonNull BDD[] b) {
    var less = BDD.and(a[INT_SIZE-1], BDD.not(b[INT_SIZE-1]));
    var equalSoFar = BDD.biimplies(a[INT_SIZE-1], b[INT_SIZE-1]);
    for (int digit = INT_SIZE-2; 0 <= digit; digit--) {
      less = BDD.or(less, BDD.and(equalSoFar, BDD.and(BDD.not(a[digit]), b[digit])));
      equalSoFar = BDD.and(equalSoFar, BDD.biimplies(a[digit], b[digit]));
    }
    return less;
  }

  public static BDD equal(@NonNull BDD[] a, @NonNull BDD[] b) {
    var equal = BDD.trueBDD();
    for (int digit = 0; digit < INT_SIZE; digit++)
      equal = BDD.and(equal, BDD.biimplies(a[digit], b[digit]));
    return equal;
  }
}
