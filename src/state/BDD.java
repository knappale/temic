package state;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * A wrapper class for the integer-based BDDs of package JDD.
 * 
 * @version 1.2
 * @since 2011-07-24
 */
@NonNullByDefault
public class BDD {
  /** a guideline value for the initial size of the node table */
  private static final int NODETABLE_SIZE = 10000;
  /** a guideline value for the initial size of the caches */
  private static final int CACHE_SIZE = 1117;
  /** the bdd manager of the underlying JDD system */
  private static jdd.bdd.BDDManager bddManager = new jdd.bdd.BDDManager(NODETABLE_SIZE, CACHE_SIZE);
  // private static jdd.bdd.BDD bddManager = new jdd.bdd.BDD(NODETABLE_SIZE, CACHE_SIZE);
  /** the canonical location (i.e. jdd-representation) of trueBDD (= 1) */
  private static final int trueBDDLocation = bddManager.getOne();
  /** the canonical BDD that is constantly true */
  private static final BDD trueBDD = new BDD(trueBDDLocation);
  /** the canonical location (i.e. jdd-representation) of falseBDD (= 0) */
  private static final int falseBDDLocation = bddManager.getZero();
  /** the canonical BDD that is constantly false */
  private static final BDD falseBDD = new BDD(falseBDDLocation);
  /** Highest variable (index) created as yet */
  private static int highestVariable = 0;
  /** the initial size of the wrapper table */
  private static final int WRAPPERTABLE_SIZE = 10000;
  /** maps canonical locations to canonical wrapper objects */
  private static final Map<Integer, WeakBDDReference> wrapperTable = new HashMap<>(WRAPPERTABLE_SIZE);
  /** a reference queue for cleaning up the wrapperTable */
  private static final ReferenceQueue<BDD> referenceQueue = new ReferenceQueue<>();

  public static void clear() {
    bddManager.cleanup();
    bddManager = new jdd.bdd.BDDManager(NODETABLE_SIZE, CACHE_SIZE);
    // bddManager = new jdd.bdd.BDD(NODETABLE_SIZE, CACHE_SIZE);
    cleanup();
    wrapperTable.clear();
    bddManager.createVar(); // dummy variable for incrementing num_vars
    highestVariable = 0;
  }

  private static class WeakBDDReference extends WeakReference<BDD> {
    final int loc;

    WeakBDDReference(BDD bdd) {
      super(bdd, BDD.referenceQueue);
      this.loc = bdd.loc;
    }
  }

  private static void cleanup() {
    WeakBDDReference bddRef;
    while ((bddRef = (WeakBDDReference)referenceQueue.poll()) != null) {
      var bddLoc = bddRef.loc;
      bddManager.deref(bddLoc);
      if (wrapperTable.get(bddLoc) == bddRef)
        wrapperTable.remove(bddLoc);
    }
  }

  final int loc;

  private BDD(int loc) {
    this.loc = loc;
  }

  public static BDD canonical(final int loc) {
    if (loc == trueBDDLocation)
      return trueBDD;
    if (loc == falseBDDLocation)
      return falseBDD;

    cleanup();

    Reference<? extends @Nullable BDD> bddRef = wrapperTable.get(loc);
    if (bddRef != null) {
      BDD bdd = bddRef.get();
      if (bdd != null)
        return bdd;
    }

    BDD bdd = new BDD(loc);
    bddManager.ref(loc);
    wrapperTable.put(loc, new WeakBDDReference(bdd));
    return bdd;
  }

  /**
   * @return the BDD representing a constantly true function
   */
  public static BDD trueBDD() {
    return trueBDD;
  }

  /**
   * @return the BDD representing a constantly false function
   */
  public static BDD falseBDD() {
    return falseBDD;
  }

  /**
   * Return the BDD representing a boolean function which 1) has
   * only one variable and 2) is true if and only if the variable is true. This
   * method acts as constructor for BDDs.
   * 
   * @param v an integer representing a variable
   * @return the location of the BDD representing {@code v} as a boolean function
   * @pre 0 \leq {@code v} < Integer.MAX_VALUE
   */
  static BDD varBDD(final int v) {
    if (v < 0 || v >= Integer.MAX_VALUE)
      throw new IllegalArgumentException("v must be a non-negative integer less than Integer.MAX_VALUE");

    int loc = 0;
    if (highestVariable < v) {
      while (highestVariable < v) {
        highestVariable++;
        loc = bddManager.createVar();
      }
    }
    else
      loc = bddManager.mk(v, falseBDDLocation, trueBDDLocation);

    return canonical(loc);
  }

  /**
   * Creates a BDD cube, which is simply a conjunction of a set of variables:
   * Given a boolean array, the BDD representing the conjunction
   * of all variables with indices between 0 and the length of the array that have
   * a their entry set in the array is returned.
   * 
   * @param v a boolean array describing a set of variables
   * @return the cube of {@code v}
   */
  public static BDD cube(final boolean[] v) {
    return canonical(bddManager.cube(v));
  }

  /**
   * If-then-else operation.
   * 
   * @param f a location of a BDD for if
   * @param g a location of a BDD for then
   * @param h a location of a BDD for else
   * @return the location of the BDD representing (f and g) or (not(f) and h)
   */
  public static BDD ite(final BDD f, final BDD g, final BDD h) {
    return canonical(bddManager.ite(f.loc, g.loc, h.loc));
  }

  /**
   * Return the BDD representing the logical negation of a boolean
   * function.
   * 
   * @param f a BDD representing a boolean function
   * @return the BDD representing not(f)
   */
  public static BDD not(final BDD f) {
    return canonical(bddManager.not(f.loc));
  }

  /**
   * Returns the BDD representing the logical conjunction of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing (f and g)
   */
  public static BDD and(final BDD f, final BDD g) {
    return canonical(bddManager.and(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the logical disjunction of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing (f or g)
   */
  public static BDD or(final BDD f, final BDD g) {
    return canonical(bddManager.or(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the logical implication of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing (f => g)
   */
  public static BDD implies(final BDD f, final BDD g) {
    return canonical(bddManager.imp(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the logical equivalence of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing (f <=> g)
   */
  public static BDD biimplies(final BDD f, final BDD g) {
    return canonical(bddManager.biimp(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the exclusive disjunction of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing (f exor g)
   */
  public static BDD exor(final BDD f, final BDD g) {
    return canonical(bddManager.xor(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the negated conjunction of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing not(f and g)
   */
  public static BDD nand(final BDD f, final BDD g) {
    return canonical(bddManager.nand(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the negated disjunction of two
   * boolean functions.
   * 
   * @param f a BDD representing a boolean function
   * @param g a BDD representing a boolean function
   * @return the BDD representing not(f or g)
   */
  public static BDD nor(final BDD f, final BDD g) {
    return canonical(bddManager.nor(f.loc, g.loc));
  }

  /**
   * Return the BDD representing the existential quantification of
   * a boolean function over a variable range.
   * 
   * @param f a BDD representing the matrix of the existential quantifier
   * @param rng a set of indices representing the variables bounded by the quantifier
   * @return the BDD representing exists {@code r} . {@code f}
   */
  public static BDD exists(final BDD f, final Set<Integer> rng) {
    boolean[] r = new boolean[util.Math.max(rng)+1];
    for (int v : rng)
      r[v] = true;
    return canonical(bddManager.exists(f.loc, bddManager.cube(r)));
  }

  /**
   * Return the BDD representing the universal quantification of a
   * boolean function over a variable range.
   * 
   * @param f a BDD representing the matrix of the universal quantifier
   * @param rng a set of indices representing the variables bounded by the quantifier
   * @return the BDD representing forall {@code r} . {@code f}
   */
  public static BDD forall(final BDD f, final Set<Integer> rng) {
    boolean[] r = new boolean[util.Math.max(rng)+1];
    for (int v : rng)
      r[v] = true;
    return canonical(bddManager.forall(f.loc, bddManager.cube(r)));
  }

  /**
   * Return true iff a BDD is the location of the BDD representing true.
   *
   * @param f a BDD representing a boolean function
   * @return true iff {@code f} represents true
   */
  public static boolean isTrue(final BDD f) {
    return f == trueBDD;
  }

  /**
   * Return true iff a BDD is the location of the BDD representing false.
   *
   * @param f a BDD representing a boolean function
   * @return true iff {@code f} represents false
   */
  public static boolean isFalse(final BDD f) {
    return f == falseBDD;
  }

  /**
   * Return the head-variable of a BDD if the BDD node is an inner
   * node and {@code Integer.MAX_VALUE} otherwise.
   * 
   * @return the head-variable of {@code f} if it represents an inner BDD node,
   *         and {@code Integer.MAX_VALUE} otherwise
   */
  public static int getVar(final BDD f) {
    if (f == trueBDD || f == falseBDD)
      return Integer.MAX_VALUE;
    return bddManager.getVar(f.loc) & jdd.bdd.BDDManager.NODE_UNMARK;
    // return bddManager.getVar(f.loc) & jdd.bdd.BDD.NODE_UNMARK;
  }

  /**
   * Return the then-successor of a BDD if the BDD node is an inner node.
   *
   * @param f a BDD representing a boolean function
   * @return the then-successor of {@code f}
   * @pre f != trueBDD() && f != falseBDD()
   */
  public static BDD getThen(final BDD f) {
    return canonical(bddManager.getHigh(f.loc));
  }

  /**
   * Return the else-successor of a BDD if the BDD node is an inner node.
   *
   * @param f a BDD representing a boolean function
   * @return the else-successor of {@code f}
   * @pre f != trueBDD() && f != falseBDD()
   */
  public static BDD getElse(final BDD f) {
    return canonical(bddManager.getLow(f.loc));
  }

  public static class Valuation {
    private final Map<Integer, Boolean> map = new HashMap<>();

    public Valuation() {
    }

    public Valuation put(int idx, boolean value) {
      this.map.put(idx, value);
      return this;
    }

    public Valuation putAll(Valuation other) {
      this.map.putAll(other.map);
      return this;
    }
 
    public boolean get(int idx) {
      Boolean value = this.map.get(idx);
      return (value != null) ? value : false;
    }

    /**
     * @return whether this valuation shows no entries
     */
    public boolean isEmpty() {
      return this.map.isEmpty();
    }

    /**
     * @return the location of the BDD representing this valuation
     */
    public BDD asBDD() {
      var result = BDD.trueBDD();
      for (Map.Entry<Integer, Boolean> entry : map.entrySet()) {
        @Nullable Integer idx = entry.getKey();
        @Nullable Boolean value = entry.getValue();
        result = BDD.and(result, value ? BDD.varBDD(idx) : BDD.not(BDD.varBDD(idx)));
      }
      return result;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.map);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Valuation other = (Valuation)object;
        return Objects.equals(this.map, other.map);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  /**
   * Return an (arbitrarily chosen) valuation satisfying the boolean function
   * represented by a BDD, if such a valuation exists. Otherwise
   * (i.e., if the BDD is constantly false) {@code null} is returned.
   * 
   * @param f a BDD representing a boolean function
   * @return a valuation satisfying the boolean function represented by this BDD,
   *         if such a valuation exists; otherwise {@code null}.
   */
  public static @Nullable Valuation satisfyingValuation(final BDD f) {
    if (f == falseBDD)
      return null;
    int[] oneSat = bddManager.oneSat(f.loc, null);
    Valuation satisfyingValuation = new Valuation();
    for (int i = 0; i < oneSat.length; i++) {
      if (oneSat[i] != 0)
        satisfyingValuation.put(i, oneSat[i] == 1);
    }
    return satisfyingValuation;
  }

  /**
   * Create and return the renamed copy of a BDD, in which all variables labeling
   * inner nodes have been renamed according to a strictly increasing mapping of
   * non-negative integers to non-negative integers.
   * 
   * @param f a BDD representing a boolean function
   * @param renaming a renaming from variables to variables
   * @return the BDD representing the renamed copy of {@code f} in
   *         which all variables labeling inner nodes have been substituted
   */
  public static BDD rename(final BDD f, final Map<Integer, Integer> renaming) {
    var size = renaming.size();
    if (size == 0)
      return f;

    var oldLocs = new int[size];
    var newLocs = new int[size];
    var i = 0;
    for (var entry : renaming.entrySet()) {
      oldLocs[i] = bddManager.mk(entry.getKey(), falseBDDLocation, trueBDDLocation);
      newLocs[i] = bddManager.mk(entry.getValue(), falseBDDLocation, trueBDDLocation);
      i++;
    }
    return canonical(bddManager.replace(f.loc, bddManager.createVariableMapping(oldLocs, newLocs)));
    // return canonical(bddManager.replace(f.loc, bddManager.createPermutation(oldLocs, newLocs)));
  }

  /**
   * Return true if all satisfying evaluations of f are also satisfying
   * evaluations of g; otherwise false.
   * 
   * @param f a BDD representing a set of satisfying evaluations
   * @param g a BDD representing a set of satisfying evaluations
   * @return true iff all satisfying evaluations of {@code f} are also satisfying
   *         evaluations of {@code g}
   */
  public static boolean subsets(final BDD f, final BDD g) {
    return bddManager.imp(f.loc, g.loc) == trueBDDLocation;
  }

  /**
   * Return the number of valuations satisfying a BDD for a set of variable numbers.
   *
   * @param f a BDD representing a boolean function
   * @param vs a set of variable numbers
   * @param m memoisation
   * @return the number of valuations satisfying {@code f}
   */
  private static BigInteger countSatVals(final BDD f, final SortedSet<Integer> vs, final Map<BDD, BigInteger> m) {
    BigInteger c = m.get(f);
    if (c != null)
      return c;

    int v = getVar(f);
    BDD t = getThen(f);
    BDD e = getElse(f);

    c =      BigInteger.valueOf(2).pow(vs.subSet(v+1, getVar(t)).size()).multiply(countSatVals(t, vs, m))
        .add(BigInteger.valueOf(2).pow(vs.subSet(v+1, getVar(e)).size()).multiply(countSatVals(e, vs, m)));
    m.put(f, c);
    return c;
  }

  /**
   * Return the number of valuations satisfying a BDD for a set of variable numbers.
   *
   * @param f a BDD representing a boolean function
   * @param vs a set of variable numbers
   * @return the number of valuations satisfying {@code f}
   */
  public static BigInteger countSatVals(final BDD f, final SortedSet<Integer> vs) {
    Map<BDD, BigInteger> m = new HashMap<>();
    m.put(trueBDD, BigInteger.valueOf(1));
    m.put(falseBDD, BigInteger.valueOf(0));
    return BigInteger.valueOf(2).pow(vs.subSet(1, getVar(f)).size()).multiply(countSatVals(f, vs, m));
  }

  /**
   * Return the number of BDD nodes a BDD is composed of.
   *
   * @param f a BDD representing a boolean function
   * @return the number of BDD nodes {@code f} is composed of
   */
  public static int size(final BDD f) {
    int count = bddManager.nodeCount(f.loc);
    return ((count > 0) ? (2 + count) : 1);
  }

  @Override
  public int hashCode() {
    return this.loc;
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      BDD other = (BDD)object;
      return this.loc == other.loc;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
