package state;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public interface Value extends Expression {
  public boolean getBoolean();
  public int getInteger();

  public static class Boolean implements Expression.Boolean, Value {
    private final boolean value;

    public Boolean(boolean value) {
      this.value = value;
    }

    @Override
    public boolean getBoolean() {
      return this.value;
    }

    @Override
    public int getInteger() {
      return this.value ? 1 : 0;
    }

    @Override
    public Type.Boolean getType() {
      return Type.booleanType();
    }

    @Override
    public Value.Boolean neg() {
      return new Value.Boolean(!this.value);
    }

    @Override
    public Value.Boolean nnf() {
      return this;
    }

    @Override
    public BDD getBDD() {
      return this.value ? BDD.trueBDD() : BDD.falseBDD();
    }

    @Override
    public BDD domainBDD() {
      return BDD.trueBDD();
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public <T> T accept(Expression.Visitor<T> visitor) {
      return visitor.onBoolean(this.value);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.value);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Value.Boolean other = (Value.Boolean)object;
        return this.value == other.value;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      return new StringBuilder().append(this.value).toString();
    }
  }

  public static class Integer extends Expression.Integer implements Value {
    private final int value;

    public Integer(int value) {
      this.value = value;
    }

    @Override
    public boolean getBoolean() {
      return this.value != 0;
    }

    @Override
    public int getInteger() {
      return this.value;
    }

    @Override
    public Type.Integer getType() {
      return Type.integerType(this.value, this.value);
    }

    @Override
    public @NonNull BDD[] getIntBDD() {
      return IntBDD.intMultiBDD(this.value);
    }

    @Override
    public BDD domainBDD() {
      return BDD.trueBDD();
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public <T> T accept(Expression.Visitor<T> visitor) {
      return visitor.onInteger(this.value);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.value);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Value.Integer other = (Value.Integer)object;
        return this.value == other.value;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      return new StringBuilder().append(this.value).toString();
    }
  }
}
