package state;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import system.Declaration;


@NonNullByDefault
public interface Variable extends Declaration, Expression {
  public interface Visitor<T> {
    public T onBoolean(Boolean variable);
    public T onInteger(Integer variable);
  }

  @NonNullByDefault({})
  public class Cases<T> implements Visitor<T> {
    private Function<@NonNull Boolean, T> booleanVarFun = null;
    private Function<@NonNull Integer, T> integerVarFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public Cases<T> booleanVar(Function<@NonNull Boolean, T> booleanVarFun) {
      this.booleanVarFun = booleanVarFun;
      return this;
    }
    
    public Cases<T> integerVar(Function<@NonNull Integer, T> integerVarFun) {
      this.integerVarFun = integerVarFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply(Variable variable) {
      return variable.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on variable";
      return this.otherwiseFun.get();
    }

    public T onBoolean(Boolean variable) {
      return this.booleanVarFun != null ? this.booleanVarFun.apply(variable) : otherwise();
    }

    public T onInteger(Integer variable) {
      return this.integerVarFun != null ? this.integerVarFun.apply(variable) : otherwise();
    }
  }

  public abstract String getName();
  public abstract Type getType();

  public abstract Value getDefault();

  public abstract Expression.Boolean getConstraint();

  public abstract <T> T accept(Visitor<T> visitor);

  public default <T> T apply(Cases<T> cases) {
    return cases.apply(this);
  }

  public static class Boolean implements Expression.Boolean, Variable {
    private final String name;

    public Boolean(String name) {
      this.name = name;
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public Type.Boolean getType() {
      return Type.booleanType();
    }

    @Override
    public Value.Boolean getDefault() {
      return this.getType().getDefault();
    }

    @Override
    public Expression.Boolean neg() {
      return new Expression.Boolean.Not(this);
    }

    @Override
    public Variable.Boolean nnf() {
      return this;
    }

    @Override
    public Expression.Boolean getConstraint() {
      return Expression.Boolean.TRUEEXPR;
    }

    @Override
    public BDD getBDD() {
      return SpaceManager.getBDD(this);
    }

    @Override
    public BDD domainBDD() {
      return BDD.trueBDD();
    }

    @Override
    public int getPrecedence() {
      return 1;
    }

    @Override
    public <T> T accept(Variable.Visitor<T> visitor) {
      return visitor.onBoolean(this);
    }

    @Override
    public <T> T accept(Expression.Visitor<T> visitor) {
      return visitor.onBooleanVariable(this);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.name);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Variable.Boolean other = (Variable.Boolean)object;
        return Objects.equals(this.name, other.name);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      return this.getName();
    }
  }

  public static class Integer extends Expression.Integer implements Variable {
    private final String name;
    private final Type.Integer type;

    public Integer(String name, Type.Integer type) {
      this.name = name;
      this.type = type;
    }

    public Integer(String name, int lower, int upper) {
      this(name, Type.integerType(lower, upper));
    }

    @Override
    public String getName() {
      return this.name;
    }

    @Override
    public Type.Integer getType() {
      return this.type;
    }

    @Override
    public Value.Integer getDefault() {
      return this.getType().getDefault();
    }

    @Override
    public Expression.Boolean getConstraint() {
      return new Expression.Boolean.Connective(new Expression.Boolean.Comparison(new Value.Integer(this.getType().getLower()), Expression.Boolean.Comparison.Kind.LEQ, this),
                                               Expression.Boolean.Connective.Kind.AND,
                                               new Expression.Boolean.Comparison(this, Expression.Boolean.Comparison.Kind.LEQ, new Value.Integer(this.getType().getUpper())));
    }

    @Override
    public @NonNull BDD[] getIntBDD() {
      return SpaceManager.getIntBDD(this);
    }

    @Override
    public BDD domainBDD() {
      return BDD.trueBDD();
    }

    @Override
    public int getPrecedence() {
      return 1;
    }

    @Override
    public <T> T accept(Variable.Visitor<T> visitor) {
      return visitor.onInteger(this);
    }

    @Override
    public <T> T accept(Expression.Visitor<T> visitor) {
      return visitor.onIntegerVariable(this);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.name, this.type);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Variable.Integer other = (Variable.Integer)object;
        return Objects.equals(this.name, other.name) &&
               Objects.equals(this.type, other.type);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      return this.getName();
    }
  }

  @Override
  public default String declaration() {
    StringBuilder builder = new StringBuilder();
    builder.append("var ");
    builder.append(this.getName());
    builder.append(" : ");
    builder.append(this.getType());
    builder.append(";");
    return builder.toString();
  }

  public default String boundDeclaration() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.getName());
    builder.append(" : ");
    builder.append(this.getType());
    return builder.toString();
  }
}
