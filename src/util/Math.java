package util;

import java.util.Set;

public class Math {
  public static int min(Set<Integer> set) {
    int ret = Integer.MAX_VALUE;
    for (int value : set) {
      if (value < ret) {
        ret = value;
      }
    }
    return ret;
  }

  public static int max(Set<Integer> set) {
    int ret = Integer.MIN_VALUE;
    for (int value : set) {
      if (ret < value) {
        ret = value;
      }
    }
    return ret;
  }

  public static int mod(int a, int b) {
    return (0 <= a % b ? a % b : java.lang.Math.abs(b) + a % b);
  }

  public static int div(int a, int b) {
    return (0 <= a % b ? a / b : (0 <= b ? -1 : 1) + (a / b));
  }

  public static int floor(double value) {
    return (int) java.lang.Math.floor(value);
  }

  public static int ceil(double value) {
    return (int) java.lang.Math.ceil(value);
  }

  public static double log2(double value) {
    return java.lang.Math.log(value) / java.lang.Math.log(2);
  }
}
