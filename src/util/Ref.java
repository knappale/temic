package util;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * A wrapper class for call by reference.
 *
 * @param <T> type of the value referred to
 */
@NonNullByDefault
public class Ref<T> {
  private T t;

  public Ref(T t) {
    this.t = t;
  }

  public T get() {
    return this.t;
  }

  public void set(T t) {
    this.t = t;
  }
}
