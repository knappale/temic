package util;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import org.eclipse.jdt.annotation.NonNull;


public class Formatter {
  public static @NonNull String format(String text, int length) {
    return (text.length() < length) ? text + numSpaces(length-text.length()) : text;
  }
	
  public static @NonNull String format(int value, int lowerBound, int upperBound) {
    return format(value, java.lang.Math.max(("" + lowerBound).length(), ("" + upperBound).length()));
  }
	
  public static @NonNull String format(int value, int length) {
    String asString = "" + value;
    return (asString.length() < length) ? (numSpaces(length-asString.length())) + asString : asString;
  }
	
  public static @NonNull String numSpaces(int n) {
    StringBuilder res = new StringBuilder(n);
    for (int i=0; i<n; i++) {
      res.append(" ");
    }
    return res.toString();
  }

  /**
   * @param item an item
   * @return item's string representation, surrounded by quotes
   */
  public static <T> @NonNull String quoted(T item) {
    StringBuilder builder = new StringBuilder();
    builder.append("`");
    builder.append(item);
    builder.append("'");
    return builder.toString();
  }

  /**
   * @param item an item
   * @param hasPrecedence test whether the item takes precedence
   * @param map yielding item's string representation
   * @return item's string representation, surrounded with parentheses
   *         if {@code hasPrecedence.test(item)} is false
   */
  public static <T> @NonNull String parenthesised(final T item, @NonNull Predicate<T> hasPrecedence, Function<T, String> map) {
    if (hasPrecedence.test(item)) {
      String representation = map.apply(item);
      if (representation == null)
        return "null";
      return representation;
    }

    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("(");
    resultBuilder.append(map.apply(item));
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  /**
   * @param item an item
   * @param hasPrecedence test whether the item takes precedence
   * @return item's string representation, surrounded with parentheses
   *         if {@code hasPrecedence.test(item)} is false
   */
  public static <T> @NonNull String parenthesised(final T item, @NonNull Predicate<T> hasPrecedence) {
    return parenthesised(item, hasPrecedence, i -> (i == null) ? "null" : i.toString());
  }


  /**
   * @param item an item
   * @return item's string representation, surrounded with parentheses
   */
  public static <T> @NonNull String parenthesised(final T item) {
    return parenthesised(item, i -> false);
  }

  /**
   * Return formatted string for a collection of items using a map for formatting each item,
   * separating the items by a separator, and adding some string before and after the formatted
   * collection if it is not empty.
   *
   * @param <T> a type
   * @param before a string to be added before if {@code items} is not empty
   * @param items a collection of items
   * @param map a map for formatting each item in {@code items}
   * @param separator a separator between the {@code items}
   * @param after a string to be added after if {@code items} is not empty
   * @return a formatted, separated string for {@code items}
   */
  public static <T> @NonNull String separated(String before, Collection<T> items, @NonNull Function<T, String> map, String separator, String after) {
    if (items.isEmpty())
      return "";

    StringBuilder builder = new StringBuilder();
    String sep = before;
    for (T item : items) {
      builder.append(sep);
      builder.append(map.apply(item));
      sep = separator;
    }
    builder.append(after);
    return builder.toString();
  }

  public static <T> @NonNull String separated(Collection<T> items, @NonNull Function<T, String> map, String separator, String after) {
    return separated("", items, map, separator, after);
  }

  public static <T> @NonNull String separated(String before, Collection<T> items, String separator, String after) {
    return separated(before, items, (item -> (item == null) ? "null" : item.toString()), separator, after);
  }

  public static <T> @NonNull String separated(Collection<T> items, String separator, String after) {
    return separated("", items, separator, after);
  }

  public static <T> @NonNull String separated(Collection<T> items, @NonNull Function<T, String> map) {
    return separated("", items, map, ", ", "");
  }

  public static <T> @NonNull String separated(Collection<T> items) {
    return separated("", items, ", ", "");
  }

  public static <T> @NonNull String set(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    if (items.isEmpty())
      return "{ }";
    return separated("{ ", items, map, ", ", " }");
  }

  public static <T> @NonNull String set(Collection<T> items) {
    return set(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <T> @NonNull String setOrSingleton(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    if (items.size() == 1) {
      String itemString = map.apply(items.iterator().next());
      if (itemString == null)
        return "";
      return itemString;
    }
    return set(items, map);
  }

  public static <T> @NonNull String setOrSingleton(Collection<T> items) {
    return setOrSingleton(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <T> @NonNull String list(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    return separated("[", items, map, ", ", "]");
  }

  public static <T> @NonNull String list(Collection<T> items) {
    return list(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <T, U> @NonNull String pair(T item1, U item2) {
    StringBuilder builder = new StringBuilder();
    builder.append("(");
    builder.append(item1);
    builder.append(", ");
    builder.append(item2);
    builder.append(")");
    return builder.toString();
  }

  public static <T> @NonNull String tuple(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    return separated("(", items, map, ", ", ")");
  }

  public static <T> @NonNull String tuple(Collection<T> items) {
    return tuple(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <K, V> @NonNull String map(Map<K, V> map, @NonNull Function<K, String> keyMap, @NonNull Function<V, String> valueMap) {
    if (map == null)
      return "";
    if (map.isEmpty())
      return "{ }";
    return separated("{ ", map.entrySet(), (e -> keyMap.apply(e.getKey()) + " = " + valueMap.apply(e.getValue())), ", ", " }");
  }

  public static <K, V> @NonNull String map(Map<K, V> map) {
    return map(map, key -> (key == null) ? "null" : key.toString(), value -> (value == null) ? "null" : value.toString());
  }
}
