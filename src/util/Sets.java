package util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toSet;


/**
 * Auxiliary set functions
 */
@NonNullByDefault
public class Sets {
  /**
   * Compute an empty set.
   *
   * @return { }
   */
  public static <T> Set<T> empty() {
    return new HashSet<>();
  }

  /**
   * Compute a singleton set.
   *
   * @param e an element
   * @return { {@code e} }
   */
  public static <T> Set<T> singleton(T e) {
    Set<T> singleton = new HashSet<>();
    singleton.add(e);
    return singleton;
  }

  /**
   * Compute a set containing some elements.
   *
   * @return { {@code e}1, ..., {@code e}n }
   */
  @SafeVarargs
  public static <T> Set<T> elements(T... e) {
    return new HashSet<>(Arrays.asList(e));
  }

  /**
   * Compute the union of two sets.
   *
   * @param set1 a set
   * @param set2 a set
   * @return {@code set1} \cup {@code set2}
   */
  public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
    Set<T> union = new HashSet<>();
    union.addAll(set1);
    union.addAll(set2);
    return union;
  }

  /**
   * Compute the intersection of two sets.
   *
   * @param set1 a set
   * @param set2 a set
   * @return {@code set1} \cap {@code set2}
   */
  public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
    return set1.stream().filter(e -> set2.contains(e)).collect(toSet());
  }

  /**
   * Compute the set difference between two sets.
   *
   * @param set1 a set
   * @param set2 a set
   * @return {@code set1} \ {@code set2}
   */
  public static <T> Set<T> difference(Set<T> set1, Set<T> set2) {
    return set1.stream().filter(e -> !set2.contains(e)).collect(toSet());
  }

  /**
   * Compute the power set of a set.
   *
   * @param set a set
   * @return the powerset of {@code set}
   */
  public static <T> Set<Set<T>> powerSet(Set<T> set) {
    Set<Set<T>> powerSet = new HashSet<>();

    if (set.isEmpty()) {
      powerSet.add(new java.util.HashSet<T>());
      return powerSet;
    }

    T element = set.iterator().next();
    Set<T> set1 = new HashSet<>(set);
    set1.remove(element);
    java.util.Set<java.util.Set<T>> powerSet1 = powerSet(set1);
    powerSet.addAll(powerSet1);
    for (java.util.Set<T> subSet1 : powerSet1) {
      Set<T> subSet2 = new HashSet<>(subSet1);
      subSet2.add(element);
      powerSet.add(subSet2);
    }
    return powerSet;
  }

  /**
   * Determine whether two sets are disjoint.
   *
   * @param set1 a set
   * @param set2 another set
   * @return whether {@code set1} and {@code set2} are disjoint
   */
  public static <T> boolean isDisjoint(Set<T> set1, Set<T> set2) {
    for (T element1 : set1) {
      if (set2.contains(element1))
	return false;
    }
    return true;
  }
}
