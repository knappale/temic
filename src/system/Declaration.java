package system;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public interface Declaration {
  public String getName();
  public String declaration();
}
