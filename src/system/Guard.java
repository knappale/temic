package system;

import org.eclipse.jdt.annotation.NonNullByDefault;

import logic.EpistemicFormula;
import logic.TemporalEpistemicFormula;


@NonNullByDefault
public class Guard extends EpistemicFormula implements Declaration {
  private final String name;
  private final EpistemicFormula formula;

  public Guard(String name, EpistemicFormula formula) {
    this.name = name;
    this.formula = formula;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public Guard neg() {
    return new Guard("not_" + this.name, this.formula.neg());
  }

  @Override
  public Guard nnf() {
    return new Guard("NNF_" + this.name, this.formula.nnf());
  }

  @Override
  public TemporalEpistemicFormula toTemporalEpistemic() {
    return this.formula.toTemporalEpistemic();
  }

  @Override
  public <T> T accept(EpistemicFormula.Visitor<T> visitor) {
    return this.formula.accept(visitor);
  }

  @Override
  public boolean isPropositional() {
    return this.formula.isPropositional();
  }

  @Override
  public int getPrecedence() {
    return 1;
  }

  @Override
  public String declaration() {
    StringBuilder builder = new StringBuilder();
    builder.append("guard ");
    builder.append(this.getName());
    builder.append(" = ");
    builder.append(this.formula);
    builder.append(";");
    return builder.toString();
  }

  @Override
  public String toString() {
    return this.getName();
  }
}
