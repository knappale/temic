package system;

import static util.Objects.assertNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import logic.Agent;
import logic.StateFormula;
import state.SpaceManager;
import state.StateSet;
import state.StateVariable;
import state.Variable;
import util.Formatter;
import util.Objects;

import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public class Specification {
  private @Nullable StateSet cachedInitialStates;

  private final String name;
  private final Set<Declaration> declarations = new HashSet<>();
  private final StateFormula initial;
  private final List<Action> actions = new ArrayList<>();
  private final List<Assertion> assertions = new ArrayList<>();

  public static final Logger logger = assertNonNull(Logger.getLogger(Specification.class.getName()));

  public Specification(String name, List<Declaration> declarations, StateFormula initial, List<Action> actions, List<Assertion> assertions) {
    this.name = name;
    this.declarations.addAll(declarations);
    this.initial = initial;
    this.actions.addAll(actions);
    this.assertions.addAll(assertions);

    SpaceManager.initialise(Objects.subListOf(declarations, StateVariable.class));
  }

  public String getName() {
    return this.name;
  }

  public List<Action> getActions() {
    return this.actions;
  }

  public List<Assertion> getAssertions() {
    return this.assertions;
  }

  public StateSet initialStates() {
    var initialStates = this.cachedInitialStates;
    if (initialStates != null)
      return initialStates;
    return this.cachedInitialStates = this.initial.satisfyingStates();
  }

  private Set<InterpretedSystem> interpretPos(InterpretedSystem ps, Collection<Action> actions, Collection<Action> openActions) {
    var resultSet = new HashSet<InterpretedSystem>();

    if (openActions.isEmpty()) {
      if (ps.equals(new InterpretedSystem(ps.initialStates(), actions.stream().map(action -> ps.kripkeStructure().interpret(action)).collect(toSet()))))
        resultSet.add(ps);
      return resultSet;
    }

    for (var nextAction : openActions) {
      var remainingOpenActions = new HashSet<>(openActions);
      remainingOpenActions.remove(nextAction);
      resultSet.addAll(interpretPos(ps.getExtended(nextAction), actions, remainingOpenActions));
    }
    return resultSet;
  }

  private PosNNegSystem interpretPosNNeg() {
    var startTime = System.currentTimeMillis();
    var pnns = new PosNNegSystem(this.initialStates(), this.actions.stream().map(a -> new PosNNegAction(a)).collect(toSet()));
    	
    for (int numberIterations = 1; true; numberIterations++) {
      logger.info("executing iteration no " + numberIterations);
      var pnnsOld = pnns;

      var posNNegs = new HashSet<PosNNegAction>();
      for (var action : this.actions) {
        logger.fine(() -> "evaluating guard " + Formatter.quoted(action.getTemporalEpistemicPre()) + " for action " + Formatter.quoted(action.getName()));
        var posNNegStartTime = System.currentTimeMillis();
        posNNegs.add(pnns.interpret(action));
        logger.fine(() -> "Time elapsed for pos/not-neg action interpretation: " + (System.currentTimeMillis() - posNNegStartTime) + " ms");
      }

      logger.info("Calculating positive-negative system");
      var reachabilityStartTime = System.currentTimeMillis();
      pnns = new PosNNegSystem(this.initialStates(), posNNegs);
      logger.info("Time elapsed for positive/not-negative system: " + (System.currentTimeMillis() - reachabilityStartTime) + " ms");
      assert pnnsOld.leq(pnns) : "Monotonicity property of core functional violated.";
      assert !pnns.isContradictory() : "Consistency property of core functional violated.";
      logger.info("Positive/not-negative system for iteration " + numberIterations + ":\n" + pnns.info());

      if (pnnsOld.equals(pnns)) {
        logger.info("Time elapsed for positive/not-negative system interpretation: " + (System.currentTimeMillis() - startTime) + " ms");
        return pnns;
      }
    }
  }

  public InterpretedSystem interpret() {
    if (this.actions.stream().allMatch(action -> action.getTemporalEpistemicPre().isPropositional())) {
      logger.info("Interpreted system is trivially decided.");
      var system = new InterpretedSystem(this.initialStates(), this.actions.stream().map(action -> new InterpretedAction(action, action.getMaxPreStates())).collect(toSet()));
      logger.info(system.info());
      return system;
    }

    var pnns = interpretPosNNeg();

    if (pnns.isDecided()) {
      logger.info("The positive/not-negative system is decided.");
      var nns = pnns.notNegativeInterpretedSystem();
      logger.fine(() -> "Initial states:\n" + nns.initialStates());
      logger.fine(() -> "Transition relation:\n" + nns.transitionRelation());
      return nns;
    }

    logger.info("The following actions are *NOT* decided in the positive/not-negative system:");
    var nonDeterminateActions = new HashSet<Action>();
    for (var action : this.actions) {
      /*
      var ia = ps.kripkeStructure().interpret(action);
      System.out.println(ia.getInterpretation());
      if (!ia.getInterpretation().subsets(ps.kripkeStructure().transitionRelation())) {
        logger.warning("Action " + Formatter.quoted(action.getName()) + " not contained in positive system");
        openActions.add(action);
      }
      */
      var posNNegAction = pnns.interpret(action);
      if (!posNNegAction.isDecided()) {
        logger.info("action " + Formatter.quoted(action.getName()));
        nonDeterminateActions.add(action);
        logger.fine(() -> "Positive interpretation:\n" + posNNegAction.positiveInterpretedAction().getInterpretation().count());
        // logger.warning("Non-negative interpretation:\n" + posNNegAction.notNegativeInterpretedAction().getInterpretation());
        logger.fine(() -> "Difference:\n" + posNNegAction.notNegativeInterpretedAction().getInterpretation().intersection(posNNegAction.positiveInterpretedAction().getInterpretation().complement()));
      }
    }
    var ps = pnns.positiveInterpretedSystem();
    var decidedActions = actions.stream().filter(a -> !nonDeterminateActions.contains(a)).collect(toSet());
    var decidedSystem = new InterpretedSystem(pnns.initialStates(), decidedActions.stream().map(a -> ps.interpret(a)).collect(toSet()));
    var fixedPoints = interpretPos(decidedSystem, actions, nonDeterminateActions);
    logger.info("There " + (fixedPoints.size() == 1 ? "is 1 fixed-point Kripke structure." : "are " + fixedPoints.size() + " fixed-point Kripke structures."));
    if (fixedPoints.size() == 1) {
      var fixedPoint = fixedPoints.iterator().next();
      logger.fine("Fixed point:\n" + fixedPoint.info());
      logger.fine("Initial states:\n" + fixedPoint.initialStates());
      logger.fine("Transition relation:\n" + fixedPoint.transitionRelation());
      logger.fine("Difference:\n" + pnns.notNegativeInterpretedSystem().transitionRelation().intersection(fixedPoint.transitionRelation().complement()));
      return fixedPoint;
    }
    return pnns.notNegativeInterpretedSystem();
    // return pnns.positiveInterpretedSystem();
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append("specification ");
    builder.append(this.getName());
    builder.append(";\n\n");

    var variables = Objects.subSetOf(this.declarations, Variable.class);
    builder.append(Formatter.separated(variables, v -> v.declaration(), "\n", "\n\n"));

    var agents = Objects.subSetOf(this.declarations, Agent.class);
    builder.append(Formatter.separated(agents, a -> a.declaration(), "\n", "\n\n"));

    var guards = Objects.subSetOf(this.declarations, Guard.class);
    builder.append(Formatter.separated(guards, g -> g.declaration(), "\n", "\n\n"));

    builder.append("initial ");
    builder.append(this.initial);
    builder.append(";\n\n");

    builder.append(Formatter.separated(this.actions, a -> a.declaration(), "\n\n", "\n\n"));

    builder.append("end;\n\n");

    builder.append(Formatter.separated(this.assertions, a -> "check " + a.toString() + ";", ", ", "\n"));
    if (!this.assertions.isEmpty())
      builder.append("\n");

    return builder.toString();
  }
}
