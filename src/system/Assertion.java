package system;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import logic.TemporalEpistemicFormula;


@NonNullByDefault
public class Assertion {
  public enum Kind {
    INITIAL,
    REACHABLE;

    public String toString() {
      switch (this) {
        case INITIAL: return "initial";
        case REACHABLE: return "reachable";
        default: throw new RuntimeException("Unknown assertion kind.");
      }
    }
  }

  private final Kind kind;
  private final TemporalEpistemicFormula formula;

  public Assertion(Kind kind, TemporalEpistemicFormula formula) {
    this.kind = kind;
    this.formula = formula;
  }

  public Result check(InterpretedSystem is) {
    switch (this.kind) {
      case INITIAL:
        return new Result(this, is.satisfies(this.formula));
      case REACHABLE: {
        var satisfyingStates = is.satisfyingStates(this.formula).intersection(is.reachableStates());
        if (satisfyingStates.isEmpty())
          return new Result(this, false);
        return new Result(this, true, is.dfs(satisfyingStates));
      }
      default: throw new RuntimeException("Unknown assertion kind.");
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.formula);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      Assertion other = (Assertion)object;
      return Objects.equals(this.kind, other.kind) &&
             Objects.equals(this.formula, other.formula);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.kind);
    builder.append(" ");
    builder.append(this.formula);
    return builder.toString();
  }
}
