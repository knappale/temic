package system;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import state.State;
import util.Formatter;


/**
 * A finite or infinite run of a transition system.
 *
 * @author Heribert Mühlberger
 * @version 1.0
 * @since 2011-07-18
 */
@NonNullByDefault
public class Run {
  /**
   * A step in an execution of a transition system.
   *
   * @version 1.0
   * @since 2008-07-10
   */
  public static class Step {
    private final InterpretedAction interpretedAction;
    private final State postState;

    /**
     * Constructs a step representing an interpreted action in an execution.
     * 
     * @param interpretedAction an interpreted action
     * @param postState the state after {@code interpretedAction} has been executed
     */
    public Step(InterpretedAction interpretedAction, State postState) {
      this.interpretedAction = interpretedAction;
      this.postState = postState;
    }

    /**
     * @return the interpreted action of this step
     */
    public InterpretedAction getInterpretedAction() {
      return this.interpretedAction;
    }

    /**
     * @return the post state of the transition system after executing this step's action
     */
    public State getPostState() {
      return this.postState;
    }
  }

  private final State initialState;
  private final List<Step> steps = new ArrayList<>();
    
  /**
   * Constructs a run with only an initial state.
   * 
   * @param initialState a state
   */
  public Run(State initialState) {
    this.initialState = initialState;
  }

  /**
   * Constructs a run from an initial state and a list of steps.
   *
   * @param initialState a state
   * @param steps a list of steps 
   */
  public Run(State initialState, List<Step> steps) {
    this.initialState = initialState;
    this.steps.addAll(steps);
  }

  /**
   * @return the number of steps in this run
   */
  public int length() {
    return this.steps.size()-1;
  }

  /**
   * @return the initial state from which this run starts
   */
  public State getInitialState() {
    return this.initialState;
  }

  /**
   * @return the post-state reached by this run
   */
  public State getPostState() {
    if (this.steps.isEmpty())
      return this.initialState;
    return this.steps.get(this.steps.size()-1).getPostState();
  }

  /**
   * Appends a step to the end of this run.
   * 
   * @param step the step to be added to the end of the run
   */
  public void add(Step step) {
    this.steps.add(step);
  }
    
  /**
   * Append another run to this run.
   *
   * @param other a run
   * @pre the post state of the last step of this run must be equal to
   *      the initial state of the other run
   */
  public void add(Run other) {
    if (!this.getPostState().equals(other.getInitialState()))
      throw new IllegalArgumentException("the ends of the runs do not match");
    this.steps.addAll(other.steps);
  }
    
  /**
   * Determine the position of a lasso in this run, i.e., the first step
   * the post-state of which equals the post-state of the last step.
   *
   * @return the lasso position greater or equal to zero if there is a lasso
   *         and -1 otherwise
   */
  public int getLasso() {
    var lastPostState = this.getPostState();
    if (lastPostState.equals(this.initialState) && this.steps.size() > 0)
      return 0;
    for (var i = 0; i < this.steps.size()-1; i++) {
      if (lastPostState.equals(this.steps.get(i).getPostState()))
        return i+1;
    }
    return -1;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    var maxLengthOfStepNumber = ("" + (this.steps.size()+1)).length();
    var maxLengthOfActionNames = this.steps.stream().map(step -> step.getInterpretedAction().getAction().getName().length()).reduce(0, (n1, n2) -> Math.max(n1, n2));
    var lassoStartIndex = this.getLasso();

    if (this.steps.size() > 0)
      builder.append(Formatter.numSpaces("step :".length() + maxLengthOfStepNumber + " action `' ".length() + maxLengthOfActionNames));
    builder.append("init-state: ");
    builder.append(this.initialState.toString());
    if (lassoStartIndex == 0)
      builder.append("\n   --- loop starts here ---");

    for (var i = 0; i < steps.size(); i++) {
      builder.append("\nstep " + Formatter.format(i+1, maxLengthOfStepNumber) + ": ");
      builder.append("action " + Formatter.format(Formatter.quoted(this.steps.get(i).getInterpretedAction().getAction().getName()), maxLengthOfActionNames+2) + " ");
      builder.append("post-state: ");
      builder.append(this.steps.get(i).getPostState());
      if (lassoStartIndex == i)
        builder.append("\n   --- loop starts here ---");
    }

    return builder.toString();
  }
}