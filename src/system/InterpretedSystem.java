package system;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import logic.KripkeStructure;
import logic.TemporalEpistemicFormula;
import state.StateSet;
import state.TransitionRelation;
import util.Pair;
import util.Ref;


/**
 * Interpreted system.
 *
 * @author Heribert Mühlberger
 * @version 1.0
 * @since 2011-05-22
 */
@NonNullByDefault
public class InterpretedSystem {
  @SuppressWarnings("null")
  public static final Logger logger = Logger.getLogger(InterpretedSystem.class.getName());

  // private final StateSet initialStates;
  private final Set<InterpretedAction> interpretedActions;
  // private final TransitionRelation transitionRelation;
  // private final StateSet reachableStates;
  // private final StateSet reachableDeadlockStates;
  private final KripkeStructure kripkeStructure;
	
  public InterpretedSystem(StateSet initialStates, Set<InterpretedAction> interpretedActions) {
    // this.initialStates = initialStates;
    this.interpretedActions = interpretedActions;

    // var transitionRelation = TransitionRelation.empty();
    // var deadlockStates = StateSet.all();
    // for (var interpretedAction : interpretedActions) {
      // transitionRelation = transitionRelation.union(interpretedAction.getInterpretation());
      // deadlockStates = deadlockStates.intersection(interpretedAction.getInterpretation().preStates().complement());
    // }
    // transitionRelation = transitionRelation.restrictPre(transitionRelation.reachableStates(initialStates));
    this.kripkeStructure = KripkeStructure.create(initialStates, interpretedActions.stream().map(InterpretedAction::getInterpretation).collect(toSet()));
    // this.transitionRelation = transitionRelation;
    // this.reachableStates = transitionRelation.reachableStates(initialStates);
    // this.reachableDeadlockStates = this.reachableStates.intersection(deadlockStates);
    // if (!reachableDeadlockStates.isEmpty())
      // logger.warning("Interpreted system with deadlock states like " + this.reachableDeadlockStates.any());
  }

  /**
   * @return the Kripke structure associated to this interpreted system
   */
  public KripkeStructure kripkeStructure() {
    return this.kripkeStructure;
  }

  /**
   * @return the interpreted system's underlying actions
   */
  public Set<Action> actions() {
    return this.interpretedActions.stream().map(InterpretedAction::getAction).collect(toSet());
  }

  /**
   * @return the interpreted system's set of all initial states
   */
  public StateSet initialStates() {
    return this.kripkeStructure.initialStates();
  }

  /**
   * @return the interpreted system's transition relation
   */
  public TransitionRelation transitionRelation() {
    return this.kripkeStructure.transitionRelation();
  }

  /**
   * @return the interpreted system's set of all reachable states
   */
  public StateSet reachableStates() {
    return this.kripkeStructure.reachableStates();
  }
    
  /**
   * Determine whether this interpreted system subsets
   * another interpreted system.
   *
   * @param other some interpreted system
   * @return whether this interpreted system subsets {@code other}
   */
  public boolean subsets(InterpretedSystem other) {
    return other.actions().containsAll(this.actions()) &&
           this.kripkeStructure.subsets(other.kripkeStructure);
  }

  /**
   * Interpret an action over this interpreted system.
   *
   * @param action an action
   * @return the interpreted action resulting from interpreting {@code action} over this interpreted system
   */
  public InterpretedAction interpret(Action action) {
    return this.kripkeStructure.interpret(action);
  }

  /**
   * Compute a new interpreted system that additionally contains an
   * action when interpreted over this interpreted system.
   * 
   * @param action an action
   * @return interpreted system extended by {@code action}
   */
  public InterpretedSystem getExtended(Action action) {
    var interpretedActions = new HashSet<>(this.interpretedActions);
    interpretedActions.add(this.interpret(action));
    return new InterpretedSystem(this.kripkeStructure.initialStates(), interpretedActions);
  }

  /**
   * Determine the set of (reachable) states of this interpreted system satisfying a temporal-epistemic formula.
   * 
   * @param formula a temporal-epistemic formula
   * @return set of states satisfying {@code formula}
   */
  public StateSet satisfyingStates(TemporalEpistemicFormula formula) {
    return this.kripkeStructure.satisfyingStates(formula);
  }
    
  /**
   * Determine whether a temporal-epistemic formula holds in this interpreted system.
   *
   * @param formula a temporal-epistemic formula
   * @return whether {@code formula} holds in this interpreted system
   */
  public boolean satisfies(TemporalEpistemicFormula formula) {
    return this.kripkeStructure.satisfies(formula);
  }
    
  /**
   * For any given set or elementary states {@code prop} a finite run that starts
   * in an initial state and ends in a state belonging to {@code prop} is produced
   * provided that such a trace exists; otherwise {@code null} is returned.
   * 
   * @param prop a bdd representing a set of elementary states
   * @return a run that starts in an initial state and ends in a state that
   *         satisfies/belongs to {@code prop} if such a trace exists; otherwise
   *         {@code null}
   */
  public @Nullable Run dfs(StateSet prop) {
    var trace = new Stack<Pair<InterpretedAction, StateSet>>();
    var found = dfs(prop, trace, new Ref<>(this.initialStates().complement()));
    if (!found)
      return null;

    logger.fine(() -> trace.toString());

    // specialize the search results in reverse direction
    // (beginning with the state that satisfies prop)
    var steps = new LinkedList<Run.Step>();
    var toReach = (trace.isEmpty() ? this.initialStates() : trace.peek().getSecond()).intersection(prop).any();
    while (!trace.isEmpty()) {
      var step = trace.pop();
      steps.add(0, new Run.Step(step.getFirst(), toReach));
      var preStates = step.getFirst().getInterpretation().preImage(toReach.singleton()).intersection(this.reachableStates());
      toReach = (trace.isEmpty() ? this.initialStates() : trace.peek().getSecond()).intersection(preStates).any();
    }
    return new Run(toReach, steps);
  }
    
  /**
   * Perform a recursive depth first search in the interpreted system.
   * If the method is called with a property {@code prop}, a stack {@code trace}
   * consisting of only one step and the post state of this step equal to
   * the negation of the bdd of {@code notVisited} states, the method on termination
   * either yields a trace with the following properties:
   * 1) the post state of the first (trivial) step of the trace is
   *    given by the initial condition,
   * 2) the post state of the last step of the trace comprises at least one
   *    elementary state that models the property {@code prop};
   * or no such trace exists.
   */ 
  private boolean dfs(StateSet prop, Stack<Pair<InterpretedAction, StateSet>> trace, Ref<StateSet> notVisited) {
    var states = trace.isEmpty() ? this.initialStates() : trace.peek().getSecond();
        
    // if state models prop then terminate the search
    if (!states.intersection(prop).isEmpty())
      return true;
        
    // calculate successor state(s) for each action
    for (var interpretedAction : this.interpretedActions) {
      var successorStates = interpretedAction.getInterpretation().image(states);
            
      // subtract all states which have already been visited
      var newPart = successorStates.intersection(notVisited.get());
      if (newPart.isEmpty())
        continue;

      notVisited.set(newPart.complement().intersection(notVisited.get()));
      trace.push(new Pair<>(interpretedAction, newPart));
      var found = dfs(prop, trace, notVisited);
      if (found)
        return true;
    }
        
    trace.pop();
    return false;
  }

  public String info() {
    return this.kripkeStructure.info();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.interpretedActions, this.kripkeStructure);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      InterpretedSystem other = (InterpretedSystem)object;
      return Objects.equals(this.interpretedActions, other.interpretedActions) &&
             Objects.equals(this.kripkeStructure, other.kripkeStructure);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
