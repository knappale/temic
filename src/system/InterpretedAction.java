package system;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.StateSet;
import state.TransitionRelation;


/**
 * Interpreted action.
 *
 * @author Alexander Knapp
 * @version 1.0
 * @since 2022-06-07
 */
@NonNullByDefault
public class InterpretedAction {
  private final Action action;
  private final TransitionRelation interpretation;

  public InterpretedAction(Action action, StateSet epreStates) {
    this.action = action;
    this.interpretation = action.evaluate(epreStates);
  }

  public Action getAction() {
    return this.action;
  }

  public TransitionRelation getInterpretation() {
    return this.interpretation;
  }

  /**
   * Determine whether this interpreted action subsets another interpreted action.
   *
   * @param other some interpreted action
   * @return whether this interpreted action subsets {@code other}
   */
  public boolean subsets(InterpretedAction other) {
    return this.interpretation.subsets(other.interpretation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.action, this.interpretation);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      InterpretedAction other = (InterpretedAction)object;
      return Objects.equals(this.action, other.action) &&
             Objects.equals(this.interpretation, other.interpretation);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.action.getName();
  }
}
