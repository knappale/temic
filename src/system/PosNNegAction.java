package system;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import state.StateSet;


/**
 * Positive/not-negative interpreted action.
 *
 * @author Alexander Knapp
 * @version 1.0
 * @since 2022-06-07
 */
@NonNullByDefault
public class PosNNegAction {
  private final InterpretedAction pos;
  private final InterpretedAction nneg;

  private PosNNegAction(InterpretedAction pos, InterpretedAction nneg) {
    this.pos = pos;
    this.nneg = nneg;
  }

  public PosNNegAction(Action action, StateSet positiveStates, StateSet notNegativeStates) {
    this(new InterpretedAction(action, positiveStates), new InterpretedAction(action, notNegativeStates));
  }

  public PosNNegAction(Action action) {
    this(action, action.getMinPreStates(), action.getMaxPreStates());
  }

  /**
   * @return the set of all positively reachable states
   */
  public InterpretedAction positiveInterpretedAction() {
    return this.pos;
  }
    
  /**
   * @return the set of all not negatively reachable states
   */
  public InterpretedAction notNegativeInterpretedAction() {
    return this.nneg;
  }

  /**
   * @return whether this positive/not-negative action is contradictory, i.e., whether
   *         its positive interpreted action does not subset its not-negative
   *         interpreted action
   */
  public boolean isContradictory() {
    return !this.pos.subsets(this.nneg);
  }

  /**
   * @return whether this positive/not-negative action is determinate, i.e., whether
   *         it is not contradictory and its positive interpreted action equals
   *         its not-negative interpreted action
   */
  public boolean isDecided() {
    return !this.isContradictory() && this.pos.equals(this.nneg);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.pos, this.nneg);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      PosNNegAction other = (PosNNegAction)object;
      return Objects.equals(this.pos, other.pos) &&
             Objects.equals(this.nneg, other.nneg);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
