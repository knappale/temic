package system;

import org.eclipse.jdt.annotation.NonNullByDefault;

import state.Expression;
import state.StateVariable;
import state.TransitionRelation;


@NonNullByDefault
public abstract class Assignment {
  public abstract StateVariable getLVar();

  public abstract TransitionRelation evaluate();

  public static class Boolean extends Assignment {
    private final StateVariable.Boolean variable;
    private final Expression.Boolean expression;

    public Boolean(StateVariable.Boolean variable, Expression.Boolean expression) {
      this.variable = variable;
      this.expression = expression;
    }

    @Override
    public StateVariable.Boolean getLVar() {
      return this.variable;
    }

    @Override
    public TransitionRelation evaluate() {
      return TransitionRelation.assignBoolean(this.variable, this.expression);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.variable.getName());
      builder.append(" := ");
      builder.append(this.expression);
      return builder.toString();
    }
  }

  public static class Integer extends Assignment {
    private final StateVariable.Integer variable;
    private final Expression.Integer expression;

    public Integer(StateVariable.Integer variable, Expression.Integer expression) {
      this.variable = variable;
      this.expression = expression;
    }

    @Override
    public StateVariable.Integer getLVar() {
      return this.variable;
    }

    @Override
    public TransitionRelation evaluate() {
      return TransitionRelation.assignInteger(this.variable, this.expression);
   }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.variable.getName());
      builder.append(" := ");
      builder.append(this.expression);
      return builder.toString();
    }
  }
}
