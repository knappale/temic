package system;

import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import logic.Propositional;
import logic.StateFormula;
import logic.TemporalEpistemicFormula;
import state.StateSet;
import state.StateVariable;
import state.TransitionRelation;
import util.Formatter;


@NonNullByDefault
public class Action {
  private final String name;
  private final @Nullable TemporalEpistemicFormula tepre;
  private final StateFormula pre;
  private final List<Assignment> assignments = new ArrayList<>();
  private @Nullable StateSet cachedStandardPre;
  private @Nullable TransitionRelation cachedStandard;

  public Action(String name, @Nullable TemporalEpistemicFormula tepre, StateFormula pre, List<Assignment> assignments) {
    this.name = name;
    this.tepre = tepre;
    this.pre = pre;
    this.assignments.addAll(assignments);
  }

  public String getName() {
    return this.name;
  }

  public TemporalEpistemicFormula getTemporalEpistemicPre() {
    var tepre = this.tepre;
    if (tepre == null)
      return new TemporalEpistemicFormula.Propositional(Propositional.trueFrm());
    return tepre;
  }

  private Set<StateVariable> getLVars() {
    return this.assignments.stream().map(a -> a.getLVar()).collect(toSet());
  }

  private Set<StateVariable> getUnchangedVariables() {
    return StateSet.getVariables().stream().filter(v -> !this.getLVars().contains(v)).collect(toSet());
  }

  /**
   * @return this action's pre-states when its temporal-epistemic precondition is assumed to be false
   */
  public StateSet getMinPreStates() {
    var tepre = this.tepre;
    if (tepre == null || tepre.equals(TemporalEpistemicFormula.trueFrm()))
      return this.getStandardPre();
    return StateSet.empty();
  }

  /**
   * @return this action's pre-states when its temporal-epistemic precondition is assumed to be true
   */
  public StateSet getMaxPreStates() {
    return this.getStandardPre();
  }

  /**
   * Determine this action's transition relation when assuming that
   * it's temporal-epistemic guard is true at a given set of states.
   *
   * @param tepreStates a set of states where the temporal-epistemic guard of this action
   *                    is assumed to be true
   * @return this action's transition relation with the temporal-epistemic guard assumed
   *         to be satisfied by {@code epreStates}
   */
  public TransitionRelation evaluate(StateSet tepreStates) {
    return this.getStandard().restrictPre(tepreStates);
  }

  /**
   * @return the set of states satisfying this action's standard pre-condition
   */
  private StateSet getStandardPre() {
    if (this.cachedStandardPre != null)
      return this.cachedStandardPre;
    this.cachedStandardPre = this.pre.satisfyingStates();
    return this.cachedStandardPre;
  }

  /**
   * @return this action's transition relation ignoring the temporal-epistemic guard
   */
  private TransitionRelation getStandard() {
    var cachedStandard = this.cachedStandard;
    if (cachedStandard != null)
      return cachedStandard;
    
    // standard precondition
    var preStates = this.pre.satisfyingStates();

    // simultaneous assignments
    var assignmentsRelation = TransitionRelation.all();
    for (var assignment : this.assignments)
      assignmentsRelation = assignmentsRelation.intersection(assignment.evaluate());

    // frame condition
    var frameRelation = TransitionRelation.id(this.getUnchangedVariables());

    cachedStandard = assignmentsRelation.intersection(frameRelation).restrictPre(preStates);
    return this.cachedStandard = cachedStandard;
  }

  public String declaration() {
    StringBuilder builder = new StringBuilder();
    builder.append("action ");
    builder.append(this.getName());
    var tepre = this.tepre;
    if (tepre != null) {
      builder.append("\ntepre ");
      builder.append(tepre);
    }
    builder.append("\npre   ");
    builder.append(this.pre);
    builder.append("\ndo    ");
    builder.append(Formatter.separated(assignments));
    builder.append(";");
    return builder.toString();
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
