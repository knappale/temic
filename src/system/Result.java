package system;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


@NonNullByDefault
public class Result {
  private final Assertion assertion;
  private final boolean b;
  private @Nullable Run run;

  public Result(Assertion assertion, boolean b) {
    this.assertion = assertion;
    this.b = b;
  }

  public Result(Assertion assertion, boolean b, @Nullable Run run) {
    this.assertion = assertion;
    this.b = b;
    this.run = run;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.assertion, this.b, this.run);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      Result other = (Result)object;
      return Objects.equals(this.assertion, other.assertion) &&
             this.b == other.b &&
             Objects.equals(this.run, other.run);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append("Assertion ");
    builder.append(Formatter.quoted(this.assertion));
    builder.append(this.b ? " holds" : " does not hold");
    if (this.run != null) {
      builder.append(" as witnessed by\n");
      builder.append(this.run);
    }
    else
      builder.append(".");
    return builder.toString();
  }
}
