package system;

import static java.util.stream.Collectors.toSet;

import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import logic.TemporalEpistemicFormula;
import state.StateSet;


/**
 * Positive/not-negative system.
 *
 * @author Heribert Mühlberger
 * @version 1.0
 * @since 2011-05-22
 */
@NonNullByDefault
public class PosNNegSystem {
  private final InterpretedSystem pos;
  private final InterpretedSystem nneg;
	
  public PosNNegSystem(StateSet initialStates, Set<PosNNegAction> posNNegActions) {
    this.pos = new InterpretedSystem(initialStates, posNNegActions.stream().map(PosNNegAction::positiveInterpretedAction).collect(toSet()));
    this.nneg = new InterpretedSystem(initialStates, posNNegActions.stream().map(PosNNegAction::notNegativeInterpretedAction).collect(toSet()));
  }

  /**
   * @return the initial states of this positive/not-negative system
   */
  public StateSet initialStates() {
    return this.pos.initialStates();
  }

  /**
   * @return the set of all positively reachable states
   */
  public InterpretedSystem positiveInterpretedSystem() {
    return this.pos;
  }
    
  /**
   * @return the set of all not negatively reachable states
   */
  public InterpretedSystem notNegativeInterpretedSystem() {
    return this.nneg;
  }

  /**
   * Determine whether this positive/not-negative system is less or equal
   * another positive/not-negative system, i.e., its positive system subsets the
   * other's positive system and the other's not-negative system subsets its
   * not-negative system.
   *
   * @param other some positive/not-negative system
   * @return whether this positive/not-negative system is less or equal {@code other}
   */
  public boolean leq(PosNNegSystem other) {
    return this.pos.subsets(other.pos) &&
           other.nneg.subsets(this.nneg);
  }

  /**
   * @return whether this positive/not-negative system is contradictory, i.e., whether
   *         its positive system does not subset its not-negative system
   */
  public boolean isContradictory() {
    return !this.pos.subsets(this.nneg);
  }

  /**
   * @return whether this positive/not-negative system is determinate, i.e., whether
   *         it is not contradictory and its positive system equals its not-negative system
   */
  public boolean isDecided() {
    return !this.isContradictory() && this.pos.equals(this.nneg);
  }

  /**
   * Interpret an action over this positive/not-negative system, yielding a
   * positive/not-negative action.
   *
   * @param action an action
   * @return the positive/not-negative interpretation of {@code action} over this positive/not-negative system
   */
  public PosNNegAction interpret(Action action) {
    var tepre = action.getTemporalEpistemicPre();
    return new PosNNegAction(action, this.positiveStates(tepre), this.notNegativeStates(tepre));
  }

  private final TemporalEpistemicFormula.Cases<StateSet> satisfying =
      new TemporalEpistemicFormula.Cases<StateSet>().atom(a -> StateSet.satisfyingStates(a)).
                                                     propositional(p -> p.satisfyingStates(f -> this.satisfying.apply(f))).
                                                     epistemic(e -> e.satisfyingStates(this.positiveInterpretedSystem().reachableStates(), this.notNegativeInterpretedSystem().reachableStates(), f -> this.satisfying.apply(f))).
                                                     temporal(t -> t.satisfyingStates(this.positiveInterpretedSystem().transitionRelation(), this.notNegativeInterpretedSystem().transitionRelation(), f -> this.satisfying.apply(f)));

  public StateSet positiveStates(TemporalEpistemicFormula formula) {
    return this.satisfying.apply(formula.nnf());
  }
    
  public StateSet notNegativeStates(TemporalEpistemicFormula formula) {
    return this.satisfying.apply(formula.neg().nnf()).complement();
  }

  public String info() {
    var builder = new StringBuilder();
    builder.append("Positive interpreted system\n");
    builder.append(this.pos.info());
    builder.append("\nNot-negative interpreted system\n");
    builder.append(this.nneg.info());
    return builder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.pos, this.nneg);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == this)
      return true;
    if (object == null)
      return false;
    try {
      PosNNegSystem other = (PosNNegSystem)object;
      return Objects.equals(this.pos, other.pos) &&
             Objects.equals(this.nneg, other.nneg);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
