package parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Symbol table mapping identifiers to values.  An identifier can
 * be mapped more than once. If an ordered pair (id, v1) is already stored in
 * the symbol table and the identifier is re-mapped by (id, v2), then (id, v2)
 * becomes the current mapping and shadows the old mapping (id, v1). The mappings
 * (id, ...), with one and the same first component id, are organized in a
 * stack-like fashion.
 * 
 * @version 1.0
 * @since 2011-03-13
 */
@NonNullByDefault
public class SymbolTable<V> {
  private final Map<String, Stack<V>> table;
    
  public SymbolTable() {
    this.table = new HashMap<>();
  }
    
  /**
   * Returns the top entry of the stack to which the specified identifier
   * is mapped, or {@code null} if this symbol table contains no mapping for the identifier.
   *
   * @param id an identifier
   * @return the value to which {@code id} is mapped, or {@code null} if this
   *         symbol table does not contain a value for {@code id}
   */
  public @Nullable V top(String id) {
    Stack<V> valueStack = this.table.get(id);
    if (valueStack != null)
      return valueStack.peek();
    return null;
  }
    
  /**
   * Record a mapping of an identifier to a value in this symbol table.
   * If an ordered pair (id, v1) is already stored in the
   * symbol table and the identifier is redeclared by put(id, v2), then (id, v2)
   * becomes the current mapping and shadows the old mapping (id, v1).
   *
   * @param id an identifier
   * @param value a value
   */
  public void push(String id, V value) {
    var valueStack = this.table.get(id);
    if (valueStack == null)
      this.table.put(id, valueStack = new Stack<>());
    valueStack.push(value);
  }
    
  /**
   * Removes the top element of the stack for an identifier.
   * If this stack is empty, the specified identifier is removed
   * from this symbol table.
   * 
   * @param id an identifier
   */
  public void pop(String id) {
    Stack<V> valueStack = this.table.get(id);
    if (valueStack == null)
      return;
    valueStack.pop();
    if (valueStack.isEmpty())
      this.table.remove(id);
  }
}
