Starting to work...
simpleRehash... done (0.0), mem_util: 0.007387566, capacity: 23, count: 8, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.007397615, capacity: 47, count: 18, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.007416203, capacity: 97, count: 37, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.0074308454, capacity: 197, count: 77, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.007480259, capacity: 397, count: 157, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.007590924, capacity: 797, count: 317, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.007821686, capacity: 1597, count: 637, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.008094341, capacity: 3203, count: 1277, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.008599638, capacity: 6421, count: 2562, tot_mem:16318464, max_mem:259522560
simpleRehash... done (0.0), mem_util: 0.009614178, capacity: 12853, count: 5136, tot_mem:16318464, max_mem:259522560
size == 169
simpleRehash... done (0.0), mem_util: 0.011775932, capacity: 25717, count: 10282, tot_mem:16318464, max_mem:259522560
satisfiable but not universally valid.
An example of a falsifying valuation is:
 V00x00, !V00x01, !V00x02, !V00x03, !V00x04, !V01x00, !V01x01,  V01x02, !V01x03, !V01x04, !V02x00, !V02x01, !V02x02, !V02x03,  V02x04, !V03x00,  V03x01, !V03x02, !V03x03, !V03x04, !V04x00, !V04x01, !V04x02,  V04x03, !V04x04

Statistical information on the BDDTable:

reads ........ == 12403
hits ......... == 2042
collisions ... == 4635
size ......... == 10361
capacity ..... == 25717
rehashingCount == 11
splitCount ... == 1

Overflow distribution of the BDDTable:

length == 0 	count == 17180
length == 1 	count == 6933
length == 2 	count == 1402
length == 3 	count == 184
length == 4 	count == 18

Gap-length distribution of the BDDTable:

length == 0 	count == 2858
length == 1 	count == 1891
length == 2 	count == 1269
length == 3 	count == 819
length == 4 	count == 571
length == 5 	count == 353
length == 6 	count == 269
length == 7 	count == 134
length == 8 	count == 134
length == 9 	count == 81
length == 10 	count == 65
length == 11 	count == 23
length == 12 	count == 22
length == 13 	count == 15
length == 14 	count == 18
length == 15 	count == 3
length == 16 	count == 5
length == 17 	count == 2
length == 18 	count == 0
length == 19 	count == 3
length == 20 	count == 1
length == 21 	count == 2

Time expired: 120 ms

    private int getHashCode(int v, BDD thenBDD, BDD elseBDD) {
        return ( ( (thenBDD.hashCode() << 8)
                 ^ (elseBDD.hashCode() << 2)
                 ^ v) & Integer.MAX_VALUE);
    } 
    