package bdd;

import java.util.SortedSet;
import java.util.Vector;

import core.Type;

public interface Universe {
    /**
     * Returns a vector that contains the string representations of all unprimed
     * typed variables (the vector does not contain binary digits of the encoding
     * of integer or enum variables). The order in which the string representations
     * occur in this vector determines the order of displaying by Valuation.printTyped().
     * The 'error' variable is at position 0. Next come the boolean variables
     * in ascending order of their indices. Next come the integer variable and
     * then the enum variables (in no specific order).
     */
	public Vector<String> displayingOrder();
	
	/**
	 * Returns the type of the given symbol, if a type is declared; otherwise,
	 * returns null. The given symbol may be an unprimed or primed variable or
	 * a constant. For the error-variable, boolean is returned.
	 */
	public Type getTypeOf(String symbol);
	
    /**
     * Returns the multi-index which <tt>symbol</tt> is mapped to. If the variable
     * is of type boolean, the multi-index is a vector of size 1 containing the index
     * of the given boolean variable. If the variable is of type int or enum, the
     * multi-index is a vector 1) whose size is the number of binary digits necessary
     * to encode the values of the variable and 2) which maps a digit to the index
     * of the bivalent variable representing the given digit of the encoding of this
     * variable.
     * @param symbol the string representation of an unprimed or primed variable
     * of type boolean, int, or enum.
     * @return the multi-index which symbol is mapped to; null, if symbol is neither
     * declared as a variable nor as symbolic constant.
     */
	public Vector<Integer> getMultiIndex(String symbol);
	
    /**
     * Returns a sorted set that contains the indices of all globally declared unprimed
     * bivalent variables in natural order.
     */
	public SortedSet<Integer> universe();
}
