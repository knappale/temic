package bdd;

import java.util.Vector;

import org.eclipse.jdt.annotation.Nullable;


/**
 * @since 5 May 2011
 *
 * TODO: implement operations for composition of two 32 bit multi bdds
 * to form a 64 bit multi bdd; in particular: implement special add- and
 * subtract-operations that account for the carry bdd.
 */
public class MultiBDD {
  private static int INT_SIZE = 32;
  private static int INT_MSB = INT_SIZE-1;

  private BDD[] bdds;
  private BDD carry;
  private BDD overflow;
  private BDD undefined;
  private boolean inTwosComplement;
	
  static class PairOfMultiBDDs {
    private MultiBDD lowInt;
    private MultiBDD highInt;

    PairOfMultiBDDs(MultiBDD low, MultiBDD high) {
      this.lowInt = low;
      this.highInt = high;
    }
		
    MultiBDD getLow() {
      return this.lowInt;
    }
		
    MultiBDD getHigh() {
      return this.highInt;
    }
  }
	
  public BDD get(int digit) {
    return this.bdds[digit];
  }
	
  @SuppressWarnings("unused")
  private void put(BDD bdd, int digit) {
    this.bdds[digit] = bdd;
  }
	
  public int size() {
    return this.bdds.length;
  }

  public BDD carry() {
    return this.carry;
  }
	
  public BDD overflow() {
    return this.overflow;
  }
	
  public BDD undef() {
    return this.undefined;
  }
	
  public void setUndef(BDD undef) {
    this.undefined = undef;
  }
	
  public boolean inTwosComplement() {
    return inTwosComplement;
  }
	
	private MultiBDD() {
	}
	
	private MultiBDD(BDD[] bdds, boolean inTwosComplement) {
		this.bdds = bdds;
		this.carry = BDD.falseBDD();
		this.overflow = BDD.falseBDD();
		this.undefined = BDD.falseBDD();
		this.inTwosComplement = inTwosComplement;
	}

	private MultiBDD(BDD[] bdds, BDD carry, BDD overflow, boolean inTwosComplement) {
		this.bdds = bdds;
		this.carry = carry;
		this.overflow = overflow;
		this.undefined = BDD.falseBDD();
		this.inTwosComplement = inTwosComplement;
	}
	
	public static MultiBDD castBDD(BDD bdd) {
		BDD[] bdds = new BDD[1]; bdds[0] = bdd;
		return new MultiBDD(bdds, false);
	}
	
	public static MultiBDD castBDDWB(BDDWB bddwb) {
		BDD[] bdds = new BDD[1]; bdds[0] = bddwb.getBDD();
		MultiBDD mvalue = new MultiBDD(bdds, false);
		mvalue.setUndef(bddwb.undef());
		return mvalue;
	}
	
	public static MultiBDD intMultiBDD(int n) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = ((n & 1) != 0) ? BDD.trueBDD() : BDD.falseBDD();
			n = n >>> 1;
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD varMultiBDD(Vector<Integer> multiIndex, int offset) {
		if (multiIndex.size() > INT_SIZE-1) {
			throw new RuntimeException("multi index too long");
		}
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			if (digit < multiIndex.size()) {
				bdds[digit] = BDD.varBDD(multiIndex.get(digit));
			}
			else {
				bdds[digit] = BDD.falseBDD();
			}
		}
		MultiBDD encodingMultiBDD = new MultiBDD(bdds, true);
		return (offset == 0) ? encodingMultiBDD : MultiBDD.add(encodingMultiBDD, MultiBDD.intMultiBDD(offset));
	}
	
	public static MultiBDD add(MultiBDD a, MultiBDD b) {
		BDD[] sumBDDs = new BDD[INT_SIZE];
		BDD carryBDD = BDD.falseBDD();
		BDD carryBDD_MSB = null; // carryBDD of most significant bit
		for (int digit=0; digit<INT_SIZE; digit++) {
			if (digit == INT_MSB) { carryBDD_MSB = carryBDD; }
			sumBDDs[digit] = BDD.exor(BDD.exor(a.get(digit),b.get(digit)), carryBDD);
			carryBDD = BDD.or(BDD.or(BDD.and(a.get(digit),b.get(digit)),
					                 BDD.and(a.get(digit),carryBDD)),
					                 BDD.and(b.get(digit),carryBDD));
		}
		return new MultiBDD(sumBDDs, carryBDD, BDD.exor(carryBDD, carryBDD_MSB), true);
	}
	
	public static MultiBDD inc(MultiBDD a) {
		return MultiBDD.add(a, MultiBDD.intMultiBDD(1));
	}
	
	public static MultiBDD sub(MultiBDD a, MultiBDD b) {
		return MultiBDD.add(a, MultiBDD.negate(b));
	}
	
	public static MultiBDD dec(MultiBDD a) {
		return MultiBDD.add(a, MultiBDD.intMultiBDD(-1));
	}

	public static MultiBDD negate(MultiBDD a) {
		MultiBDD onesComplement = MultiBDD.not(a);
		MultiBDD twosComplement = MultiBDD.inc(onesComplement);
		return twosComplement;
	}
	
	public static MultiBDD abs(MultiBDD a) {
		return MultiBDD.ite(a.get(INT_MSB), MultiBDD.negate(a), a);
	}
	
	public static PairOfMultiBDDs negate64Bit(MultiBDD low, MultiBDD high) {
		MultiBDD onesComplementLow = MultiBDD.not(low);
		MultiBDD onesComplementHigh = MultiBDD.not(high);
		MultiBDD twosComplementLow = MultiBDD.inc(onesComplementLow);
		MultiBDD carryMultiBDD = MultiBDD.ite(twosComplementLow.carry(), MultiBDD.intMultiBDD(1), MultiBDD.intMultiBDD(0));
		MultiBDD twosComplementHigh = MultiBDD.add(onesComplementHigh, carryMultiBDD);
		return new PairOfMultiBDDs(twosComplementLow, twosComplementHigh);
	}
	
	public static PairOfMultiBDDs abs64Bit(MultiBDD low, MultiBDD high) {
		PairOfMultiBDDs negatedParam = MultiBDD.negate64Bit(low, high);
		MultiBDD absLow = MultiBDD.ite(high.get(INT_MSB), negatedParam.lowInt, low);
		MultiBDD absHigh = MultiBDD.ite(high.get(INT_MSB), negatedParam.highInt, high);
		return new PairOfMultiBDDs(absLow, absHigh);
	}
	
	
	public static MultiBDD shiftRightSigned(MultiBDD a, int numBits) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE-numBits; digit++) {
			bdds[digit] = a.get(digit+numBits);
		}
		for (int digit=INT_SIZE-numBits; digit<INT_SIZE; digit++) {
			bdds[digit] = a.get(INT_MSB);
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD shiftRight(MultiBDD a, int numBits) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE-numBits; digit++) {
			bdds[digit] = a.get(digit+numBits);
		}
		for (int digit=INT_SIZE-numBits; digit<INT_SIZE; digit++) {
			bdds[digit] = BDD.falseBDD();
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD shiftLeft(MultiBDD a, int numBits) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<numBits; digit++) {
			bdds[digit] = BDD.falseBDD();
		}
		for (int digit=numBits; digit<INT_SIZE; digit++) {
			bdds[digit] = a.get(digit-numBits);
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD rotateRightThroughCarry(MultiBDD a, BDD carryIn) {
		BDD[] bdds = new BDD[INT_SIZE];
		BDD carryOut = a.get(0);
		for (int digit=0; digit<INT_SIZE-1; digit++) {
			bdds[digit] = a.get(digit+1);
		}
		bdds[INT_MSB] = carryIn;
		return new MultiBDD(bdds, carryOut, BDD.falseBDD(), true);
	}
	
	public static MultiBDD rotateLeftThroughCarry(MultiBDD a, BDD carryIn) {
		BDD[] bdds = new BDD[INT_SIZE];
		bdds[0] = carryIn;
		BDD carryOut = a.get(INT_SIZE-1);
		for (int digit=1; digit<INT_SIZE; digit++) {
			bdds[digit] = a.get(digit-1);
		}
		return new MultiBDD(bdds, carryOut, BDD.falseBDD(), true);
	}
	
	public static MultiBDD and(MultiBDD a, MultiBDD b) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = BDD.and(a.get(digit),b.get(digit));
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD or(MultiBDD a, MultiBDD b) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = BDD.or(a.get(digit),b.get(digit));
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD exor(MultiBDD a, MultiBDD b) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = BDD.exor(a.get(digit),b.get(digit));
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD not(MultiBDD a) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = BDD.not(a.get(digit));
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD ite(BDD f, MultiBDD g, MultiBDD h) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = BDD.ite(f, g.get(digit), h.get(digit));
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD filledWith(BDD bdd) {
		BDD[] bdds = new BDD[INT_SIZE];
		for (int digit=0; digit<INT_SIZE; digit++) {
			bdds[digit] = bdd;
		}
		return new MultiBDD(bdds, true);
	}
	
	public static MultiBDD multiply(MultiBDD a, MultiBDD b) {
		return MultiBDD.multiplyTo64BitResult(a, b).lowInt;
	}
	
	private static PairOfMultiBDDs multiplyTo64BitResult(MultiBDD a, MultiBDD b) {
		MultiBDD productHigh = MultiBDD.intMultiBDD(0);
		MultiBDD productLow = b;
		for (int i=0; i<INT_SIZE; i++) {
			// multiply a by LSB of productLow
			MultiBDD aTimesProduct0 = MultiBDD.ite(productLow.get(0), a, MultiBDD.intMultiBDD(0));
			// add aTimesProduct0 to productHigh
			productHigh = MultiBDD.add(productHigh, aTimesProduct0);
			// shift 64bit product register right 1 bit
			BDD carry = productHigh.get(0);
			productHigh = MultiBDD.shiftRightSigned(productHigh, 1);
			productLow = MultiBDD.rotateRightThroughCarry(productLow, carry);
		}
		return new PairOfMultiBDDs(productLow, productHigh);
	}
	
	public static MultiBDD div(MultiBDD a, MultiBDD b) {
		return MultiBDD.divide64BitDividend(a, MultiBDD.filledWith(a.get(INT_MSB)), b).lowInt;
	}
	
	public static MultiBDD mod(MultiBDD a, MultiBDD b) {
		return MultiBDD.divide64BitDividend(a, MultiBDD.filledWith(a.get(INT_MSB)), b).highInt;
	}
	
	private static PairOfMultiBDDs divide64BitDividend(MultiBDD dividendLow, MultiBDD dividendHigh, MultiBDD divisor) {
		MultiBDD absDivisor = MultiBDD.abs(divisor);
		// akku := |dividend|
		PairOfMultiBDDs absDividend = MultiBDD.abs64Bit(dividendLow, dividendHigh);
		MultiBDD akkuLow = absDividend.lowInt;
		MultiBDD akkuHigh = absDividend.highInt;
		// shift 64bit akku left 1 bit
		akkuLow = MultiBDD.rotateLeftThroughCarry(akkuLow, BDD.falseBDD());
		akkuHigh = MultiBDD.rotateLeftThroughCarry(akkuHigh, akkuLow.carry());
		// repeat 32 times
 		for (int i=0; i<INT_SIZE; i++) {
 			// subtract the absolute value of divisor from akkuHigh 
 			MultiBDD remainder = MultiBDD.sub(akkuHigh, absDivisor);
 			// case differentiation: if remainder >= 0, then akkuHigh := remainder;
 			// if remainder < 0, then leave akkuHigh unchanged.
 			BDD MSBRemainder = remainder.get(INT_MSB);
 			akkuHigh = MultiBDD.ite(MSBRemainder, akkuHigh, remainder);
 			// shift 64bit akku to the left 1 bit, setting the new LSB to MSBRemainderNeg 
 			akkuLow = MultiBDD.rotateLeftThroughCarry(akkuLow, BDD.not(MSBRemainder));
 			akkuHigh = MultiBDD.rotateLeftThroughCarry(akkuHigh, akkuLow.carry());
 		}
		// shift akkuHigh right 1 bit
		akkuHigh = MultiBDD.shiftRight(akkuHigh, 1);
		// restore correct sign of quotient
		akkuLow = MultiBDD.ite(BDD.exor(dividendHigh.get(INT_MSB),divisor.get(INT_MSB)), MultiBDD.negate(akkuLow), akkuLow);
		// restore correct sign of remainder
		akkuHigh = MultiBDD.ite(dividendHigh.get(INT_MSB), MultiBDD.negate(akkuHigh), akkuHigh);
		return new PairOfMultiBDDs(akkuLow, akkuHigh);
	}
	
	public static MultiBDD math_mod(MultiBDD a, MultiBDD b) {
		// return (0 <= a % b ? a % b : Math.abs(b) + a % b)
		MultiBDD a_mod_b = mod(a,b);
		return ite(leq(intMultiBDD(0),a_mod_b), a_mod_b, add(abs(b), a_mod_b));
	}
	
	public static MultiBDD math_div(MultiBDD a, MultiBDD b) {
		// return (0 <= a % b ? a / b : (0 <= b ? -1 : 1) + (a / b))
		PairOfMultiBDDs pair = MultiBDD.divide64BitDividend(a, MultiBDD.filledWith(a.get(INT_MSB)), b);
		MultiBDD a_div_b = pair.lowInt;
		MultiBDD a_mod_b = pair.highInt;
		return ite(leq(intMultiBDD(0),a_mod_b), a_div_b, add(ite(leq(intMultiBDD(0),b),intMultiBDD(-1),intMultiBDD(1)),a_div_b));
	}
	
	public static BDD leq(MultiBDD a, MultiBDD b) {
		return lessAUX(a, b, false);
	}
	
	public static BDD less(MultiBDD a, MultiBDD b) {
		return lessAUX(a, b, true);
	}
	
	private static BDD lessAUX(MultiBDD a, MultiBDD b, boolean strict) {
		if (!a.inTwosComplement() || !b.inTwosComplement()) {
			throw new RuntimeException("leq() called for arguments that are not in two's complement");
		}
		BDD ret = BDD.and(a.get(INT_SIZE-1), BDD.not(b.get(INT_SIZE-1)));
		BDD equalSoFar = BDD.biimplies(a.get(INT_SIZE-1),b.get(INT_SIZE-1));
		for (int digit=INT_SIZE-2; 0<=digit; digit--) {
			ret = BDD.or(ret, BDD.and(equalSoFar, BDD.and(BDD.not(a.get(digit)), b.get(digit))));
			equalSoFar = BDD.and(equalSoFar, BDD.biimplies(a.get(digit), b.get(digit)));
		}
		if (!strict) {
			ret = BDD.or(ret, equalSoFar);
		}
		return ret;
	}
		
	public static BDD equal(MultiBDD a, MultiBDD b) {
		if (!a.inTwosComplement() || !b.inTwosComplement()) {
			throw new RuntimeException("equal() called for arguments that are not in two's complement");
		}
		BDD ret = BDD.trueBDD();
		for (int digit=0; digit<INT_SIZE; digit++) {
			ret = BDD.and(ret, BDD.biimplies(a.get(digit), b.get(digit)));
		}
		return ret;
	}

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      MultiBDD other = (MultiBDD)object;
      if (this.inTwosComplement != other.inTwosComplement)
        return false;
      for (int digit = 0; digit < INT_SIZE; digit++) {
        if (this.get(digit) != other.get(digit))
          return false;
      }
      return true;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
