package bdd;

import java.util.Vector;


/**
 * This class is an implementation class of the VariableRenaming interface. An
 * instance of VariableRenamingImp stands for a (not necessarily monotonic)
 * mapping of "old" variables (e.g. primed variables) to "new" variables (e.g.
 * unprimed variables).
 *
 * @version 1.0
 * @since 2011-03-27
 */
public class VariableRenaming {
  private Vector<Integer> oldIndices;
  private Vector<Integer> newIndices;

  /**
   * Constructs a new, empty variable renaming.
   */
  public VariableRenaming() {
    this.oldIndices = new Vector<>();
    this.newIndices = new Vector<>();
  }

  /**
   * Returns an instance of jdd.bdd.Permutation which corresponds to this
   * VariableRenaming.
   */
  public jdd.bdd.VariableMapping variableMapping(jdd.bdd.BDDManager bddManager) {
    Vector<Integer> oldLocs = new Vector<Integer>();
    Vector<Integer> newLocs = new Vector<Integer>();
    for (int i = 0; i < oldIndices.size(); i++) {
      int oldLoc = bddManager.mk(oldIndices.get(i), bddManager.getZero(), bddManager.getOne());
      int newLoc = bddManager.mk(newIndices.get(i), bddManager.getZero(), bddManager.getOne());

      int m = 0, l = 0, u = oldLocs.size() - 1;
      while (l <= u) {
        m = (l + u) / 2;
        if (oldLoc == oldLocs.get(m)) {
          throw new RuntimeException("multiple specified mapping for the same oldLoc");
        } else if (oldLoc < oldLocs.get(m))
          u = m - 1;
        else
          l = m + 1;
      }
      oldLocs.insertElementAt(oldLoc, l);
      newLocs.insertElementAt(newLoc, l);
    }
    int[] oldLocsArray = new int[oldIndices.size()];
    int[] newLocsArray = new int[oldIndices.size()];
    for (int i = 0; i < oldIndices.size(); i++) {
      oldLocsArray[i] = oldLocs.get(i);
      newLocsArray[i] = newLocs.get(i);
    }
    return bddManager.createVariableMapping(oldLocsArray, newLocsArray);
  }

  /**
   * Maps the specified <tt>oldIndex</tt> to the specified <tt>newIndex</tt> in
   * this variable renaming. The put() method may be called at most once for each
   * value of oldIndex; otherwise an IllegalArgumentException is thrown. If this
   * variable renaming is already locked (i.e. variableMapping != null), the call
   * to put() is ignored.
   * 
   * @param oldIndex an index of a variable
   * @param newIndex the index of the new variable to which oldIndex should be mapped
   * @exception IllegalArgumentException
   *              if oldIndex was already in the domain of this renaming
   */
  public void put(int oldIndex, int newIndex) {
    int m = 0, l = 0, u = oldIndices.size() - 1;
    while (l <= u) {
      m = (l + u) / 2;
      if (oldIndex == oldIndices.get(m)) {
        throw new IllegalArgumentException("add() was called twice for the same oldIndex");
      } else if (oldIndex < oldIndices.get(m))
        u = m - 1;
      else
        l = m + 1;
    }
    oldIndices.insertElementAt(oldIndex, l);
    newIndices.insertElementAt(newIndex, l);
  }

  /**
   * The apply method computes the index of the new bivalent variable from the
   * index of the old one. It may raise an IllegalArgumentException if the
   * argument is not in the domain of this renaming.
   * 
   * @param oldIndex an index of a bivalent variable
   * @return the index of the new bivalent variable to which oldIndex is mapped by
   *         this renaming, if oldIndex is in the domain of this renaming;
   *         oldIndex otherwise
   */
  public int apply(int oldIndex) {
    int m = 0, l = 0, u = oldIndices.size() - 1;
    while (l <= u) {
      m = (l + u) / 2;
      if (oldIndex == oldIndices.get(m)) {
        return newIndices.get(m);
      } else if (oldIndex < oldIndices.get(m))
        u = m - 1;
      else
        l = m + 1;
    }
    return oldIndex;
  }

  /**
   * Tests if this renaming is strictly increasing.
   * 
   * @return true if and only if this renaming is strictly increasing; false
   *         otherwise.
   */
  public boolean isStrictlyIncreasing() {
    boolean first = true;
    int lastNewIndex = -1;
    for (int newIndex : newIndices) {
      if (first || newIndex > lastNewIndex) {
        lastNewIndex = newIndex;
        first = false;
      } else {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder ret = new StringBuilder();
    for (int i = 0; i < oldIndices.size(); i++) {
      ret.append("" + oldIndices.get(i) + " --> " + newIndices.get(i) + "\n");
    }
    return ret.toString();
  }
}
