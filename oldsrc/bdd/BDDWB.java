package bdd;

public class BDDWB {
	private BDD bdd;
	private BDD undef;
	
	@SuppressWarnings("unused")
	private BDDWB() {
	}
	
	public BDDWB(BDD bdd, BDD undef) {
		this.bdd = bdd;
		this.undef = undef;
	}
	
	public BDD getBDD() {
		return bdd;
	}
	
	public BDD undef() {
		return undef;
	}
}