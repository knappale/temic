package bdd;


/**
 * Class VariableRangeIntervalImp with cubes.
 * 
 * @version 1.0
 * @since 2011-03-22
 */
public class VariableRangeInterval implements VariableRange {
  private int upper;
  private int lower;
  private BDD cube;

  int getUpper() {
    return upper;
  }

  int getLower() {
    return lower;
  }

  /**
   * @pre lower <= upper
   */
  public VariableRangeInterval(int lower, int upper) {
    this.lower = lower;
    this.upper = upper;
  }

  @Override
  public VariableRange.Relation inRange(int v) {
    if (v < lower)
      return VariableRange.Relation.NOT_IN_RANGE;
    if (v > upper)
      return VariableRange.Relation.RANGE_PASSED;
    return VariableRange.Relation.IN_RANGE;
  }

  @Override
  public BDD cube() {
    if (cube == null) {
      boolean[] v = new boolean[upper + 1];
      for (int i = 0; i <= upper; i++) {
        v[i] = (lower <= i) ? true : false;
      }
      cube = BDD.cube(v);
    }
    return cube;
  }

  @Override
  public boolean equals(Object other) {
    if ((other != null) && (other instanceof VariableRangeInterval)) {
      VariableRangeInterval otherRange = (VariableRangeInterval) other;
      return (upper == otherRange.getUpper()) && (lower == otherRange.getLower());
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return (31 * upper + lower);
  }

  @Override
  public String toString() {
    return "[" + lower + ",...," + upper + "]";
  }
}
