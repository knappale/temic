package bdd;

import core.Type;
import syntax.generated.TemicParser;
import util.Formatter;

import java.io.Writer;
import java.io.IOException;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Set;
import java.util.HashSet;

/**
 * A instance of Valuation represents a partial mapping from positive integers
 * to boolean values. The positive integers are indices of bivalent variables.
 *
 * @version 1.1
 */
public class Valuation {
  /**
   * the domain of this valuation given as a vector of indices of bivalent
   * variables in ascending order
   */
  private Vector<Integer> domain;
  /** a hashtable mapping indices of bivalent variables to boolean values */
  private Hashtable<Integer, Boolean> values;

  public Valuation() {
    domain = new Vector<Integer>();
    values = new Hashtable<Integer, Boolean>();
  }

  public Valuation(int[] jddValuation) {
    domain = new Vector<Integer>();
    values = new Hashtable<Integer, Boolean>();
    for (int i = jddValuation.length - 1; 0 <= i; i--) {
      if (jddValuation[i] == 1) {
        add(i, true);
      } else if (jddValuation[i] == -1) {
        add(i, false);
      }
    }
  }

  public Boolean value(int index) {
    return values.get(index);
  }

  /**
   * @return whether this valuation is trivial, i.e. if the domain of this valuation is empty
   */
  public boolean isTrivial() {
    return (domain.isEmpty());
  }

  /**
   * @return the number of elements in the domain of this valuation
   */
  public int size() {
    return domain.size();
  }

  /**
   * Adds an index of a bivalent variable to the domain of this valuation and
   * assigns a boolean value to this index.
   * 
   * @param index the index of a bivalent variable
   * @param value the boolean value that is to be assigned to the given index
   */
  public void add(int index, boolean value) {
    if (index <= 0) {
      throw new RuntimeException("add() called for non-positive index");
    }
    int m = 0, l = 0, u = domain.size() - 1;
    boolean found = false;
    while (l <= u) {
      m = (l + u) / 2;
      if (index == domain.get(m)) {
        found = true;
        domain.set(m, index);
        break;
      } else if (index < domain.get(m))
        u = m - 1;
      else
        l = m + 1;
    }
    if (!found) {
      domain.insertElementAt(index, l);
    }
    values.put(index, value);
  }

  /* stands for v |-> {v} */
  public BDD asBDD() {
    BDD result = BDD.trueBDD();
    for (int i = 0; i < domain.size(); i++) {
      int index = domain.get(i);
      if (values.get(index)) {
        result = BDD.and(result, BDD.varBDD(index));
      } else {
        result = BDD.and(result, BDD.not(BDD.varBDD(index)));
      }
    }
    return result;
  }

  public Valuation specifyDontCares(Vector<String> symbolicNames) {
    HashSet<Integer> universe = new HashSet<Integer>();
    for (int index = 1; index <= symbolicNames.size(); index++) {
      if (!symbolicNames.get(index - 1).endsWith("'")) {
        universe.add(index);
      }
    }
    return specifyDontCares(universe, false);
  }

  public Valuation specifyDontCares(Set<Integer> universe) {
    return specifyDontCares(universe, false);
  }

  private Valuation specifyDontCares(Set<Integer> universe, boolean defaultval) {
    Valuation fullyQualifiedValuation = new Valuation();
    for (int index : universe) {
      if (values.containsKey(index)) {
        fullyQualifiedValuation.add(index, values.get(index));
      } else { // Don't care! Choose: index --> default value
        fullyQualifiedValuation.add(index, defaultval);
      }
    }
    return fullyQualifiedValuation;
  }

  public boolean equals(Object other) {
    if ((other != null) && (other instanceof Valuation)) {
      Valuation otherVal = (Valuation) other;
      if (domain.size() != otherVal.domain.size()) {
        return false;
      }
      for (int i = 0; i < domain.size(); i++) {
        int index = domain.get(i);
        if (index != otherVal.domain.get(i) || values.get(index) != otherVal.values.get(index)) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }

  public void print(Writer w) throws IOException {
    print(w, null);
  }

  public void print(Writer w, Vector<String> symbolicNames) throws IOException {
    for (int i = 0; i < domain.size(); i++) {
      int index = domain.get(i);
      if (values.get(index)) {
        w.write(" " + (symbolicNames == null ? index : symbolicNames.get(index - 1)));
      } else {
        w.write("!" + (symbolicNames == null ? index : symbolicNames.get(index - 1)));
      }
      if (i + 1 < domain.size()) {
        w.write(", ");
      }
    }
    w.write("\n");
    w.flush();
  }

  public void printTyped(Writer w, Universe vd) throws IOException {
    for (int n = 0; n < vd.displayingOrder().size(); n++) {
      String symbol = vd.displayingOrder().get(n);
      Type symbolType = vd.getTypeOf(symbol);
      Vector<Integer> mindex = vd.getMultiIndex(symbol);
      if (!fullyDefined(mindex)) {
        throw new RuntimeException("valuation not fully defined for variable '" + symbol);
      }
      if (symbolType.isBoolean()) {
          if (values.get(mindex.get(0))) {
            if (!symbol.equals(TemicParser.errorKeyword)) {
              w.write(" " + symbol);
            } else { // symbol.equals(TemicParser.errorKeyword)
              w.write("" + symbol);
              if (vd.universe().size() > 1) {
                w.write(", ");
              }
            }
          } else {
            // suppress negated error variable
            if (!symbol.equals(TemicParser.errorKeyword)) {
              w.write("!" + symbol);
            }
          }
      } else
      if (symbolType.isInteger()) {
         int value = decodeIntValue(mindex, symbolType.lowerBound());
         w.write("" + symbol + "=" + Formatter.format(value, symbolType.lowerBound(), symbolType.upperBound()));
      } else
      if (symbolType.isEnum())
        throw new RuntimeException("case ENUM in printTyped() is not implemented yet");
      else
        throw new RuntimeException("unknown typeId " + symbolType + " in printTyped()");
      if (n + 1 < vd.displayingOrder().size() && !symbol.equals(TemicParser.errorKeyword)) {
        w.write(", ");
      }
    }
    w.write("\n");
    w.flush();
  }

  private boolean fullyDefined(Vector<Integer> mindex) {
    for (int index : mindex) {
      if (!values.containsKey(index)) {
        return false;
      }
    }
    return true;
  }

  private int decodeIntValue(Vector<Integer> mindex, int offset) {
    int code = 0;
    int twopowersbit = 1;
    for (int bit = 0; bit < mindex.size(); bit++) {
      if (values.get(mindex.get(bit))) {
        code += twopowersbit;
      }
      twopowersbit *= 2;
    }
    return offset + code;
  }
}
