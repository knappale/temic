package bdd;


/**
 * Modified interface VariableRange with cubes.
 *
 * @version 1.0
 * @since 2011-07-24
 */
public interface VariableRange {
  public enum Relation {
    IN_RANGE, NOT_IN_RANGE, RANGE_PASSED;
  }

  /**
   * Check if variable with index v is in the range.
   * Possible results and their meaning are
   * IN_RANGE     : v is in the range
   * NOT_IN_RANGE : v is not in the range
   * RANGE_PASSED : v is not in the range, and no variable
   *                with an index higher than v is in the range.
   */
  public Relation inRange(int v);

  /**
   * Return the cube of this VariableRage, i.e. the logical conjunction
   * of all variables in the range.
   */
  public BDD cube();
}