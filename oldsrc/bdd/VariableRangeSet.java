package bdd;

import java.util.HashSet;
import java.util.Set;


/**
 * Variable range set with cubes.
 * 
 * @version 1.0
 * @since 2011-03-22
 */
public class VariableRangeSet implements VariableRange {
  private int upper;
  private HashSet<Integer> set;
  private BDD cube;

  Set<Integer> getSet() {
    return set;
  }

  public VariableRangeSet() {
    upper = -1;
    set = new HashSet<Integer>();
    cube = null;
  }

  public void add(int index) {
    set.add(index);
    if (index > upper) {
      upper = index;
    }
  }

  @Override
  public VariableRange.Relation inRange(int v) {
    if (v > upper)
      return VariableRange.Relation.RANGE_PASSED;
    if (set.contains(v))
      return VariableRange.Relation.IN_RANGE;
    return VariableRange.Relation.NOT_IN_RANGE;
  }

  @Override
  public BDD cube() {
    if (cube == null) {
      boolean[] v = new boolean[upper + 1];
      for (int i = 0; i <= upper; i++) {
        v[i] = (set.contains(i)) ? true : false;
      }
      cube = BDD.cube(v);
    }
    return cube;
  }

  @Override
  public boolean equals(Object other) {
    if ((other != null) && (other instanceof VariableRangeSet)) {
      VariableRangeSet otherRange = (VariableRangeSet) other;
      return set.equals(otherRange.getSet());
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return set.hashCode();
  }

  @Override
  public String toString() {
    return "" + set;
  }
}
