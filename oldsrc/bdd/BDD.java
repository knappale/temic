package bdd;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.SortedSet;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.lang.ref.ReferenceQueue;


/**
 * A wrapper class for the integer-based BDDs of package JDD.
 * 
 * @version 1.2
 * @since 2011-07-24
 */
public class BDD {
  /** the bdd manager of the underlying JDD system */
  private static jdd.bdd.@NonNull BDDManager bddManager;
  /** the canonical BDD that is constantly true */
  private static @NonNull BDD trueBDD;
  /** the canonical location (i.e. jdd-representation) of trueBDD (= 1) */
  private static int trueBDDLocation;
  /** the canonical BDD that is constantly false */
  private static @NonNull BDD falseBDD;
  /** the canonical location (i.e. jdd-representation) of falseBDD (= 0) */
  private static int falseBDDLocation;
  /** Highest variable (index) created as yet */
  private static int highestVariable;
  /** maps canonical locations to canonical wrapper objects */
  private static @NonNull Hashtable<Integer, WeakBDDReference> wrapperTable;
  /** a reference queue for cleaning up the wrapperTable */
  private static @NonNull ReferenceQueue<BDD> referenceQueue;
  /** a guideline value for the initial size of the node table */
  private static int NODETABLE_SIZE = 10000;
  /** a guideline value for the initial size of the caches */
  private static int CACHE_SIZE = 10000;
  /** the initial size of the wrapper table */
  private static int WRAPPERTABLE_SIZE = 1117;
  /** the logger for instances of wrapper class BDD */
  private static Logger logger = Logger.getLogger(core.Temic.class.getName());

  /**
   * The array index in table t_nodes of class jdd.bdd.NodeTable which is the
   * canonical JDD-representation of this BDD.
   */
  private int location;

  static {
    bddManager = new jdd.bdd.BDDManager(NODETABLE_SIZE, CACHE_SIZE);
    trueBDDLocation = bddManager.getOne();
    trueBDD = new BDD(trueBDDLocation);
    falseBDDLocation = bddManager.getZero();
    falseBDD = new BDD(falseBDDLocation);
    bddManager.createVar(); // dummy variable for incrementing num_vars
    highestVariable = 0; // highest variable created as yet
    wrapperTable = new Hashtable<Integer, WeakBDDReference>(WRAPPERTABLE_SIZE);
    referenceQueue = new ReferenceQueue<BDD>();
  }

  /**
   * A static method that can be used after startup for reconfiguring the initial
   * size of the nodetable, the initial size of the caches, and the initial size
   * of the wrapper table.
   * 
   * @param nodetableSize a guideline value for the initial size of the node table
   * @param cacheSize a guideline value for the initial size of the caches
   * @param wrappertableSize the initial size of the wrapper table
   */
  public static void reconfigure(int nodetableSize, int cacheSize, int wrappertableSize) {
    logger.config("reconfiguring bdd system");
    bddManager = new jdd.bdd.BDDManager(nodetableSize, cacheSize);
    trueBDDLocation = bddManager.getOne();
    trueBDD = new BDD(trueBDDLocation);
    falseBDDLocation = bddManager.getZero();
    falseBDD = new BDD(falseBDDLocation);
    bddManager.createVar(); // dummy variable for incrementing num_vars
    highestVariable = 0; // highest variable created as yet
    wrapperTable = new Hashtable<>(wrappertableSize);
    referenceQueue = new ReferenceQueue<BDD>();
  }

  /** Private default constructor */
  private BDD() {
    super();
  }

  /**
   * Private constructor which is called only 1) by the static initialization
   * block and 2) by the private static factory method canonicalBDD().
   * 
   * @param location
   *          the canonical jdd-representation of a bdd node for which the wrapper
   *          object is to be instantiated.
   */
  private BDD(int location) {
    super();
    this.location = location;
  }

  private static class WeakBDDReference extends WeakReference<BDD> {
    private final int location;

    int getLocation() {
      return location;
    }

    WeakBDDReference(BDD wrapper, ReferenceQueue<BDD> rq) {
      super(wrapper, rq);
      this.location = wrapper.location;
    }
  }

  public static int count = 0;

  /**
   * A private static factory method that returns the canonical wrapper object
   * (BDD) representing the given location.
   * 
   * @param location
   *          the canonical jdd-representation of a bdd node for which the wrapper
   *          object shall be returned.
   * @return the canonical BDD representing the given location
   */
  private static BDD canonicalBDD(int location) {
    count++;
    if (location == trueBDDLocation)
      return trueBDD;
    if (location == falseBDDLocation)
      return falseBDD;

    // Clean up
    WeakBDDReference ref;
    while ((ref = (WeakBDDReference) referenceQueue.poll()) != null) {
      bddManager.deref(ref.getLocation());
      if (wrapperTable.get(ref.getLocation()) == ref) {
        wrapperTable.remove(ref.getLocation());
      }
    }

    ref = wrapperTable.get(location);
    if (ref != null) {
      BDD wrapper = ref.get();
      if (wrapper != null)
        return wrapper;
    }

    BDD wrapper = new BDD(location);
    wrapperTable.put(location, new WeakBDDReference(wrapper, referenceQueue));
    bddManager.ref(location);
    return wrapper;
  }

  /**
   * Return the canonical BDD that represents a boolean
   * function which is constantly true.
   * 
   * @return the BDD that represents a constantly true function
   */
  public static BDD trueBDD() {
    return trueBDD;
  }

  /**
   * Return the canonical BDD that represents a boolean
   * function which is constantly false.
   * 
   * @return the BDD that represents a constantly false function
   */
  public static BDD falseBDD() {
    return falseBDD;
  }

  /**
   * Return the BDD that represents a boolean function which
   * 1) has only one variable <tt>v</tt> and 2) is true if and only if <tt>v</tt>
   * is true (<tt>v</tt> taken as a boolean function). This method acts as
   * constructor for BDDs.
   * 
   * @param v a nonnegative integer representing a variable (v <
   *          Integer.MAX_VALUE)
   * @return the BDD that represents v taken as a boolean function
   * @throws IllegalArgumentException
   *           if v is strictly less than 0 or equal to Integer.MAX_VALUE
   */
  public static BDD varBDD(int v) {
    if (v < 0 || v >= Integer.MAX_VALUE)
      throw new IllegalArgumentException("v must be a non-negative integer less than Integer.MAX_VALUE");

    if (highestVariable < v) {
      int loc = 0;
      while (highestVariable < v) {
        highestVariable++;
        loc = bddManager.createVar();
      }
      return canonicalBDD(loc);
    } else {
      int loc = bddManager.mk(v, falseBDDLocation, trueBDDLocation);
      return canonicalBDD(loc);
    }
  }

  /**
   * Creates a BDD <i>cube</i>, which is simply a conjunction of a set of
   * variables. Given a boolean array <i>v[]</i>, the method cube() returns the
   * conjunction of all variables 0 <= i < Min{v.length, num_vars} such that v[i]
   * is true.
   * 
   * @param v a boolean array describing a set of variables
   * @return the cube as described above
   */
  public static BDD cube(boolean[] v) {
    return canonicalBDD(bddManager.cube(v));
  }

  /**
   * ite() operation.
   * 
   * @param f a BDD that servers as criteria for case differentiation
   * @param g a BDD that represents the then-case
   * @param h a BDD that represents the else-case
   * @return the BDD that represents (f and g) or (not(f) and h)
   */
  public static BDD ite(BDD f, BDD g, BDD h) {
    return canonicalBDD(bddManager.ite(f.location, g.location, h.location));
  }

  /**
   * Return the BDD that represents the logical negation of a
   * boolean function f.
   * 
   * @param f a BDD representing a boolean function f
   * @return the BDD that represents not(f)
   */
  public static BDD not(BDD f) {
    return canonicalBDD(bddManager.not(f.location));
  }

  /**
   * Returns the BDD that represents the logical conjunction of
   * two boolean functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents (f and g)
   */
  public static BDD and(BDD f, BDD g) {
    return canonicalBDD(bddManager.and(f.location, g.location));
  }

  /**
   * Return the BDD that represents the logical disjunction of
   * two boolean functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents (f or g)
   */
  public static BDD or(BDD f, BDD g) {
    return canonicalBDD(bddManager.or(f.location, g.location));
  }

  /**
   * Return the BDD that represents the boolean function (f
   * => g) for two given functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents (f => g)
   */
  public static BDD implies(BDD f, BDD g) {
    return canonicalBDD(bddManager.imp(f.location, g.location));
  }

  /**
   * Return the BDD that represents the boolean function
   * (f <=> g) for two given functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents (f <=> g)
   */
  public static BDD biimplies(BDD f, BDD g) {
    return canonicalBDD(bddManager.biimp(f.location, g.location));
  }

  /**
   * Return the BDD that represents the exclusive disjunction
   * of two boolean functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents (f exor g)
   */
  public static BDD exor(BDD f, BDD g) {
    return canonicalBDD(bddManager.xor(f.location, g.location));
  }

  /**
   * Return the BDD that represents the boolean function not(f
   * and g) for two given functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents not(f and g)
   */
  public static BDD nand(BDD f, BDD g) {
    return canonicalBDD(bddManager.nand(f.location, g.location));
  }

  /**
   * Return the BDD that represents the boolean function not(f
   * or g) for two given functions f and g.
   * 
   * @param f a BDD representing a boolean function f
   * @param g a BDD representing a boolean function g
   * @return the BDD that represents not(f or g)
   */
  public static BDD nor(BDD f, BDD g) {
    return canonicalBDD(bddManager.nor(f.location, g.location));
  }

  /**
   * Return the canonical BDD that represents the boolean
   * formula f preceded by an existence quantifier for the variable range rng.
   * 
   * @param f a BDD representing the scope of the existence quantifier
   * @param rng the range (i.e. set) of variables that are bounded by the quantifier
   * @return the canonical BDD that represents the boolean formula f preceded by
   *         an existence quantifier for the variable range rng.
   */
  public static BDD exists(BDD f, VariableRange rng) {
    return canonicalBDD(bddManager.exists(f.location, rng.cube().location));
  }

  /**
   * Return the canonical BDD that represents the boolean
   * formula f preceded by an universal quantifier (i.e. all-quantifier) for the
   * variable range rng. This is a first version without cache.
   * 
   * @param f a BDD representing the scope of the universal quantifier
   * @param rng the range (i.e. set) of variables that are bounded by the quantifier
   * @return the canonical BDD that represents the boolean formula f preceded by
   *         an universal quantifier for the variable range rng.
   */
  public static BDD forall(BDD f, VariableRange rng) {
    return canonicalBDD(bddManager.forall(f.location, rng.cube().location));
  }

  /**
   * Return true if all satisfying evaluations of f are also
   * satisfying evaluations of g; otherwise false.
   * 
   * @param f a BDD representing a set of satisfying evaluations
   * @param g a BDD representing a set of satisfying evaluations
   * @return true if all satisfying evaluations of f are also satisfying
   *         evaluations of g; otherwise false
   */
  public static boolean subsets(BDD f, BDD g) {
    return bddManager.imp(f.location, g.location) == trueBDDLocation;
  }

  /**
   * Create and return a renamed copy of the BDD f, in
   * which all variables labeling inner nodes of f have been substituted according
   * to the VariableRenaming subst. A VariableRenaming is a strictly increasing
   * mapping of non-negative integers to non-negative integers.
   * 
   * @param f a BDD representing a boolean function f
   * @param subst a strictly increasing mapping for variable substitution
   * @return a renamed copy of the BDD f, in which all variables labeling inner
   *         nodes of f have been substituted according to the VariableRenaming
   *         subst
   */
  public static BDD renamedCopy(BDD f, VariableRenaming subst) {
    return canonicalBDD(bddManager.replace(f.location, subst.variableMapping(bddManager)));
  }

  /**
   * Returns true iff this BDD is the canonical BDD that represents a boolean
   * function which is constantly true.
   * 
   * @return true iff this BDD is constantly true
   */
  public boolean isTrue() {
    return (location == trueBDDLocation);
  }

  /**
   * Returns true iff this BDD is the canonical BDD that represents a boolean
   * function which is constantly false.
   * 
   * @return true iff this BDD is constantly false
   */
  public boolean isFalse() {
    return (location == falseBDDLocation);
  }

  /**
   * Return the then-successor of this bdd node if this bdd node is an inner
   * node.
   *
   * @pre !this.isTrue() && !this.isFalse()
   * @return the then-successor of this bdd node if this bdd node is an inner node
   */
  public BDD getThenSucc() {
    return canonicalBDD(bddManager.getHigh(location));
  }

  /**
   * Returns the else-successor of this bdd node if this bdd node is an inner
   * node.
   * 
   * @pre !this.isTrue() && !this.isFalse()
   * @return the else-successor of this bdd node if this bdd node is an inner node
   */
  public BDD getElseSucc() {
    return canonicalBDD(bddManager.getLow(location));
  }

  /**
   * Returns the head-variable of this bdd node if this bdd node is an inner node.
   * Returns Integer.MAX_VALUE if this bdd node is a terminal node.
   * 
   * @return the head-variable of this bdd node if this bdd node is an inner node;
   *         Integer.MAX_VALUE otherwise
   */
  public int getVariable() {
    if ((location == trueBDDLocation) || (location == falseBDDLocation))
      return Integer.MAX_VALUE;
    return bddManager.getVar(location) & jdd.bdd.BDDManager.NODE_UNMARK;
  }

  /**
   * @version 1.0
   * @since 2011-03-22
   */
  public interface Iterator {
    /**
     * Called when the traversal reaches a terminal node
     */
    public void terminalAction(BDD b);
    
    /**
     * Called for inner nodes before visiting left son
     * (i.e. then-successor).
     * If preAction returns false, the sons of b will not be
     * visited, and midAction and postAction won't be performed.
     */
    public boolean preAction(BDD b);
    
    /**
     * Called between visits to left (i.e. then-successor) and right
     * (i.e. else-successor) sons.
     * If midAction returns false, the right son will not be
     * visited, and postAction won't be called for node b.
     */
    public boolean midAction(BDD b) ;
    
    /**
     * Called after visiting both sons.
     */
    public void postAction(BDD b);
  }

  public double countSatVals(SortedSet<Integer> universe) {
    final class SatValCounter implements Iterator {
      private final Hashtable<BDD, Double> akku;
      private SortedSet<Integer> universe;

      double getCount(BDD b) {
        return akku.get(b);
      }

      SatValCounter(SortedSet<Integer> universe) {
        this.universe = universe;
        this.akku = new Hashtable<>();
        this.akku.put(BDD.trueBDD(), 1.0);
        this.akku.put(BDD.falseBDD(), 0.0);
      }

      public void terminalAction(BDD b) {
      }

      public boolean preAction(BDD b) {
        return true;
      }

      public boolean midAction(BDD b) {
        return true;
      }

      public void postAction(BDD b) {
        double countThenBDD = akku.get(b.getThenSucc());
        double countElseBDD = akku.get(b.getElseSucc());
        int numDontCaresThen = universe.subSet(b.getVariable() + 1, b.getThenSucc().getVariable()).size();
        int numDontCaresElse = universe.subSet(b.getVariable() + 1, b.getElseSucc().getVariable()).size();
        double count_b = Math.pow(2, numDontCaresThen) * countThenBDD + Math.pow(2, numDontCaresElse) * countElseBDD;
        akku.put(b, count_b);
      }
    }
    SatValCounter counter = new SatValCounter(universe);
    this.traverse(counter);
    int numDontCaresFrom1ToThis = universe.subSet(1, this.getVariable()).size();
    return Math.pow(2, numDontCaresFrom1ToThis) * counter.getCount(this);
  }

  /**
   * Perform a depth-first traversal of this BDD using
   * <tt>iterator</tt> as an accumulator object. For performance reasons, it is
   * strongly discouraged from using this method for implementing BDD operations.
   * 
   * @param iterator a BDD iterator for traversal
   */
  public void traverse(Iterator iterator) {
    this.traverse(iterator, new HashSet<>());
  }

  /**
   * An auxiliary method used by the traverse method.
   * 
   * @param iterator a BDD iterator
   * @param visited set of already visited BDDs
   */
  private void traverse(Iterator iterator, Set<BDD> visited) {
    visited.add(this);

    if (this.isFalse()) {
      iterator.terminalAction(this);
      return;
    }
    
    if (this.isTrue()) {
      iterator.terminalAction(this);
      return;
    }

    if (iterator.preAction(this)) {
      BDD thenSucc = this.getThenSucc();
      if (!visited.contains(thenSucc)) {
        thenSucc.traverse(iterator, visited);
      }
      if (iterator.midAction(this)) {
        BDD elseSucc = this.getElseSucc();
        if (!visited.contains(elseSucc)) {
          elseSucc.traverse(iterator, visited);
        }
        iterator.postAction(this);
      }
    }
  }

  /**
   * Returns the number of bdd nodes this bdd is composed of (including the leaf
   * nodes).
   * 
   * @return the number of bdd nodes this bdd is composed of
   */
  public int size() {
    int count = bddManager.nodeCount(location);
    return ((count > 0) ? (2 + count) : 1);
  }

  /**
   * Return an (arbitrarily chosen) Valuation
   * that satisfies the boolean function represented by this BDD, if such a
   * Valuation exists. Otherwise (i.e. if this BDD is the constantly false BDD)
   * null is returned.
   * 
   * @return a Valuation that satisfies the boolean function represented by this
   *         BDD, if such a Valuation exists; otherwise null.
   */
  public Valuation satisfyingValuation() {
    if (this.location == falseBDDLocation)
      return null;
    int[] jddValuation = bddManager.oneSat(location, null);
    return (jddValuation != null) ? new Valuation(jddValuation) : null;
  }

  @Override
  public boolean equals(Object object) {
    if (object == null)
      return false;
    try {
      BDD other = (BDD)object;
      return (this.location == other.location);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return "BDD[loc:" + location + "]";
  }
}
