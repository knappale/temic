package syntax;

import java.util.logging.Logger;
import java.io.PrintWriter;
import java.io.IOException;

import bdd.BDD;
import bdd.BDDWB;
import bdd.Valuation;
import core.StateIMP;
import core.Temic;
import core.Type;
import core.SymbolManager;
import core.SymbolTable;
import core.Specification;
import core.KripkeStructure;
import core.GenerationFailed;
import core.Interpretation;
import core.Run;
import core.OptionSet;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

public class Command extends Node {
  public enum Kind {
    SKIP, SEQ,
    INTERPRET,
    CHECKSF, CHECKRF, CHECKRT, CHECKR, CHECKF, CHECKT, CHECK,
    ASSERTSF, ASSERTR, ASSERTNR, ASSERTV, ASSERTNV,
    PRINT, PRINTLN;

    public String symbol() {
      switch(this) {
        case SKIP: return "skip";
        case SEQ: return ";";
        case INTERPRET: return "interpret";
        case CHECKSF: return "checkSF";
        case CHECKRF: return "checkRF";
        case CHECKRT: return "checkRT";
        case CHECKR: return "checkR";
        case CHECKF: return "checkF";
        case CHECKT: return "checkT";
        case CHECK: return "check";
        case ASSERTSF: return "assertSF";
        case ASSERTR: return "assertR";
        case ASSERTNR: return "assertNR";
        case ASSERTV: return "assertV";
        case ASSERTNV: return "assertNV";
        case PRINT: return "print";
        case PRINTLN: return "println";
        default: return "" + this;
      }	
    }
  }

  private @NonNull Kind kind;
  private Command first, second;
  private Node formula;
  private String text;
  /** the logger for instances of class Node */
  private static Logger logger = Logger.getLogger(Temic.class.getName());

  private enum AssertionType {
    NONE, TRUE, FALSE;
  }

  private Command(@NonNull Kind kind) {
    this.kind = kind;
  }

  private Command(@NonNull Kind kind, Command first, Command second) {
    this(kind);
    this.first = first;
    this.second = second;
  }

  private Command(@NonNull Kind kind, Node formula) {
    this(kind);
    this.formula = formula;
  }

  private Command(@NonNull Kind kind, String text) {
    this(kind);
    this.text = text;
  }

  public static Command skip() {
    return new Command(Kind.SKIP);
  }

  public static Command seq(Command first, Command second) {
    return new Command(Kind.SEQ, first, second);
  }

  public static Command interpret() {
    return new Command(Kind.INTERPRET);
  }

  public static Command checkStateFormula(Node formula) {
    return new Command(Kind.CHECKSF, formula);
  }

  public static Command assertStateFormula(Node formula) {
    return new Command(Kind.ASSERTSF, formula);
  }

  public static Command checkReachabilityFalse(Node formula) {
    return new Command(Kind.CHECKRF, formula);
  }

  public static Command checkReachabilityTrue(Node formula) {
    return new Command(Kind.CHECKRT, formula);
  }

  public static Command checkReachability(Node formula) {
    return new Command(Kind.CHECKR, formula);
  }

  public static Command assertReachable(Node formula) {
    return new Command(Kind.ASSERTR, formula);
  }

  public static Command assertNotReachable(Node formula) {
    return new Command(Kind.ASSERTNR, formula);
  }

  public static Command checkValidityFalse(Node formula) {
    return new Command(Kind.CHECKF, formula);
  }

  public static Command checkValidityTrue(Node formula) {
    return new Command(Kind.CHECKT, formula);
  }

  public static Command checkValidity(Node formula) {
    return new Command(Kind.CHECK, formula);
  }

  public static Command assertValid(Node formula) {
    return new Command(Kind.ASSERTV, formula);
  }

  public static Command assertNotValid(Node formula) {
    return new Command(Kind.ASSERTNV, formula);
  }

  public static Command print(String text) {
    return new Command(Kind.PRINT, text);
  }

  public static Command println(String text) {
    return new Command(Kind.PRINTLN, text);
  }

  private AssertionType getAssertionType() {
    switch (this.kind) {
      case SKIP:
      case SEQ:
      case INTERPRET:
        return AssertionType.NONE;

      case CHECKSF:
        return AssertionType.NONE;
      case ASSERTSF:
        return AssertionType.TRUE;

      case CHECKRF:
      case CHECKRT:
      case CHECKR:
        return AssertionType.NONE;
      case ASSERTR:
        return AssertionType.TRUE;
      case ASSERTNR:
        return AssertionType.FALSE;

      case CHECKF:
      case CHECKT:
      case CHECK:
        return AssertionType.NONE;
      case ASSERTV:
        return AssertionType.TRUE;
      case ASSERTNV:
        return AssertionType.FALSE;

      case PRINT:
      case PRINTLN:
        return AssertionType.NONE;

      default:
        throw new RuntimeException("illegal kind symbol '" + this.kind + "' in getAssertionType()");
    }
  }

  @Override
  @NonNullByDefault
  public Type check(SymbolTable<syntax.Identifier, Type> typeEnv) throws TypingException {
    type = null;
    switch (this.kind) {
      case SKIP:
        type = Type.voidType();
        return type;

      case SEQ:
        Type firstType = this.first.check(typeEnv);
        Type secondType = this.second.check(typeEnv);
        if (firstType.isVoid() && secondType.isVoid()) {
          type = Type.voidType();
          return type;
        }
        throw new TypingException("concatenator ';' cannot be applied to arguments of type " + firstType + " and " + secondType + "; should be of type void and void.", line, column);

      case INTERPRET:
        type = Type.voidType();
        return type;

      case CHECKSF:
      case CHECKRF:
      case CHECKRT:
      case CHECKR:
      case CHECKF:
      case CHECKT:
      case CHECK:
      case ASSERTSF:
      case ASSERTR:
      case ASSERTNR:
      case ASSERTV:
      case ASSERTNV:
        Type childType = this.formula.check(typeEnv);
        if (childType.isBoolean()) {
          type = Type.voidType();
          return type;
        }
        throw new TypingException("" + this.kind.symbol() + " statement with argument of type " + childType
            + " occurred; should be of type boolean.", this.first.getLine(), this.first.getColumn());

      case PRINT:
      case PRINTLN:
        type = Type.voidType();
        return type;
      default:
        throw new RuntimeException("illegal kind symbol '" + this.kind + "' in check()");
    }
  }

  public void execute(Specification spec, StateIMP sigma, PrintWriter out, OptionSet options) {
    SymbolManager sm = spec.sm();
    KripkeStructure struct = null;
    switch (this.kind) {
      case SKIP:
        break;

      case SEQ:
        first.execute(spec, sigma, out, options);
        if (!sigma.hasTerminated())
          second.execute(spec, sigma, out, options);
        break;

      case INTERPRET:
        logger.info("interpreting specification '" + spec.getName() + "'");

        Interpretation res = spec.interpret(options);

        logger.info("fixed point reached after " + res.numberIterations() + " iterations.");

        if (res.pnstruct().isDeterminated()) {
          logger.info("=> unique interpretation found.");
          res.ksNegative_c().printSummaryInfo(out);
          sigma.setFixedPoint(res.ksNegative_c());
        } else {
          logger.info("=> NO unique interpretation found.");
          spec.printSubsidiaryInfo(res.pnstruct(), out, options);
          sigma.setFixedPoint(null);
        }
        break;

      case CHECKSF:
      case ASSERTSF:
        out.println("checking state formula: " + formula);
        BDDWB svalWB = formula.evaluateSFBool(sm);
        BDD domain = BDD.and(sm.stateSpaceBDD(), BDD.not(svalWB.undef()));
        BDD sval = BDD.and(svalWB.getBDD(), domain);
        if (domain.equals(sm.stateSpaceBDD())) {
          out.println("=> formula is totally defined on state space.");
        } else if (domain.isFalse()) {
          out.println("=> formula is totally undefined on state space due to division by zero.");
          break;
        } else {
          out.println("=> formula is only partially defined on state space.");
          out.print("example for division by zero: ");
          BDD divisionByZero = BDD.and(sm.stateSpaceBDD(), svalWB.undef());
          Valuation satVal = divisionByZero.satisfyingValuation();
          Valuation satValFQ = satVal.specifyDontCares(sm.bivalentVariables());
          try {
            satValFQ.printTyped(out, sm);
          } catch (IOException e) {
            logger.warning("IOException occurred during typed output of valuation: " + e);
          }

        }

        if (sval.equals(domain)) {
          out.println("=> formula is universally valid on its domain.");
        } else if (sval.isFalse()) {
          out.println("=> formula is contradictory on its domain.");
          if (this.getAssertionType() == AssertionType.TRUE) {
            assertionViolation(sigma, out);
          }
        } else {
          out.println("=> formula is satisfiable, but not universally valid on its domain.");
          out.print("example satifying valuation:  ");
          Valuation satVal = sval.satisfyingValuation();
          Valuation satValFQ = satVal.specifyDontCares(sm.bivalentVariables());
          try {
            satValFQ.printTyped(out, sm);
          } catch (IOException e) {
            logger.warning("IOException occurred during typed output of valuation: " + e);
          }

          out.print("example falsifying valuation: ");
          Valuation falsVal = BDD.and(BDD.not(sval), domain).satisfyingValuation();
          Valuation falsValFQ = falsVal.specifyDontCares(sm.bivalentVariables());
          try {
            falsValFQ.printTyped(out, sm);
          } catch (IOException e) {
            logger.warning("IOException occurred during typed output of valuation: " + e);
          }
          if (this.getAssertionType() == AssertionType.TRUE) {
            assertionViolation(sigma, out);
          }
        }
        break;

      case CHECKRF:
      case CHECKRT:
      case CHECKR:
      case ASSERTR:
      case ASSERTNR:
        switch (this.kind) {
          case CHECKRF:
            struct = sigma.structFalse();
            out.println("checking false-reachability of: " + this.formula);
            break;
          case CHECKRT:
            struct = sigma.structTrue();
            out.println("checking true-reachability of: " + this.formula);
            break;
          case CHECKR:
            struct = sigma.fixedPoint();
            if (struct == null) {
              out.println("reachability check w.r.t. unique interpretation failed.");
              out.println("no interpretation available. please use 'interpret' command.");
            } else {
              out.println("checking reachability of: " + this.formula);
            }
            break;
          case ASSERTR:
          case ASSERTNR:
            if (sigma.fixedPoint() == null) {
              struct = sigma.structTrue();
              out.println("checking true-reachability of: " + this.formula);
              break;
            } else { // sigma.fixedPoint() != null
              struct = sigma.fixedPoint();
              out.println("checking reachability of: " + this.formula);
              break;
            }
        }
        if (struct == null) {
          break;
        }
        svalWB = this.formula.evaluateSFBool(sm);
        sval = BDD.or(svalWB.getBDD(), svalWB.undef());
        BDD reachableAndSatisfyingStates = BDD.and(struct.reachableStates(), sval);
        if (reachableAndSatisfyingStates == BDD.falseBDD()) {
          out.println("=> not reachable.");
          if (this.getAssertionType() == AssertionType.TRUE) {
            assertionViolation(sigma, out);
          }
        } else {
          out.println("=> reachable.");
          Run run = struct.dfs(sval);
          if (run == null) {
            out.println("... but depth-first search couldn't produce a run");
            logger.warning("No run found that reaches " + sval);
          } else {
            try {
              out.println();
              run.printTyped(out, sm);
              out.println();
            } catch (IOException e) {
              logger.warning("IOException occurred during typed output of a trace: " + e);
            }
            if (this.getAssertionType() == AssertionType.FALSE) {
              assertionViolation(sigma, out);
            }
          }
        }
        break;

      case CHECKF:
      case CHECKT:
      case CHECK:
      case ASSERTV:
      case ASSERTNV:
        switch (this.kind) {
          case CHECKF:
            struct = sigma.structFalse();
            out.println("checking initial false-validity of CTLK formula: " + this.formula);
            break;
          case CHECKT:
            struct = sigma.structTrue();
            out.println("checking initial true-validity of CTLK formula: " + this.formula);
            break;
          case CHECK:
            struct = sigma.fixedPoint();
            if (struct == null) {
              out.println("validity check w.r.t. unique interpretation failed.");
              out.println("no interpretation available. please use 'interpret' command.");
            } else {
              out.println("checking initial validity of CTLK formula: " + this.formula);
            }
            break;
          case ASSERTV:
          case ASSERTNV:
            if (sigma.fixedPoint() == null) {
              struct = sigma.structTrue();
              out.println("checking initial true-validity of CTLK formula: " + this.formula);
              break;
            } else { // sigma.fixedPoint() != null
              struct = sigma.fixedPoint();
              out.println("checking initial validity of CTLK formula: " + this.formula);
              break;
            }

        }
        if (struct == null) {
          break;
        }
        sval = this.formula.evaluateBool(struct);
        BDD test = BDD.implies(spec.initialCondition(), sval); // initial semantics
        if (test == BDD.trueBDD()) {
          out.println("=> CTLK formula is valid.");
          if (this.getAssertionType() == AssertionType.FALSE) {
            assertionViolation(sigma, out);
          }
        } else {
          out.print("=> CTLK formula is NOT valid");
          if (options.cxlevel() != OptionSet.CXLevel.OFF) { // generation of counter-examples enabled
            try {
              Run counterExample = this.formula.getCounterExample(struct);
              out.println("");
              out.println("as demonstrated by the following counter-example:");
              out.println();
              counterExample.printTyped(out, sm);
            } catch (GenerationFailed ex) {
              out.println(",");
              out.println("but no counter-example can be computed (" + ex + ").");
            } catch (IOException ex) {
              System.err.println("IOException while printing counter-example, aborting");
              System.exit(1);
            }
          } else { // generation of counter-examples disabled
            out.println(".");
          }
          if (this.getAssertionType() == AssertionType.TRUE) {
            assertionViolation(sigma, out);
          }
        }
        break;

      case PRINT:
        out.print(text);
        break;

      case PRINTLN:
        out.println(text);
        break;

      default:
        throw new RuntimeException("illegal kind symbol '" + this.kind + "' in execute()");
    }
  }

  private void assertionViolation(StateIMP sigma, PrintWriter out) {
    out.println("ASSERTION VIOLATION.");
    sigma.setTerminated();
  }

  @Override
  public String toString() {
    switch (this.kind) {
      case SKIP:
        return this.kind.symbol();
      case SEQ:
        return "(" + this.first + ";" + this.second + ")";
      case INTERPRET:
        return this.kind.symbol();
      case CHECKSF:
      case CHECKRF:
      case CHECKRT:
      case CHECKR:
      case CHECKF:
      case CHECKT:
      case CHECK:
      case ASSERTSF:
      case ASSERTR:
      case ASSERTNR:
      case ASSERTV:
      case ASSERTNV:
        return this.kind.symbol() + "(" + this.formula + ")";
      case PRINT:
      case PRINTLN:
        return this.kind.symbol() + "(\"" + this.text + "\")";
      default:
        throw new RuntimeException("illegal kind symbol '" + this.kind.symbol() + "' in toString()");
    }
  }
}
