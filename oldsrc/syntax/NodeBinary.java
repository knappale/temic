package syntax;

import org.eclipse.jdt.annotation.NonNullByDefault;

import java.util.Vector;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Logger;

import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;
import core.GenerationFailed;
import core.KripkeStructure;
import core.PNStructure;
import core.Run;
import core.Step;
import core.SymbolTable;
import core.SValueEnvironment;
import core.Specification;
import core.Temic;
import core.Type;
import syntax.generated.TemicParser;


/**
 * The root class of all syntactic objects with a binary uppermost constructor symbol.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-09
 */
class NodeBinary extends Node {
    /** the first (left) syntactic object which the constructor symbol is applied to */
	protected Node firstChild;
    /** the second (right) syntactic object which the constructor symbol is applied to */
	protected Node secondChild;
	/** the logger for instances of class NodeBinary */
	private static Logger logger = Logger.getLogger(Temic.class.getName());
	
    /**
     * Applies the constructor symbol to <tt>(firstChild,secondChild)</tt>.
     * @param constr the constructor symbol as a literal of enum Constr.
     * @param firstChild the first (left) syntactic object
     * which the constructor symbol is applied to.
     * @param secondChild the second (right) syntactic object
     * which the constructor symbol is applied to.
     */
	NodeBinary(Constr constr, Node firstChild, Node secondChild) {
		super(constr);
		this.firstChild = firstChild;
		this.secondChild = secondChild;
	}
	
	NodeBinary(Constr constr, Node firstChild, Node secondChild, int line, int column) {
		super(constr);
		this.firstChild = firstChild;
		this.secondChild = secondChild;
		this.line = line;
		this.column = column;
	}
		
    /**
     * Returns a string representation of this syntactic object.
     * @return a string representation of this syntactic object.
     **/
    public String toString() {
    	if (constr.isBinaryExpr()) {
    		switch(constr) {
    			case AU:
    				return "A[" + firstChild + " U " + secondChild + "]";
    			case EU:
    				return "E[" + firstChild + " U " + secondChild + "]";
    			case AW:
    				return "A[" + firstChild + " W " + secondChild + "]";
    			case EW:
    				return "E[" + firstChild + " W " + secondChild + "]";
    			default:
    				return "(" + firstChild + " " + constr.symbol() + " " + secondChild + ")";
    		}
    	}
    	else {
    		switch(constr) {
    			case DOASSN:
    				return "" + firstChild + " := " + secondChild;
    			default:
    				throw new RuntimeException("illegal constructor symbol '" + constr + "' in toString()");
    		}
    	}
    }
    
    @Override
    @NonNullByDefault
	public Type check(SymbolTable<syntax.Identifier, Type> typeEnv) throws TypingException {
		type = null;
		Type firstType = firstChild.check(typeEnv);
		Type secondType = secondChild.check(typeEnv);
		switch(constr) {
			case AND:
			case NAND:
			case OR:
			case NOR:
			case EXOR:
			case IMPL:
			case BIIMPL:
				isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
				if (firstType.isBoolean() && secondType.isBoolean()) {
					type = Type.booleanType(); break;
				}
				throw new TypingException(constr, firstType, secondType, line, column);
			case AU:
			case EU:
			case AW:
			case EW:
				isPropositional = false;
				if (firstType.isBoolean() && secondType.isBoolean()) {
					type = Type.booleanType(); break;
				}
				throw new TypingException(constr, firstType, secondType, line, column);	
			case PLUS:
			case PLUS_IMPLICIT:
				if (firstType.isInteger() && secondType.isInteger()) {
					type = Type.integerType(firstType.lowerBound()+secondType.lowerBound(),
					                        firstType.upperBound()+secondType.upperBound());
					isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
					break;
				}
				throw new TypingException(constr, firstType, secondType, line, column);
			case MINUS:
				if (firstType.isInteger() && secondType.isInteger()) {
					type = Type.integerType(firstType.lowerBound()-secondType.upperBound(),
					                        firstType.upperBound()-secondType.lowerBound());
					isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
					break;
				}
				throw new TypingException(constr, firstType, secondType, line, column);
			case TIMES:
				if (firstType.isInteger() && secondType.isInteger()) {
					HashSet<Integer> set = new HashSet<Integer>();
					set.add(firstType.lowerBound()*secondType.lowerBound());
					set.add(firstType.lowerBound()*secondType.upperBound());
					set.add(firstType.upperBound()*secondType.lowerBound());
					set.add(firstType.upperBound()*secondType.upperBound());
					type = Type.integerType(util.Math.min(set),util.Math.max(set));
					isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
					break;
				}
				throw new TypingException(constr, firstType, secondType, line, column);
			case MOD:
				if (firstType.isInteger() && secondType.isInteger()) {
					if (secondType.lowerBound() != 0 || secondType.upperBound() != 0) {
						HashSet<Integer> set = new HashSet<Integer>();
						set.add(java.lang.Math.abs(secondType.lowerBound()));
						set.add(java.lang.Math.abs(secondType.upperBound()));
						type = Type.integerType(0,util.Math.max(set)-1);
						isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
						break;
					}
					throw new TypingException("Predetermined division by zero.", line, column);
				}
				throw new TypingException(constr, firstType, secondType, line, column);
			case DIV:
				if (firstType.isInteger() && secondType.isInteger()) {
					int nl = firstType.lowerBound(); int dl = secondType.lowerBound();
					int nu = firstType.upperBound(); int du = secondType.upperBound();
					if (dl != 0 || du != 0) {
						HashSet<Integer> set = new HashSet<Integer>();
						if (dl != 0) {
							set.add(util.Math.div(nl,dl));
							set.add(util.Math.div(nu,dl));
						}
						if (du != 0) {
							set.add(util.Math.div(nl,du));
							set.add(util.Math.div(nu,du));
						}
						if (dl <= -1 && -1 <= du) {
							set.add((-1)*nl);
							set.add((-1)*nu);
						}
						if (dl <= 1 && 1 <= du) {
							set.add(nl);
							set.add(nu);
						}
						type = Type.integerType(util.Math.min(set),util.Math.max(set));
						isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
						break;
					}
					throw new TypingException("Division by zero. ", line, column);
				}
				throw new TypingException(constr, firstType, secondType, line, column);
			case EQUAL:
			case NEQUAL:
			case LESS:
			case LEQ:
			case GREATER:
			case GEQ:				
				if ((firstType.isBoolean() && secondType.isBoolean()) ||
				    (firstType.isInteger() && secondType.isInteger()) ||
				    (firstType.isEnum() && secondType.isEnum())) {
					type = Type.booleanType();
					isPropositional = firstChild.isPropositional() && secondChild.isPropositional();
					break;
				}
				throw new TypingException(constr, firstType, secondType, line, column);	
			case DOASSN:
				if (!((firstType.isBoolean() && secondType.isBoolean()) ||
				      (firstType.isInteger() && secondType.isInteger()) ||
				      (firstType.isEnum() && secondType.isEnum()))) {
					throw new TypingException(constr, firstType, secondType, line, column);	
				}
				if (!secondType.subsets(firstType)) {
					logger.warning("Type " + secondType + " of the RHS of the do-assignment doesn't subset type " + firstType + " of the LHS. (line " + line + ", column " + column + ")");
				}	
				type = Type.voidType(); break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
		}
		return type;
	}
	
    /**
     * Evaluates this expression to a BDDWB (WB: with additional bdd indicating
     * points at which the represented function is undefined) w.r.t. a given
     * SymbolicValueEnvironment, if this expression is a state formula.
     * @param env an environment of symbolic values (BDDs or MultiBDDs)
     * @return a BDDWB containing the canonical BDD that represents this expression
     * w.r.t. the given environment, if this expression is a state formula.
     */
	public BDDWB evaluateSFBool(SValueEnvironment env) {
		if(firstChild.type.isBoolean()) {
				BDDWB firstBDDWB = firstChild.evaluateSFBool(env);
				BDDWB secondBDDWB = secondChild.evaluateSFBool(env);
				BDD firstBDD = firstBDDWB.getBDD();
				BDD secondBDD = secondBDDWB.getBDD();
				BDD undefined = BDD.or(firstBDDWB.undef(), secondBDDWB.undef());
				switch(constr) {
					case AND:
						return new BDDWB(BDD.and(firstBDD, secondBDD), undefined);
					case NAND:
						return new BDDWB(BDD.nand(firstBDD, secondBDD), undefined);
					case OR:
						return new BDDWB(BDD.or(firstBDD, secondBDD), undefined);
					case NOR:
						return new BDDWB(BDD.nor(firstBDD, secondBDD), undefined);
					case EXOR:
						return new BDDWB(BDD.exor(firstBDD, secondBDD), undefined);
					case IMPL:
						return new BDDWB(BDD.implies(firstBDD, secondBDD), undefined);
					case BIIMPL:
						return new BDDWB(BDD.biimplies(firstBDD, secondBDD), undefined);
					case EQUAL:
						if (firstChild.type.isBoolean()) {
							return new BDDWB(BDD.biimplies(firstBDD, secondBDD), undefined);
						}
						throw new RuntimeException("illegal type " + firstChild.type + " in evaluateSFBool(), case EQUAL");
					case NEQUAL:
						if (firstChild.type.isBoolean()) {
							return new BDDWB(BDD.exor(firstBDD, secondBDD), undefined);
						}
						throw new RuntimeException("illegal type " + firstChild.type + " in evaluateSFBool(), case NEQUAL");
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" + " in evaluateSFBool()");
				}
		} else
		if (firstChild.type.isInteger()) {		
				MultiBDD firstMultiBDD = firstChild.evaluateSFInt(env);
				MultiBDD secondMultiBDD = secondChild.evaluateSFInt(env);
				BDD undefined = BDD.or(firstMultiBDD.undef(), secondMultiBDD.undef());
				switch(constr) {
					case EQUAL:
						return new BDDWB(MultiBDD.equal(firstMultiBDD, secondMultiBDD), undefined);
					case NEQUAL:
						return new BDDWB(BDD.not(MultiBDD.equal(firstMultiBDD, secondMultiBDD)), undefined);
					case LEQ:
						return new BDDWB(MultiBDD.leq(firstMultiBDD, secondMultiBDD), undefined);
					case LESS:
						return new BDDWB(MultiBDD.less(firstMultiBDD, secondMultiBDD), undefined);
					case GEQ:
						return new BDDWB(MultiBDD.leq(secondMultiBDD, firstMultiBDD), undefined);
					case GREATER:
						return new BDDWB(MultiBDD.less(secondMultiBDD, firstMultiBDD), undefined);
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
						" in evaluateSFBool()");
				}
		} else
		if (firstChild.type.isEnum())
			throw new RuntimeException("enum types are not implemented yet");
		else
			throw new RuntimeException("unknown type id " + firstChild.type + " in evaluateSFBool()");
	}
	
	public MultiBDD evaluateSFInt(SValueEnvironment env) {
		MultiBDD firstMultiBDD = firstChild.evaluateSFInt(env);
		MultiBDD secondMultiBDD = secondChild.evaluateSFInt(env);
		MultiBDD result;
		BDD undefined = BDD.falseBDD();
		switch(constr) {
			case PLUS:
			case PLUS_IMPLICIT:
				result = MultiBDD.add(firstMultiBDD, secondMultiBDD);
				break;
			case MINUS:
				result = MultiBDD.sub(firstMultiBDD, secondMultiBDD);
				break;
			case TIMES:
				result = MultiBDD.multiply(firstMultiBDD, secondMultiBDD);
				break;
			case DIV:
				result = MultiBDD.div(firstMultiBDD, secondMultiBDD);
				undefined = MultiBDD.equal(secondMultiBDD, MultiBDD.intMultiBDD(0));
				break;
			case MOD:
				result = MultiBDD.mod(firstMultiBDD, secondMultiBDD);
				undefined = MultiBDD.equal(secondMultiBDD, MultiBDD.intMultiBDD(0));
				break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateSFInt()");
		}
		result.setUndef(BDD.or(undefined, BDD.or(firstMultiBDD.undef(), secondMultiBDD.undef())));
		return result;
	}
	
	public BDD evaluateBool(KripkeStructure struct) {
		if (firstChild.type.isBoolean()) {
				switch(constr) {
					case AND:
						return BDD.and(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
					case NAND:
						return BDD.nand(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
					case OR:
						return BDD.or(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
					case NOR:
						return BDD.nor(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
					case EXOR:
					case NEQUAL:
						return BDD.exor(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
					case IMPL:
						return BDD.implies(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
					case BIIMPL:
					case EQUAL:
						return BDD.biimplies(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
						
					case AU:
						Node equivFormula = Node.not_typed(
							Node.EW_typed(
								Node.not_typed(secondChild),
								Node.and_typed(
									Node.not_typed(firstChild),
									Node.not_typed(secondChild)
								)
							)
						);
						return equivFormula.evaluateBool(struct);
						
					case EU:
						return struct.leastFixedpointB(firstChild.evaluateBool(struct), secondChild.evaluateBool(struct));
						
					case AW:
						equivFormula = Node.not_typed(
							Node.EU_typed(
								Node.not_typed(secondChild),
								Node.and_typed(
									Node.not_typed(firstChild),
									Node.not_typed(secondChild)
								)
							)
						);
				        return equivFormula.evaluateBool(struct);
						
					case EW:
						equivFormula = Node.or_typed(
							Node.EU_typed(firstChild,secondChild),
                            Node.EG_typed(firstChild)
						);
						return equivFormula.evaluateBool(struct);
						
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
							" in evaluateBool(), case BOOL");
				}
		} else
		if (firstChild.type.isInteger()) {	
				MultiBDD firstMultiBDD = firstChild.evaluateInt(struct);
				MultiBDD secondMultiBDD = secondChild.evaluateInt(struct);
				BDD undefined = BDD.or(firstMultiBDD.undef(), secondMultiBDD.undef());
				switch(constr) {
					case EQUAL:
						return BDD.or(MultiBDD.equal(firstMultiBDD, secondMultiBDD), undefined);
					case NEQUAL:
						return BDD.or(BDD.not(MultiBDD.equal(firstMultiBDD, secondMultiBDD)), undefined);
					case LEQ:
						return BDD.or(MultiBDD.leq(firstMultiBDD, secondMultiBDD), undefined);
					case LESS:
						return BDD.or(MultiBDD.less(firstMultiBDD, secondMultiBDD), undefined);
					case GEQ:
						return BDD.or(MultiBDD.leq(secondMultiBDD, firstMultiBDD), undefined);
					case GREATER:
						return BDD.or(MultiBDD.less(secondMultiBDD, firstMultiBDD), undefined);
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
						" in evaluateBool(), case INT");
				}
		} else
		if (firstChild.type.isEnum())	
			throw new RuntimeException("enum types are not implemented yet");
		else
			throw new RuntimeException("unknown type " + firstChild.type + " in evaluateBool()");
	}
	
	public MultiBDD evaluateInt(KripkeStructure struct) {
		MultiBDD firstMultiBDD = firstChild.evaluateInt(struct);
		MultiBDD secondMultiBDD = secondChild.evaluateInt(struct);
		MultiBDD result;
		BDD undefined = BDD.falseBDD();
		switch(constr) {
			case PLUS:
			case PLUS_IMPLICIT:
				result = MultiBDD.add(firstMultiBDD, secondMultiBDD);
				break;
			case MINUS:
				result = MultiBDD.sub(firstMultiBDD, secondMultiBDD);
				break;
			case TIMES:
				result = MultiBDD.multiply(firstMultiBDD, secondMultiBDD);
				break;
			case DIV:
				result = MultiBDD.div(firstMultiBDD, secondMultiBDD);
				undefined = MultiBDD.equal(secondMultiBDD, MultiBDD.intMultiBDD(0));
				break;
			case MOD:
				result = MultiBDD.mod(firstMultiBDD, secondMultiBDD);
				undefined = MultiBDD.equal(secondMultiBDD, MultiBDD.intMultiBDD(0));
				break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateInt()");
		}
		result.setUndef(BDD.or(undefined, BDD.or(firstMultiBDD.undef(), secondMultiBDD.undef())));
		return result;
	}
	
	public BDD evaluatePositive(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		if (firstChild.type.isBoolean()) {
				switch(constr) {
					case AND:
						return BDD.and(firstChild.evaluatePositive(pnstruct, rule1),
								       secondChild.evaluatePositive(pnstruct, rule1));
					case NAND:
						return BDD.or(firstChild.evaluateNegative(pnstruct, rule1),
									  secondChild.evaluateNegative(pnstruct, rule1));
					case OR:
						return BDD.or(firstChild.evaluatePositive(pnstruct, rule1),
									  secondChild.evaluatePositive(pnstruct, rule1));
					case NOR:
						return BDD.and(firstChild.evaluateNegative(pnstruct, rule1),
									   secondChild.evaluateNegative(pnstruct, rule1));
					case EXOR:
					case NEQUAL:
						return BDD.or(BDD.and(firstChild.evaluateNegative(pnstruct, rule1), secondChild.evaluatePositive(pnstruct, rule1)),
									  BDD.and(firstChild.evaluatePositive(pnstruct, rule1), secondChild.evaluateNegative(pnstruct, rule1)));
					case IMPL:
						return BDD.or(firstChild.evaluateNegative(pnstruct, rule1),
								      secondChild.evaluatePositive(pnstruct, rule1));
					case BIIMPL:
					case EQUAL:
						return BDD.or(BDD.and(firstChild.evaluateNegative(pnstruct, rule1), secondChild.evaluateNegative(pnstruct, rule1)),
									  BDD.and(firstChild.evaluatePositive(pnstruct, rule1), secondChild.evaluatePositive(pnstruct, rule1)));
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
							" in evaluatePositive()");
				}
		} else
		if (firstChild.type.isInteger() || firstChild.type.isEnum()) {	
				BDDWB valueBDDWB = this.evaluateSFBool(spec.sm());
				return BDD.or(valueBDDWB.getBDD(), valueBDDWB.undef());
		} else
		throw new RuntimeException("unknown type " + firstChild.type + " in evaluatePositive()");
	}
	
	public BDD evaluateNegative(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		if (firstChild.type.isBoolean()) {
				switch(constr) {
					case AND:
						return BDD.or(firstChild.evaluateNegative(pnstruct, rule1),
									  secondChild.evaluateNegative(pnstruct, rule1));
					case NAND:
						return BDD.and(firstChild.evaluatePositive(pnstruct, rule1),
									   secondChild.evaluatePositive(pnstruct, rule1));
					case OR:
						return BDD.and(firstChild.evaluateNegative(pnstruct, rule1),
									   secondChild.evaluateNegative(pnstruct, rule1));
					case NOR:
						return BDD.or(firstChild.evaluatePositive(pnstruct, rule1),
								      secondChild.evaluatePositive(pnstruct, rule1));
					case EXOR:
					case NEQUAL:
						return BDD.or(BDD.and(firstChild.evaluateNegative(pnstruct, rule1), secondChild.evaluateNegative(pnstruct, rule1)),
									  BDD.and(firstChild.evaluatePositive(pnstruct, rule1), secondChild.evaluatePositive(pnstruct, rule1)));
					case IMPL:
						return BDD.and(firstChild.evaluatePositive(pnstruct, rule1),
									   secondChild.evaluateNegative(pnstruct, rule1));
					case BIIMPL:
					case EQUAL:
						return BDD.or(BDD.and(firstChild.evaluateNegative(pnstruct, rule1), secondChild.evaluatePositive(pnstruct, rule1)),
									  BDD.and(firstChild.evaluatePositive(pnstruct, rule1), secondChild.evaluateNegative(pnstruct, rule1)));
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
							" in evaluateNegative()");
				}
		} else
	        if (firstChild.type.isInteger() || firstChild.type.isEnum()) {	
				BDDWB valueBDDWB = this.evaluateSFBool(spec.sm());
				return BDD.nor(valueBDDWB.getBDD(), valueBDDWB.undef());
	        }
	        else
		throw new RuntimeException("unknown type " + firstChild.type + " in evaluateNegative()");
	}
	
	/**
	 * Enact an assignment node (of the do-part of an action declaration),
	 * if this node is an assignment node.
	 * @param env an environment of symbolic values (BDDs or MultiBDDs)
	 * @param rhsBuffer a mapping that buffers the new values of the primed symbols.
	 * The string "error'" is mapped to the bdd that represents the set S of all states
	 * in which the expression on the right hand side of the assignment operator 
	 * 1) is undefined or 2) evaluates to an int-value that does not belong to the
	 * int-type of the symbol on the left hand side, if S is not the empty set;
	 * null, if S is the empty set.
	 */
	public void enact(SValueEnvironment env, Map<String,BDD> rhsBuffer) {
		String symbol = firstChild.getSymbol();
		Type symbolType = firstChild.getType();
		BDD errorDelta = BDD.falseBDD();
		if (symbolType.isBoolean()) {
				BDDWB rhsBDDWB = secondChild.evaluateSFBool(env);
				rhsBuffer.put(symbol + "'", rhsBDDWB.getBDD());
				errorDelta = rhsBDDWB.undef();
		} else
		if (symbolType.isInteger()) {
				MultiBDD rhsMultiBDD = secondChild.evaluateSFInt(env);
				MultiBDD rhsMultiBDDEncoding = MultiBDD.sub(rhsMultiBDD, MultiBDD.intMultiBDD(symbolType.lowerBound()));
				for (int bit=0; bit<symbolType.numBits(); bit++) {
					rhsBuffer.put(symbol + "." + bit + "'", rhsMultiBDDEncoding.get(bit));
				}
				BDD illegallyLow = MultiBDD.less(rhsMultiBDD, MultiBDD.intMultiBDD(symbolType.lowerBound()));
				BDD illegallyHigh = MultiBDD.less(MultiBDD.intMultiBDD(symbolType.upperBound()), rhsMultiBDD);
				errorDelta = BDD.or(rhsMultiBDD.undef(), BDD.or(illegallyLow, illegallyHigh));
		} else
		if (symbolType.isEnum())
                  throw new RuntimeException("enum types not implemented yet");
		else
			throw new RuntimeException("unknown type id " + symbolType + " in enact()");
		BDD error = rhsBuffer.get(TemicParser.errorKeyword + "'");
		if (error == null) {
			rhsBuffer.put(TemicParser.errorKeyword + "'", errorDelta);
		}
		else {
			rhsBuffer.put(TemicParser.errorKeyword + "'", BDD.or(error, errorDelta));
		}
	}
	
	String getSymbol() {
		return firstChild.getSymbol();
	}
	
	int getLineSymbol() {
		return firstChild.line;
	}
	
	int getColumnSymbol() {
		return firstChild.column;
	}
	
	Run getExample(BDD start, KripkeStructure struct) throws GenerationFailed {
		if (firstChild.type.isBoolean()) {
				switch(constr) {
					case AND:
						if (this.firstChild.isPropositional()) {
				            BDD newStart = BDD.and(start, this.firstChild.evaluateBool(struct));
				            return this.secondChild.getExample(newStart, struct);
				        }
				        else if (this.secondChild.isPropositional()) {
				            BDD newStart = BDD.and(start, this.secondChild.evaluateBool(struct));
				            return this.firstChild.getExample(newStart, struct);
				        }
				        else {
				        	throw new GenerationFailed("General conjunction");
				        }
					case NAND:
						Node equivFormula = Node.not_typed(Node.and_typed(firstChild, secondChild));
				        return equivFormula.getExample(start, struct);
					case OR:
				        equivFormula = Node.not_typed(Node.and_typed(Node.not_typed(firstChild), Node.not_typed(secondChild)));
				        return equivFormula.getExample(start, struct);
					case NOR:
				        equivFormula = Node.not_typed(Node.or_typed(firstChild, secondChild));
				        return equivFormula.getExample(start, struct);						
					case EXOR:
					case NEQUAL:
				        try {
				            Node disjunct1 = Node.and_typed(firstChild, Node.not_typed(secondChild));
				            return disjunct1.getExample(start, struct);
				        }
				        catch(GenerationFailed ex) {
				            Node disjunct2 = Node.and_typed(secondChild, Node.not_typed(firstChild));
				            return disjunct2.getExample(start, struct);
				        }
					case IMPL:
						equivFormula = Node.or_typed(Node.not_typed(firstChild), secondChild);
				        return equivFormula.getExample(start, struct);
					case BIIMPL:
					case EQUAL:
				        try {
				            Node disjunct1 = Node.and_typed(firstChild, secondChild);
				            return disjunct1.getExample(start, struct);
				        }
				        catch(GenerationFailed ex) {
				            Node disjunct2 = Node.and_typed(Node.not_typed(firstChild), Node.not_typed(secondChild));
				            return disjunct2.getExample(start, struct);
				        }
					case AU:
						throw new GenerationFailed("Universal path quantifier");
					case EU:
				        Vector<BDD> bStates = new Vector<BDD>();
				        bStates.add(this.secondChild.evaluateBool(struct));
				        BDD evaluatedFirstOperand = this.firstChild.evaluateBool(struct);
				        // calculates the states b[k]
				        BDD intersectionState = BDD.and(bStates.lastElement(), start);
				        while (intersectionState == BDD.falseBDD()) {
				            bStates.add(BDD.and(evaluatedFirstOperand, struct.preImageTemporal(bStates.lastElement())));
				            intersectionState = BDD.and(bStates.lastElement(), start);
				        }
				        // calculates the states s[k] and constructs the run
				        BDD elementaryStateS0 = intersectionState.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
				        Run run = new Run(elementaryStateS0);
				        for (int k = bStates.size() - 1; k > 0; --k) {
				            // calculates the elementary state s[bStates.size() - k] and determines
				            // an action which maps s[bStates.size() - (k+1)] to s[bStates.size() - k]
				            String actionName = null;
				            Iterator<String> actionNames = struct.getActionNames().iterator();
				            BDD elementaryStateS = BDD.falseBDD();
				            while(elementaryStateS == BDD.falseBDD() && actionNames.hasNext()) {
				                actionName = actionNames.next();
				                elementaryStateS = BDD.and(struct.imageUnderAction(run.getPostState(), actionName), bStates.get(k-1));
				            }
		       	            if (elementaryStateS == BDD.falseBDD()) {
		       	            	actionName = TemicParser.lambdaKeyword;
		       	            	elementaryStateS = BDD.and(BDD.and(struct.reachableDeadlockStates(), run.getPostState()), bStates.get(k-1));
		       	            	if (elementaryStateS == BDD.falseBDD()) {
		       	            		throw new RuntimeException("inconsistent search results in method getExample(), case EU");
		       	            	}
		       	            }
				            run.add(new Step(actionName, elementaryStateS));
				        }
				        // continues the calculation recursively
				        run.add(this.secondChild.getExample(run.getPostState(), struct));
				        return run;
					case AW:
						throw new GenerationFailed("Universal path quantifier");
					case EW:
						equivFormula = Node.or_typed(Node.EU_typed(firstChild,secondChild),Node.EG_typed(firstChild));
						return equivFormula.getExample(start, struct);
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
							" in getExample(), case BOOL");
				}
		} else
		if (firstChild.type.isInteger()) {
				switch(constr) {
					case EQUAL:
					case NEQUAL:
					case LEQ:
					case LESS:
					case GEQ:
					case GREATER:
						BDD state = BDD.and(this.evaluateBool(struct), start);
		       			BDD elementaryState = state.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
		       			return new Run(elementaryState);
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
							" in getExample(), case INT");
				}
		} else
		if (firstChild.type.isEnum())
			throw new GenerationFailed("enum types are not implemented yet");
		else
			throw new RuntimeException("unknown type id " + firstChild.type + " in getExample()");
	}
	
	Run getCounterExample(BDD start, KripkeStructure struct) throws GenerationFailed {
		if (firstChild.type.isBoolean()) {
				switch(constr) {
					case AND:
				        BDD testFirst = BDD.implies(start, this.firstChild.evaluateBool(struct));
				        if (testFirst != BDD.trueBDD()) { // first operand is not valid w.r.t. M
				            try {
				                return this.firstChild.getCounterExample(start, struct);
				            }
				            catch(GenerationFailed ex) { }
				        }     
				        BDD testSecond = BDD.implies(start, this.secondChild.evaluateBool(struct));
				        if (testSecond != BDD.trueBDD()) { // second operand is not valid w.r.t. M
				            try {
				                return this.secondChild.getCounterExample(start, struct);
				            }
				            catch(GenerationFailed ex) { }
				        }
				        throw new GenerationFailed("General conjunction");
					case NAND:
						Node equivFormula = Node.not_typed(Node.and_typed(firstChild, secondChild));
						return equivFormula.getCounterExample(start, struct);
					case OR:
						equivFormula = Node.not_typed(Node.and_typed(Node.not_typed(firstChild), Node.not_typed(secondChild)));
				        return equivFormula.getCounterExample(start, struct);			
					case NOR:
				        equivFormula = Node.not_typed(Node.or_typed(firstChild, secondChild));
				        return equivFormula.getCounterExample(start, struct);
					case EXOR:
					case NEQUAL:
						try {
							Node conjunct1 = Node.or_typed(firstChild, secondChild);
							return conjunct1.getCounterExample(start, struct);
						}
						catch(GenerationFailed ex) {
							Node conjunct2 = Node.not_typed(Node.and_typed(firstChild, secondChild));
							return conjunct2.getCounterExample(start, struct);
						}
					case IMPL:
						equivFormula = Node.or_typed(Node.not_typed(firstChild), secondChild);
				        return equivFormula.getCounterExample(start, struct);	
					case BIIMPL:
					case EQUAL:
						try {
				            Node conjunct1 = Node.or_typed(Node.not_typed(firstChild), secondChild);
				            return conjunct1.getCounterExample(start, struct);
				        }
				        catch(GenerationFailed ex) {
				            Node conjunct2 = Node.or_typed(Node.not_typed(secondChild), firstChild);
				            return conjunct2.getCounterExample(start, struct);
				        }
					case AU:
						equivFormula = Node.not_typed(
							Node.EW_typed(
								Node.not_typed(secondChild),
									Node.and_typed(
										Node.not_typed(firstChild),
										Node.not_typed(secondChild)
									)
								)
							);
						return equivFormula.getCounterExample(struct);  
					case EU:
						throw new GenerationFailed("Existential path quantifier");
					case AW:
						equivFormula = Node.not_typed(
							Node.EU_typed(
								Node.not_typed(secondChild),
									Node.and_typed(
										Node.not_typed(firstChild),
										Node.not_typed(secondChild)
									)
								)
							);
						return equivFormula.getCounterExample(start, struct);
					case EW:
						throw new GenerationFailed("Existential path quantifier");
					default:
						throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
							" in getCounterExample(), case BOOL");
				}
		} else
		if (firstChild.type.isInteger()) {
				switch(constr) {
				case EQUAL:
				case NEQUAL:
				case LEQ:
				case LESS:
				case GEQ:
				case GREATER:
					BDD state = BDD.and(BDD.not(this.evaluateBool(struct)), start);
	       			BDD elementaryState = state.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
	       			return new Run(elementaryState);
				default:
					throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
						" in getExample(), case INT");
			}
		} else
		if (firstChild.type.isEnum())
		  throw new GenerationFailed("enum types are not implemented yet");
		else
			throw new RuntimeException("unknown type " + firstChild.type + " in getCounterExample()");
	}
}
