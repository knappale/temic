package syntax;

import org.eclipse.jdt.annotation.NonNullByDefault;

import java.util.logging.Logger;

import core.GenerationFailed;
import core.KripkeStructure;
import core.Run;
import core.Specification;
import core.SymbolTable;
import core.PNStructure;
import core.SValueEnvironment;
import core.Temic;
import core.Type;
import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;
import syntax.generated.TemicParser;

import static util.Formatter.quoted;


/**
 * The root class of all atomic syntactic objects.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-09
 */
class NodeAtomic extends Node {
    /** a symbol, if constr is SYMBOLIC or GUARDID;
     * a literal, if constr is SIGNED or ENUM_LITERAL.
     */
    private String symbol;
	/** the logger for instances of class NodeAtomic */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Temic.class.getName());

    NodeAtomic(Constr constr, String symbol, int line, int column) {
    	super(constr);
        this.symbol = symbol;
        this.line = line;
        this.column = column;
    }
    
    String getSymbol() {
    	return symbol;
    }

    /**
     * Returns a string representation of this syntactic object.
     * @return a string representation of this syntactic object.
     */
    public String toString() {
    	switch(constr) {
    		case FALSE: 
    		case TRUE:
    			return constr.symbol();
    		case SIGNED:
    		case ENUM_LITERAL:
    		case IDENTIFIER: 
    		case GUARDID:
    			return symbol;
    		default:
    			throw new RuntimeException("illegal syntactic object during toString()");
    	}
    }
    
    @Override
    @NonNullByDefault
    public Type check(SymbolTable<syntax.Identifier, Type> typeEnv) throws TypingException {
    	type = null;
    	switch(constr) {
    		case FALSE:
    		case TRUE:
    			type = Type.booleanType(); break;
    		case SIGNED:
    			type = Type.singletonIntegerType(Integer.parseInt(symbol)); break;
        	case ENUM_LITERAL:
        		type = Type.singletonEnumType(symbol); break;
    		case IDENTIFIER:
    			if (symbol.equals(TemicParser.errorKeyword)) {
    				type = Type.booleanType();
    			}
    			else {
    				type = typeEnv.top(Identifier.variable(symbol));
    				if (type == null) {
    					type = typeEnv.top(Identifier.constant(symbol));
    					if (type == null) {
    						throw new TypingException("Undeclared symbol " + quoted(symbol), line, column);
    					}
    				}
    			}
    			break;
    		case GUARDID:
    			type = typeEnv.top(Identifier.guard(symbol)); isPropositional = false;
    			if (type == null) {
    				throw new TypingException("Undeclared symbol " + quoted(symbol), line, column);
    			}
    			break;
    		default:
    			throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
    	}
    	return type;
	}
    
    /**
     * Evaluates this expression to a BDDWB (WB: with additional bdd indicating
     * points at which the represented function is undefined) w.r.t. a given
     * SymbolicValueEnvironment, if this expression is a state formula.
     * @param env an environment of symbolic values (BDDs or MultiBDDs)
     * @return a BDDWB containing the canonical BDD that represents this expression
     * w.r.t. the given environment, if this expression is a state formula.
     */
	public BDDWB evaluateSFBool(SValueEnvironment env) {
		switch(constr) {
			case FALSE:
				return new BDDWB(BDD.falseBDD(), BDD.falseBDD());
			case TRUE:
				return new BDDWB(BDD.trueBDD(), BDD.falseBDD());
			case IDENTIFIER:
				return env.getBDDWBForSymbol(symbol);
				// return new BDDWB(env.getBDDForSymbol(symbol), BDD.falseBDD());
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateSFBool()");
		}
	}
	
	public MultiBDD evaluateSFInt(SValueEnvironment env) {
		switch(constr) {
			case SIGNED:
				return MultiBDD.intMultiBDD(Integer.parseInt(symbol));
			case IDENTIFIER:
				return env.getMultiBDDForSymbol(symbol);
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateSFInt()");
		}
	}
	
	public BDD evaluateBool(KripkeStructure struct) {
		switch(constr) {
			case FALSE:
				return BDD.falseBDD();
			case TRUE:
				return BDD.trueBDD();
			case IDENTIFIER:
				BDDWB symbolBDDWB = struct.getBDDWBForSymbol(symbol);
				return BDD.or(symbolBDDWB.getBDD(), symbolBDDWB.undef());
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateBool()");
		}
	}
	
	public MultiBDD evaluateInt(KripkeStructure struct) {
		switch(constr) {
			case SIGNED:
				return MultiBDD.intMultiBDD(Integer.parseInt(symbol));
			case IDENTIFIER:
				return struct.getMultiBDDForSymbol(symbol);
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateInt()");
		}
	}
	
	public BDD evaluatePositive(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		switch(constr) {
			case FALSE:
				return BDD.falseBDD();
			case TRUE:
				return BDD.trueBDD();
			case IDENTIFIER: 
				if (!type.isBoolean()) {
					throw new RuntimeException("evaluatePositive() called on identifier of type " + type);
				}
				BDDWB symbolBDDWB = spec.sm().getBDDWBForSymbol(symbol);
				return BDD.or(symbolBDDWB.getBDD(), symbolBDDWB.undef());
			case GUARDID:
				return spec.sm().getGuardExpressionForSymbol(symbol).evaluatePositive(pnstruct, rule1);
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "' in evaluatePositive()");
		}
	}
	
	public BDD evaluateNegative(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		switch(constr) {
			case FALSE:
				return BDD.trueBDD();
			case TRUE:
				return BDD.falseBDD();
			case IDENTIFIER: 
				if (!type.isBoolean()) {
					throw new RuntimeException("evaluateNegative() called on identifier of type " + type);
				}
				BDDWB symbolBDDWB = spec.sm().getBDDWBForSymbol(symbol);
				return BDD.nor(symbolBDDWB.getBDD(), symbolBDDWB.undef());
			case GUARDID:
				return spec.sm().getGuardExpressionForSymbol(symbol).evaluateNegative(pnstruct, rule1);
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "' in evaluateNegative()");
		}
	}
	
    Run getExample(BDD start, KripkeStructure struct) throws GenerationFailed {
       	switch(constr) {
       		case FALSE:
       			throw new GenerationFailed("Impossible"); 
       		case TRUE:
       	        BDD elementaryStateS0 = start.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
       	        return new Run(elementaryStateS0);
       		case IDENTIFIER:
       			BDD state = BDD.and(this.evaluateBool(struct), start);
       			BDD elementaryState = state.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
       			return new Run(elementaryState);
       		default:
       			throw new RuntimeException("illegal constructor symbol '" + constr + "' in getExample()");
       	}
    }
    
    Run getCounterExample(BDD start, KripkeStructure struct) throws GenerationFailed {
       	switch(constr) {
       		case FALSE:
       	        BDD elementaryStateS0 = start.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
       	        return new Run(elementaryStateS0);
       		case TRUE:
       			throw new GenerationFailed("Impossible");
       		case IDENTIFIER:
       			BDD state = BDD.and(BDD.not(this.evaluateBool(struct)), start);
       	        BDD elementaryState = state.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
       	        return new Run(elementaryState);
   			default:
   				throw new RuntimeException("illegal constructor symbol '" + constr + "' in getCounterExample()");
       	}
    }
}
