package syntax;

import core.SValueEnvironment;
import core.Specification;
import core.PNStructure;
import core.GenerationFailed;
import core.KripkeStructure;
import core.Type;
import core.SymbolManager;
import core.SymbolTable;
import core.Run;
import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;

import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Abstract root class of all syntactic objects.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-09
 */
public abstract class Node {
	/** the constructor symbol of this node */
	protected Constr constr;
	/** the type of this node */
	protected Type type = null;
	/** if this node is an expression, the following flag indicates whether the expression
	 * is purely propositional (or bounded first-order), i.e. contains neither temporal nor
	 * epistemic modalities */
	protected boolean isPropositional = true;
	/** The line number of the first character of the first token
	 * of this syntactic object. */
	protected int line;
	/** The column number of the first character of the first token
	 * of this syntactic object. */
	protected int column;
	
	/**
	 * @return the type of this syntactic object.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @return the line number of this syntactic object.
	 */
	public int getLine() {
		return line;
	}
	
	/**
	 * @return the column number of this syntactic object.
	 */
	public int getColumn() {
		return column;
	}
	
	/**
	 * Sets the line and column numbers of this syntactic object.
	 * @param line the line number of the first character of the first token
	 * of this syntactic object.
	 * @param column the column number of the first character of the first token
	 * of this syntactic object.
	 */
	public void setLineColumn(int line, int column) {
		this.line = line;
		this.column = column;
	}
	
	protected Node() {
	}
	
	protected Node(Constr constr) {
	  this.constr = constr;
	}

	private boolean isExpr() {
	  return this.constr != null && this.constr.isExpr();
	}

	/**
	 * Returns a node that represents a global var-declaration of the form
	 * <tt>var symbol : type</tt>.
	 * @pre symbol != null && type != null
	 * @param symbol the symbol that is to be declared
	 * @param type the declared type of the symbol
	 * @return a node that represents a var-declaration of the form var symbol : type.
	 */
	public static Node varDecl(String symbol, Type type) {
		return new Declaration(Constr.VAR, symbol, type, null, null);
	}
	
	public static Node letDecl(String symbol, Type type, Node expression) {
		if (expression.isExpr()) {
			return new Declaration(Constr.LET, symbol, type, expression, null);
		}
		throw new RuntimeException("illegal constructor symbol in letDecl()");
	}
	
	public static Node guardDecl(String symbol, Node expression) {
		if (expression.isExpr()) {
			return new Declaration(Constr.GUARD, symbol, Type.booleanType(), expression, null);
		}
		throw new RuntimeException("illegal constructor symbol in guardDecl()");
	}
	
	public static Node agentDecl(String agentName, Set<SymbolLineColumn> identifiers_and_positions) {
		return new Declaration(Constr.AGENT, agentName, null, null, identifiers_and_positions);
	}
	
	public static Node groupDecl(String groupName, Set<SymbolLineColumn> identifiers_and_positions) {
		return new Declaration(Constr.GROUP, groupName, null, null, identifiers_and_positions);
	}
	
	public static Node initCond(Node expression) {
		if (expression.isExpr()) {
			return new NodeUnary(Constr.INIT, expression, null);
		}
		throw new RuntimeException("illegal constructor symbol in initCond()");
	}
	
	public static Node action(@NonNull String name, Node epre, Node pre, Vector<Node> assns) {
		return Act.action(name, epre, pre, assns);
	}
	
	public static Node doAssn(Node id, Node rhs, int line, int column) {
		return new NodeBinary(Constr.DOASSN, id, rhs, line, column);
	}
	
	public static Node falseExpr(int line, int column) {
		return new NodeAtomic(Constr.FALSE, null, line, column);
	}
	
	public static Node trueExpr(int line, int column) {
		return new NodeAtomic(Constr.TRUE, null, line, column);
	}

	public static Node trueExpr() {
		return new NodeAtomic(Constr.TRUE, null, -1, -1);
	}
	
	protected static Node trueExpr_typed() {
		Node res = new NodeAtomic(Constr.TRUE, null, -1, -1);
		res.type = Type.booleanType();
		return res;
	}
	
	public static Node signedExpr(String literal, int line, int column) {
		return new NodeAtomic(Constr.SIGNED, literal, line, column);
	}
	
	public static Node enumLiteralExpr(String symbol, int line, int column) {
		return new NodeAtomic(Constr.ENUM_LITERAL, symbol, line, column);
	}
	
	public static Node symbolicExpr(String symbol, int line, int column) {
		return new NodeAtomic(Constr.IDENTIFIER, symbol, line, column);
	}
	
	public static Node guardIdentifier(String symbol, int line, int column) {
		return new NodeAtomic(Constr.GUARDID, symbol, line, column);
	}
	
	public static Node not(Node operand) {
		return unaryExpr(Constr.NOT, operand);
	}
	
	protected static Node not_typed(Node operand) {
		Node res = unaryExpr(Constr.NOT, operand);
		res.type = Type.booleanType();
		res.isPropositional = operand.isPropositional();
		return res;
	}
	
	public static Node negation(Node operand) {
		return unaryExpr(Constr.NEG, operand);
	}
	
	/**
	 * @pre firstOperand != null && firstOperand.isExpr() &&
	 *      secondOperand != null && secondOperand.isExpr()
	 * @param firstOperand
	 * @param secondOperand
	 * @return
	 */
	public static Node and(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.AND, firstOperand, secondOperand);
	}
	
	protected static Node and_typed(Node firstOperand, Node secondOperand) {
		Node res = binaryExpr(Constr.AND, firstOperand, secondOperand);
		res.type = Type.booleanType();
		res.isPropositional = firstOperand.isPropositional() && secondOperand.isPropositional();
		return res;
	}
	
	public static Node nand(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.NAND, firstOperand, secondOperand);
	}
	
	public static Node or(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.OR, firstOperand, secondOperand);
	}
	
	protected static Node or_typed(Node firstOperand, Node secondOperand) {
		Node res = binaryExpr(Constr.OR, firstOperand, secondOperand);
		res.type = Type.booleanType();
		res.isPropositional = firstOperand.isPropositional() && secondOperand.isPropositional();
		return res;
	}
	
	public static Node nor(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.NOR, firstOperand, secondOperand);
	}
	
	public static Node exor(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.EXOR, firstOperand, secondOperand);
	}
	
	public static Node implies(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.IMPL, firstOperand, secondOperand);
	}
	
	public static Node biimplies(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.BIIMPL, firstOperand, secondOperand);
	}

	public static Node exists(Vector<Node> varDecls, Node operand) {
		return new NodeQuant(Constr.EXISTS, varDecls, operand);
	}
	
	protected static Node exists_typed(Vector<Node> varDecls, Node operand) {
		Node res = new NodeQuant(Constr.EXISTS, varDecls, operand);
		res.type = Type.booleanType();
		res.isPropositional = operand.isPropositional();
		return res;
	}
	
	public static Node forall(Vector<Node> varDecls, Node operand) {
		return new NodeQuant(Constr.FORALL, varDecls, operand);
	}
	
	protected static Node forall_typed(Vector<Node> varDecls, Node operand) {
		Node res = new NodeQuant(Constr.FORALL, varDecls, operand);
		res.type = Type.booleanType();
		res.isPropositional = operand.isPropositional();
		return res;
	}
	
	public static Node plus(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.PLUS, firstOperand, secondOperand);
	}
	
	public static Node plus_implicit(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.PLUS_IMPLICIT, firstOperand, secondOperand);
	}
	
	public static Node minus(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.MINUS, firstOperand, secondOperand);
	}
	
	public static Node times(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.TIMES, firstOperand, secondOperand);
	}
	
	public static Node div(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.DIV, firstOperand, secondOperand);
	}
	
	public static Node mod(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.MOD, firstOperand, secondOperand);
	}
	
	public static Node less(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.LESS, firstOperand, secondOperand);
	}
	
	public static Node leq(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.LEQ, firstOperand, secondOperand);
	}
	
	public static Node greater(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.GREATER, firstOperand, secondOperand);
	}
	
	public static Node geq(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.GEQ, firstOperand, secondOperand);
	}
	
	public static Node equal(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.EQUAL, firstOperand, secondOperand);
	}
	
	public static Node nequal(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.NEQUAL, firstOperand, secondOperand);
	}
	
	public static Node castMOD(Type type, Node operand) {
		return new NodeUnary(Constr.CAST_MOD, operand, type);
	}
	
	public static Node castEXT(Type type, Node operand) {
		return new NodeUnary(Constr.CAST_EXT, operand, type);
	}
	
	public static Node AX(Node operand) {
		return unaryExpr(Constr.AX, operand);
	}
	
	public static Node EX(Node operand) {
		return unaryExpr(Constr.EX, operand);
	}
	
	protected static Node EX_typed(Node operand) {
		Node res = unaryExpr(Constr.EX, operand);
		res.type = Type.booleanType();
		res.isPropositional = false;
		return res;
	}
	
	public static Node AG(Node operand) {
		return unaryExpr(Constr.AG, operand);
	}
	
	public static Node EG(Node operand) {
		return unaryExpr(Constr.EG, operand);
	}
	
	protected static Node EG_typed(Node operand) {
		Node res = unaryExpr(Constr.EG, operand);
		res.type = Type.booleanType();
		res.isPropositional = false;
		return res;
	}
	
	public static Node AF(Node operand) {
		return unaryExpr(Constr.AF, operand);
	}
	
	public static Node EF(Node operand) {
		return unaryExpr(Constr.EF, operand);
	}
	
	protected static Node EF_typed(Node operand) {
		Node res = unaryExpr(Constr.EF, operand);
		res.type = Type.booleanType();
		res.isPropositional = false;
		return res;
	}
	
	public static Node AU(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.AU, firstOperand, secondOperand);
	}
	
	public static Node EU(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.EU, firstOperand, secondOperand);
	}

	protected static Node EU_typed(Node firstOperand, Node secondOperand) {
		Node res = binaryExpr(Constr.EU, firstOperand, secondOperand);
		res.type = Type.booleanType();
		res.isPropositional = false;
		return res;
	}
	
	public static Node AW(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.AW, firstOperand, secondOperand);
	}
	
	public static Node EW(Node firstOperand, Node secondOperand) {
		return binaryExpr(Constr.EW, firstOperand, secondOperand);
	}
	
	protected static Node EW_typed(Node firstOperand, Node secondOperand) {
		Node res = binaryExpr(Constr.EW, firstOperand, secondOperand);
		res.type = Type.booleanType();
		res.isPropositional = false;
		return res;
	}
	
	public static Node K(String agentName, Node operand, int line, int column, int line_agentName, int column_agentName) {
		return new NodeEpistemic(Constr.K, agentName, operand, line, column, line_agentName, column_agentName);
	}
	
	public static Node E(String groupName, Node operand, int line, int column, int line_groupName, int column_groupName) {
		return new NodeEpistemic(Constr.E, groupName, operand, line, column, line_groupName, column_groupName);
	}
	
	public static Node D(String groupName, Node operand, int line, int column, int line_groupName, int column_groupName) {
		return new NodeEpistemic(Constr.D, groupName, operand, line, column, line_groupName, column_groupName);
	}
	
	public static Node C(String groupName, Node operand, int line, int column, int line_groupName, int column_groupName) {
		return new NodeEpistemic(Constr.C, groupName, operand, line, column, line_groupName, column_groupName);
	}
	
	private static Node unaryExpr(Constr constr, Node node) {
		if (constr.isExpr() && node.isExpr()) {
			return new NodeUnary(constr, node, null);
		}
		else {
			throw new RuntimeException("malformed operand or constructor");
		}
	}
	
	private static Node binaryExpr(Constr constr, Node firstNode, Node secondNode) {
		if (constr.isExpr() &&
			firstNode.isExpr() &&
			secondNode.isExpr()) {
			return new NodeBinary(constr, firstNode, secondNode);
		}
		else {
			throw new RuntimeException("malformed operand or constructor");
		}
	}
	
    /**
     * Adds a var-declaration. 
     * @param varDecl the var-declaration to be added.
     */
	public void addVarDecl(Node varDecl) {
		throw new IllegalArgumentException();
	}

    /**
     * Adds a let-declaration.
     * @param letDecl the let-declaration to be added.
     */
	public void addLetDecl(Node letDecl) {
		throw new IllegalArgumentException();
	}
    
    /**
     * Adds a guard-declaration.
     * @param guardDecl the guard-declaration to be added.
     */
    public void addGuardDecl(Node guardDecl) {
    	throw new IllegalArgumentException();
    }
    
    /**
     * Adds a agent-declaration.
     * @param agentDecl the agent-declaration to be added.
     */
    public void addAgentDecl(Node agentDecl) {
    	throw new IllegalArgumentException();
    }
    
    /**
     * Adds a group-declaration.
     * @param groupDecl the group-declaration to be added.
     */
    public void addGroupDecl(Node groupDecl) {
    	throw new IllegalArgumentException();
    }
    
    /**
     * Adds an initial condition.
     * @param initCond the initial condition to be added.
     */
    public void addInitCond(Node initCond) {
    	throw new IllegalArgumentException();
    }
    
    /**
     * Adds an action node.
     * @param action the action node to be added.
     */
    public void addAction(Node action) {
    	throw new IllegalArgumentException();
    }
    
    public void setCommand(Node statement) {
    	throw new IllegalArgumentException();
    }
    
	String getSymbol() {
		throw new IllegalArgumentException();
	}
	
	int getLineSymbol() {
		throw new IllegalArgumentException();
	}
	
	int getColumnSymbol() {
		throw new IllegalArgumentException();
	}
	
	Type getSymbolType() {
		throw new IllegalArgumentException();
	}
    
	/**
	 * Checks the type-correctness of this syntactic object according to typing rules.
	 * @param typeEnv a type-environment determining the types of symbolic expressions.
	 * @return the type of this syntactic object, if the object is type-correct.
	 * @throws TypingException if the syntactic object violates a typing rule or
	 * contains undeclared symbols or illegal redeclarations.
	 */
	@NonNullByDefault
	public abstract Type check(SymbolTable<Identifier, Type> typeEnv)
		throws TypingException;
	
	/**
	 * Returns a vector of all var-declared variables in order of
	 * their declarations in the source file. 
	 * @return a vector of all var-declared variables in order of
	 * their declarations in the source file. 
	 */
	public Vector<String> getVariables() {
		throw new IllegalArgumentException();
	}
	
    /**
     * Evaluates this expression to a BDDWB (WB: with additional bdd indicating
     * points at which the represented function is undefined) w.r.t. a given
     * SymbolicValueEnvironment, if this expression is a state formula.
     * @param env an environment of symbolic values (BDDs or MultiBDDs)
     * @return a BDDWB containing the canonical BDD that represents this expression
     * w.r.t. the given environment, if this expression is a state formula.
     */ 
	public BDDWB evaluateSFBool(SValueEnvironment env) {
		throw new IllegalArgumentException();
	}
	
	public MultiBDD evaluateSFInt(SValueEnvironment env) {
		throw new IllegalArgumentException();
	}
	
	public BDD evaluateBool(KripkeStructure struct) {
		throw new IllegalArgumentException();
	}
	
	public MultiBDD evaluateInt(KripkeStructure struct) {
		throw new IllegalArgumentException();
	}
	
	public BDD evaluatePositive(PNStructure pnstruct, boolean rule1) {
		throw new IllegalArgumentException();
	}
	
	public BDD evaluateNegative(PNStructure pnstruct, boolean rule1) {
		throw new IllegalArgumentException();
	}
	
	/**
	 * Enact a let-, agent-, or group-declaration, if this node is a let-, agent-,
	 * or group-declaration node.
	 * @param sm the symbol manager where the declaration is to be enacted.
	 */
	public void enact(SymbolManager sm) {
		throw new IllegalArgumentException();
	}
	
	/**
	 * Enact a declaration of an initial condition or an action,
	 * if this node represents such a declaration.
	 * @param spec the instance of Specification in which the declaration is to be enacted.
	 */
	public void enact(Specification spec) {
		throw new IllegalArgumentException();
	}
	
	/**
	 * Enact an assignment node (of the do-part of an action declaration),
	 * if this node is an assignment node.
	 * @param env an environment of symbolic values (BDDs or MultiBDDs)
	 * @param rhsBuffer a mapping that buffers the new values of the primed symbols.
	 * The string "error'" is mapped to the bdd that represents the set S of all states
	 * in which the expression on the right hand side of the assignment operator 
	 * 1) is undefined or 2) evaluates to an int-value that does not belong to the
	 * int-type of the symbol on the left hand side, if S is not the empty set;
	 * null, if S is the empty set.
	 */
	public void enact(SValueEnvironment env, Map<String,BDD> rhsBuffer) {
		throw new IllegalArgumentException();
	}
	
    /**
     * Produces a run of the given KS that satisfies this formula.
     * Throws an exception 'NoCounterExample' if such a run cannot be computed
     * because this formula can conceptually not be satisfied by a single run
     * (e.g., "A"-type formula).
     *
     * Precondition: This formula must be satisfiable w.r.t. the given KS.
     *
     * @param struct a Kripke structure (KS).
     * @return a run of the given KS that satisfies this formula, 
     *         if this formula can conceptually satisfied by a single run.
     * @exception GenerationFailed if this formula can conceptually not be satisfied by a single run.
     **/
    Run getExample(KripkeStructure struct) throws GenerationFailed {
        return getExample(struct.getInitialCondition(), struct);
    }
    
    /**
     * Produces a run of a Kripke structure M that satisfies this formula.
     * The transition relation of M is the transition relation of <tt>struct</tt>,
     * whereas the set of initial states of M is given by <tt>start</tt>.
     * Throws an exception 'NoCounterExample' if such a run cannot be computed
     * because this formula can conceptually not be satisfied by a single run
     * (e.g., "A"-type formula).
     *
     * Precondition: This formula must be satisfiable w.r.t. M.
     *
     * @param start a bdd that represents a set of initial states
     * @param struct a Kripke structure
     * @return a run of the Kripke structure M that satisfies this formula,
     *         if this formula can conceptually satisfied by a single run.
     *         The transition relation of M is the transition relation of <tt>struct</tt>,
     *         whereas the set of initial states of M is given by <tt>start</tt>.
     * @exception GenerationFailed if this formula can conceptually not be satisfied by a single run.
     **/
    Run getExample(BDD start, KripkeStructure struct) throws GenerationFailed {
        throw new IllegalArgumentException();
    }

    /**
     * Produces a run of the given KS that falsifies this formula (i.e., a counterexample).
     * Throws an exception 'NoCounterExample' if such a counterexample cannot be computed
     * because this formula can conceptually not be falsified by a single run
     * (e.g., "E"-type formula).
     *
     * Precondition: This formula must be falsifiable w.r.t. the given KS.
     *
     * @param struct a Kripke structure
     * @return a run of the given KS that falsifies this formula,
     *         if this formula can conceptually be falsified by a single run.
     * @exception GenerationFailed if this formula can conceptually not be falsified by a single run.
     **/
    public Run getCounterExample(KripkeStructure struct) throws GenerationFailed {
        return getCounterExample(struct.getInitialCondition(), struct);
    }
    
    /**
     * Produces a run of a Kripke structure M that falsifies this formula
     * (i.e., a counterexample). The transition relation of M is the transition relation
     * of <tt>struct</tt>, whereas the set of initial states of M is given by <tt>start</tt>.
     * Throws an exception 'NoCounterExample' if such a counterexample cannot be computed
     * because this formula can conceptually not be falsified by a single run
     * (e.g., "E"-type formula).
     *
     * Precondition: This formula must be falsifiable w.r.t. M.
     *
     * @param start a bdd that represents a set of initial states.
     * @param struct a Kripke structure.
     * @return a run of the Kripke structure M that falsifies this formula,
     *         if this formula can conceptually be falsified by a single run.
     *         The transition relation of M is the transition relation of <tt>struct</tt>,
     *         whereas the set of initial states of M is given by <tt>start</tt>.
     * @exception GenerationFailed if this formula can conceptually not be falsified by a single run.
     **/
    Run getCounterExample(BDD start, KripkeStructure struct) throws GenerationFailed {
      throw new IllegalArgumentException();
    }
    
	/**
	 * If this node represents an expression E: the method isPropositional() returns
	 * true, iff E is purely propositional (or bounded first-order), i.e. contains
	 * neither temporal nor epistemic modalities. If this node is not an expression,
	 * the return value has no meaning. 
	 */
	public boolean isPropositional() {
		return isPropositional;
	}
}
