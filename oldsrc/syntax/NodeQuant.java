package syntax;

import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;
import bdd.VariableRangeSet;
import core.GenerationFailed;
import core.KripkeStructure;
import core.PNStructure;
import core.Run;
import core.Specification;
import core.SymbolTable;
import core.SValueEnvironment;
import core.Temic;
import core.Type;
import util.Formatter;

import java.util.Vector;
import java.util.HashSet;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;


/**
 * The root class of all exists- oder forall-quantified expressions.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-10
 */
class NodeQuant extends NodeUnary {
	private Vector<Node> varDecls;
	/** the logger for instances of class Node */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Temic.class.getName());
	
	NodeQuant(Constr constr, Vector<Node> varDecls, Node child) {
		super(constr, child, null);
		this.varDecls = varDecls;
	}
	
	public String toString() {
		switch(constr) {
		case EXISTS:
		case FORALL:
			return constr.symbol() + " " + concat(varDecls) + " . " + child;
		default:
			throw new RuntimeException("illegal constructor symbol '" + constr + "' in toString()");
		}
	}

	private static String concat(Vector<Node> varDecls) {
		StringBuffer ret = new StringBuffer();
		int size = varDecls.size();
		for (int i=0; i<size; i++) {
			ret.append("" + varDecls.get(i));
			if (i+1<size) { ret.append(", "); }
		}
		return "" + ret;
	}
	
	@Override
	public Type check(SymbolTable<syntax.Identifier, Type> typeEnv)
	throws TypingException {
		type = null;
		if (constr != Constr.EXISTS && constr != Constr.FORALL) {
			throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
		}
		HashSet<String> quantifiedVars = new HashSet<String>();
		for (Node node : varDecls) {
			if (node.constr != Constr.VAR) {
				throw new RuntimeException("malformed local var-declaration in check()");
			}
			if (quantifiedVars.contains(node.getSymbol())) {
				throw new TypingException("Illegal re-declaration of symbol " + Formatter.quoted(node.getSymbol()), line, column);
			}
			quantifiedVars.add(node.getSymbol());
			typeEnv.push(Identifier.variable(node.getSymbol()), node.getSymbolType());
		}
		if (!child.check(typeEnv).isBoolean()) {
			throw new TypingException(constr, child.getType(), line, column);
		}
		for (Node node : varDecls) {
			typeEnv.pop(Identifier.variable(node.getSymbol()));
		}
		type = Type.booleanType(); isPropositional = child.isPropositional();
		return type;
	}
	
    /**
     * Evaluates this expression to a BDDWB (WB: with additional bdd indicating
     * points at which the represented function is undefined) w.r.t. a given
     * SymbolicValueEnvironment, if this expression is a state formula.
     * @param env an environment of symbolic values (BDDs or MultiBDDs)
     * @return a BDDWB containing the canonical BDD that represents this expression
     * w.r.t. the given environment, if this expression is a state formula.
     */
	public BDDWB evaluateSFBool(SValueEnvironment env) {
		// first check if the quantification is trivial
		for (Node node : varDecls) {
			if (node.getSymbolType().isEmpty()) {
				switch(constr) {
					case EXISTS: return new BDDWB(BDD.falseBDD(), BDD.falseBDD());
					case FORALL: return new BDDWB(BDD.trueBDD(), BDD.falseBDD());
					default: throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "' in evaluateSFBool()");
				}
			}
		}
		
		VariableRangeSet range = new VariableRangeSet();
		BDD encodingConstraint = BDD.trueBDD();
		for (Node node : varDecls) {
			Vector<Integer> mindex = env.declareLocalVariable(node.getSymbol(), node.getSymbolType());
			BDD ecConjunct = MultiBDD.leq(MultiBDD.varMultiBDD(mindex, 0), MultiBDD.intMultiBDD(node.getSymbolType().encodingUpperBound()));
			encodingConstraint = BDD.and(encodingConstraint, ecConjunct);
			for (int i=0; i<mindex.size(); i++) {
				range.add(mindex.get(i));
			}	
		}
		BDDWB childBDDWB = child.evaluateSFBool(env);
		BDD childBDD = childBDDWB.getBDD();
		BDD childUndef = childBDDWB.undef();
		BDDWB result;
		switch(constr) {
			case EXISTS:
				result = new BDDWB(BDD.exists(BDD.and(encodingConstraint, childBDD), range), childUndef);
				break;
			case FORALL:
				result = new BDDWB(BDD.forall(BDD.implies(encodingConstraint, childBDD), range), childUndef);
				break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateSFBool()");
		}
		for (int i=varDecls.size()-1; 0<=i; i--) {
			env.undeclareLocalVariable(varDecls.get(i).getSymbol());
		}
		return result;
	}
	
	public BDD evaluateBool(KripkeStructure struct) {
		// first check if the quantification is trivial
		for (Node node : varDecls) {
			if (node.getSymbolType().isEmpty()) {
				switch(constr) {
					case EXISTS: return BDD.falseBDD();
					case FORALL: return BDD.trueBDD();
					default: throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "' in evaluateBool()");
				}
			}
		}
		
		VariableRangeSet range = new VariableRangeSet();
		BDD encodingConstraint = BDD.trueBDD();
		for (Node node : varDecls) {
			Vector<Integer> mindex = struct.declareLocalVariable(node.getSymbol(), node.getSymbolType());
			BDD ecConjunct = MultiBDD.leq(MultiBDD.varMultiBDD(mindex, 0), MultiBDD.intMultiBDD(node.getSymbolType().encodingUpperBound()));
			encodingConstraint = BDD.and(encodingConstraint, ecConjunct);
			for (int i=0; i<mindex.size(); i++) {
				range.add(mindex.get(i));
			}	
		}
		BDD childBDD = child.evaluateBool(struct);
		BDD result;
		switch(constr) {
			case EXISTS:
				result = BDD.exists(BDD.and(encodingConstraint, childBDD), range);
				break;
			case FORALL:
				result = BDD.forall(BDD.implies(encodingConstraint, childBDD), range);
				break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateBool()");
		}
		for (int i=varDecls.size()-1; 0<=i; i--) {
			struct.undeclareLocalVariable(varDecls.get(i).getSymbol());
		}
		return result;
	}
	
	public BDD evaluatePositive(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		VariableRangeSet range = new VariableRangeSet();
		BDD encodingConstraint = BDD.trueBDD();
		for (Node node : varDecls) {
			Vector<Integer> mindex = spec.sm().declareLocalVariable(node.getSymbol(), node.getSymbolType());
			BDD ecConjunct = MultiBDD.leq(MultiBDD.varMultiBDD(mindex, 0), MultiBDD.intMultiBDD(node.getSymbolType().encodingUpperBound()));
			encodingConstraint = BDD.and(encodingConstraint, ecConjunct);
			for (int i=0; i<mindex.size(); i++) {
				range.add(mindex.get(i));
			}	
		}
		BDD childBDD = child.evaluatePositive(pnstruct, rule1);
		BDD result;
		switch(constr) {
			case EXISTS:
				result = BDD.exists(BDD.and(encodingConstraint, childBDD), range);
				break;
			case FORALL:
				result = BDD.forall(BDD.implies(encodingConstraint, childBDD), range);
				break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluatePositive()");
		}
		for (int i=varDecls.size()-1; 0<=i; i--) {
			spec.sm().undeclareLocalVariable(varDecls.get(i).getSymbol());
		}
		return result;
	}
	
	public BDD evaluateNegative(PNStructure pnstruct, boolean rule1) {
		switch(constr) {
			case EXISTS:
				Node altFormula = Node.forall_typed(varDecls, Node.not_typed(child));
				return altFormula.evaluatePositive(pnstruct, rule1);
			case FORALL:
				altFormula = Node.exists_typed(varDecls, Node.not_typed(child));
				return altFormula.evaluatePositive(pnstruct, rule1);
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" +
					" in evaluateNegative()");
		}
	}
	
	Run getExample(BDD start, KripkeStructure struct) throws GenerationFailed {
	  throw new GenerationFailed("Not yet implemented");
	}
	
	Run getCounterExample(BDD start, KripkeStructure struct) throws GenerationFailed {
	  throw new GenerationFailed("Not yet implemented");
	}
}
