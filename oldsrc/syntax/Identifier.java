package syntax;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import core.SymbolTable;
import core.Type;

import static util.Formatter.quoted;


@NonNullByDefault
public class Identifier extends Node {
  /**
   * Identifier kind.
   * 
   * @author H. Muehlberger
   * @version 1.0
   * @since 2011-04-05
   */
  private enum Kind {
    VARIABLE,
    CONSTANT,
    GUARD,
    AGENT,
    GROUP,
    ACTION
  }

  private Kind kind;
  private String name;

  private Identifier(Kind kind, String name) {
    this.kind = kind;
    this.name = name;
  }

  public static Identifier variable(String name) {
    return new Identifier(Kind.VARIABLE, name);
  }

  public static Identifier constant(String name) {
    return new Identifier(Kind.CONSTANT, name);
  }

  public static Identifier guard(String name) {
    return new Identifier(Kind.GUARD, name);
  }

  public static Identifier agent(String name) {
    return new Identifier(Kind.AGENT, name);
  }

  public static Identifier group(String name) {
    return new Identifier(Kind.GROUP, name);
  }

  public static Identifier action(String name) {
    return new Identifier(Kind.ACTION, name);
  }

  public String getName() {
    return this.name;
  }

  @Override
  public Type check(SymbolTable<Identifier, Type> typeEnv) throws TypingException {
    Type type = typeEnv.top(this);
    if (type != null)
      return type;
    throw new TypingException("Undeclared symbol " + quoted(this.name), this.getLine(), this.getColumn());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind, this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      Identifier other = (Identifier)object;
      return this.kind == other.kind &&
             this.name.equals(other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.kind.toString());
    builder.append("::");
    builder.append(this.name);
    return builder.toString();
  }
}
