package syntax;

import org.eclipse.jdt.annotation.NonNullByDefault;

import core.Type;

import static util.Formatter.quoted;


@NonNullByDefault
public class TypingException extends Exception {
  private static final long serialVersionUID = 1L;

  private static String lineColumn(int line, int column) {
    return " (line " + line + ", column " + column + ")";
  }

  public TypingException(String message, int line, int column) {
    super(message + lineColumn(line, column));
  }
	
  public TypingException(String operator, Type type, int line, int column) {
    super("The operator " + quoted(operator) + " cannot be applied to an operand of type " + type + lineColumn(line, column));
  }
	
  public TypingException(Constr operator, Type type, int line, int column) {
    super("The operator " + quoted(operator.symbol()) + " cannot be applied to an operand of type " + type + lineColumn(line, column));
  }
	
  public TypingException(Constr operator, Type firstType, Type secondType, int line, int column) {
    super("The operator " + quoted(operator.symbol()) + " cannot be applied to operands of type " + firstType + " and " + secondType + lineColumn(line, column));
  }
}
