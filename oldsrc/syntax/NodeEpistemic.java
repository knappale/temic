package syntax;

import java.util.logging.Logger;

import bdd.BDD;
import core.GenerationFailed;
import core.KripkeStructure;
import core.PNStructure;
import core.Run;
import core.Specification;
import core.SymbolTable;
import core.Temic;
import core.Type;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static util.Formatter.quoted;


/**
 * The root class of all expressions with an epistemic modality
 * as its outermost constructor symbol.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-19
 */
class NodeEpistemic extends NodeUnary {
	/** the agent name, if constr is K; the group name, if constr is E, D, or C. */
	private String name;
	/** the line number of the name */
	private int line_name;
	/** the column number of the name */
	private int column_name;
	/** the logger for instances of class Node */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Temic.class.getName());

	NodeEpistemic(Constr constr, String name, Node child, int line, int column, int line_name, int column_name) {
		super(constr, child, null);
		this.name = name;
		this.line = line;
		this.column = column;
		this.line_name = line_name;
		this.column_name = column_name;
	}
	
	/**
     * Returns a string representation of this syntactic object.
     * @return a string representation of this syntactic object.
     **/
    public String toString() {
    	switch(constr) {
    		case K:
    		case E:
    		case D:
    		case C:
    			return constr.symbol() + "[" + name + "] " + child;
    		default:
    			throw new RuntimeException("illegal constructor symbol '" + constr + "' in toString()");
    	}
    }
    
    @Override
    @NonNullByDefault
    public Type check(SymbolTable<syntax.Identifier, Type> typeEnv) throws TypingException {
    	type = null;
		Type typeChild = child.check(typeEnv);
		switch(constr) {
			case K:
				if (typeEnv.top(Identifier.agent(name)) == null) {
					throw new TypingException("Undeclared symbol " + quoted(name), line_name, column_name);
				}
				if (!typeChild.isBoolean()) {
					throw new TypingException(constr, typeChild, line, column);
				}
				type = Type.booleanType(); isPropositional = false; break;
			case E:
			case D:
			case C:
				if (typeEnv.top(Identifier.group(name)) == null) {
					throw new TypingException("Undeclared symbol " + quoted(name),line_name,column_name);
				}
				if (!typeChild.isBoolean()) {
					throw new TypingException(constr, typeChild, line, column);
				}
				type = Type.booleanType(); isPropositional = false; break;
			default :
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
		}
		return type;
    }
	
    
    /**
     * Evaluates this CTLK formula to a BDD w.r.t. a given KS.
     * @param model a Kripke structure.
     * @return the canonical bdd that represents the set of all states which
     *         satisfy this CTLK formula w.r.t. the given KS.
     **/
	public BDD evaluateBool(KripkeStructure struct) {
		switch(constr) {
			case K:
				BDD X = BDD.and(BDD.not(child.evaluateBool(struct)), struct.reachableStates());
				BDD Y = struct.preImageRelationK(name, X);
				return BDD.and(BDD.not(Y), struct.reachableStates());
			case E:
				X = BDD.and(BDD.not(child.evaluateBool(struct)), struct.reachableStates());
				Y = struct.preImageRelationE(name, X);
				return BDD.and(BDD.not(Y), struct.reachableStates());
			case D:
				X = BDD.and(BDD.not(child.evaluateBool(struct)), struct.reachableStates());
				Y = struct.preImageRelationD(name, X);
				return BDD.and(BDD.not(Y), struct.reachableStates());
			case C:
				/* BDD X = BDD.not(getOperand().evaluate(model));
				BDD Y = model.leastFixedpointC(groupName,X);
				return BDD.and(model.reachableStates(), BDD.not(Y)); */
				throw new RuntimeException("evaluation of '" + constr.symbol() + "' not implemented yet");
			default :
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in evaluateBool()");
		}
	}
	
	public BDD evaluatePositive(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		switch(constr) {
			case K:
				BDD X = BDD.and(pnstruct.getNc(), BDD.not(child.evaluatePositive(pnstruct, rule1)));
				BDD Y = spec.preImageRelationK(name, X);
				return BDD.and(BDD.not(Y), spec.sm().stateSpaceBDD());
			case E:
				X = BDD.and(pnstruct.getNc(), BDD.not(child.evaluatePositive(pnstruct, rule1)));
				Y = spec.preImageRelationE(name, X);
				return BDD.and(BDD.not(Y), spec.sm().stateSpaceBDD());
			case D:
				X = BDD.and(pnstruct.getNc(), BDD.not(child.evaluatePositive(pnstruct, rule1)));
				Y = spec.preImageRelationD(name, X);
				return BDD.and(BDD.not(Y), spec.sm().stateSpaceBDD());
			case C:
				throw new RuntimeException("evaluation of '" + constr.symbol() + "' not implemented yet");
			default :
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in evaluatePositive()");
		}
	}
	
	public BDD evaluateNegative(PNStructure pnstruct, boolean rule1) {
		Specification spec = pnstruct.specification();
		switch(constr) {
			case K:
				BDD Z = child.evaluateNegative(pnstruct, rule1);
				BDD X = BDD.and(pnstruct.getP(), Z);
				BDD Y = spec.preImageRelationK(name, X);
				BDD res = BDD.and(Y, spec.sm().stateSpaceBDD());
				// additional rule no 1
				if (rule1) {
					X = BDD.and(pnstruct.getNc(), BDD.not(Z));
					Y = spec.preImageRelationK(name, X);
					res = BDD.or(res, BDD.and(BDD.not(Y), spec.sm().stateSpaceBDD()));
				}
				return res; 
			case E:
				X = BDD.and(pnstruct.getP(), child.evaluateNegative(pnstruct, rule1));
				Y = spec.preImageRelationE(name, X);
				return BDD.and(Y, spec.sm().stateSpaceBDD());
			case D:
				X = BDD.and(pnstruct.getP(), child.evaluateNegative(pnstruct, rule1));
				Y = spec.preImageRelationD(name, X);
				return BDD.and(Y, spec.sm().stateSpaceBDD());
			case C:
				throw new RuntimeException("evaluation of '" + constr.symbol() + "' not implemented yet");
			default :
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in evaluateNegative()");
		}
	}
	
	Run getExample(BDD start, KripkeStructure struct) throws GenerationFailed {
		throw new GenerationFailed("Examples for epistimic formulae not implemented yet");
	}
	
	Run getCounterExample(BDD start, KripkeStructure struct) throws GenerationFailed {
		throw new GenerationFailed("Counter-examples for epistimic formulae not implemented yet");
	}
}
