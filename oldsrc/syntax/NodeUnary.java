package syntax;

import java.util.logging.Logger;
import java.util.Iterator;

import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;
import core.GenerationFailed;
import core.KripkeStructure;
import core.PNStructure;
import core.Run;
import core.Step;
import core.SymbolTable;
import core.SValueEnvironment;
import core.Specification;
import core.Temic;
import core.Type;
import syntax.generated.TemicParser;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * The root class of all syntactic objects with a unary uppermost constructor
 * symbol.
 * 
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-09
 */
class NodeUnary extends Node {
  /** the syntactic object which the constructor symbol is applied to */
  protected Node child;
  /** the type in which the child is to be casted, if this node is a cast */
  protected Type castType;
  /** the logger for instances of class NodeUnary */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(Temic.class.getName());

  /**
   * Applies the constructor symbol to <tt>child</tt>.
   * 
   * @param constr the constructor symbol as a literal of enum Constr
   * @param child the syntactic object which the constructor symbol is applied to
   * @param castType the type in which the child is to be casted, if this node is a cast
   */
  NodeUnary(Constr constr, Node child, Type castType) {
    super(constr);
    this.child = child;
    this.castType = castType;
  }

  @Override
  @NonNullByDefault
  public Type check(SymbolTable<syntax.Identifier, Type> typeEnv) throws TypingException {
    Type typeChild = child.check(typeEnv);
    switch (constr) {
      case NOT:
        if (typeChild.isBoolean()) {
          var type = Type.booleanType();
          isPropositional = child.isPropositional();
          return this.type = type;
        }
        throw new TypingException(constr, typeChild, line, column);
      case AX:
      case EX:
      case AG:
      case EG:
      case AF:
      case EF:
        if (typeChild.isBoolean()) {
          var type = Type.booleanType();
          isPropositional = false;
          return this.type = type;
        }
        throw new TypingException(constr, typeChild, line, column);
      case NEG:
        if (typeChild.isInteger()) {
          var type = Type.integerType(-typeChild.upperBound(), -typeChild.lowerBound());
          isPropositional = child.isPropositional();
          return this.type = type;
        }
        throw new TypingException(constr, typeChild, line, column);
      case INIT:
        if (typeChild.isBoolean()) {
          var type = Type.voidType();
          return this.type = type;
        }
        throw new TypingException(constr, typeChild, line, column);
      case CAST_MOD:
      case CAST_EXT:
        if (castType.isBoolean()) {
          if (typeChild.isBoolean() || typeChild.isInteger()) {
            var type = castType;
            isPropositional = child.isPropositional();
            return this.type = type;
          }
          throw new TypingException("expressions of type " + typeChild + " cannot be casted", line, column);
        } else
        if (castType.isInteger()) {
          if (castType.isEmpty())
            throw new TypingException("casting into empty int-types not allowed.", line, column);
          if (typeChild.isBoolean()) {
            if (Type.integerType(0, 1).subsets(castType)) {
              var type = castType;
              isPropositional = child.isPropositional();
              return this.type = type;
            }
            throw new TypingException("If a boolean expression is casted into an int-type, the latter has to contain 0 and 1.", line, column);
          } else
          if (typeChild.isInteger()) {
            var type = castType;
            isPropositional = child.isPropositional();
            return this.type = type;
          }
          throw new TypingException("expressions of type " + typeChild + " cannot be casted", line, column);
        } else
        if (castType.isEnum() || castType.isVoid())
          throw new TypingException("casting into type " + castType + " not allowed.", line, column);
        else
          throw new RuntimeException("unknown type " + typeChild + " in check(), case CAST");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
    }
  }

  /**
   * Evaluates this expression to a BDDWB (WB: with additional bdd indicating
   * points at which the represented function is undefined) w.r.t. a given
   * SymbolicValueEnvironment, if this expression is a state formula.
   * 
   * @param env
   *          an environment of symbolic values (BDDs or MultiBDDs)
   * @return a BDDWB containing the canonical BDD that represents this
   *         expression w.r.t. the given environment, if this expression is a
   *         state formula.
   */
  public BDDWB evaluateSFBool(SValueEnvironment env) {
    switch (constr) {
      case NOT:
        BDDWB childBDDWB = child.evaluateSFBool(env);
        return new BDDWB(BDD.not(childBDDWB.getBDD()), childBDDWB.undef());
      case CAST_MOD:
      case CAST_EXT:
        if (child.type.isBoolean())
          return child.evaluateSFBool(env);
        if (child.type.isInteger()) {
            MultiBDD childMultiBDD = child.evaluateSFInt(env);
            BDD value = BDD.not(MultiBDD.equal(childMultiBDD, MultiBDD.intMultiBDD(0)));
            return new BDDWB(value, childMultiBDD.undef());
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateSFBool()");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" + " in evaluateSFBool()");
    }
  }

  public MultiBDD evaluateSFInt(SValueEnvironment env) {
    switch (constr) {
      case NEG:
        MultiBDD childMultiBDD = child.evaluateSFInt(env);
        MultiBDD result = MultiBDD.negate(childMultiBDD);
        result.setUndef(childMultiBDD.undef());
        return result;
      case CAST_MOD:
        if (child.type.isBoolean()) {
            BDDWB childBDDWB = child.evaluateSFBool(env);
            result = MultiBDD.ite(childBDDWB.getBDD(), MultiBDD.intMultiBDD(1), MultiBDD.intMultiBDD(0));
            result.setUndef(childBDDWB.undef());
            return result;
        }
        if (child.type.isInteger()) {
            childMultiBDD = child.evaluateSFInt(env);
            MultiBDD lowerBoundMultiBDD = MultiBDD.intMultiBDD(castType.lowerBound());
            result = MultiBDD.math_mod(MultiBDD.sub(childMultiBDD, lowerBoundMultiBDD),
                MultiBDD.intMultiBDD(1 + castType.upperBound() - castType.lowerBound()));
            result = MultiBDD.add(result, lowerBoundMultiBDD);
            result.setUndef(childMultiBDD.undef());
            return result;
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateSFInt()");
      case CAST_EXT:
        if (child.type.isBoolean()) {
          BDDWB childBDDWB = child.evaluateSFBool(env);
          result = MultiBDD.ite(childBDDWB.getBDD(), MultiBDD.intMultiBDD(1), MultiBDD.intMultiBDD(0));
          result.setUndef(childBDDWB.undef());
          return result;
        }
        if (child.type.isInteger()) {
          childMultiBDD = child.evaluateSFInt(env);
          MultiBDD lowerBoundMultiBDD = MultiBDD.intMultiBDD(castType.lowerBound());
          MultiBDD upperBoundMultiBDD = MultiBDD.intMultiBDD(castType.upperBound());
          result = MultiBDD.ite(MultiBDD.less(childMultiBDD, lowerBoundMultiBDD), lowerBoundMultiBDD,
                       MultiBDD.ite(MultiBDD.less(upperBoundMultiBDD, childMultiBDD), upperBoundMultiBDD, childMultiBDD));
          result.setUndef(childMultiBDD.undef());
          return result;
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateSFInt()");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" + " in evaluateSFInt()");
    }
  }

  public BDD evaluateBool(KripkeStructure struct) {
    switch (constr) {
      case NOT:
        return BDD.not(child.evaluateBool(struct));
      case AX:
        Node equivFormula = Node.not_typed(Node.EX_typed(Node.not_typed(child)));
        return equivFormula.evaluateBool(struct);
      case EX:
        return struct.preImageTemporal(child.evaluateBool(struct));
      case AG:
        equivFormula = Node.not_typed(Node.EF_typed(Node.not_typed(child)));
        return equivFormula.evaluateBool(struct);
      case EG:
        return struct.greatestFixedpointA(child.evaluateBool(struct));
      case AF:
        equivFormula = Node.not_typed(Node.EG_typed(Node.not_typed(child)));
        return equivFormula.evaluateBool(struct);
      case EF:
        equivFormula = Node.EU_typed(Node.trueExpr_typed(), child);
        return equivFormula.evaluateBool(struct);
      case CAST_MOD:
      case CAST_EXT:
        if (child.type.isBoolean())
          return child.evaluateBool(struct);
        if (child.type.isInteger()) {
          MultiBDD childMultiBDD = child.evaluateInt(struct);
          return BDD.or(BDD.not(MultiBDD.equal(childMultiBDD, MultiBDD.intMultiBDD(0))), childMultiBDD.undef());
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateBool()");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" + " in evaluateBool()");
    }
  }

  public MultiBDD evaluateInt(KripkeStructure struct) {
    switch (constr) {
      case NEG:
        MultiBDD childMultiBDD = child.evaluateInt(struct);
        MultiBDD result = MultiBDD.negate(childMultiBDD);
        result.setUndef(childMultiBDD.undef());
        return result;
      case CAST_MOD:
        if (child.type.isBoolean()) {
          BDD childBDD = child.evaluateBool(struct);
          return MultiBDD.ite(childBDD, MultiBDD.intMultiBDD(1), MultiBDD.intMultiBDD(0));
        }
        if (child.type.isInteger()) {
          childMultiBDD = child.evaluateInt(struct);
          MultiBDD lowerBoundMultiBDD = MultiBDD.intMultiBDD(castType.lowerBound());
          result = MultiBDD.math_mod(MultiBDD.sub(childMultiBDD, lowerBoundMultiBDD),
                       MultiBDD.intMultiBDD(1 + castType.upperBound() - castType.lowerBound()));
          result = MultiBDD.add(result, lowerBoundMultiBDD);
          result.setUndef(childMultiBDD.undef());
          return result;
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateInt()");
      case CAST_EXT:
        if (child.type.isBoolean()) {
          BDD childBDD = child.evaluateBool(struct);
          return MultiBDD.ite(childBDD, MultiBDD.intMultiBDD(1), MultiBDD.intMultiBDD(0));
        }
        if (child.type.isInteger()) {
          childMultiBDD = child.evaluateInt(struct);
          MultiBDD lowerBoundMultiBDD = MultiBDD.intMultiBDD(castType.lowerBound());
          MultiBDD upperBoundMultiBDD = MultiBDD.intMultiBDD(castType.upperBound());
          result = MultiBDD.ite(MultiBDD.less(childMultiBDD, lowerBoundMultiBDD), lowerBoundMultiBDD,
                       MultiBDD.ite(MultiBDD.less(upperBoundMultiBDD, childMultiBDD), upperBoundMultiBDD, childMultiBDD));
          result.setUndef(childMultiBDD.undef());
          return result;
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateInt()");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" + " in evaluateInt()");
    }
  }

  public BDD evaluatePositive(PNStructure pnstruct, boolean rule1) {
    Specification spec = pnstruct.specification();
    switch (constr) {
      case NOT:
        return child.evaluateNegative(pnstruct, rule1);
      case CAST_MOD:
      case CAST_EXT:
        if (child.type.isBoolean())
          return child.evaluatePositive(pnstruct, rule1);
        if (child.type.isInteger()) {
          MultiBDD childMultiBDD = child.evaluateSFInt(spec.sm());
          BDD value = BDD.not(MultiBDD.equal(childMultiBDD, MultiBDD.intMultiBDD(0)));
          return BDD.or(value, childMultiBDD.undef());
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluatePositive()");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "' in evaluatePositive()");
    }
  }

  public BDD evaluateNegative(PNStructure pnstruct, boolean rule1) {
    Specification spec = pnstruct.specification();
    switch (constr) {
      case NOT:
        return child.evaluatePositive(pnstruct, rule1);
      case CAST_MOD:
      case CAST_EXT:
        if (child.type.isBoolean())
          return child.evaluateNegative(pnstruct, rule1);
        if (child.type.isInteger()) {
          MultiBDD childMultiBDD = child.evaluateSFInt(spec.sm());
          BDD valueNegated = MultiBDD.equal(childMultiBDD, MultiBDD.intMultiBDD(0));
          return BDD.and(valueNegated, BDD.not(childMultiBDD.undef()));
        }
        throw new RuntimeException("illegal child type " + child.type + " in evaluateNegative()");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "' in evaluateNegative()");
    }
  }

  /**
   * Enact a declaration of an initial condition.
   * 
   * @param spec
   *          the instance of Specification in which the declaration is to be
   *          enacted.
   */
  public void enact(Specification spec) {
    switch (constr) {
      case INIT:
        BDDWB value = child.evaluateSFBool(spec.sm());
        BDD initialCondition = BDD.or(value.getBDD(), value.undef());
        spec.addInitialCondition(initialCondition);
        break;
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr.symbol() + "'" + " in enact()");
    }
  }

  Run getExample(BDD start, KripkeStructure struct) throws GenerationFailed {
    switch (constr) {
      case NOT:
        return this.child.getCounterExample(start, struct);
      case CAST_MOD:
      case CAST_EXT:
        throw new GenerationFailed("Examples for casts not implemented yet");
      case AX:
        throw new GenerationFailed("Univeral path quantifier");
      case EX:
        // calculates elementary state s0
        BDD stateS0 = BDD.and(this.evaluateBool(struct), start);
        BDD elementaryStateS0 = stateS0.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
        // evaluates the operand of this EX constructor
        BDD evaluatedOperand = this.child.evaluateBool(struct);
        // calculates elementary state s1 and determines an action which maps s0
        // to s1
        String actionName = null;
        Iterator<String> actionNames = struct.getActionNames().iterator();
        BDD elementaryStateS1 = BDD.falseBDD();
        while (elementaryStateS1 == BDD.falseBDD() && actionNames.hasNext()) {
          actionName = actionNames.next();
          elementaryStateS1 = BDD.and(struct.imageUnderAction(elementaryStateS0, actionName), evaluatedOperand);
        }
        if (elementaryStateS1 == BDD.falseBDD()) {
          actionName = TemicParser.lambdaKeyword;
          elementaryStateS1 = BDD.and(BDD.and(struct.reachableDeadlockStates(), elementaryStateS0), evaluatedOperand);
          if (elementaryStateS1 == BDD.falseBDD()) {
            throw new RuntimeException("inconsistent search results in method getExample(), case EX");
          }
        }
        // constructs the run
        Run run = new Run(elementaryStateS0);
        run.add(new Step(actionName, elementaryStateS1));
        // continues the calculation recursively
        run.add(this.child.getExample(elementaryStateS1, struct));
        return run;
      case AG:
        throw new GenerationFailed("Universal path quantifier");
      case EG:
        // calculates elementary state s0
        BDD thisEvaluated = this.evaluateBool(struct);
        stateS0 = BDD.and(thisEvaluated, start);
        elementaryStateS0 = stateS0.satisfyingValuation().specifyDontCares(struct.universe()).asBDD();
        run = new Run(elementaryStateS0);
        while (run.getLasso() == -1) {
          actionName = null;
          actionNames = struct.getActionNames().iterator();
          BDD elementaryStateS = BDD.falseBDD();
          while (elementaryStateS == BDD.falseBDD() && actionNames.hasNext()) {
            actionName = actionNames.next();
            elementaryStateS = BDD.and(struct.imageUnderAction(run.getPostState(), actionName), thisEvaluated);
          }
          if (elementaryStateS == BDD.falseBDD()) {
            actionName = TemicParser.lambdaKeyword;
            elementaryStateS = BDD.and(BDD.and(struct.reachableDeadlockStates(), run.getPostState()), thisEvaluated);
            if (elementaryStateS == BDD.falseBDD()) {
              throw new RuntimeException("inconsistent search results in method getExample(), case EG");
            }
          }
          run.add(new Step(actionName, elementaryStateS));
        }
        return run;
      case AF:
        throw new GenerationFailed("Universal path quantifier");
      case EF:
        Node equivFormula = Node.EU_typed(Node.trueExpr_typed(), this.child);
        return equivFormula.getExample(start, struct);
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr + "' in getExample()");
    }
  }

  Run getCounterExample(BDD start, KripkeStructure struct) throws GenerationFailed {
    switch (constr) {
      case NOT:
        return this.child.getExample(start, struct);
      case CAST_MOD:
      case CAST_EXT:
        throw new GenerationFailed("Counter-examples for casts not implemented yet");
      case AX:
        Node equivFormula = Node.not_typed(Node.EX_typed(Node.not_typed(this.child)));
        return equivFormula.getCounterExample(start, struct);
      case EX:
        throw new GenerationFailed("Existential path quantifier");
      case AG:
        equivFormula = Node.not_typed(Node.EF_typed(Node.not_typed(this.child)));
        return equivFormula.getCounterExample(start, struct);
      case EG:
        throw new GenerationFailed("Existential path quantifier");
      case AF:
        equivFormula = Node.not_typed(Node.EG_typed(Node.not_typed(this.child)));
        return equivFormula.getCounterExample(start, struct);
      case EF:
        throw new GenerationFailed("Existential path quantifier");
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr + "' in getExample()");
    }
  }

  @Override
  public String toString() {
    switch (constr) {
      case NOT:
      case NEG:
        return constr.symbol() + child;
      case INIT:
        return constr.symbol() + " " + child;
      case CAST_MOD:
        return "[" + castType + "] " + child;
      case CAST_EXT:
        return "[" + castType + " or extremum] " + child;
      case AX:
      case EX:
      case AG:
      case EG:
      case AF:
      case EF:
        return constr.symbol() + " " + child;
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr + "' in toString()");
    }
  }
}
