package syntax;

/**
 * Enumeration type Operator.
 *
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-09
 */
public enum Constr {
	// constructor symbols for expressions
	
	// 0-ary operators
	
	// boolean literals
	FALSE, TRUE,
	
	// signed integer literals
	SIGNED,
	
	// literals of enumeration types
	ENUM_LITERAL,
	
	// identifiers (variables)
	// standing for functions defined on the state space 
	IDENTIFIER,
	
	// identifiers standing for guard expressions
	GUARDID,
	
	// 1-ary operators, propositional
	NOT, EXISTS, FORALL,
	
	// 1-ary operators, arithmetic and cast
	NEG, CAST_MOD, CAST_EXT,
	
	// 1-ary operators, temporal
	AX, EX, AG, EG, AF, EF,
	
	// 1-ary operators, epistemic
	K, E, D, C,
	
	// 2-ary operators, propositional
	AND, NAND, OR, NOR, EXOR, IMPL, BIIMPL,
	
	// 2-ary operators, arithmetic
	PLUS, PLUS_IMPLICIT, MINUS, TIMES, DIV, MOD,
	
	// 2-ary operators, temporal
	AU, EU, AW, EW,
	
	// 2-ary operators, comparison
	EQUAL, NEQUAL, LESS, LEQ, GREATER, GEQ,
	
	// declarations
	// (DOASSN means a clause LHS := RHS within the do-part
	// of an action declaration)
	SPEC, VAR, LOCAL, LET, AGENT, GROUP, GUARD, INIT, DOASSN;
	
	public boolean isExpr() {
		return (this.ordinal() < SPEC.ordinal());
	}
	
	public boolean isUnaryExpr() {
		return (NOT.ordinal() <= this.ordinal() && this.ordinal() < AND.ordinal());
	}
	
	public boolean isBinaryExpr() {
		return (AND.ordinal() <= this.ordinal() && this.ordinal() < SPEC.ordinal());
	}
	
	public boolean isTemporalOperator() {
		return (AX.ordinal() <= this.ordinal() && this.ordinal() <= EF.ordinal()) ||
			   (AU.ordinal() <= this.ordinal() && this.ordinal() <= EW.ordinal());
	}
	
	public boolean isEpistemicOperator() {
		return (K.ordinal() <= this.ordinal() && this.ordinal() <= C.ordinal());
	}
	
	public String symbol() {
		switch(this) {
			case FALSE:  return "false";
			case TRUE: 	 return "true";
			case NOT:	 return "~";
			case EXISTS: return "exists";
			case FORALL: return "forall";
			case NEG:	 return "-";
			case AND:	 return "and";
			case NAND:	 return "nand";
			case OR:	 return "or";
			case NOR:	 return "nor";
			case EXOR:	 return "exor";
			case IMPL:	 return "->";
			case BIIMPL: return "<->";
			case PLUS:	 return "+";
			case PLUS_IMPLICIT: return "[+]";
			case MINUS:	 return "-";
			case TIMES:	 return "*";
			case DIV:	 return "div";
			case MOD:	 return "mod";
			case EQUAL:	 return "=";
			case NEQUAL: return "!=";
			case LESS:	 return "<";
			case LEQ:	 return "<=";
			case GREATER: return ">";
			case GEQ:	 return ">=";
			case VAR:	 return "var";
			case LET:	 return "let";
			case AGENT:	 return "agent";
			case GROUP:	 return "group";
			case INIT:	 return "init";
			case DOASSN: return ":=";
			default: return "" + this;
		}	
	}
}
