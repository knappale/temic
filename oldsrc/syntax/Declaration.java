package syntax;

import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNullByDefault;

import bdd.BDDWB;
import bdd.MultiBDD;
import core.SymbolManager;
import core.SymbolTable;
import core.Temic;
import core.Type;
import util.Formatter;

import static util.Formatter.quoted;


/**
 * The class NodeDecl stands for declarations of kind var and let.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-10
 */
class Declaration extends NodeUnary {
  /** the declared symbol */
  private String symbol;
  /** the declared type of the symbol */
  private Type typeSymbol;
  /** in case of an agent declaration: a set of identifiers.
   *  in case of a group declaration: a set of agentNames */
  private HashSet<String> identifiers;
  /** the line numbers of the identifiers */
  private HashMap<String,Integer> lines;
  /** the column numbers of the identifiers */
  private HashMap<String,Integer> columns;
  /** the logger for instances of class Node */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(Temic.class.getName());
	    
  Declaration(Constr constr, String symbol, Type typeSymbol, Node expression, Set<SymbolLineColumn> identifiers_and_positions) {
    super(constr, expression, null);
    this.symbol = symbol;
    this.typeSymbol = typeSymbol;
    if (identifiers_and_positions != null) {
      this.identifiers = new HashSet<String>();
      this.lines = new HashMap<String,Integer>();
      this.columns = new HashMap<String,Integer>();
      for(SymbolLineColumn idPos : identifiers_and_positions) {
        identifiers.add(idPos.symbol());
        lines.put(idPos.symbol(), idPos.line());
        columns.put(idPos.symbol(), idPos.column());
      }
    }
    else { // identifiers_and_positions == null
      this.identifiers = null;
      this.lines = null;
      this.columns = null; 
    }
  }

    String getSymbol() {
    	return symbol;
    }
    
    Type getSymbolType() {
    	return typeSymbol;
    }
	
  @Override
  @NonNullByDefault
  public Type check(SymbolTable<Identifier, Type> typeEnv) throws TypingException {
    type = null;
    Type typeChild;
		switch(constr) {
			case VAR:
				if (typeEnv.top(Identifier.variable(symbol)) != null ||
				    typeEnv.top(Identifier.constant(symbol)) != null ) {
					throw new TypingException("Illegal re-declaration of symbol " + quoted(symbol), line, column);
				}
				typeEnv.push(Identifier.variable(symbol), typeSymbol);
				type = Type.voidType(); break;
			case LET:
				if (typeEnv.top(Identifier.variable(symbol)) != null ||
   				    typeEnv.top(Identifier.constant(symbol)) != null) {
					throw new TypingException("Illegal re-declaration of symbol " + quoted(symbol),line,column);
				}
				typeChild = child.check(typeEnv);
				if (typeChild.isVoid())
					throw new TypingException("let-declaration with right hand side" +
							" of type " + typeChild + " occurred.", child.line, child.column);
				if (!typeChild.subsets(typeSymbol))
					throw new TypingException("let-declaration with right hand side " + quoted(child) + " occurred whose type '" + child.type + "'" +
							" doesn't subset the type " + typeSymbol + " of the declared symbol " + quoted(symbol), child.line, child.column);
				typeEnv.push(Identifier.constant(symbol), typeChild);
				type = Type.voidType();
				break;
			case GUARD:
				if (typeEnv.top(Identifier.guard(symbol)) != null) {
					throw new TypingException("Illegal re-declaration of symbol " + quoted(symbol),line,column);
				}
				typeChild = child.check(typeEnv);
				if (typeChild.isBoolean()) {
					typeEnv.push(Identifier.guard(symbol), typeChild);
					type = Type.voidType();
				}
				else
					throw new TypingException("guard-declaration with right hand side" +
						" of type " + typeChild + "' occurred.", child.line, child.column);
				break;
			case AGENT:
				if (typeEnv.top(Identifier.agent(symbol)) != null) {
					throw new TypingException("Illegal re-declaration of symbol " + quoted(symbol), line, column);
				}
				for (String identifier : identifiers) {
					Type typeIdentifier = typeEnv.top(Identifier.variable(identifier));
	    			if (typeIdentifier == null) {
	    				throw new TypingException("Undeclared symbol " + quoted(identifier), lines.get(identifier), columns.get(identifier));
	    			}
				}
				typeEnv.push(Identifier.agent(symbol), Type.voidType());
				type = Type.voidType(); break;
			case GROUP:
				if (typeEnv.top(Identifier.group(symbol)) != null) {
					throw new TypingException("Illegal re-declaration of symbol " + quoted(symbol), line, column);
				}
				for (String identifier : identifiers) {
					Type typeIdentifier = typeEnv.top(Identifier.agent(identifier));
	    			if (typeIdentifier == null) {
	    				throw new TypingException("Undeclared symbol " + quoted(identifier), lines.get(identifier), columns.get(identifier));
	    			}
				}
				typeEnv.push(Identifier.group(symbol), Type.voidType());
				type = Type.voidType(); break;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
		}
		return type;
	}
	
	/**
	 * Enact a let-, agent-, group-, or guard-declaration, if this node is
	 * a let-, agent-, group-, or guard-declaration node.
	 * @param sm the symbol manager where the declaration is to be enacted.
	 */
	public void enact(SymbolManager sm) {
		switch(constr) {
			case LET:
				if (typeSymbol.isBoolean()) {
					BDDWB value = child.evaluateSFBool(sm);
					sm.declareConstant(symbol, value);
					return;
				}
				if (typeSymbol.isInteger() || typeSymbol.isEnum()) {
					MultiBDD mvalue = child.evaluateSFInt(sm);
					sm.declareConstant(symbol, mvalue);
					return;
				}
				throw new RuntimeException("unknown type " + typeSymbol + " in enact(), case LET.");
			case AGENT:
				sm.declareAgent(symbol, identifiers);
				return;
			case GROUP:
				sm.declareGroup(symbol, identifiers);
				return;
			case GUARD:
				sm.declareGuardExpression(symbol, child);
				return;
			default:
				throw new RuntimeException("illegal constructor symbol '" + constr + "' in enact()");
		}
	}

  @Override
  public String toString() {
    switch(constr) {
      case VAR:
        return this.symbol + " : " + this.typeSymbol;
      case LET:
      case GUARD:
        return constr.symbol() + " " + this.symbol + " : " + this.typeSymbol + " = " + this.child;
      case AGENT:
      case GROUP:
        return constr.symbol() + " " + this.symbol + " = " + Formatter.set(this.identifiers);
      default:
        throw new RuntimeException("illegal constructor symbol '" + constr + "' in toString()");
    }
  }
}
