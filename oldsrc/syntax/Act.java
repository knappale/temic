/*
 * NodeAction.java
 *
 * Created on 12 April 2011
 */

package syntax;

import bdd.BDD;
import bdd.BDDWB;

import core.Specification;
import core.SymbolTable;
import core.Temic;
import core.Type;
import syntax.generated.TemicParser;
import util.Formatter;

import java.util.Vector;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Instances of NodeAction represent action declarations.
 * @author H. Muehlberger
 * @version 1.0
 */
public class Act extends Node {
  /** the identifier of the declared action */
  private @NonNull Identifier id;
  /** the epistemic pre-condition of this action; null, if not specified */
  private Node epre;
  /** the standard pre-condition of this action */
  private Node pre;
  /** a vector of assignment nodes specifying the post-condition */
  private Vector<Node> assns = new Vector<Node>();
  /** the logger for instances of class NodeAction */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(Temic.class.getName());
    
  private Act(Identifier id, Node epre, Node pre, Vector<Node> assns) {
    this.id = id;
    this.epre = epre;
    this.pre = pre;
    this.assns = assns;
  }

  public static Act action(Identifier id, Node epre, Node pre, Vector<Node> assns) {
    return new Act(id, epre, pre, assns);
  }
	
  public static Act action(@NonNull String name, Node epre, Node pre, Vector<Node> assns) {
    return new Act(Identifier.action(name), epre, pre, assns);
  }
	
  @Override
  @NonNullByDefault
  public Type check(SymbolTable<Identifier, Type> typeEnv) throws TypingException {

    if (typeEnv.top(this.id) != null)
      throw new TypingException("Illegal re-declaration of " + Formatter.quoted(this.id), this.line, this.column);
    typeEnv.push(this.id, Type.voidType());
    this.type = Type.voidType();

    if (epre != null && !epre.check(typeEnv).equals(Type.booleanType()))
      throw new TypingException("An epistemic pre-condition of type " + epre.getType() +
                                " occurred. Should be of type boolean.", epre.getLine(), epre.getColumn());
    if (!pre.check(typeEnv).equals(Type.booleanType()))
      throw new TypingException("A standard pre-condition of type " + pre.getType() +
                                " occurred. Should be of type boolean.", pre.getLine(), pre.getColumn());
    Set<String> symbolsLHS = new HashSet<>();
    for (Node assn : assns) {
      if (!assn.check(typeEnv).equals(Type.voidType()))
        throw new TypingException("A do-assignment of type " + assn.getType() +
                                  " occured. Should be of type void.", assn.getLine(), assn.getColumn()); 
      if (symbolsLHS.contains(assn.getSymbol()))
        throw new TypingException("Illegal re-assignment of symbol " + Formatter.quoted(assn.getSymbol()), assn.getLineSymbol(), assn.getColumnSymbol());
      symbolsLHS.add(assn.getSymbol());
    }

    return this.type;
  }
	
  /**
   * Enacts this action-declaration.
   *
   * @param spec the instance of Specification in which the action-declaration
   *             is to be enacted
   */
  public void enact(Specification spec) {
    // evaluate standard precondition; interpret precondition
    // as true, if the precondition is undefined at certain points
    BDDWB preBDDWB = pre.evaluateSFBool(spec.sm());
    BDD preBDD = BDD.or(preBDDWB.getBDD(), preBDDWB.undef());
    // only normal states come into consideration for pre-states
    // i.e. no error states and no illegal encodings
    BDD preCond = BDD.and(preBDD, spec.sm().normalStates());
    // evaluate assignment nodes (do-part of action declaration)
    HashMap<String,BDD> rhsBuffer = new HashMap<String,BDD>();
    for(Node assn : assns)
      assn.enact(spec.sm(), rhsBuffer);

    // calculate postcondition
    BDD postCondStd = BDD.trueBDD();
    BDD postCondErr = BDD.trueBDD();
    Vector<String> bivalentVariables = spec.sm().bivalentVariables();
    for(int index=1; index<=bivalentVariables.size(); index++) {
      String variable = bivalentVariables.get(index-1);
      if (variable.endsWith("'")) {
        int unprimedIndex = spec.sm().getPrimedToUnprimed().apply(index);
        if (variable.equals(TemicParser.errorKeyword + "'")) {
          postCondErr = BDD.and(postCondErr, BDD.biimplies(BDD.varBDD(index), BDD.trueBDD()));
          postCondStd = BDD.and(postCondStd, BDD.biimplies(BDD.varBDD(index), BDD.varBDD(unprimedIndex)));
        }
        else {
          // If the execution of the action leads to an error, all variables (besides error)
          // remain unchanged in order to serve as a stack dump.
          postCondErr = BDD.and(postCondErr, BDD.biimplies(BDD.varBDD(index), BDD.varBDD(unprimedIndex)));
          BDD rhsBDD = rhsBuffer.get(variable);
          if (rhsBDD != null) {
            postCondStd = BDD.and(postCondStd, BDD.biimplies(BDD.varBDD(index), rhsBDD));
          }
          else { // frame condition
            postCondStd = BDD.and(postCondStd, BDD.biimplies(BDD.varBDD(index), BDD.varBDD(unprimedIndex)));
          }
        }
      }
    }
    BDD error = rhsBuffer.get(TemicParser.errorKeyword + "'");
    BDD postCond = error == null ? postCondStd : BDD.ite(error, postCondErr, postCondStd);
    spec.addAction(this.id.getName(), epre, preCond, postCond);
  }

  @Override
  public String toString() {
    StringBuffer ret = new StringBuffer();
    ret.append("action " + this.id.getName() + " [\n");
    if (epre != null) {
      ret.append("epre := " + epre + "\n");
    }
    if (pre != null) {
      ret.append("pre := " + pre + "\n");
    }
    ret.append("do ");
    for (int i=0; i<assns.size(); i++) {
      ret.append(assns.get(i));
      if (i+1<assns.size()) { ret.append(", "); }
    }
    ret.append("]");
    return "" + ret;
  }
}
