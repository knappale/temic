package syntax;

import core.StateIMP;
import core.Temic;
import core.Type;
import core.Specification;
import core.SymbolManager;
import core.SymbolTable;
import core.OptionSet;

import java.io.PrintWriter;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * The root node of a syntax tree should be an instance of NodeSpec
 * standing for the specification of a Kripke structure.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-04-10
 */
public class Spec extends Node {
	/** the name of the specification */
	private String name;
    
    /** a vector of var-declarations ordered
     * in the sequence of their occurrence in the input file */
    private Vector<Node> varDeclarations = new Vector<Node>();
    
    /** a vector of let-declarations ordered
     * in the sequence of their occurrence in the input file */
    private Vector<Node> letDeclarations = new Vector<Node>();
    
    /** a vector of guard-declarations ordered
     * in the sequence of their occurrence in the input file */
    private Vector<Node> guardDeclarations = new Vector<Node>();
    
    /** a vector of agent-declarations ordered
     * in the sequence of their occurrence in the input file */
    private Vector<Node> agentDeclarations = new Vector<Node>();
    
    /** a vector of group-declarations ordered
     * in the sequence of their occurrence in the input file */
    private Vector<Node> groupDeclarations = new Vector<Node>();
        
    /** a vector of action nodes ordered
     * in the sequence of their occurrence in the input file */
    private Vector<Act> actions = new Vector<>();
    
    /** a vector of initial conditions */
    private Vector<Node> initialConditions = new Vector<Node>();
    
    /** the command that is to be executed in the scope of this specification */
	private Command command = null;
    
	/** the logger for instances of class Node */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Temic.class.getName());
    
    Spec(String name) {
    	super(Constr.SPEC);
    	this.name = name;
    }
    
	/**
	 * Returns an empty specification with name <tt>name</tt>.
	 * @pre name != null
	 * @param name the name of the specification.
	 * @return a node that represents an empty specification with name <tt>name</tt>.
	 */
	public static Spec specification(String name) {
		return new Spec(name);
	}
	
	/**
	 * Returns a vector of all var-declared variables in order of
	 * their declarations in the source file. 
	 * @return a vector of all var-declared variables in order of
	 * their declarations in the source file. 
	 */
    public Vector<String> getVariables() {
    	Vector<String> ret = new Vector<String>(varDeclarations.size());
    	for (Node varDecl : varDeclarations) {
    		ret.add(varDecl.getSymbol());
    	}
    	return ret;
    }
    
    /**
     * Adds a var-declaration to this specification node.
     * @param varDecl the var-declaration to be added.
     */
    public void addVarDecl(Node varDecl) {
    	if (varDecl.constr == Constr.VAR) {
    		varDeclarations.add(varDecl);
    		return;
    	}
    	throw new RuntimeException("illegal constructor symbol '" + constr + "' in addVarDecl()");
    }

    /**
     * Adds a let-declaration to this specification node.
     * @param letDecl the let-declaration to be added.
     */
    public void addLetDecl(Node letDecl) {
    	if (letDecl.constr == Constr.LET) {
    		letDeclarations.add(letDecl);
    		return;
    	}
    	throw new RuntimeException("illegal constructor symbol '" + constr + "' in addLetDecl()");
    }
    
    /**
     * Adds a guard-declaration to this specification node.
     * @param guardDecl the guard-declaration to be added.
     */
    public void addGuardDecl(Node guardDecl) {
    	if (guardDecl.constr == Constr.GUARD) {
    		guardDeclarations.add(guardDecl);
    		return;
    	}
    	throw new RuntimeException("illegal constructor symbol '" + constr + "' in addGuardDecl()");
    }
    
    /**
     * Adds a agent-declaration to this specification node.
     * @param agentDecl the agent-declaration to be added.
     */
    public void addAgentDecl(Node agentDecl) {
    	if (agentDecl.constr == Constr.AGENT) {
    		agentDeclarations.add(agentDecl);
    		return;
    	}
    	throw new RuntimeException("illegal constructor symbol '" + constr + "' in addAgentDecl()");
    }
    
    /**
     * Adds a group-declaration to this specification node.
     * @param groupDecl the group-declaration to be added.
     */
    public void addGroupDecl(Node groupDecl) {
    	if (groupDecl.constr == Constr.GROUP) {
    		groupDeclarations.add(groupDecl);
    		return;
    	}
    	throw new RuntimeException("illegal constructor symbol '" + constr + "' in addGroupDecl()");
    }
    
    /**
     * Adds an initial condition to this specification node.
     * @param initCond the initial condition to be added.
     */
    public void addInitCond(Node initCond) {
    	if (initCond.constr == Constr.INIT) {
    		initialConditions.add(initCond);
    		return;
    	}
    	throw new RuntimeException("illegal constructor symbol '" + constr + "' in addInitCond()");
    }
    
    /**
     * Adds an action node to this specification.
     * 
     * @param action the action to be added
     */
    public void addAction(Act action) {
      this.actions.add(action);
    }
	
    /**
     * Set the command for this specification.
     * 
     * @param command the command to be set
     */
    public void setCommand(Command command) {
      this.command = command;
    }

	public String toString() {
		StringBuffer ret = new StringBuffer();
		ret.append("specification " + name + "\n");
		ret.append("var-declarations:\n");
		for (Node node : varDeclarations) {
			ret.append("" + node + "\n");
		}
		ret.append("let-declarations:\n");
		for (Node node : letDeclarations) {
			ret.append("" + node + "\n");
		}
		ret.append("guard-declarations:\n");
		for (Node node : guardDeclarations) {
			ret.append("" + node + "\n");
		}		
		ret.append("agent-declarations:\n");
		for (Node node : agentDeclarations) {
			ret.append("" + node + "\n");
		}
		ret.append("group-declarations:\n");
		for (Node node : groupDeclarations) {
			ret.append("" + node + "\n");
		}
		ret.append("actions:\n");
		for (Node node : actions) {
			ret.append("" + node + "\n");
		}
		ret.append("end");
		if (this.command != null) {
			ret.append(";\n" + this.command);
		}
		return "" + ret;
	}
	
	@Override
	@NonNullByDefault
	public Type check(SymbolTable<syntax.Identifier, Type> typeEnv) throws TypingException {
		type = null;
		if (constr != Constr.SPEC) {
			throw new RuntimeException("illegal constructor symbol '" + constr + "' in check()");
		}
		for(Node varDecl : varDeclarations) {
			if (!varDecl.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("val-declaration of type " + varDecl.getType() +
					" occured. Should be of type void.", varDecl.getLine(), varDecl.getColumn()); 
			}
		}
		for(Node letDecl : letDeclarations) {
			if (!letDecl.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("A let-declaration of type " + letDecl.getType() +
					" occured. Should be of type void.", letDecl.getLine(), letDecl.getColumn()); 
			}
		}
		for(Node agentDecl : agentDeclarations) {
			if (!agentDecl.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("agent-declaration of type " + agentDecl.getType() +
					" occured. Should be of type void.", agentDecl.getLine(), agentDecl.getColumn()); 
			}
		}
		for(Node groupDecl : groupDeclarations) {
			if (!groupDecl.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("group-declaration of type " + groupDecl.getType() +
					" occured. Should be of type void.", groupDecl.getLine(), groupDecl.getColumn()); 
			}
		}
		for(Node guardDecl : guardDeclarations) {
			if (!guardDecl.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("guard-declaration of type " + guardDecl.getType() +
					" occured. Should be of type void.", guardDecl.getLine(), guardDecl.getColumn()); 
			}
		}
		for(Node action : actions) {
			if (!action.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("action-declaration of type " + action.getType() +
					" occured. Should be of type void.", action.getLine(), action.getColumn()); 
			}
		}	
		for(Node initCond : initialConditions) {
			if (!initCond.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("init-declaration of type " + initCond.getType() +
					" occured. Should be of type void.", initCond.getLine(), initCond.getColumn()); 
			}
		}
		if (this.command != null) {
			if (!this.command.check(typeEnv).equals(Type.voidType())) {
				throw new TypingException("command of type " + this.command.getType() +
					" occured. Should be of type void.", this.command.getLine(), this.command.getColumn()); 
			}
		}
		
		type = Type.voidType();
		return type;
	}
	
	/**
	 * Creates and returns the instance of class Specification that
	 * is the semantic counterpart to this syntactic specification node.
	 * @return the instance of class Specification that is the
	 * semantic counterpart to this syntactic specification node.
	 */
	public Specification specification(SymbolManager sm) {
		Specification spec = new Specification(name, sm);
		// enact declarations of symbolic constants
		for(Node letDecl : letDeclarations) {
			letDecl.enact(sm);
		}
		// enact declarations of agents
		for (Node agentDecl : agentDeclarations) {
			agentDecl.enact(sm);
		}
		// enact declarations of groups
		for (Node groupDecl : groupDeclarations) {
			groupDecl.enact(sm);
		}
		// enact declarations of guards
		for (Node guardDecl : guardDeclarations) {
			guardDecl.enact(sm);
		}
		// enact declarations of initial conditions
		for (Node initCond : initialConditions) {
			initCond.enact(spec);
		}
		// enact declarations of actions
		for (Node action : actions) {
			action.enact(spec);
		}
		return spec;
	}
	
	/**
	 * Executes the statements.
	 */
	public void execute(Specification spec, StateIMP sigma, PrintWriter out, OptionSet options) {
		if (this.command != null) {
			this.command.execute(spec, sigma, out, options);
		}
	}
}
