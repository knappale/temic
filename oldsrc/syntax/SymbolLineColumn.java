package syntax;

public class SymbolLineColumn {
	private String symbol;
	private int line;
	private int column;
	
	public SymbolLineColumn(String symbol, int line, int column) {
		this.symbol = symbol;
		this.line = line;
		this.column = column;
	}
	
	public String symbol() {
		return symbol;
	}
	
	public int line() {
		return line;
	}
	
	public int column() {
		return column;
	}
}
