package core;

import java.util.logging.Level;

public class OptionSet {
  /**
   * Enumeration type CXLevel.
   * Elements of CXLevel determine the granularity level of the generation
   * of counter-examples for CTLK formulas that are not valid w.r.t. a given
   * Kripke structure. 
   *
   * @author H. Muehlberger
   * @version 1.0
   * @since 2011-07-21
   */
  public enum CXLevel {
    OFF, NORMAL, FULL;
	
    public static CXLevel parse(String literal) throws IllegalArgumentException {
      String normLiteral = literal.trim().toUpperCase();
      if (normLiteral.equals("OFF"))
        return OFF;
      if (normLiteral.equals("NORMAL"))
        return NORMAL;
      if (normLiteral.equals("FULL"))
        return FULL;
      throw new IllegalArgumentException();
    }
  }

  /** a guideline value for the initial size of the node table */
  private static final int NODETABLE_SIZE = 10000;
  /** a guideline value for the initial size of the caches */
  private static final int CACHE_SIZE = 10000;
  /** the initial size of the table for BDD wrapper-instances */
  private static final int WRAPPERTABLE_SIZE = 1117;
  /** if the following value is greater than 0, the output of any set
   * of elementary states with cardinality > OUTPUT_LIMIT is aborted
   * after the OUTPUT_LIMITth elementary state is output; a value
   * of 0 switches this feature off */
  private static final int OUTPUT_LIMIT = 100;

  private String inputFile = null;
  private String outputFile = null;
  private String option_iv = null;	           // read order of variables from file
  private String option_ov = null;	           // write order of variables to file
  private Level option_ll = Level.WARNING;       // logging level
  private String option_lf = null;               // write logging info to file
  private int ntabsize = NODETABLE_SIZE; 
  private int cachesize = CACHE_SIZE;
  private int wtabsize = WRAPPERTABLE_SIZE;
  private CXLevel cxlevel = CXLevel.NORMAL;       // level of counter-example generation
  private int outputLimit = OUTPUT_LIMIT;   // limits the output of elementary states (0=off)
  private boolean rule1 = false;                  // additional rule no 1 for p-n-semantics
    
  public String inputFile() { return inputFile; }
  public String outputFile() { return outputFile; }
  public String option_iv() { return option_iv; }
  public String option_ov() { return option_ov; }
  public Level option_ll() { return option_ll; }
  public String option_lf() { return option_lf; }
  public int ntabsize() { return ntabsize; }
  public int cachesize() { return cachesize; }
  public int wtabsize() { return wtabsize; }
  public CXLevel cxlevel() { return cxlevel; }
  public int outputLimit() { return outputLimit; }
  public boolean rule1() { return rule1; }
    
  public void setInputFile(String pathname) { inputFile = pathname; }
    
  public static OptionSet parseCommandLineOptions(String[] args) {
    OptionSet set = new OptionSet();
    int argNo = 0; String argument = null;
    while (argNo < args.length) {
      argument = args[argNo];
      if (argument.equals("-h") || argument.equals("-help"))
        return null;

      if (argument.equals("-ll")) { // set logging level
        if (argNo+1 >= args.length)
          return null;
        try {
          set.option_ll = Level.parse(args[argNo+1]); argNo += 2;
        }
        catch(IllegalArgumentException e) {
          return null;
        }
        continue;
      }

      if (argument.equals("-lf")) { //  write logging information to file args[argNo + 1]
        if (argNo+1 >= args.length)
          return null;
        set.option_lf = args[argNo+1]; argNo += 2;
        continue;
      }

      if (argument.equals("-ntabsize")) { // set approx. initial size of the node table
        if (argNo+1 >= args.length)
          return null;
        try {
          set.ntabsize = Integer.parseInt(args[argNo+1]); argNo += 2;
        }
        catch (NumberFormatException e) {
          return null;
        }
        continue;
      }

      if (argument.equals("-cachesize")) { // set approx. initial size of caches
        if (argNo+1 >= args.length)
          return null;
        try {
          set.cachesize = Integer.parseInt(args[argNo+1]); argNo += 2;
        }
        catch (NumberFormatException e) {
          return null;
        }
        continue;
      }

      if (argument.equals("-wtabsize")) { // set initial size of wrapper table
        if (argNo+1 >= args.length)
          return null;
        try {
          set.wtabsize = Integer.parseInt(args[argNo+1]); argNo += 2;
        }
        catch (NumberFormatException e) {
          return null;
        }
        continue;
      }

      if (argument.equals("-cxl")) { // set level of counter-example generation
        if (argNo+1 >= args.length)
          return null;
        try {
          set.cxlevel = CXLevel.parse(args[argNo+1]); argNo += 2;
        }
       	catch (IllegalArgumentException e) {
       	  return null;
       	}
        continue;
      }

      if (argument.equals("-olimit")) { // set limit for outputting elementary states
        if (argNo+1 >= args.length)
          return null;
        try {
          set.outputLimit = Integer.parseInt(args[argNo+1]); argNo += 2;
        }
        catch(NumberFormatException e) {
          return null;
        }
        continue;
      }

      if (argument.equals("-rule1")) { // activate rule 1
        set.rule1 = true; argNo += 1;
        continue;
      }

      if (set.inputFile == null) {
        set.inputFile = argument; argNo += 1;
        continue;
      }
 
      if (set.outputFile == null) {
        set.outputFile = argument; argNo += 1;
        continue;
      }

      return null;
    }

    if (set.inputFile == null || set.ntabsize < 0 || set.cachesize < 0 || set.wtabsize < 0)
      return null;

    return set;
  }
    
  public static void usage() {
    System.out.println("usage: java Temic (<option>)* <input file> [<output file>]\n");
    System.out.println("available options are:");
    System.out.println("-ll <log_level>    set logging level to SEVERE, WARNING, INFO, FINE, or ALL; default is WARNING");
    System.out.println("-lf <log_file>     write logging information to file <log_file>");
    System.out.println("-h | -help         print help/usage");
    System.out.println("-ntabsize <size>   set approx. initial size of node table to <size> (default is " + NODETABLE_SIZE + ")");
    System.out.println("-cachesize <size>  set approx. initial size of caches to <size> (default is " + CACHE_SIZE + ")");
    System.out.println("-wtabsize <size>   set initial size of wrapper table to <size> (default is " + WRAPPERTABLE_SIZE + ")");
    System.out.println("-cxl <cx_level>    set level of counter-example generation to OFF, NORMAL, or FULL; default is NORMAL");
    System.out.println("-olimit <number>   limits the number of elementary states written to output; default is " + OUTPUT_LIMIT + "; 0 is OFF");
    System.out.println("-rule1             activate additional rule no 1 for P-N-semantics; deactivated by default");
  }   
}
