package core;

import java.util.Vector;

import bdd.BDDWB;
import bdd.MultiBDD;

public interface SValueEnvironment {
    /**
     * Returns the BDDWB ("BDD with Bottom") which the bivalent variable
     * or boolean constant <tt>symbol</tt> stands for.
     * @param symbol the string representation of a bivalent variable or
     * a boolean constant.
     * @return the BDDWB which the symbol <tt>symbol</tt> stands for, if symbol
     * is either the string representation of a bivalent variable or a boolean constant;
     * null, if <tt>symbol</tt> is neither declared as a variable nor as a constant.
     * If <tt>symbol</tt> is declared as a variable or constant of type int or enum,
     * the return value of this method has no meaning.
     */
	public BDDWB getBDDWBForSymbol(String symbol);
	
	/**
	 * Returns the MultiBDD which the variable or constant <tt>symbol</tt>
	 * stands for, if <tt>symbol</tt> is declared to be of type int or enum.
	 * @param symbol the string representation of variable or constant of
	 * type int or enum.
     * @return the MultiBDD which the variable or constant <tt>symbol</tt>
	 * stands for, if <tt>symbol</tt> is declared to be of type int or enum.
	 * @throws RuntimeException, if <tt>symbol</tt> is a bivalent variable.
	 * If <tt>symbol</tt> is a constant of type boolean, the return value
	 * of this method has no meaning.
	 */
	public MultiBDD getMultiBDDForSymbol(String symbol);
	
    /**
     * Declares an unprimed variable <tt>variable</tt> (without primed sibling)
     * locally to the scope of an forall/exists quantifier. This method is called
     * by the evaluation methods of instances of NodeQuant, whenever the evaluation 
     * process enters the scope of an forall/exists quantifier.
     * @param variable the string representation of an unprimed local variable
     * @param type the type of the local variable
     * @returns the multi-index of the new locally declared variable
     */
    public Vector<Integer> declareLocalVariable(String variable, Type type);
    
    /**
     * Undeclares an unprimed local variable <tt>variable</tt>. This method is called
     * by the evaluation methods of instances of NodeQuant, whenever the evaluation
     * process leaves the scope of an forall/exists quantifier. Local variables have
     * to be redeclared in reverse order of their declarations.
     * @param variable the string representation of an unprimed local variable
     */
    public void undeclareLocalVariable(String variable);
}
