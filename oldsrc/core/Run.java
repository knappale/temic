package core;

import bdd.BDD;
import bdd.Universe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import java.io.Writer;
import java.io.IOException;

import util.Formatter;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * A finite or infinite run of a transition system.
 *
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-07-18
 */
@NonNullByDefault
public class Run {
  private List<Step> steps;
    
  /**
   * Constructs a Run with only one step that represents
   * the initial state of the run.
   * 
   * @param initialState a bdd that represents the initial state of this run
   */
  public Run(BDD initialState) {
    this.steps = new ArrayList<Step>();
    this.steps.add(new Step(initialState));
  }

  /**
   * Constructs a Run from an (ordered) collection of steps.
   *
   * @param trace
   */
  public Run(Collection<Step> trace) {
    this.steps = new ArrayList<Step>();
    this.steps.addAll(trace);
  }

  /**
   * @return the initial state from which this run starts
   */
  public BDD getInitialState() {
    BDD initialState = this.steps.get(0).getPostState();
    assert initialState != null;
    return initialState;
  }

  /**
   * @return the post-state reached by this run
   */
  public BDD getPostState() {
    BDD postState = this.steps.get(this.steps.size()-1).getPostState();
    assert postState != null;
    return postState;
  }

  /**
   * Appends the specified <tt>step</tt> to the end of this run.
   * 
   * @param step the Step to be added to the end of the run
   */
  public void add(Step step) {
    this.steps.add(step);
  }
    
  /**
   * Append another Run to this Run.
   *
   * @pre the post state of the last step of this run must be equal to
   *      the initial state of the other run
   * @param other a run
   */
  public void add(Run other) {
    if (this.getPostState() != other.getInitialState())
      throw new IllegalArgumentException("the ends of the runs do not match");

    this.steps.addAll(other.steps.subList(1, other.steps.size()));
  }
    
  /**
   * Remove the last Step from this Run.
   */
  public void pop() {
    this.steps.remove(this.steps.size()-1);
  }
      
  /**
   * Push a step to this run, i.e., add a step to the end of this run.
   * 
   * @param step the Step to be added to the end of the run
   * @see #add(Step)
   */
  public void push(Step step) {
    this.add(step);
  }
    
  /**
   * Determine the position of a lasso in this run, i.e., the first step
   * the post-state of which equals the post-state of the last step.
   *
   * @return the lasso position greater or equal to zero if there is a lasso
   *         and -1 otherwise
   */
  public int getLasso() {
    BDD lastPostState = getPostState();
    for (int i = 0; i < this.steps.size()-1; i++) {
      if (lastPostState.equals(this.steps.get(i).getPostState()))
        return i;
    }
    return -1;
  }

  /**
   * Prints a human readable, typed and fully qualified representation of
   * this run to {@code out}.
   * 
   * @param out a Writer
   * @param vd the variable decoding for typed output
   * @throws IOException if an I/O error occurs
   */
  public void printTyped(Writer out, Universe vd) throws IOException {
    int maxLengthOfStepNumber = ("" + steps.size()).length();
    int maxLengthOfActionNames = this.steps.stream().skip(1).map(step -> step.getActionName().length()).max(Integer::compare).orElse(0);
    int lassoStartIndex = this.getLasso();

    out.write("step " + Formatter.format(1, maxLengthOfStepNumber) + ":");
    out.write(Formatter.numSpaces(11 + maxLengthOfActionNames));
    out.write("init-state: ");
    steps.get(0).getPostState().satisfyingValuation().specifyDontCares(vd.universe()).printTyped(out, vd);

    if (lassoStartIndex == 0)
      out.write("   --- loop starts here ---\n");
    for (int i = 1; i < steps.size(); i++) {
      out.write("step " + Formatter.format(i+1, maxLengthOfStepNumber) + ": ");
      out.write("action " + Formatter.format("'"+steps.get(i).getActionName()+"'", maxLengthOfActionNames+2) + " ");
      out.write("post-state: ");
      steps.get(i).getPostState().satisfyingValuation().specifyDontCares(vd.universe()).printTyped(out, vd);
      if (lassoStartIndex == i)
        out.write("   --- loop starts here ---\n");
    }
  }
}