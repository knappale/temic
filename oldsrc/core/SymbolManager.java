package core;

import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;
import bdd.Universe;
import bdd.VariableRange;
import bdd.VariableRangeSet;
import bdd.VariableRenaming;
import syntax.Identifier;
import syntax.Node;
import syntax.generated.TemicParser;

import java.util.Set;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.HashSet;
import java.util.Vector;
import java.util.Hashtable;

import org.eclipse.jdt.annotation.NonNull;


public class SymbolManager implements SValueEnvironment, Universe {
    /**
     * A vector that maps the (by 1 decremented) index of the unprimed or primed
     * version of a globally declared bivalent variable to a string representation
     * of this variable. In the context of this class, the term "bivalent variable"
     * stands both for boolean variables and binary digits of the encoding of an
     * integer or enum variable y. Examples of string representations are "x",
     * "x'", "x.3", or "x.3'". The size of this vector is the number of all
     * bivalent variables that are spawning the state space (primed or unprimed).
     */
    private Vector<String> bivalentVariables;
    
    /**
     * A sorted set that contains the indices of all (globally declared)
     * unprimed bivalent variables in natural order.
     */
    private TreeSet<Integer> universe;
    
    /**
     * The number of local bivalent variables that are currently declared. This
     * counter is used for producing unique indices (bdd node labels) for local
     * variables. Local variables do not contribute to the state space. They are
     * neither stored in the vector 'bivalentVariables' nor in the vector 'displayingOrder'.
     * Encoding constraints that result from the encondingUpperBound of the type
     * of the local variable are not included in the stateSpaceBDD. 
     */
    private int localBivalentVariableCount;
    
    /**
     * A BDD representing the set of all valuations of above-mentioned
     * unprimed bivalent variables which belong to the state space.
     * Note: There may be illegal valuations of the binary digits of the encoding
     * of variables of type int or enum.
     */
    private BDD stateSpaceBDD;
    
    /**
     * The logical conjunction of stateSpaceBDD and no-error-constraint.
     */
    private BDD normalStates;
    
    /**
     * A vector that contains the string representations of all unprimed
     * typed variables (the vector does not contain binary digits of the encoding
     * of integer or enum variables). The order in which the string representations
     * occur in this vector determines the order of displaying by Valuation.printTyped().
     * The 'error' variable is at position 0. Next come the boolean variables
     * in ascending order of their indices. Next come the integer variable and
     * then the enum variables (in no specific order).
     */
    private Vector<String> displayingOrder;
	
    /**
     * A symboltable (with stack structure) that maps the string representation of
     * an unprimed or primed variable of type boolean, int, or enum to a multi-index.
     * If the variable is of type boolean, the multi-index is a vector of size 1
     * containing the index of the given boolean variable.
     * If the variable is of type int or enum, the multi-index is a vector
     * 1) whose size is the number of binary digits necessary to encode the values
     * of the variable and 2) which maps a digit to the index of the bivalent
     * variable representing the given digit of the encoding of this variable.
     * The string representations of primed variables are stored with the
     * trailing primes. 
     */
    private SymbolTable<@NonNull String, Vector<Integer>> multiindexTable;
    
	/**
     * A hashtable that maps let-declared symbolic constants to multi bdds.
     */
    private Hashtable<String,MultiBDD> constantTable;
    
    /**
     * A hashtable that maps names to sets of unprimed variable indices. 
     */
    private Hashtable<String,Set<Integer>> agentTable;
    
    /**
     * A hashtable that maps names to sets of sets of unprimed variable indices.  
     */
    private Hashtable<String,Set<Set<Integer>>> groupTable;
    
    /**
     * A mapping of primed to unprimed bivalent variable indices;
     * for a good performance this mapping should be strictly increasing.
     */
    private VariableRenaming renamingPrimedToUnprimed;
    
    /**
     * A mapping of unprimed to primed bivalent variable indices;
     * for a good performance this mapping should be strictly increasing.
     */
    private VariableRenaming renamingUnprimedToPrimed;
    
    /**
     * The set of all unprimed bivalent variable indices (for quantifications).
     */
    private VariableRangeSet rangeUnprimed;
    
    /**
     * The set of all primed bivalent variable indices (for quantifications).
     */
    private VariableRangeSet rangePrimed;
    
    /**
     * a vector that maps the index of an epistemic guard (minus 1)
     * to the corresponding guard expressions; the size of this vector
     * is the number of all epistemic guards declared by "guard".
     */
    private Vector<Node> guardExpressions;
    
    /**
     * a vector that maps the index of an epistemic guard (minus 1)
     * to the string representation of the constant symbol that was
     * used for declaring this guard. 
     */
    private Vector<String> guardConstants;
    
	/**
	 * a hash table that maps symbolic constants (of type guard) to the
	 * corresponding index (= array index in array guardExpressions plus 1).
     */
    private Hashtable<String,Integer> guardIndexTable;
    
    /**
     * the identity relation on state space x state space'.
     */
    private BDD identityRelation;
    
    /** the type environment of this symbol manager */
    private SymbolTable<syntax.Identifier, Type> typeEnv;
    
    @SuppressWarnings("unused")
	private SymbolManager() {
    }
    
    /**
     * Creates a new symbol manager and initializes it with the given variables
     * and with the error keyword.
     * @param variables
     * @param typeEnv
     */
	public SymbolManager(Vector<String> variables, SymbolTable<syntax.Identifier, Type> typeEnv) {
    	System.out.println("Before initialisation: " + bdd.BDD.count);
		this.bivalentVariables = new Vector<String>();
		this.universe = new TreeSet<Integer>();
		this.localBivalentVariableCount = 0;
		this.stateSpaceBDD = BDD.trueBDD();
		this.displayingOrder = new Vector<String>();
		this.multiindexTable = new SymbolTable<>();
		this.constantTable = new Hashtable<String,MultiBDD>();
		this.agentTable = new Hashtable<String,Set<Integer>>();
		this.groupTable = new Hashtable<String,Set<Set<Integer>>>();
		this.renamingPrimedToUnprimed = new VariableRenaming();
		this.renamingUnprimedToPrimed = new VariableRenaming();
		this.rangeUnprimed = new VariableRangeSet();
		this.rangePrimed = new VariableRangeSet();
        this.guardExpressions = new Vector<Node>();
        this.guardConstants = new Vector<String>();
		this.guardIndexTable = new Hashtable<String,Integer>();
		this.identityRelation = null; // calculated on demand
		this.typeEnv = typeEnv;
		
		// declare error keyword
		declareBivalentVariable(TemicParser.errorKeyword);
		normalStates = BDD.not(getBDDWBForSymbol(TemicParser.errorKeyword).getBDD());
		
		// declare variables
		Vector<String> booleanVars = new Vector<String>();
		Vector<String> integerVars = new Vector<String>();
		Vector<String> enumVars = new Vector<String>();
		for (String v : variables) {
			Type vType = typeEnv.top(Identifier.variable(v));
			if (vType.isBoolean()) {
				declareBivalentVariable(v);
				booleanVars.add(v);
				continue;
			}
			if (vType.isInteger()) {
				declareMultivalentVariable(v, vType.numBits());
				integerVars.add(v);
				continue;
			}
			if (vType.isEnum()) {
				declareMultivalentVariable(v, vType.numBits());
				enumVars.add(v);
				continue;
			}
			throw new RuntimeException("illegal type " + vType + " occurred in SymbolManager()");
		}
		
		// define displaying order
		displayingOrder.add(TemicParser.errorKeyword);
		for (String v : booleanVars) {
			displayingOrder.add(v);
		}
		for (String v : integerVars) {
			displayingOrder.add(v);
		}
		for (String v : enumVars) {
			displayingOrder.add(v);
		}
    	System.out.println("After initialisation: " + bdd.BDD.count);
	}
	
	private void declareBivalentVariable(String v) {
		// declare unprimed version of v
		bivalentVariables.add(v);
		Vector<Integer> mindex = new Vector<Integer>(1);
		mindex.add(bivalentVariables.size());
		multiindexTable.push(v, mindex);
		rangeUnprimed.add(bivalentVariables.size());
		universe.add(bivalentVariables.size());
		// declare primed version of v
		bivalentVariables.add(v + "'");
		mindex = new Vector<Integer>(1);
		mindex.add(bivalentVariables.size());
		multiindexTable.push(v + "'", mindex);
		rangePrimed.add(bivalentVariables.size());
		// declare variable renaming
		int indexUnprimed = bivalentVariables.size()-1;
		int indexPrimed = bivalentVariables.size();
		renamingPrimedToUnprimed.put(indexPrimed, indexUnprimed);
		renamingUnprimedToPrimed.put(indexUnprimed, indexPrimed);
		// create unprimed and primed version of the variable
		// in the underlying jdd package
		BDD.varBDD(indexUnprimed);
		BDD.varBDD(indexPrimed);
	}
	
	private void declareMultivalentVariable(String v, int numBits) {
		Vector<Integer> mindexUnprimed = new Vector<Integer>(numBits);
		Vector<Integer> mindexPrimed = new Vector<Integer>(numBits);
		for (int bit = numBits-1; 0 <= bit; bit--) {
			// declare unprimed bivalent variable v + "." + bit
			bivalentVariables.add(v + "." + bit);
			mindexUnprimed.add(0, bivalentVariables.size());
			Vector<Integer> mindex = new Vector<Integer>(1);
			mindex.add(bivalentVariables.size());
			multiindexTable.push(v + "." + bit, mindex);
			rangeUnprimed.add(bivalentVariables.size());
			universe.add(bivalentVariables.size());
			// declare primed bivalent variable v + "." + bit + "'"
			bivalentVariables.add(v + "." + bit + "'");
			mindexPrimed.add(0, bivalentVariables.size());
			mindex = new Vector<Integer>(1);
			mindex.add(bivalentVariables.size());
			multiindexTable.push(v + "." + bit + "'", mindex);
			rangePrimed.add(bivalentVariables.size());
			// declare variable renaming
			int indexUnprimed = bivalentVariables.size()-1;
			int indexPrimed = bivalentVariables.size();
			renamingPrimedToUnprimed.put(indexPrimed, indexUnprimed);
			renamingUnprimedToPrimed.put(indexUnprimed, indexPrimed);
			// create unprimed and primed version of the variable
			// in the underlying jdd package
			BDD.varBDD(indexUnprimed);
			BDD.varBDD(indexPrimed);
		}
		// declare unprimed multivalent variable v
		multiindexTable.push(v, mindexUnprimed);
		// declare primed multivalent variable v + "'"
		multiindexTable.push(v + "'", mindexPrimed);
		// update stateSpaceBDD and normalStates
		MultiBDD encoding = MultiBDD.varMultiBDD(mindexUnprimed, 0);
		MultiBDD encodingUpperBound = MultiBDD.intMultiBDD(getTypeOf(v).encodingUpperBound());
		BDD constraint = MultiBDD.leq(encoding, encodingUpperBound);
		stateSpaceBDD = BDD.and(stateSpaceBDD, constraint);
		normalStates = BDD.and(normalStates, constraint);
	}
	
    /**
     * Returns the number of all bivalent variables in the symbol manager
     * (primed or unprimed) including the error keyword.
     * @return the number of all bivalent variables in the symbol manager
     * (primed or unprimed) including the error keyword.
     */
    public int bivalentVariableCount() {
        return bivalentVariables.size();
    }
    
    /**
     * Returns the number of locally declared bivalent variables that are
     * currently stored in the symbol manager.
     * @return the number of locally declared bivalent variables that are
     * currently stored in the symbol manager.
     */
    public int localBivalentVariableCount() {
    	return localBivalentVariableCount;
    }
	
	/**
	 * Returns a vector that maps the (by 1 decremented) index of the primed or unprimed
     * version of a globally declared bivalent variable (including the error keyword)
     * to a string representation of this variable. In the context of this class,
     * the term "bivalent variable" stands both for boolean variables and binary digits
     * of the encoding of an integer or enum variable y. Examples of representations
     * are "error", "error'" "x", "x'", "x.3", or "x.3'".
     * The size of this vector is the number of all bivalent variables (including the
     * error keyword) that are spawning the state space (primed or unprimed).
     */
	public Vector<String> bivalentVariables() {
		return bivalentVariables;
	}
	
    /**
     * Returns a sorted set that contains the indices of all (globally declared)
     * unprimed bivalent variables in natural order.
     */
	public SortedSet<Integer> universe() {
		return universe;
	}
	
    /**
     * Returns a BDD representing the set of all valuations of unprimed bivalent
     * variables which belong to the state space.
     * Note: There may be illegal valuations of the binary digits of the encoding
     * of variables of type int or enum.
     */
    public BDD stateSpaceBDD() {
    	return stateSpaceBDD;
    }
    
    /**
     * Returns a BDD representing the logical conjunction of
     * the state-space-constraint and the no-error-constraint.
     */
    public BDD normalStates() {
    	return normalStates;
    }
	
    /**
     * Returns a vector that contains the string representations of all unprimed
     * typed variables (the vector does not contain binary digits of the encoding
     * of integer or enum variables). The order in which the string representations
     * occur in this vector determines the order of displaying by Valuation.printTyped().
     * The 'error' variable is at position 0. Next come the boolean variables
     * in ascending order of their indices. Next come the integer variable and
     * then the enum variables (in no specific order).
     */
	public Vector<String> displayingOrder() {
		return displayingOrder;
	}
	
    /**
     * Returns the multi-index which <tt>symbol</tt> is mapped to. If the variable
     * is of type boolean, the multi-index is a vector of size 1 containing the index
     * of the given boolean variable. If the variable is of type int or enum, the
     * multi-index is a vector 1) whose size is the number of binary digits necessary
     * to encode the values of the variable and 2) which maps a digit to the index
     * of the bivalent variable representing the given digit of the encoding of this
     * variable.
     * @param symbol the string representation of an unprimed or primed variable
     * of type boolean, int, or enum.
     * @return the multi-index which symbol is mapped to; null, if symbol is neither
     * declared as a variable nor as symbolic constant.
     */
	public Vector<Integer> getMultiIndex(String symbol) {
		return multiindexTable.top(symbol);
	}
	
    /**
     * Returns the BDDWB ("BDD with Bottom") which the bivalent variable
     * or boolean constant <tt>symbol</tt> stands for.
     * @param symbol the string representation of a bivalent variable or
     * a boolean constant.
     * @return the BDDWB which the symbol <tt>symbol</tt> stands for, if symbol
     * is either the string representation of a bivalent variable or a boolean constant;
     * null, if <tt>symbol</tt> is neither declared as a variable nor as a constant.
     * If <tt>symbol</tt> is declared as a variable or constant of type int or enum,
     * the return value of this method has no meaning.
     */
	public BDDWB getBDDWBForSymbol(String symbol) {
	   	Vector<Integer> mindex = multiindexTable.top(symbol);
        if (mindex != null) {
        	return new BDDWB(BDD.varBDD(mindex.get(0)), BDD.falseBDD());
        }
        MultiBDD mvalue = constantTable.get(symbol);
        if (mvalue != null) {
        	return new BDDWB(mvalue.get(0), mvalue.undef());
        }
        return null;
    }
	
  /**
   * Returns the MultiBDD which the variable or constant <tt>symbol</tt>
   * stands for, if <tt>symbol</tt> is declared to be of type int or enum.
   * 
   * @param symbol the string representation of variable or constant of
   * type int or enum
   * @return the MultiBDD which the variable or constant <tt>symbol</tt>
   * stands for, if <tt>symbol</tt> is declared to be of type int or enum
   * @throws RuntimeException, if <tt>symbol</tt> is a bivalent variable.
   * If <tt>symbol</tt> is a constant of type boolean, the return value
   * of this method has no meaning.
   */
  public MultiBDD getMultiBDDForSymbol(String symbol) {
    Vector<Integer> mindex = multiindexTable.top(symbol);
    if (mindex == null)
      return constantTable.get(symbol);

    Type symbolType = getTypeOf(symbol);
    if (symbolType.isInteger())
      return MultiBDD.varMultiBDD(mindex, symbolType.lowerBound());
    throw new RuntimeException("illegal type " + symbolType + " occurred in getMultiBDDForSymbol()");
  }
	
	/**
	 * Returns the type of the given symbol, if a type is declared; otherwise,
	 * returns null. The given symbol may be an unprimed or primed variable or
	 * a constant. For the error-variable, the boolean type is returned.
	 */
	public Type getTypeOf(String symbol) {
       	String unprimedSymbol = symbol;
    	boolean primed = symbol.endsWith("'");
        if (primed) { // remove prime
        	unprimedSymbol = symbol.substring(0, symbol.length()-1);
        }
        if (unprimedSymbol.equals(TemicParser.errorKeyword) ||
        	unprimedSymbol.contains(".")) {
        	return Type.booleanType();
        }
        Type ret = typeEnv.top(Identifier.variable(unprimedSymbol));
        if (ret == null) {
        	ret = typeEnv.top(Identifier.constant(unprimedSymbol));
        }
        return ret;
	}
	
    /**
     * Declares an unprimed variable <tt>variable</tt> (without primed sibling)
     * locally to the scope of an forall/exists quantifier. This method is called
     * by the evaluation methods of instances of NodeQuant, whenever the evaluation 
     * process enters the scope of an forall/exists quantifier.
     * @param variable the string representation of an unprimed local variable
     * @param type the type of the local variable
     * @returns the multi-index of the new locally declared variable
     */
    public Vector<Integer> declareLocalVariable(String variable, Type type) {
    	typeEnv.push(Identifier.variable(variable), type);
    	if (type.isBoolean()) {
			localBivalentVariableCount++;
			Vector<Integer> mindex = new Vector<>(1);
			mindex.add(bivalentVariables.size()+localBivalentVariableCount);
			multiindexTable.push(variable, mindex);
			return mindex;
    	}
    	if (type.isInteger() || type.isEnum()) {
			int numBits = type.numBits();
			Vector<Integer> mindex = new Vector<>(numBits);
			for (int bit = numBits-1; 0 <= bit; bit--) {
				localBivalentVariableCount++;
				mindex.add(0, bivalentVariables.size()+localBivalentVariableCount);
				Vector<Integer> mindexBit = new Vector<Integer>(1);
				mindexBit.add(bivalentVariables.size()+localBivalentVariableCount);
				multiindexTable.push(variable + "." + bit, mindexBit);
			}
			multiindexTable.push(variable, mindex);
			return mindex;
    	}
	throw new RuntimeException("illegal type " + type + " occurred in declareLocalVariable()");
    }
    
    /**
     * Undeclares an unprimed local variable <tt>variable</tt>. This method is called
     * by the evaluation methods of instances of NodeQuant, whenever the evaluation
     * process leaves the scope of an forall/exists quantifier. Local variables have
     * to be redeclared in reverse order of their declarations.
     * @param variable the string representation of an unprimed local variable
     */
    public void undeclareLocalVariable(String variable) {
    	Type type = getTypeOf(variable);
    	if (type.isBoolean()) {
		multiindexTable.pop(variable);
		localBivalentVariableCount--;
		typeEnv.pop(Identifier.variable(variable));
		return;
    	}
	if (type.isInteger() || type.isEnum()) {
		multiindexTable.pop(variable);
		int numBits = type.numBits();
		for (int bit = 0; bit < numBits; bit++) {
			localBivalentVariableCount--;
			multiindexTable.pop(variable + "." + bit);
		}
		typeEnv.pop(Identifier.variable(variable));
		return;
	}
	throw new RuntimeException("illegal type " + type + " occurred in declareLocalVariable()");
    }
    
    /**
     * The method declareConstant(String, MultiBDD) declares a symbolic constant
     * <tt>constant</tt> (of type int or enum) and assigns a MultiBDD <tt>value</tt>
     * to this constant.
     * Preconditions: There has been no call to declareVariable() or declareConstant()
     * for the symbol <tt>constant</tt> before.
     * @param constant the symbolic constant that should be declared.
     * @param value the instance of MultiBDD that should be assigned to <tt>constant</tt>
     * as its value.
     */
    public void declareConstant(String constant, MultiBDD value) {
    	constantTable.put(constant, value);
    }
    
    /**
     * The method declareConstant(String, BDDWB) declares a symbolic constant
     * <tt>constant</tt> (of type boolean) and assigns a BDDWB <tt>value</tt>
     * to this constant.
     * Preconditions: There has been no call to declareVariable() or declareConstant()
     * for the symbol <tt>constant</tt> before.
     * @param constant the symbolic constant that should be declared.
     * @param value the instance of BDDWB that should be assigned to <tt>constant</tt>
     * as its value.
     */
    
    public void declareConstant(String constant, BDDWB value) {
    	constantTable.put(constant, MultiBDD.castBDDWB(value));
    }
    
    /**
     * The method declareAgent() declares a named set of unprimed variables (= agent).
     * @param name the name of the agent.
     * @param symbols a set of unprimed variables (= agent).
     */
    public void declareAgent(String name, Set<String> symbols) {
    	agentTable.put(name, setOfIndicesFor(symbols));
    }
    
    /**
     * The method declareGroup() declares a named set of agents (= group).
     * @param name the name of the group.
     * @param agentNames a set of agent names.
     */
    public void declareGroup(String name, Set<String> agentNames) {
    	groupTable.put(name, setOfSetsOfIndicesFor(agentNames));
    }
    
    private Set<Integer> setOfIndicesFor(Set<String> symbols) {
    	HashSet<Integer> result = new HashSet<Integer>();
    	for (String symbol : symbols) {
    		Vector<Integer> mindex = multiindexTable.top(symbol);
    		if (mindex != null) {
    			for (int index : mindex) {
    				result.add(index);
    			}
    		}
    	}
    	return result;
    }
    
    private Set<Set<Integer>> setOfSetsOfIndicesFor(Set<String> agentnames) {
    	HashSet<Set<Integer>> result = new HashSet<Set<Integer>>();
    	for (String symbol : agentnames) {
    		result.add(agentTable.get(symbol));
    	}
    	return result;
    }
    
    public Set<Integer> getSetOfIndicesForAgent(String name) {
    	return agentTable.get(name);
    }

    public Set<Set<Integer>> getSetOfSetsOfIndicesForGroup(String name) {
    	return groupTable.get(name);
    }
    
    /**
     * Returns an implementation class of the VariableRenaming interface, which maps
     * indices of primed bivalent variables to indices of the corresponding unprimed
     * bivalent variables.
     * @return an implementation class of the VariableRenaming interface, which maps
     *         indices of primed bivalent variables to indices of the corresponding
     *         unprimed bivalent variables.
     */
    public VariableRenaming getPrimedToUnprimed() {
        return renamingPrimedToUnprimed;
    }
    
    /**
     * Returns an implementation class of the VariableRenaming interface, which maps
     * indices of unprimed bivalent variables to indices of the corresponding primed
     * bivalent variables.
     * @return an implementation class of the VariableRenaming interface, which maps
     *         indices of unprimed bivalent variables to indices of the corresponding
     *         bivalent primed variables.
     */
    public VariableRenaming getUnprimedToPrimed() {
        return renamingUnprimedToPrimed;
    }
    
    /**
     * Returns an implementation class of the VariableRange interface, which represents
     * the set of all unprimed bivalent variable indices (for exists/forall quantification).
     * @return an implementation class of the VariableRange interface, which represents
     * the set of all unprimed bivalent variable indices.
     */
    public VariableRange getRangeUnprimed() {
        return rangeUnprimed;
    }
    
    /**
     * Returns an implementation class of the VariableRange interface, which represents
     * the set of all primed bivalent variable indices (for exists/forall quantification).
     * @return an implementation class of the VariableRange interface, which represents
     * the set of all primed bivalent variable indices.
     */
    public VariableRange getRangePrimed() {
        return rangePrimed;
    }
    
    public void declareGuardExpression(String constant, Node formula) {
    	guardExpressions.add(formula);
    	guardConstants.add(constant);
    	guardIndexTable.put(constant,guardExpressions.size());
    }

    public Node getGuardExpressionForSymbol(String symbol) {
    	Integer index = guardIndexTable.get(symbol);
    	if (index != null) {
    		return guardExpressions.get(index-1);
    	}
    	else {
    		return null;
    	}
    }
    
    public int getIndexForSymbolTypeFormula(String symbol) {
    	return guardIndexTable.get(symbol);
    }
    
    /**
     * Returns the identity relation on S x S'
     * where S is the full state space that is spawned by the
     * set of bivalent variables (possibly including illegal states).
     * @return the identity relation on S x S'
     */
    public BDD identityRelation() {
    	if (identityRelation == null) {
    		identityRelation = BDD.trueBDD();
    		for (int index=1; index<=bivalentVariables.size(); index++) {
    			String variable = bivalentVariables.get(index-1);
    			boolean primed = variable.endsWith("'");
    			if (primed) {
    				// i.e. index is a primed variable index
    				int unprimedIndex = renamingPrimedToUnprimed.apply(index);
    				identityRelation = BDD.and(identityRelation, BDD.biimplies(BDD.varBDD(index), BDD.varBDD(unprimedIndex)));
    			}
    		}
    	}
    	return identityRelation;
    }
}
