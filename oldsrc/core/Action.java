package core;

import bdd.BDD;
import syntax.Node;


class Action {
  /** the name of this action */
  String name;
  /** the epistemic precondition of this action */
  Node epre;
  /** the index of this epistemically guarded action in vector guardedActions;
   *  or -1, if this action has no epistemic guard */
  int index;
  /** the canonical bdd that represents the standard precondition of this action */
  BDD preBDD;
  /** the canonical bdd that represents the postcondition of this action */
  BDD postBDD;
  /** the canonical bdd that represents the conjunction of the standard precondition
   *  and the postcondition of this action (= transition relation of this action if
   *  the epistemic precondition is null or evaluates to true) */
  BDD actionBDDTrue;

  Action(String name, Node epre, int index, BDD pre, BDD post) {
      this.name = name;
      this.epre = epre;
      this.index = (epre != null) ? index : -1;
      this.preBDD = pre;
      this.postBDD = post;
      this.actionBDDTrue = BDD.and(pre, post);
  }

  boolean isEpistemicallyGuarded() {
    return (0 <= index);
  }
}
