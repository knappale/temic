/**
 * Specification.java
 * 
 * Created on 5 April 2011
 * 
 */

package core;

import bdd.BDD;
import bdd.Universe;
import bdd.Valuation;
import syntax.Node;
import util.Formatter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.Vector;
import java.util.Hashtable;
import java.util.logging.Logger;

/**
 * The class Specification implements a temporal-epistemic specification 
 * of a Kripke structure. Elementary states of these Kripke structures
 * correspond to valuations of all declared unprimed variables
 * (fully qualified valuation).
 *
 * @author H. Muehlberger
 * @version 1.0
 */
public class Specification {
	/** the name of this specification */
    private String name;
    
    /** the symbol manager for this specification */
    private SymbolManager sm;
    
    /** the BDD that represents the set of all initial states */
    private BDD initialCondition;
    
    /** a vector that contains the names of all declared actions */
    private Vector<String> actionNames;
    
    /** a hashtable that maps names (of actions) to actions */
    private Hashtable<String,Action> actions;
    
    /** a vector that contains all epistemically guarded actions */
    private Vector<Action> guardedActions;
    
    /** the bdd that represents the (temporal) transition relation
     * if all epistemic preconditions are constantly false */
    private BDD tempRelationFalse;
    
    /** the bdd that represents the (temporal) transition relation
     * if all epistemic preconditions are constantly true */
    private BDD tempRelationTrue;
    
    /** the bdd that represents the set of all (temporal) deadlock states
     * if all epistemic preconditions are constantly false */
    private BDD deadlockStatesFalse;
    
    /** the bdd that represents the set of all (temporal) deadlock states
     * if all epistemic preconditions are constantly true */
    private BDD deadlockStatesTrue;
    
    /** a hashtable that maps an agent (= set of indices) to the canonical bdd
     * which represents the epistemic accessibility relation of this agent */
    private Hashtable<Set<Integer>,BDD> accRelationsK;
    
    /** a hashtable that maps a group (= set of sets of indices) to the canonical bdd
     * which represents the accessibility relation for universal knowledge in this group */
    private Hashtable<Set<Set<Integer>>,BDD> accRelationsE;
    
    /** a hashtable that maps a group (= set of sets of indices) to the canonical bdd
     * which represents the accessibility relation for distributed knowledge in this group */
    private Hashtable<Set<Set<Integer>>,BDD> accRelationsD;
    
	/** the logger for instances of class Specification */
	private static Logger logger = Logger.getLogger(Temic.class.getName());
    
    /**
     * Returns the name of this specification.
     * @return the name of this specification
     */
    public String getName() {
    	return name;
    }
    
    /**
     * Returns the symbol manager of this specification.
     * @return the symbol manager of this specification.
     */
    public SymbolManager sm() {
    	return sm;
    }
    
    /**
     * Returns the BDD that represents the set of all initial states
     * of this specification (initial condition).
     * @return the BDD that represents the set of all initial states
     *         of this specification
     */
    public BDD initialCondition() {
    	return initialCondition;
    }
    
    public Vector<String> actionNames() {
    	return actionNames;
    }
    
    public Action getAction(String name) {
    	return actions.get(name);
    }
    
    public Vector<Action> guardedActions() {
    	return guardedActions;
    }
    
    /**
     * Returns the bdd that represents the (temporal) transition relation
     * in the case that all epistemic preconditions are constantly false.
     * @return the bdd that represents the (temporal) transition relation
     * in the case that all epistemic preconditions are constantly false.
     */
    public BDD tempRelationFalse() {
        return tempRelationFalse;
    }
    
    /**
     * Returns the bdd that represents the (temporal) transition relation
     * in the case that all epistemic preconditions are constantly true.
     * @return the bdd that represents the (temporal) transition relation
     * in the case that all epistemic preconditions are constantly true.
     */
    public BDD tempRelationTrue() {
        return tempRelationTrue;
    }
    
    /**
     * Returns the bdd that represents the set of all (temporal) deadlock states
     * in the case that all epistemic preconditions are constantly false.
     * @return the bdd that represents the set of all (temporal) deadlock states
     * in the case that all epistemic preconditions are constantly false.
     */
    public BDD deadlockStatesFalse() {
    	return deadlockStatesFalse;
    }
    
    /**
     * Returns the bdd that represents the set of all (temporal) deadlock states
     * in the case that all epistemic preconditions are constantly true.
     * @return the bdd that represents the set of all (temporal) deadlock states
     * in the case that all epistemic preconditions are constantly true.
     */
    public BDD deadlockStatesTrue() {
    	return deadlockStatesTrue;
    }
    
    @SuppressWarnings("unused")
	private Specification() {
    }
    
    /**
     * Creates a new specification with name <tt>name</tt> and
     * symbol manager <tt>sm</tt>.
     * @param name the name of the new specification.
     * @param sm the symbol manager of this specification.
     */
    public Specification(String name, SymbolManager sm) {
    	this.name = name;
    	this.sm = sm;
    	this.actionNames = new Vector<String>();
    	this.actions = new Hashtable<String,Action>();
	    this.guardedActions = new Vector<Action>();
	    // BDDWB errorStates = sm.getBDDWBForSymbol(TemicParser.errorKeyword);
    	// this.initialCondition = BDD.and(sm.stateSpaceBDD(),BDD.not(errorStates.getBDD()));
    	this.initialCondition = sm.normalStates();
	    this.tempRelationFalse = BDD.falseBDD();
	    this.tempRelationTrue = BDD.falseBDD();
	    this.deadlockStatesFalse = sm.stateSpaceBDD();
	    this.deadlockStatesTrue = sm.stateSpaceBDD();
	    this.accRelationsK = new Hashtable<Set<Integer>,BDD>();
	    this.accRelationsE = new Hashtable<Set<Set<Integer>>,BDD>();
	    this.accRelationsD = new Hashtable<Set<Set<Integer>>,BDD>();
    	logger.fine("specification '" + name + "' created.");
    }
    
    /**
     * Adds a constraint to the set of all initial states of this specification
     * (initial condition). To "add a constraint" means that the actual parameter
     * for <tt>initialCondition</tt> and the previously stored initial condition are
     * combined by a logical and-operation to result in the new initial condition.
     * @param initialCondition the bdd that specifies a constraint for the set
     *        of all initial states of this specification.
     */
    public void addInitialCondition(BDD constraint) {
        this.initialCondition = BDD.and(this.initialCondition, constraint);
        logger.fine("initial condition " + constraint + " added to specification.");
    }
    
    /**
     * Declares a named action with epistemic precondition <tt>epre</tt>,
     * standard precondition <tt>pre</tt>, and postcondition <tt>post</tt>.
     * If <tt>epre == null</tt>, a standard action (without epistemic precondition)
     * is declared. If an action with name <tt>name</tt> has already been declared,
     * the method call is ignored.
     * @param name the name of the action
     * @param epre the epistemic precondition of the action as a boolean combination
     *        of guard identifiers; null, if the action has no epistemic precondition.
     * @param pre the standard precondition of the action
     *        (including no-error-constraint and encoding-constraint)
     * @param post the postcondition of the action
     */
    public void addAction(String name, Node epre, BDD pre, BDD post) {
    	if (!actions.containsKey(name)) {
    		actionNames.add(name);
            Action action = new Action(name, epre, guardedActions.size(), pre, post);
            actions.put(name, action);
            tempRelationTrue = BDD.or(tempRelationTrue, action.actionBDDTrue);
            deadlockStatesTrue = BDD.and(deadlockStatesTrue, BDD.not(pre));
            if (epre != null) {
            	guardedActions.add(action);
            }
            else { // epre == null
            	tempRelationFalse = BDD.or(tempRelationFalse, action.actionBDDTrue);
            	deadlockStatesFalse = BDD.and(deadlockStatesFalse, BDD.not(pre));
            }
            logger.fine("action '" + action.name + "' added to specification:\n" +
            	"epre: " + epre + "\n" +
            	"pre:  " + pre + "\n" +
            	"post: " + post);
        }
    }
    
    /**
     * Tests if an action with name <tt>name</tt> has been declared for
     * this specification.
     * @param name a name of an action
     * @return <tt>true</tt> if and only if an action with name <tt>name</tt>
     *         has been declared for this specification; <tt>false</tt> otherwise.
     */
    public boolean containsAction(String name) {
    	return (getActionBDDTrue(name) == null) ? false : true;
    }
     
    /**
     * Returns the canonical bdd that represents the conjunction of the
     * standard precondition and the postcondition of the action
     * with name <tt>name</tt> ("action bdd"), if an action with
     * name <tt>name</tt> has been declared.
     * @param name a name of an action
     * @return the canonical bdd that represents the conjunction of the standard precondition
     *         and the postcondition of the action with name <tt>name</tt>, if an action
     *         with name <tt>name</tt> has been declared; <tt>null</tt> otherwise.
     */
    public BDD getActionBDDTrue(String name) {
    	Action action = actions.get(name);
    	return (action != null) ? action.actionBDDTrue : null;
    }

    public BDD getRelationK(Set<Integer> indices) {
    	BDD relationK = accRelationsK.get(indices);
    	if (relationK == null) {
    		relationK = BDD.trueBDD();
    		for (int index : indices) {
    			relationK = BDD.and(relationK, BDD.biimplies(BDD.varBDD(index), BDD.varBDD(sm.getUnprimedToPrimed().apply(index))));
    		}
    		// relationK = BDD.and(sm.stateSpaceBDD_x_stateSpaceBDD(), relationK);
    		accRelationsK.put(indices, relationK);
    	}
    	return relationK;
    }
     
    public BDD getRelationE(Set<Set<Integer>> agents) {
    	BDD relationE = accRelationsE.get(agents);
    	if (relationE == null) {
    		relationE = BDD.falseBDD();
    		for (Set<Integer> agent : agents) {
    			relationE = BDD.or(relationE, getRelationK(agent));
    		}
    		accRelationsE.put(agents, relationE);
    	}
    	return relationE;
    }
     
    public BDD getRelationD(Set<Set<Integer>> agents) {
    	BDD relationD = accRelationsD.get(agents);
    	if (relationD == null) {
    		relationD = BDD.trueBDD(); // sm.stateSpaceBDD_x_stateSpaceBDD();
    		for (Set<Integer> agent : agents) {
    			relationD = BDD.and(relationD, getRelationK(agent));
    		}
    		accRelationsD.put(agents, relationD);
    	}
    	return relationD;
    }
     
    public BDD preImageRelationK(String name, BDD set) {
    	Set<Integer> indices = sm.getSetOfIndicesForAgent(name);
    	if (indices != null) {
    		BDD relationK = getRelationK(indices);
    		BDD setPrimed = BDD.renamedCopy(set, sm.getUnprimedToPrimed());
    		BDD preImage = BDD.exists(BDD.and(setPrimed, relationK), sm.getRangePrimed());
    		return BDD.and(preImage, sm.stateSpaceBDD());
    	}
    	else {
    		return null;
    	}
    }
    
    public BDD imageRelationK(String name, BDD set) {
    	Set<Integer> indices = sm.getSetOfIndicesForAgent(name);
    	if (indices != null) {
    		BDD relationK = getRelationK(indices);
    		BDD imagePrimed = BDD.exists(BDD.and(set, relationK), sm.getRangeUnprimed());
            BDD image = BDD.renamedCopy(imagePrimed, sm.getPrimedToUnprimed());
            return BDD.and(image, sm.stateSpaceBDD());
    	}
    	else {
    		return null;
    	}
    }
     
    public BDD preImageRelationE(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationE = getRelationE(agents);
    		BDD setPrimed = BDD.renamedCopy(set, sm.getUnprimedToPrimed());
    		BDD preImage = BDD.exists(BDD.and(setPrimed, relationE), sm.getRangePrimed());
            return BDD.and(preImage, sm.stateSpaceBDD());
    	}
    	else {
    		return null;
    	}
    }
    
    public BDD imageRelationE(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationE = getRelationE(agents);
            BDD imagePrimed = BDD.exists(BDD.and(set, relationE), sm.getRangeUnprimed());
            BDD image = BDD.renamedCopy(imagePrimed, sm.getPrimedToUnprimed());
            return BDD.and(image, sm.stateSpaceBDD());
        }
        else {
            return null;
        }
    }   
    
    public BDD preImageRelationD(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationD = getRelationD(agents);
            BDD setPrimed = BDD.renamedCopy(set, sm.getUnprimedToPrimed());
            BDD preImage = BDD.exists(BDD.and(setPrimed, relationD), sm.getRangePrimed());
            return BDD.and(preImage, sm.stateSpaceBDD());
        }
        else {
            return null;
        }
    }
    
    public BDD imageRelationD(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationD = getRelationD(agents);
            BDD imagePrimed = BDD.exists(BDD.and(set, relationD), sm.getRangeUnprimed());
            BDD image = BDD.renamedCopy(imagePrimed, sm.getPrimedToUnprimed());
            return BDD.and(image, sm.stateSpaceBDD());
        }
        else {
            return null;
        }
    }
    
    public Vector<BDD> combTrue() {
    	Vector<BDD> combTrue = new Vector<BDD>();
    	for (int i=0; i<guardedActions.size(); i++) {
    		combTrue.add(BDD.trueBDD());
    	}
    	return combTrue;
    }
     
    public Vector<BDD> combFalse() {
    	Vector<BDD> combFalse = new Vector<BDD>();
    	for (int i=0; i<guardedActions.size(); i++) {
    		combFalse.add(BDD.falseBDD());
    	}
    	return combFalse;
    }

    /*
 	public Interpretation interpret(PrintWriter out) {
    	PNStructure pnstruct = PNStructure.bottom(this);
    	PNStructure pnstructNew = null;
    	KripkeStructure ksPositive = null;
    	KripkeStructure ksNegative_c = null;
    	
    	boolean fixedPointFound = false;
    	int numberIterations = 0;
    	while (!fixedPointFound) {
    		out.println("executing iteration no " + (numberIterations+1));
    		logger.info("executing iteration no " + (numberIterations+1));
    		Vector<BDD> combPositive = new Vector<BDD>();
    		Vector<BDD> combNegative_c = new Vector<BDD>();
    		for (Action action : guardedActions) {
    			logger.info("evaluating epre-clause for action '" + action.name + "'");
    			combPositive.add(action.epre.evaluatePositive(pnstruct));
    			combNegative_c.add(BDD.and(BDD.not(action.epre.evaluateNegative(pnstruct)), sm.stateSpaceBDD()));
    		}
    		logger.fine("creating ksPositive");
    		ksPositive = KripkeStructure.kripkeStructure(this, combPositive, true);
    		logger.fine("creating ksNegative_c");
    		ksNegative_c = KripkeStructure.kripkeStructure(this, combNegative_c, true);
    		logger.fine("creating pnstructNew");
    		pnstructNew = new PNStructure(ksPositive.reachableStates(),
    			BDD.and(BDD.not(ksNegative_c.reachableStates()),sm.stateSpaceBDD()), this);
    		if (!pnstruct.leq(pnstructNew)) {
    			throw new RuntimeException("monotonicity property of core functional violated.");
    		}
    		if (pnstruct.equals(pnstructNew)) {
    			fixedPointFound = true;
    		}
    		pnstruct = pnstructNew;
    		numberIterations++;
    	}
    	
    	return new Interpretation(pnstruct, ksPositive, ksNegative_c, numberIterations);
    }
    */
    
    public Interpretation interpret(OptionSet options) {
    	PNStructure pnstruct = PNStructure.bottom(this);
    	PNStructure pnstructNew = null;
    	KripkeStructure ksPositive = null;
    	KripkeStructure ksNegative_c = null;
    	BDD reachPositive = null;
    	BDD reachNegative_c = null;
    	
    	boolean fixedPointFound = false;
    	int numberIterations = 0;
    	while (!fixedPointFound) {
    		logger.info("executing iteration no " + (numberIterations+1));
    		
    		Vector<BDD> combPositive = new Vector<BDD>();
    		Vector<BDD> combNegative_c = new Vector<BDD>();
    		for (Action action : guardedActions) {
    			logger.info("evaluating epre-clause for action '" + action.name + "'");
    			var positive = action.epre.evaluatePositive(pnstruct, options.rule1());
    			combPositive.add(positive);
    			var notNegative = BDD.and(BDD.not(action.epre.evaluateNegative(pnstruct, options.rule1())), sm.stateSpaceBDD());
    			combNegative_c.add(notNegative);
    		}
    		
    		logger.fine("creating ksPositive");
    		ksPositive = KripkeStructure.kripkeStructure(this, combPositive, true);
    		reachPositive = ksPositive.reachableStates();
    		ksPositive = null;
    		
    		logger.fine("creating ksNegative_c");
    		ksNegative_c = KripkeStructure.kripkeStructure(this, combNegative_c, true);
    		var t = new PrintWriter(System.out);
    		ksNegative_c.printSummaryInfo(t);
    		t.flush();
    		reachNegative_c = ksNegative_c.reachableStates();
    		
    		logger.fine("creating pnstructNew");
    		pnstructNew = new PNStructure(reachPositive, BDD.and(BDD.not(reachNegative_c),sm.stateSpaceBDD()), this);
    		
    		if (!pnstruct.leq(pnstructNew)) {
    			throw new RuntimeException("monotonicity property of core functional violated.");
    		}
    		if (pnstructNew.isContradictory()) {
    			throw new RuntimeException("consistency property of core functional violated.");
    		}
    		
    		if (pnstruct.equals(pnstructNew)) {
    			fixedPointFound = true;
    		}
    		pnstruct = pnstructNew;
    		numberIterations++;
    	}
    	
    	ksNegative_c.setName("unique interpretation");
    	return new Interpretation(pnstruct, ksPositive, ksNegative_c, numberIterations);
    }

  private void printTypedSatisfyingValuations(BDD bdd, Universe vd, int limit, PrintWriter out) throws IOException {
    if (bdd.isFalse()) {
      out.println("none");
      return;
    }

    int count = 0;
    int limitDigits = ("" + limit).length();
    while (bdd != BDD.falseBDD() && (limit <= 0 || count < limit)) {
      out.write("no " + Formatter.format(++count, limitDigits) + ". ");
      Valuation val = bdd.satisfyingValuation();
      val = val.specifyDontCares(vd.universe());
      val.printTyped(out, vd);
      bdd = BDD.and(bdd, BDD.not(val.asBDD()));
    }
    if (bdd != BDD.falseBDD()) {
      double numSatVals = bdd.countSatVals(vd.universe());
      out.write("(" + (numSatVals - limit) + " more)\n");
    }
  }

  public void printSubsidiaryInfo(PNStructure pnstruct, PrintWriter out, OptionSet options) {
    // print the set P
    out.println("Set P of definitely reachable states:");
    try {
      printTypedSatisfyingValuations(pnstruct.getP(), sm, options.outputLimit(), out);
    }
    catch(IOException e) {}
    out.println();
		
    // print the set Nc
    out.println("Set Nc of possibly reachable states:");
    try {
      printTypedSatisfyingValuations(pnstruct.getNc(), sm, options.outputLimit(), out);
    }
    catch(IOException e) {}
    out.println();
    	
    for (Action action : guardedActions) {
      out.println("Definitely reachable pre-states of epistemically guarded action '" + action.name + "'");
      out.println("for which the truth value of the epistemic guard is unknown:");
      BDD eprePositive = action.epre.evaluatePositive(pnstruct, options.rule1());
      BDD epreNegative = action.epre.evaluateNegative(pnstruct, options.rule1());
      BDD res = BDD.and(pnstruct.getP(), action.preBDD);
      res = BDD.and(res, BDD.not(eprePositive));
      res = BDD.and(res, BDD.not(epreNegative));
      try {
        printTypedSatisfyingValuations(res, sm, options.outputLimit(), out);
      }
      catch(IOException e) {}
    }
    out.println();
  }
}
