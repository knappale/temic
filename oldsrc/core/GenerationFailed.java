package core;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class GenerationFailed extends Exception {
  private static final long serialVersionUID = 1L;

  public GenerationFailed(String message) {
    super(message);
  }
}
