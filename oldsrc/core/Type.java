package core;

import java.util.Vector;
import java.util.Set;
import java.util.HashSet;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Class Type represents the types of syntactic objects of the input language of
 * TEMIC.
 * 
 * @author H. Muehlberger
 * @version 1.1
 * @since 2011-04-05
 */
@NonNullByDefault
public class Type {
  private enum Kind {
    VOID, BOOL, INT, ENUM, ERROR;

    public String toString() {
      switch (this) {
        case VOID:
          return "void";
        case BOOL:
          return "boolean";
        case INT:
          return "int";
        case ENUM:
          return "enum";
        case ERROR:
          return "error";
        default:
          throw new RuntimeException("illegal type id in toString()");
      }
    }
  }

  /** the kind identifies the rough base type, i.e. VOID, BOOL, INT, ... */
  private Kind kind;
  /** the lower bound of the value range, if this type is a bounded integer */
  private int lowerBound;
  /** the upper bound of the value range, if this type is a bounded integer */
  private int upperBound;
  /** the set of indices of enum values belonging to this type, if this type is an enum type */
  private Set<Integer> indices;
  /** a static vector of enum literals */
  private static Vector<String> allLiterals = new Vector<>();
  /** the unique void type */
  private static Type voidType = new Type(Kind.VOID, 0, 0, new HashSet<>());
  /** the unique boolean type */
  private static Type booleanType = new Type(Kind.BOOL, 0, 1, new HashSet<>());
  /** the logger for instances of class Temic */
  private static Logger logger = util.Objects.assertNonNull(Logger.getLogger(Temic.class.getName()));

  /**
   * Private constructor for types that is called by static factory methods.
   * 
   * @param typeId the type id identifies the rough base type.
   * @param lowerBound the lower bound of the value range, if this type is a bounded
   *          integer; 0, otherwise.
   * @param upperBound the upper bound of the value range, if this type is a bounded
   *          integer; 0, otherwise.
   * @param indices a hashset containing the indices of the enum values (= literals) of
   *          this type, if this type is an enum type; null, otherwise.
   */
  private Type(Kind typeId, int lowerBound, int upperBound, Set<Integer> indices) {
    this.kind = typeId;
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
    this.indices = indices;
  }

  public static Type voidType() {
    return voidType;
  }

  public static Type booleanType() {
    return booleanType;
  }

  public static Type integerType(int lowerBound, int upperBound) {
    if (lowerBound <= upperBound)
      return new Type(Kind.INT, lowerBound, upperBound, new HashSet<>());
    return new Type(Kind.INT, 0, -1, new HashSet<>());
  }

  public static Type emptyIntegerType() {
    return new Type(Kind.INT, 0, -1, new HashSet<>());
  }

  public static Type singletonIntegerType(int value) {
    return new Type(Kind.INT, value, value, new HashSet<>());
  }

  public static Type enumType(Vector<String> literals) {
    Set<Integer> indices = new HashSet<>();
    for (String literal : literals) {
      int index = allLiterals.indexOf(literal);
      if (index < 0) {
        allLiterals.add(literal);
        index = allLiterals.size()-1;
      }
      if (!indices.contains(index))
        indices.add(index);
      else
        logger.warning("redundant specification of value '" + literal + "'");
    }
    return new Type(Kind.ENUM, 0, 0, indices);
  }

  public static Type singletonEnumType(String literal) {
    Vector<String> literals = new Vector<String>();
    literals.add(literal);
    return enumType(literals);
  }

  /**
   * @return true iff this type is the void type
   */
  public boolean isVoid() {
    return (this.kind == Kind.VOID);
  }

  /**
   * @return true iff this type is the boolean type
   */
  public boolean isBoolean() {
    return (this.kind == Kind.BOOL);
  }

  /**
   * @return true iff this type is a bounded integer type
   */
  public boolean isInteger() {
    return (this.kind == Kind.INT);
  }

  /**
   * @return true iff this type is an enumeration type
   */
  public boolean isEnum() {
    return (this.kind == Kind.ENUM);
  }

  /**
   * @return true iff this type is empty
   */
  public boolean isEmpty() {
    return (this.size() == 0);
  }

  /**
   * @return the size of this type
   */
  public int size() {
    switch (kind) {
      case VOID:
        return 0;
      case BOOL:
        return 2;
      case INT:
        return 1 + upperBound - lowerBound;
      case ENUM:
        return indices.size();
      default:
        throw new RuntimeException("unknown type " + this + " during call of size()");
    }
  }

  /**
   * Returns the number of binary digits that are necessary for encoding values of
   * this type.
   * 
   * @pre this.size() > 0
   * @return the number of binary digits that are necessary for encoding values of
   *         this type.
   */
  public int numBits() {
    int encodingUpperBound = this.encodingUpperBound();
    if (encodingUpperBound >= 0)
      return util.Math.ceil(util.Math.log2(encodingUpperBound + 1));
    throw new IllegalArgumentException("numBits() called for empty type");
  }

  /**
   * Returns the maximum value of the encoding of values of this type. In the case
   * of an empty bounded integer or a void type, -1 is returned. In the case of an
   * enum type, the number of encodings is determined by the number of different
   * enum literals occurring in the specification. In the case of the boolean
   * type, 1 is returned. The minimum value of an encoding is always 0.
   * 
   * @return the maximum value of the encoding of values of this type
   */
  public int encodingUpperBound() {
    switch (kind) {
      case VOID:
        return -1;
      case BOOL:
      case INT:
        return this.size()-1;
      case ENUM:
        return allLiterals.size()-1;
      default:
        throw new RuntimeException("unknown typeId '" + kind + "' during call of encodingUpperBound()");
    }
  }

  /**
   * Returns the minimum value of this type, if this type is bounded integer.
   * Returns 0, if this type is boolean.
   * 
   * @return the minimum value of this type, if this type is bounded integer; or
   *         0, if this type is boolean
   * @throws IllegalArgumentException
   *           if this type is neither bounded integer nor boolean
   */
  public int lowerBound() {
    if (this.kind == Kind.INT || this.kind == Kind.BOOL)
      return this.lowerBound;
    throw new IllegalArgumentException("lowerBound() called for type " + this.kind);
  }

  /**
   * Returns the maximum value of this type, if this type is bounded integer.
   * Returns 1, if this type is boolean.
   * 
   * @return the maximum value of this type, if this type is bounded integer; or
   *         1, if this type is boolean
   * @throws IllegalArgumentException
   *           if this type is neither bounded integer nor boolean
   */
  public int upperBound() {
    if (kind == Kind.INT || kind == Kind.BOOL)
      return this.upperBound;
    throw new IllegalArgumentException("upperBound() called for type " + this.kind);
  }

  /**
   * Determine whether this type is an extensional sub-type of another type.
   *
   * @param other another type
   * @return whether this type is an extensional sub-type of {@link other}
   */
  public boolean subsets(Type other) {
    if (this.kind != other.kind)
      return false;

    switch (this.kind) {
      case VOID:
        return true;
      case BOOL:
        return true;
      case INT:
        return this.isEmpty() || ((other.lowerBound <= this.lowerBound) && (this.upperBound <= other.upperBound));
      case ENUM:
        return other.indices.containsAll(this.indices);
      default:
        throw new RuntimeException("unknown type during call of subsets()");
    }
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      Type other = (Type)object;
      if (this.kind != other.kind)
        return false;
      switch (this.kind) {
        case VOID:
          return true;
        case BOOL:
          return true;
        case INT:
          return (this.lowerBound == other.lowerBound) && (this.upperBound == other.upperBound);
        case ENUM:
          return (this.subsets(other) && other.subsets(this));
        default:
          throw new RuntimeException("unknown typeId during call of equals()");
      }
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    switch (this.kind) {
      case VOID:
        return "void";
      case BOOL:
        return "boolean";
      case INT: {
        StringBuilder builder = new StringBuilder();
        builder.append(this.lowerBound);
        builder.append("..");
        builder.append(this.upperBound);
        return builder.toString();
      }
      case ENUM: {
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        String sep = " ";
        for (int index : this.indices) {
          builder.append(sep);
          builder.append(allLiterals.get(index));
          sep = ", ";
        }
        return builder.append(" }").toString();
      }
      default:
        throw new RuntimeException("unknown typeId during call of toString()");
    }
  }
}
