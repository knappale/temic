package core;

import bdd.BDD;
import bdd.BDDWB;
import bdd.MultiBDD;
import bdd.Valuation;
import util.Pair;
import util.Ref;

import java.util.Set;
import java.util.Stack;
import java.util.Vector;
import java.util.Iterator;
import java.util.logging.Logger;

import java.io.PrintWriter;

import org.eclipse.jdt.annotation.Nullable;


/**
 * Class KripkeStructure.
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-05-22
 */
public class KripkeStructure {
  private Specification spec;
  private SymbolManager sm;
	
  /** the name of this Kripke structure */
  private String name;
	
  /** the combination which this Kripke structure is calculated from;
    *  i.e. a vector of bdds that defines for each action in spec.guardedActions()
    *  the set of states in which the epistemic precondition of the action is true. */
  private Vector<BDD> combination;
	
  /** the bdd that represents the total (temporal) transition relation */
  private BDD tempRelation;
    
  /** the bdd that represents the set of all (temporally) reachable states */
  private BDD reachableStates;
    
  /** The bdd that represents the set of all deadlock states, if this Kripke
   *  structure is not serialized; otherwise, the bdd that represents the set
   *  of all states with an implicit temporal self loop (= lambda states). */
  private BDD deadlockStates;
	
  /** null, if this Kripke structure is not serialized; otherwise, the bdd
   * that represents the relation of all implicit temporal self loops. */
  private BDD lambdaRelation;
    
  /** system diameter: number of iterations in reachableStates() */
  private int diameter;
	
  /** a flag that indicates whether deadlock states have been eliminated
   * by attaching self-loops to them.  */
  private boolean serialized;
    
  /** the logger for instances of class KripkeStructure */
  private static Logger logger = Logger.getLogger(Temic.class.getName());

  /**
   * @return the name of this Kripke structure.
   */
  public String getName() {
    return name;
  }
    
  /**
   * Sets the name of this Kripke structure.
   *
   * @param name the name of this Kripke structure.
   */
  public void setName(String name) {
    this.name = name;
  }
    
  /**
   * @return the bdd that represents the set of all temporally reachable states.
   */
  public BDD reachableStates() {
    return reachableStates;
  }
    
    /**
     * Returns the bdd that represents the set of all temporally reachable
     * deadlock/lambda states.
     * @return the bdd that represents the set of all temporally reachable
     * deadlock/lambda states.
     */
    public BDD reachableDeadlockStates() {
    	return BDD.and(reachableStates, deadlockStates);
    }
    
    /**
     * Returns the bdd that represents the preimage of the set of states,
     * which is described by <tt>post</tt>, under the temporal transition
     * relation of this Kripke structure.
     * @param post a bdd that represents a set of states
     * @return the bdd that represents the preimage of the set of states,
     *         which is represented by <tt>post</tt>, under the temporal
     *         transition relation of this Kripke structure
     */
    public BDD preImageTemporal(BDD post) {
        BDD primedPostState = BDD.renamedCopy(post, sm.getUnprimedToPrimed());
        return BDD.exists(BDD.and(primedPostState, tempRelation), sm.getRangePrimed());
    }
    
    /**
     * Returns the bdd that represents the preimage of the set of states,
     * which is described by <tt>post</tt>, under that part of the temporal
     * transition relation which belongs to the action <tt>name</tt>.
     * If no action with name <tt>name</tt> has been declared, then
     * a RuntimeException is thrown.
     * @param post a bdd that represents a set of states
     * @param name a name of an action
     * @return the bdd that represents the preimage of the set of states, which
     *         is represented by <tt>post</tt>, under that part of the temporal
     *         transition relation which belongs to the action <tt>name</tt>,
     *         if an action with name <tt>name</tt> has been declared.
     * @return RuntimeException is no action with <tt>name</tt> has been declared.
     */
    public BDD preImageUnderAction(BDD post, String name) {
        Action action = spec.getAction(name);
        if (action != null) {
            BDD actionBDD = null;
        	if (action.isEpistemicallyGuarded()) {
        		actionBDD = BDD.and(action.actionBDDTrue, combination.get(action.index));
        	}
        	else { // action without epistemic guard
        		actionBDD = action.actionBDDTrue;
        	}
            BDD primedPostState = BDD.renamedCopy(post, sm.getUnprimedToPrimed());
            BDD ancestorState = BDD.exists(BDD.and(primedPostState, actionBDD), sm.getRangePrimed());
            return ancestorState;
        }
        else {
            throw new RuntimeException("An action with name '" + name + "' has not been declared.");
        }
    }   
    
    /**
     * Returns the bdd that represents the image of the set of states,
     * which is described by <tt>pre</tt>, under the temporal transition
     * relation of this Kripke structure.
     * @param pre a bdd that represents a set of states
     * @return the bdd that represents the image of the set of states,
     *         which is represented by <tt>pre</tt>, under the temporal
     *         transition relation of this Kripke structure
     */    
    public BDD imageTemporal(BDD pre) {
        BDD primedPostState = BDD.exists(BDD.and(pre, tempRelation), sm.getRangeUnprimed());
        return BDD.renamedCopy(primedPostState, sm.getPrimedToUnprimed());
    }
    
    /**
     * Returns the bdd that represents the image of the set of states,
     * which is described by <tt>pre</tt>, under that part of the temporal
     * transition relation which belongs to the action <tt>name</tt>.
     * If no action with name <tt>name</tt> has been declared, then
     * a RuntimeException is thrown.
     * @param pre a bdd that represents a set of states
     * @param name a name of an action
     * @return the bdd that represents the image of the set of states, which
     *         is represented by <tt>pre</tt>, under that part of the temporal
     *         transition relation which belongs to the action <tt>name</tt>,
     *         if an action with name <tt>name</tt> has been declared.
     * @return RuntimeException is no action with <tt>name</tt> has been declared.
     */
    public BDD imageUnderAction(BDD pre, String name) {
        Action action = spec.getAction(name);
        if (action != null) {
            BDD actionBDD = null;
        	if (action.isEpistemicallyGuarded()) {
        		actionBDD = BDD.and(action.actionBDDTrue, combination.get(action.index));
        	}
        	else { // action without epistemic guard
        		actionBDD = action.actionBDDTrue;
        	}
        	BDD primedSuccessorState = BDD.exists(BDD.and(pre, actionBDD), sm.getRangeUnprimed());	
        	BDD successorState = BDD.renamedCopy(primedSuccessorState, sm.getPrimedToUnprimed());
        	return successorState;
        }
        else {
            throw new RuntimeException("An action with name '" + name + "' has not been declared.");
        }
    }
    
    /**
     * Returns the bdd that represents the preimage of <tt>set</tt> under the
     * epistemic accessibility relation of the agent with name <tt>name</tt>.
     * The accessibility relation used for calculating the preimage is defined
     * on the full state space spawned by all bivalent variables (including
     * illegal states).
     * @param name the name of an agent.
     * @param set a bdd that represents a set of states.
     * @return the bdd representing the preimage of <tt>set</tt> under the
     * epistemic accessibility relation of agent <tt>name</tt>. 
     */
    public BDD preImageRelationK(String name, BDD set) {
    	Set<Integer> indices = sm.getSetOfIndicesForAgent(name);
    	if (indices != null) {
    		BDD relationK = getRelationK(indices);
    		BDD setPrimed = BDD.renamedCopy(set, sm.getUnprimedToPrimed());
    		BDD preImage = BDD.exists(BDD.and(setPrimed, relationK), sm.getRangePrimed());
    		return BDD.and(preImage, reachableStates);
    	}
    	else {
    		return null;
    	}
    }
    
    /**
     * Returns the bdd that represents the image of <tt>set</tt> under the
     * epistemic accessibility relation of the agent with name <tt>name</tt>.
     * The accessibility relation used for calculating the image is defined
     * on the full state space spawned by all bivalent variables (including
     * illegal states).
     * @param name the name of an agent.
     * @param set a bdd that represents a set of states.
     * @return the bdd representing the image of <tt>set</tt> under the
     * epistemic accessibility relation of agent <tt>name</tt>. 
     */
    public BDD imageRelationK(String name, BDD set) {
    	Set<Integer> indices = sm.getSetOfIndicesForAgent(name);
    	if (indices != null) {
    		BDD relationK = getRelationK(indices);
    		BDD imagePrimed = BDD.exists(BDD.and(set, relationK), sm.getRangeUnprimed());
            BDD image = BDD.renamedCopy(imagePrimed, sm.getPrimedToUnprimed());
            return BDD.and(image, reachableStates);
    	}
    	else {
    		return null;
    	}
    }
    
    public BDD preImageRelationE(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationE = getRelationE(agents);
    		BDD setPrimed = BDD.renamedCopy(set, sm.getUnprimedToPrimed());
    		BDD preImage = BDD.exists(BDD.and(setPrimed, relationE), sm.getRangePrimed());
            return BDD.and(preImage, reachableStates);
    	}
    	else {
    		return null;
    	}
    }
    
    public BDD imageRelationE(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationE = getRelationE(agents);
            BDD imagePrimed = BDD.exists(BDD.and(set, relationE), sm.getRangeUnprimed());
            BDD image = BDD.renamedCopy(imagePrimed, sm.getPrimedToUnprimed());
            return BDD.and(image, reachableStates);
        }
        else {
            return null;
        }
    }
    
    public BDD preImageRelationD(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationD = getRelationD(agents);
            BDD setPrimed = BDD.renamedCopy(set, sm.getUnprimedToPrimed());
            BDD preImage = BDD.exists(BDD.and(setPrimed, relationD), sm.getRangePrimed());
            return BDD.and(preImage, reachableStates);
        }
        else {
            return null;
        }
    }
    
    public BDD imageRelationD(String name, BDD set) {
    	Set<Set<Integer>> agents = sm.getSetOfSetsOfIndicesForGroup(name);
    	if (agents != null) {
    		BDD relationD = getRelationD(agents);
            BDD imagePrimed = BDD.exists(BDD.and(set, relationD), sm.getRangeUnprimed());
            BDD image = BDD.renamedCopy(imagePrimed, sm.getPrimedToUnprimed());
            return BDD.and(image, reachableStates);
        }
        else {
            return null;
        }
    }
    
    private BDD getRelationK(Set<Integer> indices) {
    	return spec.getRelationK(indices);
    }
    
    private BDD getRelationE(Set<Set<Integer>> agents) {
    	return spec.getRelationE(agents);
    }
    
    private BDD getRelationD(Set<Set<Integer>> agents) {
    	return spec.getRelationD(agents);
    }
	
    /**
     * Calculates nu(A) with A := Z. f /\ EX Z, i.e. [[EG f]].
     * @param f a bdd that represents a set of states.
     * @return the bdd that represents nu Z. f /\ EX Z, i.e. [[EG f]].
     */
    public BDD greatestFixedpointA(BDD f) {
        BDD Zold = null;
        BDD Znew = BDD.trueBDD();
        while (Znew != Zold) {
            Zold = Znew;
            Znew = BDD.and(f, preImageTemporal(Zold));
        }
        return Znew;
    }
    
    /**
     * Calculates mu(B) with B := Z. g \/ (f /\ EX Z), i.e. [[f EU g]].
     * @param f a bdd that represents a set of states.
     * @param g a bdd that represents a set of states.
     * @return the bdd that represents mu Z. g \/ (f /\ EX Z), i.e. [[f EU g]].
     */
    public BDD leastFixedpointB(BDD f, BDD g) {
        BDD Zold = null;
        BDD Znew = BDD.falseBDD();
        while (Znew != Zold) {
            Zold = Znew;
            Znew = BDD.or(g, BDD.and(f, preImageTemporal(Zold)));
        }
        return Znew;
    }
    
    /**
     * Returns the BDDWB ("BDD with Bottom") which the bivalent variable
     * or boolean constant <tt>symbol</tt> stands for.
     * @param symbol the string representation of a bivalent variable or
     * a boolean constant.
     * @return the BDDWB which the symbol <tt>symbol</tt> stands for, if symbol
     * is either the string representation of a bivalent variable or a boolean constant;
     * null, if <tt>symbol</tt> is neither declared as a variable nor as a constant.
     * If <tt>symbol</tt> is declared as a variable or constant of type int or enum,
     * the return value of this method has no meaning.
     */
	public BDDWB getBDDWBForSymbol(String symbol) {
		return sm.getBDDWBForSymbol(symbol);
	}
	
	/**
	 * Returns the MultiBDD which the variable or constant <tt>symbol</tt>
	 * stands for, if <tt>symbol</tt> is declared to be of type int or enum.
	 * @param symbol the string representation of variable or constant of
	 * type int or enum.
     * @return the MultiBDD which the variable or constant <tt>symbol</tt>
	 * stands for, if <tt>symbol</tt> is declared to be of type int or enum.
	 * @throws RuntimeException, if <tt>symbol</tt> is a bivalent variable.
	 * If <tt>symbol</tt> is a constant of type boolean, the return value
	 * of this method has no meaning.
	 */
	public MultiBDD getMultiBDDForSymbol(String symbol) {
		return sm.getMultiBDDForSymbol(symbol);
	}
    
    /**
     * Declares an unprimed variable <tt>variable</tt> (without primed sibling)
     * locally to the scope of an forall/exists quantifier. This method is called
     * by the evaluation methods of instances of NodeQuant, whenever the evaluation 
     * process enters the scope of an forall/exists quantifier.
     * @param variable the string representation of an unprimed local variable
     * @param type the type of the local variable
     * @returns the multi-index of the new locally declared variable
     */
    public Vector<Integer> declareLocalVariable(String variable, Type type) {
    	return sm.declareLocalVariable(variable, type);
    }
    
    /**
     * Undeclares an unprimed local variable <tt>variable</tt>. This method is called
     * by the evaluation methods of instances of NodeQuant, whenever the evaluation
     * process leaves the scope of an forall/exists quantifier. Local variables have
     * to be redeclared in reverse order of their declarations.
     * @param variable the string representation of an unprimed local variable
     */
    public void undeclareLocalVariable(String variable) {
    	sm.undeclareLocalVariable(variable);
    }
	
	private KripkeStructure(String name, Specification spec, Vector<BDD> combination, BDD tempRelation, BDD deadStates) {
 		this.name = name;
		this.spec = spec;
		this.combination = combination;
		this.sm = spec.sm();
		this.tempRelation = tempRelation;
		this.deadlockStates = deadStates;
		this.lambdaRelation = null;
		this.serialized = false;
		this.reachableStates = null; // calculated by factory method
		this.diameter = -1;          // calculated by factory method
	}

	public static KripkeStructure kripkeStructure(Specification spec, Vector<BDD> combination, boolean serialized) {
		return kripkeStructure("anonymous", spec, combination, serialized);
	}
	
  /** 
   * Creates and returns the Kripke structure that is uniquely determined
   * by <tt>combination</tt> in the context of specification <tt>spec</tt>.
   * @param name the name of the Kripke structure
   * @param spec the underlying structure specification
   * @param combination a vector of bdds that defines for each action in
   * spec.guardedActions() the set of states in which the epistemic precondition
   * of action is true.
   * @param serialized if true, deadlock states are eliminated by attaching
   * self-loops to them. 
   * @return the Kripke structure that is defined by comb w.r.t. spec.
   */
  public static KripkeStructure kripkeStructure(String name, Specification spec, Vector<BDD> combination, boolean serialized) {
    	if (combination.size() != spec.guardedActions().size()) {
    		throw new RuntimeException("combination doesn't match specification.");
    	}
    	BDD tempRelation = spec.tempRelationFalse();
    	BDD deadStates = spec.deadlockStatesFalse();
    	Iterator<BDD> comb = combination.iterator();
    	for (Action action : spec.guardedActions()) {
    		// epreStates is the set of states such that
    		// the epistemic precondition of action is true 
    		BDD epreStates = comb.next();
    		BDD actionBDD = BDD.and(action.actionBDDTrue, epreStates);
    		tempRelation = BDD.or(tempRelation, actionBDD);
    		deadStates = BDD.and(deadStates, BDD.not(BDD.and(action.preBDD, epreStates)));
    	}
    	KripkeStructure ks = new KripkeStructure(name, spec, combination, tempRelation, deadStates);
    	if (serialized) { ks.serialization(); }
    	logger.info("calculating reachable states");
	var info = ks.reachableStates(spec.initialCondition());
	ks.reachableStates = info.getFirst();
	ks.diameter = info.getSecond();
    	return ks;
    }

    /**
     * Determine the set of states which are temporally
     * reachable from a state in {@link from} and the system diameter.
     *
     * @param from a set of states
     * @return a pair of a BDD and the system diameter
     */
    private Pair<BDD, Integer> reachableStates(BDD from) {
      BDD Zold = null;
      BDD Znew = from;
      int diameter = 0;
      while (Znew != Zold) {
        logger.fine("reachability: loop no " + (diameter+1));
        Zold = Znew;
        Znew = BDD.or(Zold, imageTemporal(Zold));
        diameter++;
      }
      return new Pair<>(Znew, diameter);
    }
    
    private void serialization() {
    	this.lambdaRelation = BDD.and(sm.identityRelation(), deadlockStates);
    	this.tempRelation = BDD.or(tempRelation, lambdaRelation);
    	this.serialized = true;
    }
    
  /**
   * For any given set or elementary states {@code prop}
   * a finite run that starts in an initial state and
   * ends in a state belonging to {@code prop} is produced
   * provided that such a trace exists; otherwise {@code null} is returned.
   *,
   * @param prop a bdd representing a set of elementary states
   * @return a run that starts in an initial state and ends in
   *         a state that satisfies/belongs to {@code prop} if such a trace exists;
   *         otherwise {@code null}
   */
  public @Nullable Run dfs(BDD prop) {
    Stack<Step> trace = new Stack<>(); trace.push(new Step(spec.initialCondition()));
    boolean found = dfs(prop, trace, new Ref<>(BDD.not(spec.initialCondition())));
        
    if (!found)
      return null;
        
    // specialize the search results in reverse direction
    // (beginning with the state that satisfies prop)
    Step lastStep = trace.peek();
    Valuation satisfyingValuation = BDD.and(lastStep.getPostState(), prop).satisfyingValuation();
    lastStep.setPostState(satisfyingValuation.specifyDontCares(sm.bivalentVariables()).asBDD());
    int i = trace.size() - 1;
    while (i > 0) {
      Step step = trace.get(i);
      BDD state = step.getPostState();
      String actionName = step.getActionName();

      // calculate predecessor state(s) for the action with name actionName
      BDD predecessorState = preImageUnderAction(state, actionName);

      --i;
      trace.get(i).setPostState(BDD.and(trace.get(i).getPostState(),predecessorState));
    }
        
    return new Run(trace);
  }
    
  /**
   * Perform a recursive depth first search in the interpreted system.
   * If the method is called with a property {@code prop}, a stack {@code trace}
   * consisting of only one step and the post state of this step equal to
   * the negation of the bdd of {@code notVisited} states, the method on termination
   * either yields a trace with the following properties:
   * 1) the post state of the first (trivial) step of the trace is
   *    given by the initial condition,
   * 2) the post state of the last step of the trace comprises at least one
   *    elementary state that models the property {@code prop};
   * or no such trace exists.
   */ 
  private boolean dfs(BDD prop, Stack<Step> trace, Ref<BDD> notVisited) {
    BDD state = trace.peek().getPostState();
        
    // if state models prop then terminate the search
    if (BDD.and(state, prop) != BDD.falseBDD())
      return true;
        
    // calculate successor state(s) for each action
    for (String actionName : spec.actionNames()) {
      // calculate successor state(s) for the action with name actionName
      BDD successorState = imageUnderAction(state, actionName);
            
      // subtract all states which have already been visited
      BDD newPart = BDD.and(successorState, notVisited.get());
      if (newPart == BDD.falseBDD())
        continue;

      notVisited.set(BDD.and(BDD.not(newPart), notVisited.get()));
      trace.push(new Step(actionName, newPart));
      boolean found = dfs(prop, trace, notVisited);
      if (found)
        return true;
    }
        
    trace.pop();
    return false;
  }
    
    public void printSummaryInfo(PrintWriter out) {
    	out.println("summary information on Kripke structure '" + name + "':");
    	out.println("system diameter.............................. " + diameter);
    	out.println("number of unprimed bivalent variables........ 1+" + (sm.universe().size()-1));
    	out.println("number of legal states....................... " + sm.stateSpaceBDD().countSatVals(sm.universe()));
    	out.println("number of initial states..................... " + this.getInitialCondition().countSatVals(sm.universe()));
    	out.println("number of reachable states................... " + this.reachableStates.countSatVals(sm.universe()));
    	out.println("number of reachable deadlock/lambda states... " + this.reachableDeadlockStates().countSatVals(sm.universe()));
    	out.println("serialized................................... " + serialized);
    }

    /**
     * Returns a set that contains the indices of all (globally declared)
     * unprimed bivalent variables.
     */
    public Set<Integer> universe() {
    	return sm.universe();
    }
    
    /**
     * Returns the bdd that represents the set of all initial states
     * of this Kripke structure (initial condition).
     * @return the bdd that represents the set of all initial states
     *         of this Kripke structure.
     */
    public BDD getInitialCondition() {
    	return spec.initialCondition();
    }
    
    /**
     * Returns a vector containing the names of all actions
     * which have been declared for this Kripke structure.
     * @return a vector containing the names of all actions which
     *         have been declared for this Kripke structure.
     */
    public Vector<String> getActionNames() {
    	return spec.actionNames();
    }
}
