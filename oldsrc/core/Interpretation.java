package core;

/**
 * Wrapper class for the return values of method interpret() of class Specification.
 * 
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-05-25
 */
public class Interpretation {
  private PNStructure pnstruct;
  private KripkeStructure ksPositive, ksNegative_c;
  private int numberIterations;
	
  public Interpretation(PNStructure pnstruct, KripkeStructure ksPositive, KripkeStructure ksNegative_c, int numberIterations) {
    this.pnstruct = pnstruct;
    this.ksPositive = ksPositive;
    this.ksNegative_c = ksNegative_c;
    this.numberIterations = numberIterations;
  }
	
  public PNStructure pnstruct() {
    return pnstruct;
  }
	
  public KripkeStructure ksPositive() {
    return ksPositive;	
  }
	
  public KripkeStructure ksNegative_c() {
    return ksNegative_c;	
  }
	
  public int numberIterations() {
    return numberIterations;
  }
}
