package core;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Symbol table, which maps symbols id of type S (e.g., String)
 * to values v of type T (e.g., BDDs). A symbol id can be declared more than once. If an
 * ordered pair (id,v1) is already stored in the symbol table and the symbol id is redeclared
 * by (id,v2), then (id,v2) becomes the current mapping and shadows the old mapping (id,v1).
 * The mappings (id,...), with one and the same first component id, are organized in
 * a stack-like fashion. 
 * 
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-03-13
 */
public class SymbolTable<@NonNull S, T> {
  /** A map mapping symbols id to (the top entry of) a stack(id) of entries of type B. */
  private Map<@NonNull S, @Nullable Stack<T>> table;
    
  /**
   * Constructs a new, empty symbol table.
   */
  public SymbolTable() {
    this.table = new HashMap<>();
  }
    
  /**
   * Returns the top entry (of type B) of the stack to which the specified symbol id
   * is mapped, or {@code null} if this symbol table contains no mapping for the symbol id.
   *
   * @param id the symbol whose associated value is to be returned
   * @return the value to which the specified symbol is mapped, or {@code null} if this
   *         symbol table does not contain the specified symbol
   */
  public @Nullable T top(@NonNull S id) {
    Stack<T> stack = table.get(id);
    if (stack != null)
      return stack.peek();
    return null;
  }
    
  /**
   * Maps the specified symbol <tt>id</tt> to the specified <tt>value</tt>
   * in this symbol table. If an ordered pair (id,v1) is already stored in the
   * symbol table and the symbol id is redeclared by put(id,v2), then (id,v2)
   * becomes the current mapping and shadows the old mapping (id,v1).
   *
   * @param id a symbol
   * @param value a value
   */
  public void push(@NonNull S id, T value) {
    Stack<T> stack = this.table.get(id);
    if (stack == null)
      table.put(id, stack = new Stack<>());
    stack.push(value);
  }
    
  /**
   * Removes the top element of the stack(id) which the specified symbol id
   * is mapped to. If stack(id) is empty, the specified symbol id is removed
   * from this symbol table.
   * 
   * @param id the symbol for which the top element of stack(id) is to be removed
   */
  public void pop(@NonNull S id) {
    Stack<T> stack = this.table.get(id);
    if (stack == null)
      return;
    stack.pop();
    if (stack.isEmpty())
      table.put(id, null);
  }
}
