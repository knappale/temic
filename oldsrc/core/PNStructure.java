package core;

import bdd.BDD;


/**
 * Class PNStructure.
 * 
 * @author H. Muehlberger
 * @version 1.0
 * @since 2011-05-23
 */
public class PNStructure {
  private Specification spec;
  private BDD P;
  private BDD N;
  private BDD Nc;
	
  public PNStructure(BDD P, BDD N, Specification spec) {
    this.P = P;
    this.N = N;
    this.Nc = BDD.and(BDD.not(N), spec.sm().stateSpaceBDD());
    this.spec = spec;
  }
	
  public static PNStructure bottom(Specification spec) {
    return new PNStructure(BDD.falseBDD(), BDD.falseBDD(), spec);
  }
	
  public BDD getP() {
    return P;
  }
	
  public BDD getN() {
    return N;
  }
	
  public BDD getNc() {
    return Nc;
  }
	
  public Specification specification() {
    return spec;
  }
    
  public boolean leq(PNStructure other) {
    return BDD.subsets(this.P, other.P) && BDD.subsets(this.N, other.N);
  }

  public boolean isBottom() {
    return this.P == BDD.falseBDD() && this.N == BDD.falseBDD();
  }
	
  public boolean isDeterminated() {
    return BDD.or(P, N) == spec.sm().stateSpaceBDD() && !isContradictory();
  }
	
  public boolean isContradictory() {
    return BDD.and(P, N) != BDD.falseBDD();
  }

  @Override
  public boolean equals(Object object) {
    if (object == null)
      return false;
    try {
      PNStructure other = (PNStructure)object;
      return P == other.P && N == other.N && spec == other.spec;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
