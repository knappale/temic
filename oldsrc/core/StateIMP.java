package core;

public class StateIMP {
	private boolean hasTerminated;
	private KripkeStructure structFalse;
	private KripkeStructure structTrue;
	private KripkeStructure structFixedPoint;
	
	public StateIMP(KripkeStructure structFalse, KripkeStructure structTrue) {
		this.hasTerminated = false;
		this.structFalse = structFalse;
		this.structTrue = structTrue;
		this.structFixedPoint = null;
	}
	
	public boolean hasTerminated() {
		return hasTerminated;
	}
	
	public void setTerminated() {
		hasTerminated = true;
	}
	
	public KripkeStructure structFalse() {
		return structFalse;
	}

	public KripkeStructure structTrue() {
		return structTrue;
	}
	
	public KripkeStructure fixedPoint() {
		return structFixedPoint;
	}
	
	public void setFixedPoint(KripkeStructure fixedPoint) {
		this.structFixedPoint = fixedPoint;
	}
}
