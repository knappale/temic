package core;

import syntax.generated.TemicParser;
import syntax.generated.ParseException;
import syntax.generated.TokenMgrError;
import syntax.Identifier;
import syntax.Spec;
import syntax.TypingException;

import java.util.Enumeration;
import java.util.logging.*;

import org.eclipse.jdt.annotation.NonNull;

import bdd.BDD;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;

/**
 * A temporal-epistemic model interpreter and checker (TEMIC).
 *
 * @author H. Muehlberger
 * @version 1.1
 * @since 2011-03-31
 */
public class Temic {
  /** the version number of TEMIC */
  public static final String VERSION = "1.1";
  /** the logger for instances of class Temic */
  private static Logger logger = Logger.getLogger(Temic.class.getName());
  private static OptionSet optionSet = null;

  private static void evaluateCommandLineOptions(String[] args) {
    optionSet = OptionSet.parseCommandLineOptions(args);
    if (optionSet == null) {
      OptionSet.usage();
      System.exit(1);
    }
  }

  private static void configureLoggingSystem() {
    LogManager logManager = LogManager.getLogManager();
    Logger rootLogger = logManager.getLogger("");
    rootLogger.setLevel(optionSet.option_ll());
    for (Handler h : rootLogger.getHandlers())
      rootLogger.removeHandler(h);
    if (optionSet.option_ll() != Level.OFF) {
      ConsoleHandler ch = new ConsoleHandler();
      ch.setLevel(optionSet.option_ll());
      rootLogger.addHandler(ch);
      if (optionSet.option_lf() != null) {
        FileHandler fh;
        try {
          fh = new FileHandler(optionSet.option_lf());
          SimpleFormatter sf = new SimpleFormatter();
          fh.setFormatter(sf);
          fh.setLevel(optionSet.option_ll());
          rootLogger.addHandler(fh);
          ch.setLevel(Level.OFF);
        } catch (IOException e) {
          System.out.println("IOException while setting FileHandler for rootLogger, aborting: " + e);
          System.exit(1);
        }
      }
    }
    for (Enumeration<String> lNames = logManager.getLoggerNames(); lNames.hasMoreElements();) {
      logManager.getLogger(lNames.nextElement()).setLevel(optionSet.option_ll());
    }
    logger.config("logging system configured");
  }

  private static @NonNull PrintWriter openOutput() {
    if (optionSet.outputFile() == null) {
      logger.fine("opening output stream (standard output)");
      return new PrintWriter(System.out, true);
    }

    try {
      logger.fine("opening output file " + optionSet.outputFile());
      return new PrintWriter(new FileOutputStream(optionSet.outputFile(), true));
    } catch (IOException e) {
      printlnAndLog(Level.SEVERE, "Error opening output file " + optionSet.outputFile() + ", aborting: " + e);
      System.exit(1);
    }

    // unreachable
    return new PrintWriter(System.out, true);
  }

  private static @NonNull Reader openInput() {
    try {
      logger.fine("opening input file " + optionSet.inputFile());
      return new BufferedReader(new InputStreamReader(new FileInputStream(optionSet.inputFile())));
    } catch (FileNotFoundException e) {
      printlnAndLog(Level.SEVERE, "Error opening file " + optionSet.inputFile() + ", aborting: " + e);
      System.exit(1);
    }

    // unreachable
    return new BufferedReader(new InputStreamReader(System.in));
  }

  public static void main(String[] args) {
    evaluateCommandLineOptions(args);
    configureLoggingSystem();
    bdd.BDD.reconfigure(optionSet.ntabsize(), optionSet.cachesize(), optionSet.wtabsize());

    // opening input and output streams
    PrintWriter out = openOutput();
    Reader rdr = openInput();

    try {
      // creating parser and starting timer
      TemicParser parser = new TemicParser(rdr);
      long startTime = System.currentTimeMillis();

      // Phase 1: parsing
      logger.info("Phase 1: parsing");
      Spec nodeSpec = parser.specification(out);

      // Phase 2: static analysis
      logger.info("Phase 2: static analysis");
      SymbolTable<@NonNull Identifier, @NonNull Type> typeEnv = new SymbolTable<>(); // new ExtendedSymbolTable<Namespace, Type>(Namespace.values());
      nodeSpec.check(typeEnv);

      // Phase 3: identifying state space
      logger.info("Phase 3: identifying state space and variable order");
      SymbolManager sm = new SymbolManager(nodeSpec.getVariables(), typeEnv);

      // Phase 4: generating specification, enacting all declarations
      logger.info("Phase 4: generating Kripke-structure specification");
      Specification spec = nodeSpec.specification(sm);

      // Phase 5: generating Kripke structures (trivial cases)
      // and outputting summary information
      logger.info("Phase 5: generating Kripke structures (trivial cases)");
      KripkeStructure structTrue = KripkeStructure.kripkeStructure("trivial case, all epre-clauses true", spec, spec.combTrue(), true);
      structTrue.printSummaryInfo(out);
      KripkeStructure structFalse = KripkeStructure.kripkeStructure("trivial case, all epre-clauses false", spec, spec.combFalse(), true);
      structFalse.printSummaryInfo(out);
      StateIMP sigma = new StateIMP(structFalse, structTrue);

      // Phase 6: executing statements
      logger.info("Phase 6: executing statements");
      nodeSpec.execute(spec, sigma, out, optionSet);

      if (optionSet.rule1()) {
        out.println("additional rule no 1 was activated.");
        logger.info("additional rule no 1 was activated.");
      } else {
        out.println("additional rule no 1 was NOT activated.");
        logger.info("additional rule no 1 was NOT activated.");
      }

      logger.info("BDD count: " + BDD.count);
      out.println();
      out.print("Time expired: ");
      out.print(System.currentTimeMillis() - startTime);
      out.println(" ms");
    } catch (TokenMgrError e) {
      logger.info("lexical analysis failed: unknown token.\n" + e.getMessage());
      out.println("lexical analysis failed: unknown token.\n" + e.getMessage());
    } catch (ParseException e) {
      logger.info("parsing failed: syntax error.\n" + e.getMessage());
      out.println("parsing failed: syntax error.\n" + e.getMessage());
    } catch (TypingException e) {
      logger.info("static analysis failed: typing rules violated.\n" + e.getMessage());
      out.println("static analysis failed: typing rules violated.\n" + e.getMessage());
    } finally {
      logger.info("closing IO streams");
      out.close();
      try {
        rdr.close();
      }
      catch (IOException e) {
      }
    }
  }

  private static void printlnAndLog(Level level, String text) {
    System.out.println(text);
    logger.log(level, text);
  }
}
