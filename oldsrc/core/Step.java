package core;

import bdd.BDD;


/**
 * A step in an execution of a transition system.
 *
 * @author H. Muehlberger
 * @version 1.0
 * @since 2008-07-10
 */
public class Step {
  private String actionName; // name of the action or null for 'initial'
  private BDD postState; // post-state of the action as bdd

  /**
   * Constructs a step which represents the initial state of an execution.
   * 
   * @param initialState a bdd that represents the initial state of an execution
   */
  public Step(BDD initialState) {
    this.actionName = null;
    this.postState = initialState;
  }

  /**
   * Constructs a step which represents a fired action in an execution.
   * 
   * @param actionName the name of the fired action
   * @param postState the post state of the transition system after the action has been fired
   */
  public Step(String actionName, BDD postState) {
    this.actionName = actionName;
    this.postState = postState;
  }

  /**
   * Returns the name of the fired action which this step stands for. If this step
   * stands for the initial state, then <tt>null</tt> is returned.
   * 
   * @return the name of the fired action which this step stands for
   */
  public String getActionName() {
    return actionName;
  }

  /**
   * Returns the bdd that represents the post state of the transition system after
   * the action, which this step stands for, has been fired.
   * 
   * @return the bdd that represents the post state of the transition system after
   *         the action, which this step stands for, has been fired.
   */
  public BDD getPostState() {
    return postState;
  }

  /**
   * Sets the bdd that represents the post state of the transition system after
   * the action, which this step stands for, has been fired.
   * 
   * @param postState the bdd that represents the post state of the transition system
   *                  after the action, which this step stands for, has been fired
   */
  public void setPostState(BDD postState) {
    this.postState = postState;
  }
}
