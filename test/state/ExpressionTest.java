package state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.BeforeClass;
import org.junit.Test;


public class ExpressionTest {
  private static final StateVariable.@NonNull Boolean b1 = new StateVariable.Boolean("b1");
  private static final StateVariable.@NonNull Boolean b2 = new StateVariable.Boolean("b2");
  private static final StateVariable.@NonNull Boolean b3 = new StateVariable.Boolean("b3");
  private static final StateVariable.@NonNull Integer i1 = new StateVariable.Integer("i1", -16, 15);
  private static final StateVariable.@NonNull Integer i2 = new StateVariable.Integer("i2", 100, 115);
  private static final StateVariable.@NonNull Integer i3 = new StateVariable.Integer("i3", 0, 257);

  @BeforeClass 
  public static void setUp() {
    var variables = new ArrayList<@NonNull StateVariable>();
    variables.add(b1);
    variables.add(b2);
    variables.add(b3);
    variables.add(i1);
    variables.add(i2);
    variables.add(i3);

    SpaceManager.initialise(variables);
  }

  @Test
  public void valueTest() {
    Value value = new Value.Integer(3);
    assertEquals(value, new Value.Integer(value.getInteger()));
  }

  @Test
  public void valuesTest() {
    assertEquals(BigInteger.valueOf(2*2*2*16*(2+4+6+8+10+12+14+16+18+20+22+24+26+28+30)),
                 StateSet.satisfyingStates(new Expression.Boolean.Comparison(new Expression.Integer.Arithmetic(new Value.Integer(2), Expression.Integer.Arithmetic.Kind.MULT, i1),
                                                                             Expression.Boolean.Comparison.Kind.GT,
                                                                             i3)).count());
    assertEquals(BigInteger.valueOf(2*2*2*32*16*3),
                 StateSet.satisfyingStates(new Expression.Boolean.Comparison(new Value.Integer(254), Expression.Boolean.Comparison.Kind.LT, i3)).count());
    assertTrue(StateSet.satisfyingStates(new Expression.Boolean.Comparison(i3, Expression.Boolean.Comparison.Kind.GT, new Value.Integer(257))).isEmpty());
  }
}
