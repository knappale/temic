package state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


public class StateTest {
  private static final StateVariable.@NonNull Boolean b1 = new StateVariable.Boolean("b1");
  private static final StateVariable.@NonNull Boolean b2 = new StateVariable.Boolean("b2");
  private static final StateVariable.@NonNull Boolean b3 = new StateVariable.Boolean("b3");
  private static final StateVariable.@NonNull Integer i1 = new StateVariable.Integer("i1", -16, 15);
  private static final StateVariable.@NonNull Integer i2 = new StateVariable.Integer("i2", 100, 115);
  private static final StateVariable.@NonNull Integer i3 = new StateVariable.Integer("i3", 0, 255);

  @BeforeClass 
  public static void setUp() {
    var variables = new ArrayList<@NonNull StateVariable>();
    variables.add(b1);
    variables.add(b2);
    variables.add(b3);
    variables.add(i1);
    variables.add(i2);
    variables.add(i3);

    SpaceManager.initialise(variables);
  }

  @AfterClass
  public static void tearDown() {
  }

  @Test
  public void variableTest() {
    assertEquals(b1.getName(), "b1");
    assertEquals(b1.prime().getName(), "b1'");
    assertNotEquals(b1, b1.prime());
    assertEquals(b1, b1.unprime());
    assertEquals(b1, b1.prime().unprime());
    assertEquals(b1.prime(), b1.unprime().prime());
    assertEquals(b1.prime(), b1.prime().prime());
  }

  @Test
  public void valuationTest() {
    var valuation = new HashMap<@NonNull Variable, @NonNull Value>();
    valuation.put(b1, new Value.Boolean(true));
    valuation.put(b2, new Value.Boolean(false));
    valuation.put(b3, new Value.Boolean(false));
    valuation.put(i1, new Value.Integer(-3));
    valuation.put(i2, new Value.Integer(114));
    valuation.put(i3, new Value.Integer(0));
    State state = new State(valuation);

    assertEquals(state, state.singleton().any());
  }
}
