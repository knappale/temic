package state;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import util.Sets;

import java.util.HashMap;
import java.util.Set;


public class BDDTest {
  @SuppressWarnings("null")
  private static @NonNull BDD p1, p2, p3, p4, p5, p6, p7, p8;
  @SuppressWarnings({ "null", "unused" })
  private static @NonNull BDD n1, n2, n3, n4, n5, n6, n7, n8;
  @SuppressWarnings("null")
  private static @NonNull BDD bdd1, bdd2, bdd3, bdd4, bdd5, bdd6, bdd7, bdd8;
  @SuppressWarnings("null")
  private static @NonNull BDD trueBDD, falseBDD;
	
  @BeforeClass
  public static void setUp() {
    BDD.clear();
    trueBDD = BDD.trueBDD();
    falseBDD = BDD.falseBDD();
		
    p1 = BDD.varBDD(1); n1 = BDD.not(p1);
    p2 = BDD.varBDD(2); n2 = BDD.not(p2);
    p3 = BDD.varBDD(3); n3 = BDD.not(p3);
    p4 = BDD.varBDD(4); n4 = BDD.not(p4);
    p5 = BDD.varBDD(5); n5 = BDD.not(p5);
    p6 = BDD.varBDD(6); n6 = BDD.not(p6);
    p7 = BDD.varBDD(7); n7 = BDD.not(p7);
    p8 = BDD.varBDD(8); n8 = BDD.not(p8);
        
    // bdd1 :<-> (3 /\ true ) \/ (~3 /\ false) <-> 3  
    bdd1 = BDD.ite(p3, trueBDD, falseBDD);
		
    // bdd2 :<-> (3 /\ false) \/ (~3 /\ true ) <-> ~3
    bdd2 = BDD.ite(p3, falseBDD, trueBDD);
       
    // bdd3 :<-> (2 /\ bdd2 ) \/ (~2 /\ bdd1 ) <-> (2 /\ ~3) \/ (~2 /\ 3)
    bdd3 = BDD.ite(p2, bdd2, bdd1);
        
    // bdd4 :<-> (2 /\ false) \/ (~2 /\ bdd1 ) <-> (~2 /\ 3)
    bdd4 = BDD.ite(p2, falseBDD, bdd1);
        
    // bdd5 :<-> [1 /\ ((2 /\ ~3) \/ (~2 /\ 3))] \/ [~1 /\ (~2 /\ 3)]
    //       <-> [1 /\   2 /\ ~3] \/ [~2 /\ 3]
    bdd5 = BDD.ite(p1, bdd3, bdd4);
        
    // bdd6 :<-> 1 /\ (2 \/ ~3)
    bdd6 = BDD.and(p1, BDD.or(p2, n3));
        
    // bdd7 :<-> (1 /\ 2) \/ (1 /\ 3) \/ (2 /\ 3)
    bdd7 = BDD.or(BDD.and(p1, p2), BDD.or(BDD.and(p1, p3), BDD.and(p2, p3)));
        
    // bdd8 :<-> ~[~(~(2 /\ 3) => 1) \/ (~2 /\ ~3)] <-> ((2 /\ 3) \/ 1) /\ (2 \/ 3) <-> bdd7 
    bdd8 = BDD.nor(BDD.not(BDD.implies(BDD.nand(p2, p3), p1)),BDD.and(n2, n3));
  }	

  @AfterClass
  public static void tearDown() {
  }

  @Test
  public void trueBDD() {
    assertTrue(BDD.trueBDD() == BDD.trueBDD());
    assertTrue(trueBDD == BDD.trueBDD());
    assertTrue(BDD.isTrue(BDD.trueBDD()));
    assertFalse(BDD.isFalse(BDD.trueBDD()));
    assertTrue(BDD.getVar(BDD.trueBDD()) == Integer.MAX_VALUE);
  }
	
  @Test
  public void falseBDD() {
    assertTrue(BDD.falseBDD() == BDD.falseBDD());
    assertTrue(falseBDD == BDD.falseBDD());
    assertFalse(BDD.isTrue(BDD.falseBDD()));
    assertTrue(BDD.isFalse(BDD.falseBDD()));
    assertTrue(BDD.getVar(BDD.falseBDD()) == Integer.MAX_VALUE);
  }
	
  @Test
  public void varBDD() {
    assertTrue(BDD.varBDD(17) == BDD.varBDD(17));
    assertFalse(BDD.varBDD(17) == BDD.varBDD(16));
    assertFalse(BDD.isTrue(BDD.varBDD(17)));
    assertFalse(BDD.isFalse(BDD.varBDD(17)));
    assertTrue(BDD.getVar(BDD.varBDD(17)) == 17);
    assertTrue(BDD.getThen(BDD.varBDD(17)) == trueBDD);
    assertTrue(BDD.getElse(BDD.varBDD(17)) == falseBDD);
  }
	
  @Test
  public void ite() {
    assertFalse(bdd1 == bdd2 || bdd1 == bdd3 || bdd1 == bdd4 || bdd1 == bdd5 || bdd1 == bdd6 || bdd1 == bdd7);
    assertFalse(bdd2 == bdd3 || bdd2 == bdd4 || bdd2 == bdd5 || bdd2 == bdd6 || bdd2 == bdd7);
    assertFalse(bdd3 == bdd4 || bdd3 == bdd5 || bdd3 == bdd6 || bdd3 == bdd7);
    assertFalse(bdd4 == bdd5 || bdd4 == bdd6 || bdd4 == bdd7 || bdd5 == bdd6 || bdd5 == bdd7 || bdd6 == bdd7);
     
    assertTrue(bdd1 == p3);
    assertTrue(bdd2 == BDD.not(bdd1));
    assertTrue(bdd3 == BDD.or(BDD.and(p2, n3), bdd4));
        
    assertTrue(BDD.ite(trueBDD, bdd5, bdd1) == bdd5);
    assertTrue(BDD.ite(falseBDD, bdd5, bdd1) == bdd1);
    assertTrue(BDD.ite(bdd5, trueBDD, falseBDD) == bdd5);
    assertTrue(BDD.ite(bdd5, BDD.or(p2, p3), falseBDD) == bdd5);
        
    assertTrue(bdd7 == bdd8);
    assertTrue(BDD.and(bdd7, n3) == BDD.and(p1, BDD.and(p2,n3)));       
  }

  @Test
  public void not() {
    assertTrue(BDD.not(falseBDD) == trueBDD);
    assertTrue(BDD.not(trueBDD) == falseBDD);
    assertFalse(BDD.not(bdd5) == bdd5);
    assertTrue(BDD.not(BDD.not(bdd5)) == bdd5);
    assertTrue(BDD.not(bdd3) == BDD.or(BDD.and(p2, p3), BDD.and(n2, n3)));
  }

  @Test
  public void and() {
    assertTrue(BDD.and(bdd4, falseBDD) == falseBDD);
    assertTrue(BDD.and(falseBDD, bdd4) == falseBDD);
    assertTrue(BDD.and(bdd4, trueBDD) == bdd4);
    assertTrue(BDD.and(trueBDD, bdd4) == bdd4);
    assertFalse(BDD.and(bdd4, bdd8) == bdd4);
    assertTrue(BDD.and(bdd4, bdd8) == BDD.and(p1, bdd4));
    assertFalse(BDD.and(bdd4, bdd8) == bdd8);
    assertFalse(BDD.and(bdd4, bdd8) == falseBDD);
    assertTrue(BDD.and(bdd4, bdd8) == BDD.and(bdd8, bdd4));
    assertTrue(BDD.and(BDD.and(p1, p2), p3) == BDD.and(p1, BDD.and(p2, p3)));
  }

  @Test
  public void or() {
    assertTrue(BDD.or(bdd4, falseBDD) == bdd4);
    assertTrue(BDD.or(falseBDD, bdd4) == bdd4);
    assertTrue(BDD.or(bdd4, trueBDD) == trueBDD);
    assertTrue(BDD.or(trueBDD, bdd4) == trueBDD);
    assertFalse(BDD.or(bdd4, bdd8) == bdd4);
    assertFalse(BDD.or(bdd4, bdd8) == bdd8);
    assertFalse(BDD.or(bdd4, bdd8) == trueBDD);
    assertTrue(BDD.or(bdd4, bdd8) == BDD.or(bdd8, bdd4));
    assertTrue(BDD.or(BDD.or(p1, p2), p3) == BDD.or(p1, BDD.or(p2, p3)));
    assertTrue(BDD.not(BDD.or(bdd4, bdd8)) == BDD.and(BDD.not(bdd4), BDD.not(bdd8)));
  }

  @Test
  public void implies() {
    assertTrue(BDD.implies(bdd4, bdd8) == BDD.or(BDD.not(bdd4), bdd8));
  }

  @Test
  public void biimplies() {
    assertTrue(BDD.biimplies(bdd4, bdd8) == BDD.biimplies(bdd8, bdd4));
    assertTrue(BDD.biimplies(bdd4, bdd8) == BDD.and(BDD.implies(bdd4, bdd8), BDD.implies(bdd8, bdd4)));
  }

  @Test
  public void exor() {
    assertTrue(BDD.exor(bdd1, trueBDD) == BDD.not(bdd1));
    assertTrue(BDD.exor(bdd5, bdd8) == BDD.and(BDD.or(bdd5, bdd8), BDD.not(BDD.and(bdd5, bdd8))));
  }

  @Test
  public void nand() {
    assertTrue(BDD.nand(bdd4, bdd8) == BDD.not(BDD.and(bdd4, bdd8)));
  }

  @Test
  public void nor() {
    assertTrue(BDD.nor(bdd4, bdd8) == BDD.not(BDD.or(bdd4, bdd8)));
  }	
	
  @Test
  public void size() {
    assertTrue(BDD.size(bdd1) == 3);
    assertTrue(BDD.size(bdd2) == 3);
    assertTrue(BDD.size(bdd3) == 5);
    assertTrue(BDD.size(bdd4) == 4);
    assertTrue(BDD.size(bdd5) == 7);
  }
	
  @Test
  public void cube() {
    var varset1 = new boolean[12];
    varset1[1] = true;
    varset1[2] = true;
    varset1[8] = true;
    var cube = BDD.cube(varset1);	
    assertTrue(cube == BDD.and(p1, BDD.and(p2, p8)));
    var varset2 = new boolean[1];
    cube = BDD.cube(varset2);
    assertTrue(cube == trueBDD);
    varset2[0] = true;
    cube = BDD.cube(varset2);
    assertTrue(cube == BDD.varBDD(0));
  }
	
  @Test
  public void exists() {
    @NonNull Set<@NonNull Integer> from5to7 = Sets.elements(5, 6, 7);
    assertTrue(BDD.exists(trueBDD, from5to7) == trueBDD);
    assertTrue(BDD.exists(falseBDD, from5to7) == falseBDD);
        
    assertTrue(BDD.exists(p4, from5to7) == p4);
    assertTrue(BDD.exists(p8, from5to7) == p8);
    assertTrue(BDD.exists(p5, from5to7) == trueBDD);
    assertTrue(BDD.exists(BDD.and(p4, p5), from5to7) == p4);
    assertTrue(BDD.exists(BDD.and(p4, n5), from5to7) == p4);
        
    assertTrue(BDD.exists(bdd5, from5to7) == bdd5);
    assertTrue(BDD.exists(BDD.or(bdd5, p7), from5to7) == trueBDD);
    assertTrue(BDD.exists(BDD.and(bdd5, p7), from5to7) == bdd5);
    
    var n1p5p6p8 = BDD.and(n1, BDD.and(p5, BDD.and(p6, p8)));
    var p1n5n6p8 = BDD.and(p1, BDD.and(n5, BDD.and(n6, p8)));
    var p1n5p6n8 = BDD.and(p1, BDD.and(n5, BDD.and(p6, n8)));
    var bdd11 = BDD.or(n1p5p6p8, BDD.or(p1n5n6p8, p1n5p6n8));
    var bdd12 = BDD.exists(bdd11, from5to7);
    var bdd13 = BDD.or(p1,p8);
        
    assertTrue(bdd12 == bdd13);
        
    @NonNull Set<@NonNull Integer> from1to1 = Sets.singleton(1);
    @NonNull Set<@NonNull Integer> from2to2 = Sets.singleton(2);
    @NonNull Set<@NonNull Integer> from3to3 = Sets.singleton(3);
        
    var t1 = BDD.or(BDD.and(p1, BDD.and(n2, n3)), BDD.or(BDD.and(n1, BDD.and(p2,n3)), BDD.or(BDD.and(p1, BDD.and(n2, p3)), BDD.and(p1, BDD.and(p2,p3)))));
    var t2 = BDD.exists(t1, from1to1);
    assertTrue(t2 == BDD.trueBDD());
    var t3 = BDD.exists(t1, from2to2);
    assertTrue(t3 == BDD.not(BDD.and(n1, p3)));
    var t4 = BDD.exists(t1, from3to3);
    assertTrue(t4 == BDD.not(BDD.and(n1, n2)));
  }
    
  @Test
  public void forall() {
    @NonNull Set<@NonNull Integer> from5to7 = Sets.elements(5, 6, 7);
        
    var n1p5p6p8 = BDD.and(n1, BDD.and(p5, BDD.and(p6, p8)));
    var p1n5n6p8 = BDD.and(p1, BDD.and(n5, BDD.and(n6, p8)));
    var p1n5p6n8 = BDD.and(p1, BDD.and(n5, BDD.and(p6, n8)));
    var bdd11 = BDD.or(n1p5p6p8, BDD.or(p1n5n6p8, p1n5p6n8));
    var bdd12 = BDD.exists(bdd11, from5to7);
    var bdd13 = BDD.not(BDD.forall(BDD.not(bdd11), from5to7));
        
    assertTrue(bdd12 == bdd13);
  }
    
  @Test
  public void rename() {
    var subst = new HashMap<@NonNull Integer, @NonNull Integer>();
    subst.put(1, 4);
    subst.put(3, 6);
    var bdd15 = BDD.rename(bdd5, subst);
    var bdd16 = BDD.or(BDD.and(p4, BDD.or(BDD.and(p2, n6), BDD.and(n2, p6))), BDD.and(n4, BDD.and(n2, p6)));
    assertTrue(bdd15 == bdd16);
  }
    
  @Test
  public void satisfyingValuation() {
    assertNull(BDD.satisfyingValuation(falseBDD));
    var val1 = BDD.satisfyingValuation(trueBDD);
    assertNotNull(val1);
    assertTrue(val1.isEmpty());
    assertTrue(val1.asBDD() == trueBDD);
    var val2 = BDD.satisfyingValuation(p1);
    assertNotNull(val2);
    assertFalse(val2.isEmpty());
    assertTrue(val2.asBDD() == p1);

    var n1p5p6p8 = BDD.and(n1, BDD.and(p5, BDD.and(p6, p8)));
    var p1n5n6p8 = BDD.and(p1, BDD.and(n5, BDD.and(n6, p8)));
    var p1n5p6n8 = BDD.and(p1, BDD.and(n5, BDD.and(p6, n8)));
    var bdd11 = BDD.or(BDD.or(n1p5p6p8, p1n5n6p8), p1n5p6n8);
    BDD.Valuation val11 = BDD.satisfyingValuation(bdd11);
    assertNotNull(val11);
    assertFalse(val11.isEmpty());
    assertTrue(val11.asBDD() == n1p5p6p8 ||
               val11.asBDD() == p1n5n6p8 ||
               val11.asBDD() == p1n5p6n8);

    var bdd12 = BDD.or(BDD.or(BDD.varBDD(3), BDD.varBDD(1)), BDD.varBDD(2));
    BDD.Valuation val12 = BDD.satisfyingValuation(bdd12);
    assertNotNull(val12);
    assertTrue(val12.asBDD() == BDD.varBDD(1));
  }

  @Test
  public void getVariable() {
    assertTrue(BDD.getVar(p1) == 1);
    assertTrue(BDD.getVar(n1) == 1);
    assertTrue(BDD.getVar(p2) == 2);
    assertTrue(BDD.getVar(n2) == 2);
    assertTrue(BDD.getVar(p8) == 8);
    assertTrue(BDD.getVar(n8) == 8);
  }
}
