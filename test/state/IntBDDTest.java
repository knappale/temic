package state;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Vector;


public class IntBDDTest {
  @SuppressWarnings({ "null" })
  private static @NonNull BDD @NonNull [] int_0, int_1, int_2, int_3, int_4, int_5, int_6, int_7, int_8, int_9;
  @SuppressWarnings({ "null" })
  private static @NonNull BDD @NonNull [] int_10, int_17, int_17copy, int_1152, int_max, int_maxMinus1;
  @SuppressWarnings({ "null", "unused" })
  private static @NonNull BDD @NonNull [] int_n1, int_n2, int_n3, int_n4, int_n5, int_n6, int_n7, int_n8, int_n9;
  @SuppressWarnings({ "null" })
  private static @NonNull BDD @NonNull [] int_n10, int_n17, int_n54, int_min, int_minPlus1, int_mindiv2;
  @SuppressWarnings({ "null", "unused" })
  private static @NonNull BDD @NonNull [] var_A, var_B, var_C, var_D, var_E;
  @SuppressWarnings("null")
  private static @NonNull BDD v1, v2, v3;
  @SuppressWarnings("null")
  private static @NonNull BDD n1, n2, n3;

  @BeforeClass
  public static void setUp() throws Exception {
    BDD.clear();

    int_0 = IntBDD.intMultiBDD(0);
    int_1 = IntBDD.intMultiBDD(1);
    int_2 = IntBDD.intMultiBDD(2);
    int_3 = IntBDD.intMultiBDD(3);
    int_4 = IntBDD.intMultiBDD(4);
    int_5 = IntBDD.intMultiBDD(5);
    int_6 = IntBDD.intMultiBDD(6);
    int_7 = IntBDD.intMultiBDD(7);
    int_8 = IntBDD.intMultiBDD(8);
    int_9 = IntBDD.intMultiBDD(9);
    int_10 = IntBDD.intMultiBDD(10);
    int_17 = IntBDD.intMultiBDD(17);
    int_17copy = IntBDD.intMultiBDD(17);
    int_1152 = IntBDD.intMultiBDD(1152);
    int_max = IntBDD.intMultiBDD(Integer.MAX_VALUE);
    int_maxMinus1 = IntBDD.intMultiBDD(Integer.MAX_VALUE - 1);
    int_n1 = IntBDD.intMultiBDD(-1);
    int_n2 = IntBDD.intMultiBDD(-2);
    int_n3 = IntBDD.intMultiBDD(-3);
    int_n4 = IntBDD.intMultiBDD(-4);
    int_n5 = IntBDD.intMultiBDD(-5);
    int_n6 = IntBDD.intMultiBDD(-6);
    int_n7 = IntBDD.intMultiBDD(-7);
    int_n8 = IntBDD.intMultiBDD(-8);
    int_n9 = IntBDD.intMultiBDD(-9);
    int_n10 = IntBDD.intMultiBDD(-10);
    int_n17 = IntBDD.intMultiBDD(-17);
    int_n54 = IntBDD.intMultiBDD(-54);
    int_min = IntBDD.intMultiBDD(Integer.MIN_VALUE);
    int_minPlus1 = IntBDD.intMultiBDD(Integer.MIN_VALUE + 1);
    int_mindiv2 = IntBDD.intMultiBDD(Integer.MIN_VALUE / 2);

    Vector<@NonNull Integer> mindex1 = new Vector<>();
    mindex1.add(1);
    mindex1.add(2);
    mindex1.add(3);
    var_A = IntBDD.varMultiBDD(mindex1);
    v1 = BDD.varBDD(1);
    n1 = BDD.not(v1);
    v2 = BDD.varBDD(2);
    n2 = BDD.not(v2);
    v3 = BDD.varBDD(3);
    n3 = BDD.not(v3);
    var_B = IntBDD.add(IntBDD.varMultiBDD(mindex1), int_n4);
  }

  @Test
  public void leq() throws Exception {
    assertTrue(IntBDD.leq(int_0, int_0) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_min, int_min) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_0, int_1) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_1, int_0) == BDD.falseBDD());
    assertTrue(IntBDD.leq(int_0, int_n1) == BDD.falseBDD());
    assertTrue(IntBDD.leq(int_min, int_n54) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_min, int_0) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_min, int_17) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_min, int_max) == BDD.trueBDD());

    assertTrue(IntBDD.leq(int_0, var_A) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_0, var_B) == v3);
    assertTrue(IntBDD.leq(var_B, var_A) == BDD.trueBDD());
    assertTrue(IntBDD.leq(var_A, var_B) == BDD.falseBDD());
    assertTrue(IntBDD.leq(var_A, int_0) == BDD.and(BDD.and(n1, n2), n3));
    assertTrue(IntBDD.leq(int_1, var_A) == BDD.or(BDD.or(v1, v2), v3));
    assertTrue(IntBDD.leq(var_A, var_A) == BDD.trueBDD());
    assertTrue(IntBDD.leq(int_5, var_A) == BDD.or(BDD.and(v2, v3), BDD.and(v1, v3)));
    assertTrue(IntBDD.leq(IntBDD.add(var_A, int_9), int_10) == BDD.or(IntBDD.equal(var_A, int_0), IntBDD.equal(var_A, int_1)));
  }

  @Test
  public void less() {
    assertTrue(IntBDD.less(int_0, int_0) == BDD.falseBDD());
    assertTrue(IntBDD.less(int_min, int_min) == BDD.falseBDD());
    assertTrue(IntBDD.less(int_0, int_1) == BDD.trueBDD());
    assertTrue(IntBDD.less(int_1, int_0) == BDD.falseBDD());
    assertTrue(IntBDD.less(int_0, int_n1) == BDD.falseBDD());
    assertTrue(IntBDD.less(int_n1, int_0) == BDD.trueBDD());
    assertTrue(IntBDD.less(int_min, int_n54) == BDD.trueBDD());
    assertTrue(IntBDD.less(int_min, int_0) == BDD.trueBDD());
    assertTrue(IntBDD.less(int_min, int_17) == BDD.trueBDD());
    assertTrue(IntBDD.less(int_min, int_max) == BDD.trueBDD());

    assertTrue(IntBDD.less(int_n1, var_A) == BDD.trueBDD());
    assertTrue(IntBDD.less(int_n1, var_B) == v3);
    assertTrue(IntBDD.less(var_B, var_A) == BDD.trueBDD());
    assertTrue(IntBDD.less(var_A, var_B) == BDD.falseBDD());
    assertTrue(IntBDD.less(var_A, int_1) == BDD.and(BDD.and(n1, n2), n3));
    assertTrue(IntBDD.less(var_A, int_0) == BDD.falseBDD());
    assertTrue(IntBDD.less(int_0, var_A) == BDD.or(BDD.or(v1, v2), v3));
    assertTrue(IntBDD.less(var_A, var_A) == BDD.falseBDD());
    assertTrue(IntBDD.less(int_4, var_A) == BDD.or(BDD.and(v2, v3), BDD.and(v1, v3)));
    assertTrue(IntBDD.less(IntBDD.add(var_A, int_8), int_10) == BDD.or(IntBDD.equal(var_A, int_0), IntBDD.equal(var_A, int_1)));
  }

  @Test
  public void get() {
    assertTrue(var_A[0] == BDD.varBDD(1));
    assertTrue(var_A[1] == BDD.varBDD(2));
    assertTrue(var_A[2] == BDD.varBDD(3));
    assertTrue(var_A[3] == BDD.falseBDD());
    assertTrue(var_B[3] == n3);
    assertTrue(var_A[31] == BDD.falseBDD());
    assertTrue(var_B[31] == n3);
  }

  @Test
  public void add() {
    var carryRef = new BDD[1];
    var sum = IntBDD.addCarry(int_3, int_0, carryRef);
    assertTrue(Arrays.equals(sum, int_3));
    assertTrue(carryRef[0] == BDD.falseBDD());

    sum = IntBDD.addCarry(int_0, int_3, carryRef);
    assertTrue(Arrays.equals(sum, int_3));
    assertTrue(carryRef[0] == BDD.falseBDD());

    sum = IntBDD.addCarry(int_3, int_7, carryRef);
    assertTrue(Arrays.equals(sum, int_10));
    assertTrue(carryRef[0] == BDD.falseBDD());

    sum = IntBDD.addCarry(int_7, int_3, carryRef);
    assertTrue(Arrays.equals(sum, int_10));
    assertTrue(carryRef[0] == BDD.falseBDD());

    sum = IntBDD.addCarry(int_10, int_n7, carryRef);
    assertTrue(Arrays.equals(sum, int_3));
    assertTrue(carryRef[0] == BDD.trueBDD());

    sum = IntBDD.addCarry(int_n7, int_10, carryRef);
    assertTrue(Arrays.equals(sum, int_3));
    assertTrue(carryRef[0] == BDD.trueBDD());

    sum = IntBDD.addCarry(int_17, int_n17, carryRef);
    assertTrue(Arrays.equals(sum, int_0));
    assertTrue(carryRef[0] == BDD.trueBDD());

    sum = IntBDD.addCarry(int_max, int_1, carryRef);
    assertTrue(Arrays.equals(sum, int_min));
    assertTrue(carryRef[0] == BDD.falseBDD());

    sum = IntBDD.addCarry(int_min, int_1, carryRef);
    assertTrue(Arrays.equals(sum, int_minPlus1));
    assertTrue(carryRef[0] == BDD.falseBDD());

    sum = IntBDD.addCarry(int_min, int_n1, carryRef);
    assertTrue(Arrays.equals(sum, int_max));
    assertTrue(carryRef[0] == BDD.trueBDD());

    sum = IntBDD.addCarry(int_max, int_n1, carryRef);
    assertTrue(Arrays.equals(sum, int_maxMinus1));
    assertTrue(carryRef[0] == BDD.trueBDD());

    sum = IntBDD.addCarry(var_B, int_4, carryRef);
    assertTrue(Arrays.equals(sum, var_A));
    assertTrue(carryRef[0] == IntBDD.leq(var_B, int_n1));
  }

  @Test
  public void inc() {
    assertTrue(Arrays.equals(IntBDD.inc(int_3), int_4));
    assertTrue(Arrays.equals(IntBDD.inc(int_max), int_min));
  }

  @Test
  public void sub() {
    assertTrue(Arrays.equals(IntBDD.sub(int_7, int_7), int_0));
    assertTrue(Arrays.equals(IntBDD.sub(int_7, int_6), int_1));
    assertTrue(Arrays.equals(IntBDD.sub(int_7, int_1), int_6));
    assertTrue(Arrays.equals(IntBDD.sub(int_7, int_0), int_7));
    assertTrue(Arrays.equals(IntBDD.sub(int_7, int_n1), int_8));
    assertTrue(Arrays.equals(IntBDD.sub(int_max, int_max), int_0));
    assertTrue(Arrays.equals(IntBDD.sub(int_min, int_min), int_0));
    assertTrue(Arrays.equals(IntBDD.sub(int_max, int_0), int_max));
    assertTrue(Arrays.equals(IntBDD.sub(int_min, int_0), int_min));
    assertTrue(Arrays.equals(IntBDD.sub(int_max, int_1), int_maxMinus1));
    assertTrue(Arrays.equals(IntBDD.sub(int_minPlus1, int_1), int_min));
  }

  @Test
  public void dec() {
    assertTrue(Arrays.equals(IntBDD.dec(int_4), int_3));
    assertTrue(Arrays.equals(IntBDD.dec(int_min), int_max));
  }

  @Test
  public void negate() {
    assertTrue(Arrays.equals(IntBDD.negate(int_7), int_n7));
    assertTrue(Arrays.equals(IntBDD.negate(int_n7), int_7));
    assertTrue(Arrays.equals(IntBDD.negate(int_0), int_0));
    assertTrue(Arrays.equals(IntBDD.negate(int_min), int_min));
    assertTrue(Arrays.equals(IntBDD.negate(int_max), int_minPlus1));
  }

  @Test
  public void abs() {
    assertTrue(Arrays.equals(IntBDD.abs(int_0), int_0));
    assertTrue(Arrays.equals(IntBDD.abs(int_1), int_1));
    assertTrue(Arrays.equals(IntBDD.abs(int_n1), int_1));
    assertTrue(Arrays.equals(IntBDD.abs(int_n7), int_7));
    assertTrue(Arrays.equals(IntBDD.abs(int_max), int_max));
    assertTrue(Arrays.equals(IntBDD.abs(int_minPlus1), int_max));
    assertTrue(Arrays.equals(IntBDD.abs(int_min), int_min));
    assertTrue(IntBDD.equal(IntBDD.abs(var_B), int_1) == BDD.or(IntBDD.equal(var_B, int_1), IntBDD.equal(var_B, int_n1)));
  }

  @Test
  public void shiftRightSigned() {
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(int_min, 0), int_min));
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(int_min, 1), int_mindiv2));
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(int_min, 29), int_n4));
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(int_min, 30), int_n2));
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(int_min, 31), int_n1));
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(int_min, 32), int_n1));
    assertTrue(Arrays.equals(IntBDD.shiftRightSigned(IntBDD.add(int_min, int_10), 1), IntBDD.add(int_mindiv2, int_5)));
  }

  @Test
  public void shiftRight() {
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_max, 31), int_0));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_max, 30), int_1));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_max, 29), int_3));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_max, 0), int_max));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_min, 31), int_1));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_min, 30), int_2));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_min, 29), int_4));
    assertTrue(Arrays.equals(IntBDD.shiftRight(int_10, 1), int_5));
    assertTrue(Arrays.equals(IntBDD.shiftRight(IntBDD.add(int_min, int_10), 1), IntBDD.add(IntBDD.negate(int_mindiv2), int_5)));
  }

  @Test
  public void shiftLeft() {
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_0, 1), int_0));
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_1, 1), int_2));
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_2, 1), int_4));
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_9, 0), int_9));
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_9, 7), int_1152));
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_9, 31), int_min));
    assertTrue(Arrays.equals(IntBDD.shiftLeft(int_9, 32), int_0));
  }

  @Test
  public void rotateRightThroughCarry() {
    var carryRef = new @NonNull BDD[1];
    var val = IntBDD.rotateRightThroughCarry(int_min, BDD.falseBDD(), carryRef);
    assertTrue(Arrays.equals(val, IntBDD.negate(int_mindiv2)));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateRightThroughCarry(int_min, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_mindiv2));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateRightThroughCarry(int_n2, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_n1));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateRightThroughCarry(val, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_n1));
    assertTrue(carryRef[0] == BDD.trueBDD());

    val = IntBDD.rotateRightThroughCarry(val, BDD.falseBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_max));
    assertTrue(carryRef[0] == BDD.trueBDD());

    val = IntBDD.rotateRightThroughCarry(int_1, BDD.falseBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_0));
    assertTrue(carryRef[0] == BDD.trueBDD());
  }

  @Test
  public void rotateLeftThroughCarry() {
    var carryRef = new @NonNull BDD[1];
    var val = IntBDD.rotateLeftThroughCarry(int_0, BDD.falseBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_0));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateLeftThroughCarry(int_0, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_1));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateLeftThroughCarry(int_1, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_3));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateLeftThroughCarry(int_max, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_n1));
    assertTrue(carryRef[0] == BDD.falseBDD());

    val = IntBDD.rotateLeftThroughCarry(int_min, BDD.falseBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_0));
    assertTrue(carryRef[0] == BDD.trueBDD());

    val = IntBDD.rotateLeftThroughCarry(int_min, BDD.trueBDD(), carryRef);
    assertTrue(Arrays.equals(val, int_1));
    assertTrue(carryRef[0] == BDD.trueBDD());
  }

  @Test
  public void multiply() {
    assertTrue(Arrays.equals(IntBDD.multiply(int_0, int_10), int_0));
    assertTrue(Arrays.equals(IntBDD.multiply(int_n7, int_0), int_0));
    assertTrue(Arrays.equals(IntBDD.multiply(int_min, int_0), int_0));
    assertTrue(Arrays.equals(IntBDD.multiply(int_min, int_1), int_min));
    assertTrue(Arrays.equals(IntBDD.multiply(int_min, int_n1), int_min));
    assertTrue(Arrays.equals(IntBDD.multiply(int_max, int_1), int_max));
    assertTrue(Arrays.equals(IntBDD.multiply(int_max, int_n1), int_minPlus1));
    assertTrue(Arrays.equals(IntBDD.multiply(int_1, int_10), int_10));
    assertTrue(Arrays.equals(IntBDD.multiply(int_10, int_1), int_10));
    assertTrue(Arrays.equals(IntBDD.multiply(int_1, int_n10), int_n10));
    assertTrue(Arrays.equals(IntBDD.multiply(int_2, int_2), int_4));
    assertTrue(Arrays.equals(IntBDD.multiply(int_2, int_n3), int_n6));
    assertTrue(Arrays.equals(IntBDD.multiply(int_n3, int_n3), int_9));
    assertTrue(Arrays.equals(IntBDD.multiply(IntBDD.intMultiBDD(45113), IntBDD.intMultiBDD(22893)), IntBDD.intMultiBDD(1032771909)));
    assertTrue(Arrays.equals(IntBDD.multiply(IntBDD.intMultiBDD(-45113), IntBDD.intMultiBDD(22893)), IntBDD.intMultiBDD(-1032771909)));

    var val = IntBDD.multiply(var_A, int_2);
    assertTrue(IntBDD.leq(val, int_2) == BDD.or(IntBDD.equal(var_A, int_0), IntBDD.equal(var_A, int_1)));
    assertTrue(IntBDD.less(int_10, val) == BDD.or(IntBDD.equal(var_A, int_6), IntBDD.equal(var_A, int_7)));
    assertTrue(IntBDD.equal(val, int_10) == IntBDD.equal(var_A, int_5));

    val = IntBDD.multiply(var_A, var_A);
    assertTrue(IntBDD.equal(val, IntBDD.intMultiBDD(36)) == IntBDD.equal(var_A, int_6));
    assertTrue(IntBDD.leq(IntBDD.intMultiBDD(36), val) == BDD.or(IntBDD.equal(var_A, int_6), IntBDD.equal(var_A, int_7)));

    val = IntBDD.multiply(var_B, var_B);
    assertTrue(IntBDD.equal(val, int_9) == BDD.or(IntBDD.equal(var_B, int_3), IntBDD.equal(var_B, int_n3)));
  }

  @Test
  public void div() {
    assertTrue(Arrays.equals(IntBDD.div(int_0, int_1), int_0));
    assertTrue(Arrays.equals(IntBDD.div(int_0, int_7), int_0));
    assertTrue(Arrays.equals(IntBDD.div(int_0, int_max), int_0));
    assertTrue(Arrays.equals(IntBDD.div(int_1, int_1), int_1));
    assertTrue(Arrays.equals(IntBDD.div(int_7, int_1), int_7));
    assertTrue(Arrays.equals(IntBDD.div(int_max, int_1), int_max));
    assertTrue(Arrays.equals(IntBDD.div(int_1, int_2), int_0));
    assertTrue(Arrays.equals(IntBDD.div(int_7, int_2), int_3));
    assertTrue(Arrays.equals(IntBDD.div(int_6, int_2), int_3));
    assertTrue(Arrays.equals(IntBDD.div(int_max, int_2), IntBDD.intMultiBDD(Integer.MAX_VALUE / 2)));
    assertTrue(Arrays.equals(IntBDD.div(int_max, int_max), int_1));
    // tests with negative arguments
    assertTrue(Arrays.equals(IntBDD.div(int_n7, int_2), int_n3));
    assertTrue(Arrays.equals(IntBDD.div(int_7, int_n2), int_n3));
    assertTrue(Arrays.equals(IntBDD.div(int_n7, int_n2), int_3));
    // division by zero (implementation dependent)
    assertTrue(Arrays.equals(IntBDD.div(int_1, int_0), int_n1));
    assertTrue(Arrays.equals(IntBDD.div(int_0, int_0), int_n1));
  }

  @Test
  public void mod() throws Exception {
    assertTrue(Arrays.equals(IntBDD.mod(int_0, int_1), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_0, int_7), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_0, int_max), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_1, int_1), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_7, int_1), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_max, int_1), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_1, int_2), int_1));
    assertTrue(Arrays.equals(IntBDD.mod(int_7, int_2), int_1));
    assertTrue(Arrays.equals(IntBDD.mod(int_6, int_2), int_0));
    assertTrue(Arrays.equals(IntBDD.mod(int_max, int_2), int_1));
    assertTrue(Arrays.equals(IntBDD.mod(int_max, int_max), int_0));
    assertTrue(IntBDD.equal(IntBDD.mod(var_A, int_3), int_0) == BDD.or(BDD.or(IntBDD.equal(var_A, int_0), IntBDD.equal(var_A, int_3)), IntBDD.equal(var_A, int_6)));
    // tests with negative arguments
    assertTrue(Arrays.equals(IntBDD.mod(int_n7, int_2), int_n1));
    assertTrue(Arrays.equals(IntBDD.mod(int_7, int_n2), int_1));
    assertTrue(Arrays.equals(IntBDD.mod(int_n7, int_n2), int_n1));
    // division by zero (implementation dependent)
    assertTrue(Arrays.equals(IntBDD.mod(int_1, int_0), int_1));
    assertTrue(Arrays.equals(IntBDD.mod(int_0, int_0), int_0));
  }

  @Test
  public void math_mod() throws Exception {
    assertTrue(Arrays.equals(IntBDD.math_mod(int_7, int_3), int_1));
    assertTrue(Arrays.equals(IntBDD.math_mod(int_n7, int_3), int_2));
    assertTrue(Arrays.equals(IntBDD.math_mod(int_7, int_n3), int_1));
    assertTrue(Arrays.equals(IntBDD.math_mod(int_n7, int_n3), int_2));
  }

  @Test
  public void math_div() throws Exception {
    assertTrue(Arrays.equals(IntBDD.math_div(int_7, int_3), int_2));
    assertTrue(Arrays.equals(IntBDD.math_div(int_n7, int_3), int_n3));
    assertTrue(Arrays.equals(IntBDD.math_div(int_7, int_n3), int_n2));
    assertTrue(Arrays.equals(IntBDD.math_div(int_n7, int_n3), int_3));
  }

  @Test
  public void negateExt() {
    var val = IntBDD.negateExt(int_n1, int_n1);
    assertTrue(Arrays.equals(val[0], int_1));
    assertTrue(Arrays.equals(val[1], int_0));

    val = IntBDD.negateExt(int_1, int_0);
    assertTrue(Arrays.equals(val[0], int_n1));
    assertTrue(Arrays.equals(val[1], int_n1));

    val = IntBDD.negateExt(int_0, int_0);
    assertTrue(Arrays.equals(val[0], int_0));
    assertTrue(Arrays.equals(val[1], int_0));
  }

  @Test
  public void absExt() {
    var val = IntBDD.absExt(int_n1, int_n1);
    assertTrue(Arrays.equals(val[0], int_1));
    assertTrue(Arrays.equals(val[1], int_0));

    val = IntBDD.absExt(int_1, int_0);
    assertTrue(Arrays.equals(val[0], int_1));
    assertTrue(Arrays.equals(val[1], int_0));

  }

  @Test
  public void equal() {
    assertTrue(IntBDD.equal(int_10, int_10) == BDD.trueBDD());
    assertTrue(IntBDD.equal(int_10, int_n10) == BDD.falseBDD());
    assertTrue(IntBDD.equal(var_A, int_0) == BDD.and(BDD.and(n1, n2), n3));
    assertTrue(IntBDD.equal(var_B, int_0) == BDD.and(BDD.and(n1, n2), v3));
    assertTrue(IntBDD.equal(IntBDD.add(var_B, int_4), var_A) == BDD.trueBDD());
  }

  @Test
  public void equals() {
    assertTrue(Arrays.equals(int_17, int_17copy));
    assertFalse(Arrays.equals(int_0, int_1));
    assertFalse(Arrays.equals(int_0, var_A));
    assertTrue(Arrays.equals(var_A, var_A));
    assertFalse(Arrays.equals(var_A, var_B));
  }
}
