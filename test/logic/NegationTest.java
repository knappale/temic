package logic;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

import state.Expression;
import state.Value;
import state.Variable;


public class NegationTest {
  @Test
  public void negTest() {
    var a = new Variable.Integer("a", 2, 99);
    var b = new Variable.Integer("b", 2, 99);
    var s = new Variable.Integer("s", 2, 198);
    var p = new Variable.Integer("p", 4, 9801);

    var a_gt_99 = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.GT, new Value.Integer(99)));
    var a_leq_99 = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.LEQ, new Value.Integer(99)));
    assertEquals(a_leq_99, a_gt_99.neg());
    assertEquals(a_leq_99, a_leq_99.neg().neg());
    assertEquals(a_leq_99, new StateFormula.Propositional(new Propositional.Not<>(a_leq_99)).neg());
    assertEquals(a_leq_99, new StateFormula.Propositional(new Propositional.Not<>(new StateFormula.Propositional(new Propositional.Not<>(a_gt_99)))).neg());

    var a_leq_b = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.LEQ, b));
    var a_gt_b = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.GT, b));
    assertEquals(a_leq_b, a_gt_b.neg());
    assertEquals(a_leq_b, a_leq_b.neg().neg());
    assertEquals(a_leq_b, new StateFormula.Propositional(new Propositional.Not<>(a_leq_b)).neg());

    var s_eq_a_plus_b = new StateFormula.Atom(new Expression.Boolean.Comparison(s, Expression.Boolean.Comparison.Kind.EQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, b)));
    var s_neq_a_plus_b = new StateFormula.Atom(new Expression.Boolean.Comparison(s, Expression.Boolean.Comparison.Kind.NEQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, b)));
    assertEquals(s_eq_a_plus_b, s_neq_a_plus_b.neg());
    assertEquals(s_eq_a_plus_b, s_eq_a_plus_b.neg().neg());
    assertEquals(s_eq_a_plus_b, new StateFormula.Propositional(new Propositional.Not<>(s_eq_a_plus_b)).neg());

    var p_eq_a_times_b = new StateFormula.Atom(new Expression.Boolean.Comparison(p, Expression.Boolean.Comparison.Kind.EQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.MULT, b)));
    var p_neq_a_times_b = new StateFormula.Atom(new Expression.Boolean.Comparison(p, Expression.Boolean.Comparison.Kind.NEQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.MULT, b)));
    assertEquals(p_eq_a_times_b, p_neq_a_times_b.neg());
    assertEquals(p_eq_a_times_b, p_eq_a_times_b.neg().neg());
    assertEquals(p_eq_a_times_b, new StateFormula.Propositional(new Propositional.Not<>(p_eq_a_times_b)).neg());

    var f1 = new StateFormula.Propositional(new Propositional.And<>(a_leq_b,
                                            new StateFormula.Propositional(new Propositional.And<>(s_eq_a_plus_b, p_eq_a_times_b))));
    var nf1 = new StateFormula.Propositional(new Propositional.Or<>(a_gt_b,
                                             new StateFormula.Propositional(new Propositional.Or<>(s_neq_a_plus_b, p_neq_a_times_b))));
    assertEquals(nf1, f1.neg());
    assertEquals(f1, f1.neg().neg());
    assertEquals(f1, new StateFormula.Propositional(new Propositional.Not<>(f1)).neg());

    var x = new Variable.Integer("x", 3, 9);
    var x_eq_a_plus_b = new StateFormula.Atom(new Expression.Boolean.Comparison(x, Expression.Boolean.Comparison.Kind.EQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, b)));
    var x_neq_a_plus_b = new StateFormula.Atom(new Expression.Boolean.Comparison(x, Expression.Boolean.Comparison.Kind.NEQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, b)));
    assertEquals(x_neq_a_plus_b, x_eq_a_plus_b.neg());
    assertEquals(x_eq_a_plus_b, x_eq_a_plus_b.neg().neg());
    assertEquals(x_eq_a_plus_b, new StateFormula.Propositional(new Propositional.Not<>(x_eq_a_plus_b)).neg());

    var ex_x_x_eq_a_plus_b = new StateFormula.Propositional(new Propositional.Quantified<>(Propositional.Quantified.Kind.EXISTS, new ArrayList<>(Collections.singletonList(x)), x_eq_a_plus_b));
    var all_x_x_neq_a_plus_b = new StateFormula.Propositional(new Propositional.Quantified<>(Propositional.Quantified.Kind.FORALL, new ArrayList<>(Collections.singletonList(x)), x_neq_a_plus_b));
    assertEquals(all_x_x_neq_a_plus_b, ex_x_x_eq_a_plus_b.neg());
    assertEquals(all_x_x_neq_a_plus_b, all_x_x_neq_a_plus_b.neg().neg());
    assertEquals(all_x_x_neq_a_plus_b, new StateFormula.Propositional(new Propositional.Not<>(all_x_x_neq_a_plus_b)).neg());

    var f2 = new StateFormula.Propositional(new Propositional.And<>(ex_x_x_eq_a_plus_b, f1));
    var nf2 = new StateFormula.Propositional(new Propositional.Or<>(all_x_x_neq_a_plus_b, nf1));
    assertEquals(nf2, f2.neg());
    assertEquals(f2, f2.neg().neg());
    assertEquals(f2, new StateFormula.Propositional(new Propositional.Not<>(f2)).neg());
    assertEquals(f2, new StateFormula.Propositional(new Propositional.Not<>(new StateFormula.Propositional(new Propositional.Not<>(nf2)))).neg());
  }
}
