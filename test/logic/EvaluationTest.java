package logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.Test;

import state.Expression;
import state.SpaceManager;
import state.State;
import state.StateSet;
import state.StateVariable;
import state.Value;
import state.Variable;


public class EvaluationTest {
  @Test
  public void statesTest() {
    var a = new StateVariable.Integer("a", 2, 99);
    var b = new StateVariable.Integer("b", 2, 99);
    var s = new StateVariable.Integer("s", 2, 198);
    var p = new StateVariable.Integer("p", 4, 9801);
    jdd.util.Options.verbose = false;
    SpaceManager.initialise(new ArrayList<>(Arrays.asList(a, b, s, p)));

    var a_leq_b = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.LEQ, b));
    var s_eq_a_plus_b = new StateFormula.Atom(new Expression.Boolean.Comparison(s, Expression.Boolean.Comparison.Kind.EQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, b)));
    var p_eq_a_times_b = new StateFormula.Atom(new Expression.Boolean.Comparison(p, Expression.Boolean.Comparison.Kind.EQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.MULT, b)));

    var initial = new StateFormula.Propositional(new Propositional.And<>(a_leq_b,
                                                 new StateFormula.Propositional(new Propositional.And<>(s_eq_a_plus_b, p_eq_a_times_b))));
    var valuation = new HashMap<@NonNull Variable, @NonNull Value>();
    valuation.put(a, new Value.Integer(2));
    valuation.put(b, new Value.Integer(66));
    valuation.put(s, new Value.Integer(68));
    valuation.put(p, new Value.Integer(132));
    assertTrue(initial.satisfyingStates().contains(new State(valuation)));

    var overflow = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.GT, new Value.Integer(99)));
    assertTrue(overflow.satisfyingStates().isEmpty());

    var x = new Variable.Integer("x", 3, 9);
    var x_eq_a_plus_b = new StateFormula.Atom(new Expression.Boolean.Comparison(x, Expression.Boolean.Comparison.Kind.EQ, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, b)));
    var ex_x_x_eq_a_plus_b = new StateFormula.Propositional(new Propositional.Quantified<>(Propositional.Quantified.Kind.EXISTS, new ArrayList<>(Collections.singletonList(x)), x_eq_a_plus_b));
    var exists = new StateFormula.Propositional(new Propositional.And<>(ex_x_x_eq_a_plus_b, initial));
    assertEquals(BigInteger.valueOf(12), exists.satisfyingStates().count());
  }

  @Test
  public void existsTest() {
    var p = new StateVariable.Boolean("p");
    jdd.util.Options.verbose = false;
    SpaceManager.initialise(new ArrayList<>(Collections.singletonList(p)));

    var x = new Variable.Integer("x", 3, 9);
    var x_eq_4 = new StateFormula.Atom(new Expression.Boolean.Comparison(x, Expression.Boolean.Comparison.Kind.EQ, new Value.Integer(4)));
    var ex_x_x_eq_4 = new StateFormula.Propositional(new Propositional.Quantified<>(Propositional.Quantified.Kind.EXISTS, new ArrayList<>(Collections.singletonList(x)), x_eq_4));
    assertEquals(StateSet.all(), ex_x_x_eq_4.satisfyingStates());
  }
}
