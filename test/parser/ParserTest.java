package parser;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import org.junit.Test;

import parser.generated.ParseException;
import parser.generated.TemicParser;
import parser.generated.TokenMgrError;
import util.Formatter;


public class ParserTest {
  @Test
  public void test() {
    jdd.util.Options.verbose = false;
    java.util.Locale.setDefault(java.util.Locale.US);
    var inputFile = "input/nosolution1.spec";
    // var inputFile = "input/sum-product-leq.spec";
    try {
      var rdr = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
      var parser = new TemicParser(rdr);

      var specification = parser.specification();
      var interpretedSystem = specification.interpret();
      for (var assertion : specification.getAssertions())
        System.out.println(assertion.check(interpretedSystem));
    }
    catch (FileNotFoundException fnfe) {
      System.err.println("Error opening file " + Formatter.quoted(inputFile) + ": " + fnfe.getMessage());
      fail(fnfe.getMessage());
    }
    catch (TokenMgrError tme) {
      System.err.println("Error parsing file " + Formatter.quoted(inputFile) + ": " + tme.getMessage());
      fail(tme.getMessage());
    }
    catch (ParseException pe) {
      System.err.println("Error parsing file " + Formatter.quoted(inputFile) + ": " + pe.getMessage());
      fail(pe.getMessage());
    }
  }
}
