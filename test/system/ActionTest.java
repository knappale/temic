package system;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.BeforeClass;
import org.junit.Test;

import logic.StateFormula;
import state.Expression;
import state.SpaceManager;
import state.StateSet;
import state.StateVariable;
import state.Transition;
import state.Value;
import state.Variable;


public class ActionTest {
  @BeforeClass
  public static void setUp() {
    jdd.util.Options.verbose = false;
  }

  @Test
  public void actionTest() {
    var a = new StateVariable.Integer("a", 2, 99);
    var b = new StateVariable.Integer("b", 2, 99);
    var p = new StateVariable.Boolean("p");
    var q = new StateVariable.Boolean("q");
    SpaceManager.initialise(new ArrayList<>(Arrays.asList(a, b, p, q)));

    var pre = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.NEQ, new Value.Integer(3)));
    var assignment = new Assignment.Integer(a, new Value.Integer(3));
    var action = new Action("test", null, pre, Arrays.asList(assignment));
    var valuation = new HashMap<@NonNull Variable, @NonNull Value>();
    valuation.put(a, new Value.Integer(2));
    valuation.put(b, new Value.Integer(3));
    valuation.put(a.prime(), new Value.Integer(3));
    valuation.put(b.prime(), new Value.Integer(3));
    var trans = action.evaluate(StateSet.all());

    assertTrue(trans.contains(new Transition(valuation)));

    valuation.put(b.prime(), new Value.Integer(2));
    assertFalse(trans.contains(new Transition(valuation)));
  }

  @Test
  public void transitiveTest() {
    var a = new StateVariable.Integer("a", 2, 99);
    var b = new StateVariable.Integer("b", 2, 99);
    SpaceManager.initialise(new ArrayList<>(Arrays.asList(a, b)));

    var pre = new StateFormula.Atom(new Expression.Boolean.Comparison(a, Expression.Boolean.Comparison.Kind.LEQ, new Value.Integer(5)));
    var assignment = new Assignment.Integer(a, new Expression.Integer.Arithmetic(a, Expression.Integer.Arithmetic.Kind.ADD, new Value.Integer(1)));
    var action = new Action("test", null, pre, Arrays.asList(assignment));
    var trans = action.evaluate(StateSet.all()).transitiveHull();

    var valuation = new HashMap<@NonNull Variable, @NonNull Value>();
    valuation.put(a, new Value.Integer(2));
    valuation.put(b, new Value.Integer(3));

    valuation.put(a.prime(), new Value.Integer(3));
    valuation.put(b.prime(), new Value.Integer(3));
    assertTrue(trans.contains(new Transition(valuation)));

    valuation.put(a.prime(), new Value.Integer(4));
    valuation.put(b.prime(), new Value.Integer(3));
    assertTrue(trans.contains(new Transition(valuation)));

    valuation.put(a.prime(), new Value.Integer(4));
    valuation.put(b.prime(), new Value.Integer(2));
    assertFalse(trans.contains(new Transition(valuation)));

    valuation.put(a.prime(), new Value.Integer(7));
    valuation.put(b.prime(), new Value.Integer(3));
    assertFalse(trans.contains(new Transition(valuation)));
  }
}
